const path = require('path')

const entry = {
    'app': './frontend/app/index.ts',
}

const getOutputConfig = () => {
    return {
        path: path.resolve(__dirname, 'public/compiled'),
        filename: '[name].[contenthash].js',
        chunkFilename: '[name].[contenthash].chunk.js',
        publicPath: '/compiled/',
    }
}

const getModuleConfig = require('./frontend/webpack/module.js')
const getPlugins = require('./frontend/webpack/plugins.js')
const statsConfig = require('./frontend/webpack/stats.js')
const getOptimizationConfig = require('./frontend/webpack/optimization.js')

module.exports = (env, argv) => {
    env = env === undefined ? {} : env

    const mode = argv.mode
    const isProductionMode = mode === 'production'

    return {
        mode: mode,
        // don't attempt to continue if there are any errors.
        bail: isProductionMode,
        entry: entry,
        output: getOutputConfig(),
        watch: env.watch,
        watchOptions: {
            aggregateTimeout: 100,
        },
        module: getModuleConfig(isProductionMode),
        plugins: getPlugins(isProductionMode, env.analyze),
        resolve: {
            alias: {
                'app': 'frontend/app',
                'images': 'frontend/images',
            },
            modules: [
                __dirname,
                'node_modules',
            ],
            extensions: ['.ts', '.tsx', '.js', '.jsx', '.css', '.scss'],
        },
        devtool: isProductionMode ? 'nosources-source-map' : false,
        performance: {
            hints: false,
        },
        stats: statsConfig,
        optimization: getOptimizationConfig(isProductionMode),
        // Some libraries import Node modules but don't use them in the browser.
        // Tell Webpack to provide empty mocks for them so importing them works.
        node: {
            dgram: 'empty',
            fs: 'empty',
            net: 'empty',
            tls: 'empty',
            child_process: 'empty',
        },
    }
}
