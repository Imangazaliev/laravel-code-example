<?php

declare(strict_types=1);

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        (new Language())->truncate();

        $languagesFilePath = storage_path('data/languages.json');
        $languagesInfo = json_decode(file_get_contents($languagesFilePath), true, 512, JSON_THROW_ON_ERROR);

        $languagesInfo = array_values(array_filter($languagesInfo, function ($languageInfo) {
            return in_array($languageInfo['code'], ['ar', 'en', 'ru']);
        }));

        foreach ($languagesInfo as $languageInfo) {
            Language::create([
                'code' => $languageInfo['code'],
                'code_iso_639_1' => $languageInfo['code_iso_639_1'],
                'code_iso_639_2b' => $languageInfo['code_iso_639_2b'],
                'code_iso_639_2t' => $languageInfo['code_iso_639_2t'],
                'code_iso_639_3' => $languageInfo['code_iso_639_3'],
                'script_direction' => $languageInfo['script_direction'],
                'native_name' => $languageInfo['native_name'],
                'english_name' => $languageInfo['english_name'],
                'is_active' => true,
            ]);
        }
    }
}
