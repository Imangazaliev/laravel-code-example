<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\Language;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Propaganistas\LaravelPhone\PhoneNumber;

class UserFactory extends Factory
{
    protected $model = User::class;

    public function definition()
    {
        $sex = $this->faker->randomElement(['m', 'f']);
        $birthDay = $this->faker->dateTime(sprintf('-%dyears', config('profile.min_age')));

        $phoneNumber = $this->generateRandomPhoneNumber();

        $phoneNumberCountryCode = 'RU';
        $phoneNumberObj = PhoneNumber::make($phoneNumber, $phoneNumberCountryCode);
        $phoneNumberInNationalFormat = $phoneNumberObj->formatForMobileDialingInCountry($phoneNumberCountryCode);

        $languageId = (new Language())->where('code', 'en')->value('id');

        $currentTime = Carbon::now();

        return [
            'name' => $this->faker->firstName($sex === User::SEX_MALE ? 'male' : 'female'),
            'sex' => $sex,
            'birthday' => $birthDay,
            // we don't use Faker for generating a phone number
            // because it generates invalid ones
            'phone_number' => $phoneNumber,
            'phone_number_country_code' => $phoneNumberCountryCode,
            'phone_number_in_national_format' => $phoneNumberInNationalFormat,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'password_updated_at' => $currentTime->format(DATE_TIME_FORMAT),
            'remember_token' => str_random(10),
            'language_id' => $languageId,
            'language_code' => 'en',
            'registered_at' => $currentTime,
            'was_online_at' => $currentTime,
            'last_activity_at' => $currentTime,
            'status_updated_at' => $currentTime,
            'coins' => config('profile.registration_gift_coins_amount'),
        ];
    }

    private function generateRandomPhoneNumber(): string
    {
        $lineNumber = '';

        for ($i = 0; $i < 7; $i++) {
            $lineNumber .= random_int(0, 9);
        }

        return '+7977' . $lineNumber;
    }
}
