<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\UserPhoto;
use App\Services\UserPhotoUploader\UploadPathGenerator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use ExampleProject\FileStorage\StorageFactoryInterface;

class UserPhotoFactory extends Factory
{
    protected $model = UserPhoto::class;

    public function definition()
    {
        $originalImagePath = UploadPathGenerator::generateUploadPath();
        $uploadPath = UploadPathGenerator::generateUploadPath();
        $blurredImagePath = UploadPathGenerator::generateUploadPath();

        return [
            'original_file_name' => 'IMG-2020-01-09.jpg',
            'original_file_name_transliterated' => 'IMG-2020-01-09.jpg',
            'width' => 1080,
            'height' => 2450,
            'size' => 4728,
            'exif_data' => [],
            'sha256_hash' => 'de32368ec23161ae252dce95af409cde8a45eb626af41f74a7d59e09777df57c',
            'storage_type' => StorageFactoryInterface::STORAGE_TYPE_LOCAL,
            'original_image_file_name' => basename($originalImagePath),
            'original_image_path' => $originalImagePath,
            'file_name' => basename($uploadPath),
            'path' => $uploadPath,
            'blurred_image_file_name' => basename($blurredImagePath),
            'blurred_image_path' => $blurredImagePath,
            'uploaded_at' => Carbon::today()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'is_being_processed' => false,
            'caption' => 'My photo',
            'status_updated_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
        ];
    }
}
