<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Models\ChatRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ChatRequestFactory extends Factory
{
    protected $model = ChatRequest::class;

    public function definition()
    {
        $currentTime = Carbon::now();

        return [
            'status' => ChatRequest::STATUS_NEW,
            'status_updated_at' =>  $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'sent_at' =>  $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ];
    }
}
