<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable20190811 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->smallIncrements('id')->generatedAs();
            $table->string('code', 3)->unique()->index();
            $table->string('code_iso_639_1', 2)->nullable()->unique();
            $table->string('code_iso_639_2b', 3)->nullable()->unique();
            $table->string('code_iso_639_2t', 3)->nullable()->unique();
            $table->string('code_iso_639_3', 3)->unique();
            $table->string('locale', 8)->nullable();
            $table->string('script_direction', 3);
            $table->string('native_name', 32)->nullable();
            $table->string('english_name', 32);
            $table->smallInteger('fallback_language_id')->nullable();
            $table->smallInteger('level')->default(1);
            $table->boolean('is_active')->default(false)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('languages');
    }
}
