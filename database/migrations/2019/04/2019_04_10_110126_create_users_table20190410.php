<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable20190410 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs('start with 300001');
            $table->string('name', 64);
            $table->string('last_accepted_name', 64)->nullable();
            $table->string('sex', 1);
            $table->date('birthday');
            $table->string('phone_number', 32)->unique();
            $table->char('phone_number_country_code', 2)->nullable();
            $table->string('phone_number_in_national_format', 32)->unique();
            $table->timestampTz('phone_number_verified_at')->nullable();
            $table->string('email', 64)->nullable()->unique();
            $table->timestampTz('email_verified_at')->nullable();
            $table->string('password', 128);
            $table->timestampTz('password_updated_at');
            $table->string('remember_token', 128)->nullable();
            $table->timestampTz('registered_at');
            $table->timestampTz('completed_registration_at')->nullable();
            $table->timestampTz('updated_at')->nullable();

            $table->smallInteger('language_id')->index();
            $table->string('language_code', 3);

            $table->string('status', 32)->index();
            $table->timestampTz('status_updated_at', 6);
            $table->smallInteger('reputation')->default(0);
            $table->timestampTz('reputation_updated_at')->nullable();
            $table->smallInteger('internal_reputation')->default(0);
            $table->timestampTz('internal_reputation_updated_at')->nullable();
            $table->integer('coins')->default(0);
            $table->timestampTz('coins_updated_at', 6)->useCurrent();
            $table->smallInteger('profile_completion_percent')->default(0);
            $table->timestampTz('profile_completion_percent_updated_at')->nullable();
            $table->boolean('required_fields_filled')->default(false);
            $table->timestampTz('required_fields_checked_at')->nullable();
            $table->bigInteger('main_photo_id')->nullable();
            $table->jsonb('notification_settings')->nullable();

            $table->boolean('is_test_user')->default(false);
            $table->boolean('is_protected')->default(false);
            $table->boolean('is_hidden')->default(false);
            $table->boolean('is_verified')->default(false);
            $table->boolean('is_incognito_mode_enabled')->default(false);

            $table->bigInteger('ban_id')->nullable();
            $table->bigInteger('deactivation_id')->nullable();
            $table->bigInteger('deletion_id')->nullable();

            $table->timestampTz('was_online_at');
            $table->timestampTz('last_activity_at');

            $table->bigInteger('attached_moderator_id')->nullable();
            $table->bigInteger('moderation_moderator_id')->nullable();
            $table->timestampTz('moderation_claimed_at', 6)->nullable();
            $table->bigInteger('moderation_forwarded_to')->nullable();
            $table->bigInteger('moderation_forwarded_by')->nullable();
            $table->timestampTz('moderation_forwarded_at', 6)->nullable();

            $table->smallInteger('ethnicity_id')->nullable();
            $table->smallInteger('country_id')->nullable();
            $table->smallInteger('subdivision_level_1_id')->nullable();
            $table->smallInteger('subdivision_level_2_id')->nullable();
            $table->smallInteger('subdivision_level_3_id')->nullable();
            $table->integer('locality_id')->nullable();
            $table->smallInteger('native_country_id')->nullable();
            $table->smallInteger('native_subdivision_level_1_id')->nullable();
            $table->smallInteger('native_subdivision_level_2_id')->nullable();
            $table->smallInteger('native_subdivision_level_3_id')->nullable();
            $table->integer('native_locality_id')->nullable();

            $table->smallInteger('time_zone_id')->nullable();
            $table->string('time_zone_name', 64)->nullable();
            $table->string('date_time_format', 32)->nullable();
            $table->string('length_unit', 32)->nullable();
            $table->string('weight_unit', 32)->nullable();

            $table->string('profile_headline', 256)->nullable();

            $table->smallInteger('height')->nullable();
            $table->smallInteger('weight_from')->nullable();
            $table->smallInteger('weight_to')->nullable();
            $table->string('body_type', 32)->nullable();
            $table->string('hair_color', 32)->nullable();
            $table->string('eye_color', 32)->nullable();
            $table->string('facial_hair', 32)->nullable();

            $table->string('smoking', 32)->nullable();
            $table->string('alcohol_consumption', 32)->nullable();
            $table->string('nutrition', 32)->nullable();
            $table->string('self_description', 512)->nullable();
            $table->string('hobbies_and_interests', 256)->nullable();
            $table->string('sport', 128)->nullable();
            $table->string('sport_exercises_frequency', 32)->nullable();
            $table->string('marital_status', 32)->nullable();
            $table->smallInteger('child_count')->nullable();
            $table->smallInteger('children_in_household')->nullable();
            $table->string('living_with_parents', 32)->nullable();
            $table->string('desired_number_of_children', 32)->nullable();
            $table->string('children_adoption', 32)->nullable();
            $table->string('housing_type', 32)->nullable();
            $table->string('relocation', 32)->nullable();
            $table->string('education', 32)->nullable();
            $table->string('profession_or_occupation', 128)->nullable();
            $table->string('employment', 32)->nullable();
            $table->string('job_title', 128)->nullable();
            $table->string('financial_status', 32)->nullable();
            $table->string('religion', 32)->nullable();
            $table->smallInteger('future_spouse_age_from')->nullable();
            $table->smallInteger('future_spouse_age_to')->nullable();
            $table->smallInteger('future_spouse_height_from')->nullable();
            $table->smallInteger('future_spouse_height_to')->nullable();
            $table->smallInteger('future_spouse_weight_from')->nullable();
            $table->smallInteger('future_spouse_weight_to')->nullable();
            $table->string('future_spouse_body_type', 32)->nullable();
            $table->string('future_spouse_aqidah', 32)->nullable();
            $table->string('future_spouse_character_traits', 256)->nullable();
            $table->string('future_spouse_description', 512)->nullable();
            $table->string('future_spouse_children', 32)->nullable();
        });

        Schema::table('users', function () {
            DB::statement('CREATE INDEX IF NOT EXISTS users_main_photo_id_index ON users (main_photo_id) WHERE main_photo_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_ban_id_index ON users (ban_id) WHERE ban_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_deactivation_id_index ON users (deactivation_id) WHERE deactivation_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_deletion_id_index ON users (deletion_id) WHERE deletion_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_attached_moderator_id_index ON users (attached_moderator_id) WHERE attached_moderator_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_moderation_moderator_id_index ON users (moderation_moderator_id) WHERE moderation_moderator_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_moderation_forwarded_to_index ON users (moderation_forwarded_to) WHERE moderation_forwarded_to IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_moderation_forwarded_by_index ON users (moderation_forwarded_by) WHERE moderation_forwarded_by IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_ethnicity_id_index ON users (ethnicity_id) WHERE ethnicity_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_country_id_index ON users (country_id) WHERE country_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_subdivision_level_1_id_index ON users (subdivision_level_1_id) WHERE subdivision_level_1_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_subdivision_level_2_id_index ON users (subdivision_level_2_id) WHERE subdivision_level_2_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_subdivision_level_3_id_index ON users (subdivision_level_3_id) WHERE subdivision_level_3_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_locality_id_index ON users (locality_id) WHERE locality_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_native_country_id_index ON users (native_country_id) WHERE native_country_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_native_subdivision_level_1_id_index ON users (native_subdivision_level_1_id) WHERE native_subdivision_level_1_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_native_subdivision_level_2_id_index ON users (native_subdivision_level_2_id) WHERE native_subdivision_level_2_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_native_subdivision_level_3_id_index ON users (native_subdivision_level_3_id) WHERE native_subdivision_level_3_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_native_locality_id_index ON users (native_locality_id) WHERE native_locality_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS users_time_zone_id_index ON users (time_zone_id) WHERE time_zone_id IS NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
