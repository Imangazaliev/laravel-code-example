<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable20191219 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('user_id')->index();
            $table->smallInteger('coins_amount');
            $table->string('payment_gateway', 32);
            $table->decimal('price', 8, 2);
            $table->string('currency_code', 3);
            $table->decimal('currency_rate', 8, 2);
            $table->string('status', 32);
            $table->timestampTz('created_at', 6);
            $table->timestampTz('finished_at', 6)->nullable();
            $table->text('description');
            $table->string('token', 64);
            $table->string('payment_gateway_transaction_id', 64)->nullable();
            $table->string('application_version', 40);

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('currency_code')
                ->references('code')
                ->on('currencies')
                ->onDelete('restrict')
            ;

            $table->unique(['user_id', 'token']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
    }
}
