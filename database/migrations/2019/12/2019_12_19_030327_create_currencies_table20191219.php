<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable20191219 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->smallIncrements('id')->generatedAs();
            $table->string('code', 3)->unique();
            $table->string('code_numeric', 3)->unique();
            $table->string('english_display_name', 64);
            $table->string('symbol', 4);
            $table->string('native_symbol', 3)->nullable();
            $table->smallInteger('decimal_digits');
            $table->string('thousand_delimiter', 1)->nullable();
            $table->string('decimal_separator', 1)->nullable();
            $table->string('format', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currencies');
    }
}
