<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBansTable20191219 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bans', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('user_id')->index();
            $table->jsonb('reasons');
            $table->text('comment')->nullable();
            $table->text('comment_for_user')->nullable();
            $table->timestampTz('banned_at');
            $table->bigInteger('banned_by')->nullable();
            $table->timestampTz('unban_at')->nullable();
            $table->bigInteger('unbanned_by')->nullable();
            $table->timestampTz('unbanned_at')->nullable();
            $table->text('unban_comment')->nullable();

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('banned_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('unbanned_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;
        });

        Schema::table('user_bans', function () {
            DB::statement('CREATE INDEX IF NOT EXISTS user_bans_banned_by_index ON user_bans (banned_by) WHERE banned_by IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS user_bans_unbanned_by_index ON user_bans (unbanned_by) WHERE unbanned_by IS NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_bans');
    }
}
