<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinTransactionsTable20191219 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_transactions', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->string('type', 64);
            $table->bigInteger('user_id')->index();
            $table->integer('amount');
            $table->text('description');
            $table->integer('balance_before');
            $table->integer('balance_after');
            $table->timestampTz('transaction_time', 6);
            $table->bigInteger('invoice_id')->nullable();
            $table->bigInteger('transfer_source_transaction_id')->nullable();
            $table->bigInteger('admin_id')->nullable();
            $table->text('admin_comment')->nullable();

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('invoice_id')
                ->references('id')
                ->on('invoices')
                ->onDelete('restrict')
            ;
            ;

            $table
                ->foreign('transfer_source_transaction_id')
                ->references('id')
                ->on('coin_transactions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('admin_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;
        });

        Schema::table('coin_transactions', function () {
            DB::statement('CREATE INDEX IF NOT EXISTS coin_transactions_invoice_id_index ON coin_transactions (invoice_id) WHERE invoice_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS coin_transactions_transfer_source_transaction_id_index ON coin_transactions (transfer_source_transaction_id) WHERE transfer_source_transaction_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS coin_transactions_admin_id_index ON coin_transactions (admin_id) WHERE admin_id IS NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coin_transactions');
    }
}
