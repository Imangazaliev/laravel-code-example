<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneNumberChangeRequestsTable20191016 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone_number_change_requests', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('user_id')->index();
            $table->string('current_phone_number', 32);
            $table->char('current_phone_number_country_code', 2);
            $table->string('new_phone_number', 32);
            $table->char('new_phone_number_country_code', 2);
            $table->string('code', 16);
            $table->string('status', 32)->index();
            $table->timestampTz('status_updated_at', 6);
            $table->timestampTz('created_at');

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
            ;

            $table
                ->foreign('current_phone_number_country_code')
                ->references('code_alpha2')
                ->on('countries')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('new_phone_number_country_code')
                ->references('code_alpha2')
                ->on('countries')
                ->onDelete('restrict')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phone_number_change_requests');
    }
}
