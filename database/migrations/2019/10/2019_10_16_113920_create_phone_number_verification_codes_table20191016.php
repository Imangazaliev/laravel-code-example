<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhoneNumberVerificationCodesTable20191016 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phone_number_verification_codes', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->string('phone_number', 32)->index();
            $table->string('code', 16);
            $table->timestampTz('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phone_number_verification_codes');
    }
}
