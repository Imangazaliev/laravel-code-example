<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable20191006 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('chat_request_id')->index();
            $table->bigInteger('requestor_id');
            $table->bigInteger('target_user_id');
            $table->timestampTz('created_at', 6);
            $table->timestampTz('last_activity_at', 6)->nullable();
            $table->smallInteger('language_id')->nullable();
            $table->bigInteger('attached_moderator_id')->nullable();
            $table->timestampTz('finished_at', 6)->nullable();
            $table->bigInteger('finished_by')->nullable();
            $table->boolean('is_finished_automatically')->nullable();
            $table->bigInteger('moderation_moderator_id')->nullable();
            $table->timestampTz('moderation_claimed_at', 6)->nullable();
            $table->bigInteger('moderation_forwarded_to')->nullable();
            $table->bigInteger('moderation_forwarded_by')->nullable();
            $table->timestampTz('moderation_forwarded_at', 6)->nullable();

            $table
                ->foreign('requestor_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('target_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('attached_moderator_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('finished_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('moderation_moderator_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('moderation_forwarded_to')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('moderation_forwarded_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table->index(['requestor_id', 'target_user_id']);
        });

        Schema::table('chats', function () {
            DB::statement('CREATE INDEX IF NOT EXISTS chats_moderation_moderator_id_index ON chats (moderation_moderator_id) WHERE moderation_moderator_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS chats_moderation_forwarded_to_index ON chats (moderation_forwarded_to) WHERE moderation_forwarded_to IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS chats_moderation_forwarded_by_index ON chats (moderation_forwarded_by) WHERE moderation_forwarded_by IS NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chats');
    }
}
