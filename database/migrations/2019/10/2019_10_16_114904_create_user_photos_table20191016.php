<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPhotosTable20191016 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_photos', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('user_id')->index();
            $table->string('original_file_name', 512);
            $table->string('original_file_name_transliterated');
            $table->integer('width');
            $table->integer('height');
            $table->bigInteger('size');
            $table->jsonb('exif_data')->nullable();
            $table->string('sha256_hash', 64);
            $table->string('caption', 256)->nullable();
            $table->jsonb('crop_area')->nullable();
            $table->jsonb('preview_area')->nullable();
            $table->smallInteger('rotation_angle')->default(0);
            $table->string('storage_type', 16);
            $table->string('original_image_file_name', 128);
            $table->string('original_image_path', 256);
            $table->string('file_name', 128)->nullable();
            $table->string('path', 256)->nullable();
            $table->string('blurred_image_file_name', 128)->nullable();
            $table->string('blurred_image_path', 256)->nullable();
            $table->timestampTz('uploaded_at');
            $table->boolean('is_being_processed');
            $table->jsonb('thumbnails')->nullable();
            $table->timestampTz('deleted_at')->nullable();
            $table->string('status', 32)->index();
            $table->timestampTz('status_updated_at', 6);
            $table->bigInteger('moderation_moderator_id')->nullable();
            $table->timestampTz('moderation_claimed_at', 6)->nullable();
            $table->bigInteger('moderation_forwarded_to')->nullable();
            $table->bigInteger('moderation_forwarded_by')->nullable();
            $table->timestampTz('moderation_forwarded_at', 6)->nullable();

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
            ;

            $table
                ->foreign('moderation_moderator_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('moderation_forwarded_to')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('moderation_forwarded_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;
        });

        Schema::table('user_photos', function () {
            DB::statement('CREATE INDEX IF NOT EXISTS user_photos_moderation_moderator_id_index ON user_photos (moderation_moderator_id) WHERE moderation_moderator_id IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS user_photos_moderation_forwarded_to_index ON user_photos (moderation_forwarded_to) WHERE moderation_forwarded_to IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS user_photos_moderation_forwarded_by_index ON user_photos (moderation_forwarded_by) WHERE moderation_forwarded_by IS NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_photos');
    }
}
