<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalitiesTable20191006 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('localities', function (Blueprint $table) {
            $table->increments('id')->generatedAs();
            $table->string('native_name', 128)->nullable();
            $table->string('english_name', 128);
            $table->string('type', 32)->nullable();
            $table->smallInteger('country_id');
            $table->string('country_code', 2);
            $table->smallInteger('subdivision_level_1_id')->nullable();
            $table->string('subdivision_level_1_code', 3)->nullable();
            $table->smallInteger('subdivision_level_2_id')->nullable();
            $table->string('subdivision_level_2_code', 3)->nullable();
            $table->smallInteger('subdivision_level_3_id')->nullable();
            $table->string('subdivision_level_3_code', 3)->nullable();
            $table->smallInteger('time_zone_id')->nullable();
            $table->string('time_zone_name', 64)->nullable();
            $table->bigInteger('geoname_id')->nullable()->unique();
            $table->boolean('is_capital')->default(false);
            $table->bigInteger('population')->nullable();
            $table->decimal('latitude', 9, 6)->nullable();
            $table->decimal('longitude', 9, 6)->nullable();

            $table
                ->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('country_code')
                ->references('code_alpha2')
                ->on('countries')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('subdivision_level_1_id')
                ->references('id')
                ->on('subdivisions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('subdivision_level_2_id')
                ->references('id')
                ->on('subdivisions')
                ->onDelete('restrict')
            ;

            $table->index(['country_id', 'subdivision_level_1_id', 'subdivision_level_2_id', 'subdivision_level_3_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('localities');
    }
}
