<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileViewsTable20191006 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_views', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('visitor_id')->index();
            $table->bigInteger('viewed_profile_id')->index();
            $table->timestampTz('viewed_at');
            $table->boolean('is_incognito');
            $table->timestampTz('seen_by_owner_at')->nullable();

            $table
                ->foreign('visitor_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('viewed_profile_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table->index(['visitor_id', 'viewed_profile_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_views');
    }
}
