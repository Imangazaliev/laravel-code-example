<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotoModerationsTable20191017 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_moderations', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('moderation_session_id')->index();
            $table->bigInteger('photo_id')->index();
            $table->bigInteger('photo_owner_id')->index();
            $table->string('status', 32);
            $table->jsonb('rejection_reasons')->nullable();
            $table->text('comment')->nullable();
            $table->text('comment_for_user')->nullable();
            $table->timestampTz('sent_to_moderation_at', 6);
            $table->timestampTz('started_at');
            $table->timestampTz('moderated_at', 6)->nullable();
            $table->bigInteger('forwarded_to')->nullable();
            $table->bigInteger('forwarded_by')->nullable();
            $table->timestampTz('forwarded_at', 6)->nullable();
            $table->timestampTz('seen_by_user_at')->nullable();
            $table->text('appeal_text')->nullable();
            $table->timestampTz('appealed_at')->nullable();
            $table->boolean('is_appeal_considered')->default(false);
            $table->boolean('is_appeal_confirmed')->default(false);
            $table->timestampTz('paid_at')->nullable();

            $table
                ->foreign('moderation_session_id')
                ->references('id')
                ->on('moderation_sessions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('photo_id')
                ->references('id')
                ->on('user_photos')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('photo_owner_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('forwarded_to')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('forwarded_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photo_moderations');
    }
}
