<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubdivisionsTable20191006 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subdivisions', function (Blueprint $table) {
            $table->smallIncrements('id')->generatedAs();
            $table->string('code', 3);
            $table->string('native_name', 128)->nullable();
            $table->string('english_name', 128);
            $table->string('type', 64)->nullable();
            $table->smallInteger('country_id')->index();
            $table->string('country_code', 2)->index();
            $table->smallInteger('parent_subdivision_id')->nullable();
            $table->string('parent_subdivision_code', 3)->nullable();
            $table->boolean('has_child_subdivisions')->default(false);
            $table->decimal('latitude', 9, 6)->nullable();
            $table->decimal('longitude', 9, 6)->nullable();

            $table->unique(['country_code', 'code']);

            $table
                ->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('country_code')
                ->references('code_alpha2')
                ->on('countries')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('parent_subdivision_id')
                ->references('id')
                ->on('subdivisions')
                ->onDelete('restrict')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subdivisions');
    }
}
