<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatMessagesTable20191006 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_messages', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('chat_id')->index();
            $table->bigInteger('user_id')->index();
            $table->bigInteger('peer_id')->index();
            $table->bigInteger('replied_message_id')->nullable();
            $table->text('replied_message_text')->nullable();
            $table->text('text');
            $table->timestampTz('sent_at', 6);
            $table->timestampTz('seen_at')->nullable();
            $table->string('status', 32)->index();
            $table->timestampTz('status_updated_at', 6);
            $table->jsonb('auto_rejection_reasons')->nullable();
            $table->bigInteger('transaction_id')->index();
            $table->smallInteger('auto_detected_language_id')->nullable();

            $table
                ->foreign('chat_id')
                ->references('id')
                ->on('chats')
                ->onDelete('cascade')
            ;

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('peer_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('replied_message_id')
                ->references('id')
                ->on('chat_messages')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('auto_detected_language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('restrict')
            ;
        });

        Schema::table('chat_messages', function () {
            DB::statement('CREATE INDEX IF NOT EXISTS chat_messages_replied_message_id_index ON chat_messages (replied_message_id) WHERE replied_message_id IS NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chat_messages');
    }
}
