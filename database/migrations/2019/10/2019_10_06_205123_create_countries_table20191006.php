<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable20191006 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->smallIncrements('id')->generatedAs();
            $table->char('code_alpha2', 2)->unique();
            $table->char('code_alpha3', 3)->unique();
            $table->char('code_numeric', 3)->nullable()->unique();
            $table->string('native_name', 64);
            $table->string('native_official_name', 64)->nullable();
            $table->string('english_name', 64);
            $table->string('english_official_name', 64)->nullable();
            $table->string('native_name_transliteration', 64)->nullable();
            $table->string('native_official_name_transliteration', 64)->nullable();
            $table->string('continent_code', 2);
            $table->boolean('is_independent');
            $table->char('sovereign_state_country_code', 2)->nullable();
            $table->boolean('show_in_list');
            $table->boolean('is_in_european_union')->default(false);
            $table->jsonb('currencies')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
    }
}
