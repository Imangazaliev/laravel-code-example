<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LanguageTexts20191103 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language_texts', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->smallInteger('language_entity_id');
            $table->smallInteger('language_id');
            $table->string('name', 64);

            $table
                ->foreign('language_entity_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade')
            ;

            $table
                ->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('restrict')
            ;

            $table->index(['language_entity_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('language_texts');
    }
}
