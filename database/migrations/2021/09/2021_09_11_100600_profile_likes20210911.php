<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProfileLikes20210911 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_likes', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('user_id');
            $table->bigInteger('target_user_id');
            $table->timestampTz('liked_at');
            $table->timestampTz('seen_by_owner_at')->nullable();
            $table->timestampTz('removed_at')->nullable();

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
            ;

            $table
                ->foreign('target_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_likes');
    }
}
