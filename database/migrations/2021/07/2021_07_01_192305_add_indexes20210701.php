<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexes20210701 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_authentications', function (Blueprint $table) {
            $table->index('auth_key');
        });

        Schema::table('notifications', function (Blueprint $table) {
            $table->index(['user_id', 'is_hidden', 'seen_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_authentications', function (Blueprint $table) {
            $table->dropIndex('user_authentications_auth_key_index');
        });

        Schema::table('notifications', function (Blueprint $table) {
            $table->dropIndex('notifications_user_id_is_hidden_seen_at_index');
        });
    }
}
