<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEthnicityTextsTable20200225 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ethnicity_texts', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->smallInteger('ethnicity_id');
            $table->smallInteger('language_id');
            $table->string('plural_name', 64);
            $table->string('female_name', 64)->nullable();
            $table->string('male_name', 64)->nullable();

            $table
                ->foreign('ethnicity_id')
                ->references('id')
                ->on('ethnicities')
                ->onDelete('cascade')
            ;

            $table
                ->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('restrict')
            ;

            $table->index(['ethnicity_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ethnicity_texts');
    }
}
