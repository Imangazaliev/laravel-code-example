<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalityTextsTable20200208 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locality_texts', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->integer('locality_id');
            $table->smallInteger('language_id');
            $table->string('name', 128);

            $table
                ->foreign('locality_id')
                ->references('id')
                ->on('localities')
                ->onDelete('cascade')
            ;

            $table
                ->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('restrict')
            ;

            $table->index(['locality_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locality_texts');
    }
}
