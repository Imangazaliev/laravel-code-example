<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubdivisionTextsTable20200207 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subdivision_texts', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->smallInteger('subdivision_id');
            $table->smallInteger('language_id');
            $table->string('name', 128);

            $table
                ->foreign('subdivision_id')
                ->references('id')
                ->on('subdivisions')
                ->onDelete('cascade')
            ;

            $table
                ->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('restrict')
            ;

            $table->index(['subdivision_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subdivision_texts');
    }
}
