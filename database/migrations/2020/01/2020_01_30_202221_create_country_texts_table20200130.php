<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryTextsTable20200130 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_texts', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->smallInteger('country_id');
            $table->smallInteger('language_id');
            $table->string('name', 64);
            $table->string('name_official', 64)->nullable();

            $table
                ->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('cascade')
            ;

            $table
                ->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('restrict')
            ;

            $table->index(['country_id', 'language_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('country_texts');
    }
}
