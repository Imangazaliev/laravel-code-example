<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDeactivationsTable20200323 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_deactivations', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('user_id')->index();
            $table->text('comment');
            $table->timestampTz('deactivated_at');
            $table->bigInteger('deactivated_by')->nullable();
            $table->timestampTz('reactivated_at')->nullable();
            $table->bigInteger('reactivated_by')->nullable();

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('deactivated_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('reactivated_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;
        });

        Schema::table('user_deactivations', function () {
            DB::statement('CREATE INDEX IF NOT EXISTS user_deactivations_deactivated_by_index ON user_deactivations (deactivated_by) WHERE deactivated_by IS NOT NULL;');
            DB::statement('CREATE INDEX IF NOT EXISTS user_deactivations_reactivated_by_index ON user_deactivations (reactivated_by) WHERE reactivated_by IS NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_deactivations');
    }
}
