<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentGatewayNotificationsTable20200301 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_gateway_notifications', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('invoice_id')->index();
            $table->jsonb('data');
            $table->timestampTz('created_at');

            $table
                ->foreign('invoice_id')
                ->references('id')
                ->on('invoices')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_gateway_notifications');
    }
}
