<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeys20200323 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table
                ->foreign('phone_number_country_code')
                ->references('code_alpha2')
                ->on('countries')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('time_zone_id')
                ->references('id')
                ->on('time_zones')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('main_photo_id')
                ->references('id')
                ->on('user_photos')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('ban_id')
                ->references('id')
                ->on('user_bans')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('deactivation_id')
                ->references('id')
                ->on('user_deactivations')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('deletion_id')
                ->references('id')
                ->on('user_deletions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('attached_moderator_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('moderation_moderator_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('moderation_forwarded_to')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('moderation_forwarded_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('ethnicity_id')
                ->references('id')
                ->on('ethnicities')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('subdivision_level_1_id')
                ->references('id')
                ->on('subdivisions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('subdivision_level_2_id')
                ->references('id')
                ->on('subdivisions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('subdivision_level_3_id')
                ->references('id')
                ->on('subdivisions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('locality_id')
                ->references('id')
                ->on('localities')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('native_country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('native_subdivision_level_1_id')
                ->references('id')
                ->on('subdivisions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('native_subdivision_level_2_id')
                ->references('id')
                ->on('subdivisions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('native_subdivision_level_3_id')
                ->references('id')
                ->on('subdivisions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('native_locality_id')
                ->references('id')
                ->on('localities')
                ->onDelete('restrict')
            ;
        });

        Schema::table('user_authentications', function (Blueprint $table) {
            $table
                ->foreign('login_ip_country_code')
                ->references('code_alpha2')
                ->on('countries')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('logout_ip_country_code')
                ->references('code_alpha2')
                ->on('countries')
                ->onDelete('restrict')
            ;
        });

        Schema::table('chats', function (Blueprint $table) {
            $table
                ->foreign('chat_request_id')
                ->references('id')
                ->on('chat_requests')
                ->onDelete('restrict')
            ;
        });

        Schema::table('countries', function (Blueprint $table) {
            $table
                ->foreign('sovereign_state_country_code')
                ->references('code_alpha2')
                ->on('countries')
                ->onDelete('restrict')
            ;
        });

        Schema::table('localities', function (Blueprint $table) {
            $table
                ->foreign('time_zone_id')
                ->references('id')
                ->on('time_zones')
                ->onDelete('restrict')
            ;
        });

        Schema::table('chat_requests', function (Blueprint $table) {
            $table
                ->foreign('transaction_id')
                ->references('id')
                ->on('coin_transactions')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('return_transaction_id')
                ->references('id')
                ->on('coin_transactions')
                ->onDelete('restrict')
            ;
        });

        Schema::table('chat_messages', function (Blueprint $table) {
            $table
                ->foreign('transaction_id')
                ->references('id')
                ->on('coin_transactions')
                ->onDelete('restrict')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_phone_number_country_code_foreign');
            $table->dropForeign('users_language_id_foreign');
            $table->dropForeign('users_time_zone_id_foreign');
            $table->dropForeign('users_main_photo_id_foreign');
            $table->dropForeign('users_ban_id_foreign');
            $table->dropForeign('users_deactivation_id_foreign');
            $table->dropForeign('users_deletion_id_foreign');
            $table->dropForeign('users_attached_moderator_id_foreign');
            $table->dropForeign('users_moderation_moderator_id_foreign');
            $table->dropForeign('users_moderation_forwarded_to_foreign');
            $table->dropForeign('users_moderation_forwarded_by_foreign');
            $table->dropForeign('users_ethnicity_id_foreign');
            $table->dropForeign('users_country_id_foreign');
            $table->dropForeign('users_subdivision_level_1_id_foreign');
            $table->dropForeign('users_subdivision_level_2_id_foreign');
            $table->dropForeign('users_subdivision_level_3_id_foreign');
            $table->dropForeign('users_locality_id_foreign');
            $table->dropForeign('users_native_country_id_foreign');
            $table->dropForeign('users_native_subdivision_level_1_id_foreign');
            $table->dropForeign('users_native_subdivision_level_2_id_foreign');
            $table->dropForeign('users_native_subdivision_level_3_id_foreign');
            $table->dropForeign('users_native_locality_id_foreign');
        });

        Schema::table('user_authentications', function (Blueprint $table) {
            $table->dropForeign('user_authentications_login_ip_country_code_foreign');
            $table->dropForeign('user_authentications_logout_ip_country_code_foreign');
        });

        Schema::table('chats', function (Blueprint $table) {
            $table->dropForeign('chats_chat_request_id_foreign');
        });

        Schema::table('countries', function (Blueprint $table) {
            $table->dropForeign('countries_sovereign_state_country_code_foreign');
        });

        Schema::table('localities', function (Blueprint $table) {
            $table->dropForeign('localities_time_zone_id_foreign');
        });

        Schema::table('chat_requests', function (Blueprint $table) {
            $table->dropForeign('chat_requests_transaction_id_foreign');
            $table->dropForeign('chat_requests_return_transaction_id_foreign');
        });

        Schema::table('chat_messages', function (Blueprint $table) {
            $table->dropForeign('chat_messages_transaction_id_foreign');
        });
    }
}
