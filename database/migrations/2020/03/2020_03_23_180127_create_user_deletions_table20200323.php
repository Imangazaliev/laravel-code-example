<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDeletionsTable20200323 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_deletions', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('user_id')->index();
            $table->string('name', 64);
            $table->string('phone_number', 32);
            $table->string('email', 64)->nullable();
            $table->string('reason', 64);
            $table->text('comment')->nullable();
            $table->timestampTz('deleted_at');
            $table->bigInteger('deleted_by')->nullable();
            $table->timestampTz('canceled_at')->nullable();

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;

            $table
                ->foreign('deleted_by')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
            ;
        });

        Schema::table('user_deletions', function () {
            DB::statement('CREATE INDEX IF NOT EXISTS user_deletions_deleted_by_index ON user_deletions (deleted_by) WHERE deleted_by IS NOT NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_deletions');
    }
}
