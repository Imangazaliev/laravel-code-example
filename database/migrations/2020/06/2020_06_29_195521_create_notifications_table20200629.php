<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable20200629 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id')->generatedAs();
            $table->bigInteger('user_id');
            $table->string('type', 64);
            $table->string('title', 512);
            $table->text('body');
            $table->jsonb('parameters')->default('{}');
            $table->string('context', 32)->default('');
            $table->jsonb('additional_data')->nullable();
            $table->boolean('is_hidden');
            $table->timestampTz('created_at', 6);
            $table->timestampTz('sent_at')->nullable();
            $table->timestampTz('seen_at')->nullable();

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
