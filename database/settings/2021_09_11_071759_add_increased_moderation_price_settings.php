<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class AddIncreasedModerationPriceSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('moderation.profile_moderation_price_2', 0.5);
        $this->migrator->add('moderation.photo_moderation_price_2', 0.4);
        $this->migrator->add('moderation.message_moderation_price_2', 0.3);
    }

    public function down(): void
    {
        $this->migrator->delete('moderation.profile_moderation_price_2');
        $this->migrator->delete('moderation.photo_moderation_price_2');
        $this->migrator->delete('moderation.message_moderation_price_2');
    }
}
