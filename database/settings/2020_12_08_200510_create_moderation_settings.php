<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateModerationSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('moderation.profile_moderation_price', 0.4);
        $this->migrator->add('moderation.photo_moderation_price', 0.3);
        $this->migrator->add('moderation.message_moderation_price', 0.2);
        $this->migrator->add('moderation.moderation_price_currency', 'RUB');
    }

    public function down(): void
    {
        $this->migrator->delete('moderation.profile_moderation_price');
        $this->migrator->delete('moderation.photo_moderation_price');
        $this->migrator->delete('moderation.message_moderation_price');
        $this->migrator->delete('moderation.moderation_price_currency');
    }
}
