<?php

declare(strict_types=1);

(function () {
    foreach (glob(__DIR__ . '/helpers/*/*.php') as $filename) {
        require_once $filename;
    }
})();
