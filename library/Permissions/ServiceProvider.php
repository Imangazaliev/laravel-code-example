<?php

declare(strict_types=1);

namespace ExampleProject\Permissions;

use Illuminate\Contracts\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{
    public function boot(PermissionRegistrar $permissionLoader): void
    {
        $permissionLoader->clearPermissionsInMemoryCache();

        $this->registerPermissions();
    }

    /**
     * Register the permission check method on the gate.
     *
     * We resolve the Gate fresh here, for benefit of long-running instances.
     */
    public function registerPermissions(): void
    {
        app(Gate::class)->before(function (Authorizable $user, string $ability) {
            return $user->checkPermissionTo($ability, language()->id) ?: null;
        });
    }
}
