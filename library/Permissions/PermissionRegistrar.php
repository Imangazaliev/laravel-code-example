<?php

declare(strict_types=1);

namespace ExampleProject\Permissions;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Cache\CacheManager;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Cache\Store as CacheStore;
use ExampleProject\Permissions\Exceptions\PermissionNotFoundException;
use ExampleProject\Permissions\Exceptions\RoleNotFoundException;

class PermissionRegistrar
{
    private CacheRepository $cache;

    private CacheManager $cacheManager;

    private ?array $permissions = null;

    public static $cacheExpirationTime;

    public static string $cacheKey;

    /**
     * PermissionRegistrar constructor.
     *
     * @param CacheManager $cacheManager
     */
    public function __construct(CacheManager $cacheManager)
    {
        $this->cacheManager = $cacheManager;

        self::$cacheExpirationTime = config('permissions.cache.expiration_time', config('permissions.cache_expiration_time'));
        self::$cacheKey = config('permissions.cache.key');

        $this->cache = $this->resolveCacheRepository();
    }

    private function resolveCacheRepository(): CacheRepository
    {
        // the 'default' fallback here is from the permission.php config file, where 'default' means to use config(cache.default)
        $cacheDriver = config('permissions.cache.store', 'default');

        // when 'default' is specified, no action is required since we already have the default instance
        if ($cacheDriver === 'default') {
            return $this->cacheManager->store();
        }

        // if an undefined cache store is specified, fallback to 'array' which is Laravel's closest equivalent to 'none'
        if ( ! array_key_exists($cacheDriver, config('cache.stores'))) {
            $cacheDriver = 'array';
        }

        return $this->cacheManager->store($cacheDriver);
    }

    /**
     * Clear the permissions cache.
     *
     * @return bool
     */
    public function clearCachedPermissions(): bool
    {
        $this->clearPermissionsInMemoryCache();

        return $this->cache->forget(self::$cacheKey);
    }

    /**
     * Clear the permissions in-memory cache.
     *
     * This is only intended to be called by the PermissionServiceProvider on boot,
     * so that long-running instances like Swoole don't keep old data in memory.
     */
    public function clearPermissionsInMemoryCache(): void
    {
        $this->permissions = null;
    }

    /**
     * Retrieve the ID of a permission by code.
     *
     * @param string $code
     *
     * @return int
     *
     * @throws PermissionNotFoundException
     */
    public function getPermissionIdByCode(string $code): int
    {
        $permissions = $this->getPermissions();

        if (array_key_exists($code, $permissions)) {
            return $permissions[$code];
        }

        throw PermissionNotFoundException::withCode($code);
    }

    /**
     * Check that a permission with the specified ID exists.
     *
     * @param int $permissionId
     *
     * @return bool
     */
    public function hasPermissionWithId(int $permissionId): bool
    {
        return in_array($permissionId, array_values($this->getPermissions()), true);
    }

    /**
     * Get the permissions code to ID map.
     *
     * @return array
     */
    public function getPermissions(): array
    {
        return $this->getCachedData()['permissions'];
    }

    /**
     * Get the roles code to ID map.
     *
     * @return array
     */
    public function getRoles(): array
    {
        return $this->getCachedData()['roles'];
    }

    /**
     * Get the role ID to permission ID map.
     *
     * @return array
     */
    public function getRolesToPermissions(): array
    {
        return $this->getCachedData()['roles_to_permissions'];
    }

    /**
     * @param $roleIdsOrCodes
     *
     * @return array
     *
     * @throws RoleNotFoundException
     */
    public function getRoleIds($roleIdsOrCodes): array
    {
        if ( ! is_array($roleIdsOrCodes)) {
            $roleIdsOrCodes = [$roleIdsOrCodes];
        }

        $rolesIds = [];

        foreach ($roleIdsOrCodes as $roleIdOrCode) {
            if (is_numeric($roleIdOrCode)) {
                $rolesIds[] = $roleIdOrCode;

                continue;
            }

            $roleId = (new Role())->where('code', $roleIdOrCode)->value('id');

            if ($roleId === null) {
                throw RoleNotFoundException::create($roleIdOrCode);
            }

            $rolesIds[] = $roleId;
        }

        return $rolesIds;
    }

    /**
     * @param $permissionIdsOrCodes
     *
     * @return int[]
     *
     * @throws PermissionNotFoundException
     */
    public function getPermissionIds($permissionIdsOrCodes): array
    {
        if ( ! is_array($permissionIdsOrCodes)) {
            $permissionIdsOrCodes = [$permissionIdsOrCodes];
        }

        $permissionIds = [];

        foreach ($permissionIdsOrCodes as $permissionIdOrCode) {
            if (is_numeric($permissionIdOrCode)) {
                $permissionIds[] = $permissionIdOrCode;

                continue;
            }

            $roleId = (new Permission())->where('code', $permissionIdOrCode)->value('id');

            if ($roleId === null) {
                throw PermissionNotFoundException::withCode($permissionIdOrCode);
            }

            $permissionIds[] = $roleId;
        }

        return $permissionIds;
    }

    private function getCachedData(): array
    {
        return $this->cache->remember(self::$cacheKey, self::$cacheExpirationTime, function () {
            $result = [];

            $result['permissions'] = (new Permission())
                ->orderBy('code')
                ->pluck('id', 'code')
                ->all()
            ;
            $result['roles'] = (new Role())
                ->orderBy('code')
                ->pluck('id', 'code')
                ->all()
            ;

            $result['roles_to_permissions'] = (new Role())
                ->with('permissions:permissions.id')
                ->get(['id'])
                ->reduce(function ($result, Role $role) {
                    $result[$role->id] = $role->permissions->pluck('id')->all();

                    return $result;
                }, [])
            ;

            return $result;
        });
    }

    /**
     * Get the instance of the Cache Store.
     *
     * @return CacheStore
     */
    public function getCacheStore(): CacheStore
    {
        return $this->cache->getStore();
    }
}
