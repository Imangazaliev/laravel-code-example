<?php

declare(strict_types=1);

namespace Omnipay\Tinkoff\Message;

class AbstractResponse extends \Omnipay\Common\Message\AbstractResponse
{
    /**
     * {@inheritdoc}
     */
    public function getMessage()
    {
        return $this->data['Message'] ?? null;
    }

    /**
     * {@inheritdoc}
     */
    public function getCode()
    {
        return $this->data['ErrorCode'] ?? null;
    }

    /**
     * {@inheritdoc}
     */
    public function isSuccessful(): bool
    {
        return $this->data['Success'];
    }

    /**
     * {@inheritdoc}
     */
    public function getTransactionReference(): string
    {
        return $this->data['PaymentId'];
    }
}
