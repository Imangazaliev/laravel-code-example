<?php

declare(strict_types=1);

namespace Omnipay\Tinkoff\Message;

class PurchaseRequest extends AbstractRequest
{
    public function getEndpointPath(): string
    {
        return '/Init';
    }

    public function getResponseClass(): string
    {
        return PurchaseResponse::class;
    }

    public function getData(): array
    {
        return [
            'TerminalKey' => $this->getTerminalKey(),
            'OrderId' => $this->getTransactionId(),
            'Amount' => $this->getParameter('amount') * 100,
            'Description' => $this->getDescription(),
            'NotificationURL' => $this->getNotifyUrl(),
            'SuccessURL' => $this->getReturnUrl(),
            'FailURL' => $this->getCancelUrl(),
        ];
    }

    public function getDescription(): string
    {
        return $this->getParameter('Description');
    }

    public function setDescription($value): self
    {
        return $this->setParameter('Description', $value);
    }

    public function getReturnUrl(): string
    {
        return $this->getParameter('SuccessURL');
    }

    public function setReturnUrl($value): self
    {
        return $this->setParameter('SuccessURL', $value);
    }

    public function getLanguage(): ?string
    {
        return $this->getParameter('Language');
    }

    public function setLanguage(string $value): self
    {
        return $this->setParameter('Language', $value);
    }

    public function getClientIp(): string
    {
        return $this->getParameter('IP');
    }

    public function setClientIp($ip): self
    {
        return $this->setParameter('IP', $ip);
    }
}
