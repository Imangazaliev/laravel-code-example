<?php

declare(strict_types=1);

namespace Omnipay\Tinkoff\Message;

use Omnipay\Common\Message\RedirectResponseInterface;

class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
    /**
     * @return bool
     */
    public function isRedirect(): bool
    {
        return array_key_exists('PaymentURL', $this->data) ? true : false;
    }

    /**
     * Get the URL of the payment form to which the client's browser should be redirected.
     *
     * @return mixed|null
     */
    public function getRedirectUrl()
    {
        return $this->data['PaymentURL'] ?? null;
    }

    /**
     * @return mixed
     */
    public function getRedirectData()
    {
        return $this->data;
    }

    /**
     * If response with an error. Error message.
     *
     * @return mixed|null
     */
    public function getError()
    {
        return $this->data['ErrorCode'] ?? null;
    }
}
