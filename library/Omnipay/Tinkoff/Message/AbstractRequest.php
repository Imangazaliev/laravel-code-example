<?php

declare(strict_types=1);

namespace Omnipay\Tinkoff\Message;

use Omnipay\Common\Exception\InvalidRequestException;
use Omnipay\Common\Message\ResponseInterface;

abstract class AbstractRequest extends \Omnipay\Common\Message\AbstractRequest
{
    /**
     * The an endpoint path.
     *
     * @return string
     */
    abstract public function getEndpointPath(): string;

    abstract public function getResponseClass(): string;

    /**
     * Get the base API URL.
     *
     * @return string
     */
    public function getBaseApiUrl(): string
    {
        return 'https://securepay.tinkoff.ru/v2';
    }

    public function getTerminalKey()
    {
        return $this->getParameter('TerminalKey');
    }

    public function setTerminalKey(string $value): self
    {
        return $this->setParameter('TerminalKey', $value);
    }

    public function getHeaders(): array
    {
        return [
            'Content-Type' => 'application/json',
        ];
    }

    public function getHttpMethod(): string
    {
        return 'POST';
    }

    public function getCurrencyDecimalPlaces(): int
    {
        return 0;
    }

    /**
     * @param mixed $data
     *
     * @return ResponseInterface
     *
     * @throws InvalidRequestException
     */
    public function sendData($data): ResponseInterface
    {
        $url = $this->getBaseApiUrl() . $this->getEndpointPath();

        $this->validate('TerminalKey');

        $data['TerminalKey'] = $this->getTerminalKey();
        $data['Token'] = $this->calculateToken($data);

        $response = $this->httpClient->request(
            $this->getHttpMethod(),
            $url,
            $this->getHeaders(),
            json_encode($data, JSON_THROW_ON_ERROR),
        );

        $decodedResponse = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

        $responseClass = $this->getResponseClass();

        return new $responseClass($this, $decodedResponse);
    }

    private function calculateToken(array $data): string
    {
        unset($data['Receipt'], $data['DATA']);

        $data['Password'] = config('services.tinkoff.terminal_password');

        ksort($data);

        return hash('sha256', implode('', array_values($data)));
    }
}
