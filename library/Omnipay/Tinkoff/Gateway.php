<?php

declare(strict_types=1);

namespace Omnipay\Tinkoff;

use Omnipay\Common\AbstractGateway;
use Omnipay\Common\Message\RequestInterface;
use Omnipay\Tinkoff\Message\PurchaseRequest;

class Gateway extends AbstractGateway
{
    /**
     * Get gateway display name.
     *
     * This can be used by carts to get the display name for each gateway.
     */
    public function getName(): string
    {
        return 'Tinkoff';
    }

    public function getDefaultParameters(): array
    {
        return [
            'TerminalKey' => '',
        ];
    }

    public function getTerminalKey()
    {
        return $this->getParameter('TerminalKey');
    }

    public function setTerminalKey(string $value): self
    {
        return $this->setParameter('TerminalKey', $value);
    }

    /**
     * Request for order registration.
     *
     * @param array $options array of options
     *
     * @return RequestInterface
     */
    public function purchase(array $options = []): RequestInterface
    {
        return $this->createRequest(PurchaseRequest::class, $options);
    }

    /**
     * Order status request.
     *
     * @param array $options
     *
     * @return RequestInterface
     */
    public function completeAuthorize(array $options = []): RequestInterface
    {
        return $this->createRequest(OrderStatusRequest::class, $options);
    }

    /**
     * Refund order request.
     *
     * @param array $options
     *
     * @return RequestInterface
     */
    public function refund(array $options = []): RequestInterface
    {
        return $this->createRequest(RefundRequest::class, $options);
    }

    /**
     * Order status request.
     *
     * @param array $options
     *
     * @return RequestInterface
     */
    public function completePurchase(array $options = []): RequestInterface
    {
        return $this->createRequest(OrderStatusRequest::class, $options);
    }

    /**
     * Order status request.
     *
     * @param array $options
     *
     * @return RequestInterface
     */
    public function orderStatus(array $options = []): RequestInterface
    {
        return $this->createRequest(OrderStatusRequest::class, $options);
    }
}
