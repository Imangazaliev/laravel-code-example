<?php

declare(strict_types=1);

namespace Omnipay\YooKassa\Message;

use Omnipay\Common\Message\AbstractResponse;

class IncomingNotificationResponse extends AbstractResponse
{
    public function getTransactionId()
    {
        return $this->data['object']['metadata']['transactionId'] ?? null;
    }

    public function getTransactionReference()
    {
        return $this->data['object']['id'] ?? null;
    }

    public function isSuccessful()
    {
        return $this->getTransactionReference() !== null;
    }
}
