<?php

declare(strict_types=1);

namespace Omnipay\YooKassa\Message;

use YooKassa\Client;

abstract class AbstractRequest extends \Omnipay\Common\Message\AbstractRequest
{
    protected Client $client;

    public function getShopId()
    {
        return $this->getParameter('shopId');
    }

    public function setShopId($value)
    {
        return $this->setParameter('shopId', $value);
    }

    public function getSecret()
    {
        return $this->getParameter('secret');
    }

    public function setSecret($value)
    {
        return $this->setParameter('secret', $value);
    }

    public function getCapture()
    {
        return $this->getParameter('capture');
    }

    public function setCapture($value)
    {
        return $this->setParameter('capture', $value);
    }

    /**
     * Get the request receipt data.
     *
     * @return array
     */
    public function getReceipt()
    {
        return $this->getParameter('receipt');
    }

    /**
     * Sets the request receipt data.
     *
     * @param array $value
     *
     * @return $this
     */
    public function setReceipt($value)
    {
        return $this->setParameter('receipt', $value);
    }

    public function setYooKassaClient(Client $client): void
    {
        $this->client = $client;
    }
}
