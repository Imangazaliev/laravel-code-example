<?php

declare(strict_types=1);

namespace ExampleProject\RateLimiter;

use Illuminate\Cache\RateLimiter as LaravelRateLimiter;

class RateLimiter
{
    private $laravelRateLimiter;

    private array $limits = [];

    public function __construct(LaravelRateLimiter $laravelRateLimiter)
    {
        $this->laravelRateLimiter = $laravelRateLimiter;
    }

    public function registerRateLimits(string $key, array $limits): void
    {
        foreach ($limits as $limitParams) {
            [$attempts, $interval] = $limitParams;

            $this->addLimit($key, $attempts, $interval);
        }
    }

    private function addLimit(string $key, int $attempts, int $interval): void
    {
        if ( ! array_key_exists($key, $this->limits)) {
            $this->limits[$key] = [];
        }

        $this->limits[$key][] = [$attempts, $interval];
    }

    public function hit(string $baseKey, $identifier): void
    {
        foreach ($this->limits[$baseKey] as $limitParams) {
            [$attempts, $interval] = $limitParams;

            $key = $this->getKey($baseKey, $identifier, $attempts, $interval);

            $this->laravelRateLimiter->hit($key, $interval);
        }
    }

    public function tooManyAttempts(string $baseKey, $identifier): bool
    {
        foreach ($this->limits[$baseKey] as $limitParams) {
            [$attempts, $interval] = $limitParams;

            $key = $this->getKey($baseKey, $identifier, $attempts, $interval);

            if ($this->laravelRateLimiter->tooManyAttempts($key, $attempts)) {
                return true;
            }
        }

        return false;
    }

    public function availableIn(string $baseKey, $identifier): int
    {
        $result = 0;

        foreach ($this->limits[$baseKey] as $limitParams) {
            [$attempts, $interval] = $limitParams;

            $key = $this->getKey($baseKey, $identifier, $attempts, $interval);

            $remainingAttempts = $this->laravelRateLimiter->retriesLeft($key, $attempts);

            $result = max($result, $remainingAttempts > 0 ? 0 : $this->laravelRateLimiter->availableIn($key));
        }

        return $result;
    }

    public function clear(string $baseKey, $identifier): void
    {
        foreach ($this->limits[$baseKey] as $limitParams) {
            [$attempts, $interval] = $limitParams;

            $key = $this->getKey($baseKey, $identifier, $attempts, $interval);

            $this->laravelRateLimiter->clear($key);
        }
    }

    private function getKey(string $baseKey, $identifier, $attempts, $interval): string
    {
        return "{$baseKey}:{$identifier}:{$attempts}:{$interval}";
    }
}
