<?php

declare(strict_types=1);

namespace ExampleProject\Translations;

use Illuminate\Contracts\Translation\Translator as LaravelTranslatorContract;

class LaravelTranslator implements LaravelTranslatorContract
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function get($key, array $replace = [], $locale = null): string
    {
        return $this->getTranslation($key, $replace, [
            'lng' => $locale,
        ]);
    }

    public function choice($key, $number, array $replace = [], $locale = null): string
    {
        return $this->getTranslation($key, $replace, [
            'lng' => $locale,
            'count' => $number,
        ]);
    }

    private function getTranslation($key, array $replace = [], array $options = [])
    {
        $convertedKey = $this->convertKey($key);

        $result = $this->translator->getTranslation($convertedKey, $replace, $options);

        if (starts_with($convertedKey, 'validation:')) {
            $result = str_replace('{{field}}', '{{attribute}}', $result);
            $result = preg_replace('/{{([a-z]+)}}/', ':$1', $result);
        }

        return $result === $convertedKey ? $key : $result;
    }

    private function convertKey(string $key): string
    {
        return implode(':', explode('.', $key, 2));
    }

    public function getLocale(): string
    {
        return $this->translator->getLocale();
    }

    public function setLocale($locale): void
    {
        $this->translator->setLocale($locale);
    }
}
