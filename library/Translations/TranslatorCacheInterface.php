<?php

declare(strict_types=1);

namespace ExampleProject\Translations;

interface TranslatorCacheInterface
{
    public const NAMESPACES = [
        'admin',
        'auth',
        'chat',
        'coins',
        'common',
        'guardian',
        'landing',
        'mail',
        'moderation',
        'notifications',
        'photo',
        'profile',
        'search',
        'validation',
    ];

    public function setStringsInNamespace(string $languageCode, string $namespace, array $strings): void;

    public function getStringsInNamespace(string $languageCode, string $namespace): array;

    public function setLastUpdateTime(string $languageCode, int $time): void;

    public function getLastUpdateTime(string $languageCode): ?int;
}
