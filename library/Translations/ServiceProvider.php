<?php

declare(strict_types=1);

namespace ExampleProject\Translations;

use Illuminate\Contracts\Events\Dispatcher as EventDispatcher;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Foundation\Events\LocaleUpdated;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->bind(TranslatorCacheInterface::class, TranslatorCache::class);

        $this->app->singleton(TranslatorInterface::class, function ($app) {
            $cache = $app->make(TranslatorCacheInterface::class);

            $locale = $app['config']['app.locale'];

            return new Translator($cache, $locale);
        });

        $this->app->singleton('translator', function ($app) {
            return new LaravelTranslator($app->make(TranslatorInterface::class));
        });
    }

    public function boot(EventDispatcher $dispatcher): void
    {
        $dispatcher->listen(LocaleUpdated::class, function (LocaleUpdated $event) {
            app(TranslatorInterface::class)->setLocale($event->locale);
        });
    }

    public function provides(): array
    {
        return [
            TranslatorCacheInterface::class,
            Translator::class,
            'translator',
        ];
    }
}
