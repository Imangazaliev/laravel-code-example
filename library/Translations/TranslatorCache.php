<?php

declare(strict_types=1);

namespace ExampleProject\Translations;

use Illuminate\Contracts\Redis\Factory as RedisFactoryInterface;
use Illuminate\Redis\Connections\Connection;

class TranslatorCache implements TranslatorCacheInterface
{
    /**
     * @var RedisFactoryInterface
     */
    private RedisFactoryInterface $redis;

    public function __construct(RedisFactoryInterface $redis)
    {
        $this->redis = $redis;
    }

    private function connection(): Connection
    {
        return $this->redis->connection('cache');
    }

    public function setStringsInNamespace(string $languageCode, string $namespace, array $strings): void
    {
        $key = $this->getNamespaceCacheKey($languageCode, $namespace);

        $stringsEncoded = json_encode($strings, JSON_THROW_ON_ERROR);

        $this->connection()->set($key, $stringsEncoded);
    }

    public function getStringsInNamespace(string $languageCode, string $namespace): array
    {
        $key = $this->getNamespaceCacheKey($languageCode, $namespace);

        $stringsEncoded = $this->connection()->get($key);

        if ($stringsEncoded !== null) {
            return json_decode($stringsEncoded, true, 512, JSON_THROW_ON_ERROR);
        }

        return [];
    }

    private function getNamespaceCacheKey(string $languageCode, string $namespace): string
    {
        return "ui-strings.strings.{$languageCode}-{$namespace}";
    }

    public function setLastUpdateTime(string $languageCode, int $time): void
    {
        $key = $this->getLastUpdateTimeCacheKey($languageCode);

        $this->connection()->set($key, $time);
    }

    public function getLastUpdateTime(string $languageCode): ?int
    {
        $key = $this->getLastUpdateTimeCacheKey($languageCode);

        $lastUpdateTime = $this->connection()->get($key);

        return $lastUpdateTime === null ? null : (int) $lastUpdateTime;
    }

    private function getLastUpdateTimeCacheKey(string $languageCode): string
    {
        return "ui-strings.last-update-time.{$languageCode}";
    }
}
