<?php

declare(strict_types=1);

namespace ExampleProject\Translations;

interface TranslatorInterface
{
    public function getTranslation(string $fullyQualifiedStringCode, array $parameters = [], array $options = []): string;

    public function getLocale(): string;

    public function setLocale(string $locale): void;
}
