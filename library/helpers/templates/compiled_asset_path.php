<?php

declare(strict_types=1);

/**
 * Get the path to a JavaScript or CSS asset compiled via Webpack.
 *
 * @param string $assetName
 *
 * @throws Exception
 *
 * @return string
 */
function compiled_asset_path(string $assetName): string
{
    $manifestPath = public_path('compiled/manifest.json');

    if ( ! file_exists($manifestPath)) {
        throw new Exception('Manifest not found (public/compiled/manifest.json)');
    }

    $manifest = json_decode(file_get_contents($manifestPath), true, 512, JSON_THROW_ON_ERROR);

    if (array_key_exists($assetName, $manifest)) {
        return $manifest[$assetName];
    }

    $exception = new Exception("Unable to locate asset \"{$assetName}\"");

    if (config('app.debug')) {
        throw $exception;
    }

    report($exception);

    return $assetName;
}
