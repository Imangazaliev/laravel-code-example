<?php

declare(strict_types=1);

/**
 * Returns the hash of the last commit.
 *
 * @return string
 */
function git_last_commit_hash(): string
{
    // in-memory cache for the hash of the last commit in Git
    static $gitHash = null;

    if ($gitHash === null) {
        $gitHash = exec(sprintf('cd %s; git rev-parse --verify HEAD', base_path()));
    }

    return $gitHash;
}
