<?php

declare(strict_types=1);

use App\Models\User;

/**
 * An alias for auth()->user().
 *
 * This function was created to help the IDE recognize the class of the current user.
 * auth()->user() returns object that implements \Illuminate\Contracts\Auth\Authenticatable interface
 * and the IDE can not recognize that it's an instance of App\Models\User class.
 * Use this function instead of auth()->user().
 *
 * @return User|null
 */
function current_user(): ?User
{
    return auth()->guard('user')->user();
}
