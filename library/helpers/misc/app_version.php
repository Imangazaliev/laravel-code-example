<?php

declare(strict_types=1);

function app_version(): string
{
    return sha1(git_last_commit_hash());
}
