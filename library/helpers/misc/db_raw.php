<?php

declare(strict_types=1);

use Illuminate\Support\Facades\DB;

function db_raw($expression)
{
    return DB::raw($expression);
}
