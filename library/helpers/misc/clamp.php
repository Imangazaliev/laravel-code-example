<?php

declare(strict_types=1);

/**
 * Clamps number within the inclusive lower and upper bounds.
 *
 * @param int $number
 * @param int $lower
 * @param int $upper
 *
 * @return int
 */
function clamp(int $number, int $lower, int $upper): int
{
    return max($lower, min($upper, $number));
}
