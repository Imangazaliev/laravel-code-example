<?php

declare(strict_types=1);

use App\Services\Cache\CacheInfo;

function cache_info(): CacheInfo
{
    return CacheInfo::instance();
}
