<?php

declare(strict_types=1);

use App\Models\Language;

/**
 * Return a model of the current language.
 *
 * @return Language
 */
function language(): Language
{
    // static variable for caching a model of the current language
    static $currentLanguage = null;

    if ($currentLanguage === null) {
        $currentLanguage = cache_info()->currentLanguage(app()->getLocale())->remember(function () {
            return (new Language())
                ->where('code', app()->getLocale())
                ->first()
            ;
        });

        if ($currentLanguage === null) {
            throw new RuntimeException(sprintf('Language with code %s not found', app()->getLocale()));
        }

        return $currentLanguage;
    }

    if ($currentLanguage->code !== app()->getLocale()) {
        // if locale was changed and the code of cached model
        // is not equal to the current locale
        $currentLanguage = cache_info()->currentLanguage(app()->getLocale())->remember(function () {
            return (new Language())
                ->where('code', app()->getLocale())
                ->first()
            ;
        });

        if ($currentLanguage === null) {
            throw new RuntimeException(sprintf('Language with code %s not found', app()->getLocale()));
        }
    }

    return $currentLanguage;
}
