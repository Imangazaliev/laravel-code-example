<?php

declare(strict_types=1);

use ExampleProject\Translations\TranslatorInterface;

/**
 * Returns UI string translation by the code.
 *
 * @param string $stringCode
 * @param array $parameters
 * @param array|null $options
 *
 * @return string
 */
function __(string $stringCode, array $parameters = [], array $options = []): string
{
    /**
     * @var TranslatorInterface $translator
     */
    $translator = app(TranslatorInterface::class);

    return $translator->getTranslation($stringCode, $parameters, $options);
}
