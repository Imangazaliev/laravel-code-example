<?php

declare(strict_types=1);

use App\Models\Language;

function language_id(string $languageCode = null): int
{
    if ($languageCode === null) {
        return language()->id;
    }

    return (int) cache()->rememberForever("language_id_by_code_{$languageCode}", function () use ($languageCode) {
        $language = (new Language())
            ->where('code', $languageCode)
            ->firstOrFail(['id'])
        ;

        return (int) $language->id;
    });
}
