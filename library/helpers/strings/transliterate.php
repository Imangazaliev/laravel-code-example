<?php

declare(strict_types=1);

/**
 * Transliterate a string.
 *
 * @param string $string
 *
 * @return string Transliterated string.
 */
function transliterate(string $string): string
{
    return transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\uffff] remove', $string);
}
