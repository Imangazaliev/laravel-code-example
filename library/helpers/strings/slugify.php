<?php

declare(strict_types=1);

/**
 * @param string $string
 * @param bool $preserveCase
 *
 * @return string
 *
 * TODO: check and refactor
 */
function slugify(string $string, bool $preserveCase = false): string
{
    $result = trim($string);

    if ($preserveCase === false) {
        $result = mb_strtolower($result);
    }

    $result = transliterate($result);

    $replacements = [
        ' ' => '-', '_' => '-',
        '.' => '', '/' => '',
        '\'' => '', '‘' => '',
        '’' => '',
    ];

    $result = strtr($result, $replacements);

    $result = preg_replace('/[^a-z0-9]/i', '-', $result);
    $result = preg_replace('/-+/', '-', $result);
    $result = trim($result, '-');

    // TODO: remove (think how to handle this case better)
    return $result !== '' ? $result : '-';
}
