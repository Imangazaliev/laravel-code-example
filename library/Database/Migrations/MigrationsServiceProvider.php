<?php

declare(strict_types=1);

namespace ExampleProject\Database\Migrations;

use Illuminate\Database\MigrationServiceProvider as LaravelMigrationServiceProvider;

class MigrationsServiceProvider extends LaravelMigrationServiceProvider
{
    protected function registerCreator(): void
    {
        $this->app->singleton('migration.creator', function ($app) {
            return new MigrationCreator($app['files'], $app->basePath('stubs'));
        });
    }

    /**
     * Register the migrator service.
     */
    protected function registerMigrator(): void
    {
        // The migrator is responsible for actually running and rollback the migration
        // files in the application. We'll pass in our database connection resolver
        // so the migrator can resolve any of these connections when it needs to.
        $this->app->singleton('migrator', function ($app) {
            $repository = $app['migration.repository'];

            return new Migrator($repository, $app['db'], $app['files']);
        });
    }
}
