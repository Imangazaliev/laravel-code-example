<?php

declare(strict_types=1);

namespace ExampleProject\Database\Migrations;

use Illuminate\Database\Migrations\MigrationCreator as LaravelMigrationCreator;

class MigrationCreator extends LaravelMigrationCreator
{
    protected function getPath($name, $path): string
    {
        $path = $path.'/'.date('Y').'/'.date('m');

        if ( ! $this->files->exists($path)) {
            $this->files->makeDirectory($path, 0775, true);
        }

        $timestamp = date('Ymd');

        return $path.'/'.$this->getDatePrefix().'_'.$name.$timestamp.'.php';
    }

    /**
     * Populate the place-holders in the migration stub.
     *
     * @param string $name
     * @param string $stub
     * @param string $table
     *
     * @return string
     */
    protected function populateStub($name, $stub, $table): string
    {
        $timestamp = date('Ymd');

        $stub = str_replace('{{ class }}', $this->getClassName($name) . $timestamp, $stub);

        // Here we will replace the table place-holders with the table specified by
        // the developer, which is useful for quickly creating a tables creation
        // or update migration from the console instead of typing it manually.
        if ( ! is_null($table)) {
            $stub = str_replace('{{ table }}', $table, $stub);
        }

        return $stub;
    }
}
