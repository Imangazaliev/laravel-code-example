<?php

declare(strict_types=1);

namespace ExampleProject\Database\Migrations;

use Illuminate\Database\Migrations\Migrator as LaravelMigrator;

class Migrator extends LaravelMigrator
{
    public function paths(): array
    {
        $migrationsDirectory = database_path('migrations');

        return array_unique(array_merge(
            // migrations-directory/year/month
            $this->files->glob("{$migrationsDirectory}/*/*"),
            $this->paths,
        ));
    }
}
