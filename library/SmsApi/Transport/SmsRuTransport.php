<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi\Transport;

use GuzzleHttp\ClientInterface;
use ExampleProject\SmsApi\Exceptions\SmsSendFailedException;

class SmsRuTransport implements SmsTransportInterface
{
    private const API_URL = 'https://sms.ru/sms/send';

    private ClientInterface $httpClient;

    private string $apiId;

    private string $from;

    public function __construct(ClientInterface $httpClient, string $apiId, string $from)
    {
        $this->httpClient = $httpClient;
        $this->apiId = $apiId;
        $this->from = $from;
    }

    public function sendMessage(string $phoneNumber, string $text): void
    {
        $parameters = [
            'api_id' => $this->apiId,
            'to' => $phoneNumber,
            'from' => $this->from,
            'msg' => $text,
            'json' => 1,
        ];

        $response = $this->httpClient->request('GET', self::API_URL, [
            'query' => $parameters,
        ]);

        $decodedResponse = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

        if ($decodedResponse['status'] !== 'OK') {
            throw new SmsSendFailedException(sprintf('Status code: %d', $decodedResponse['status_code']));
        }
    }
}
