<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi\Transport;

use Psr\Log\LoggerInterface;

class LogTransport implements SmsTransportInterface
{
    private LoggerInterface $logger;

    private string $from;

    public function __construct(LoggerInterface $logger, string $from)
    {
        $this->logger = $logger;
        $this->from = $from;
    }

    public function sendMessage(string $phoneNumber, string $text): void
    {
        $message = implode("\n", [
            'To: ' . $phoneNumber,
            'From: ' . $this->from,
            'Text: ' . $text,
        ]);

        $this->logger->debug($message);
    }
}
