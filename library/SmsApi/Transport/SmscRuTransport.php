<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi\Transport;

use GuzzleHttp\ClientInterface;
use ExampleProject\SmsApi\Exceptions\SmsSendFailedException;

class SmscRuTransport implements SmsTransportInterface
{
    private const API_URL = 'https://smsc.ru/sys/send.php';

    private ClientInterface $httpClient;

    private string $login;

    private string $password;

    private bool $setSender;

    private string $from;

    public function __construct(
        ClientInterface $httpClient,
        string $login,
        string $password,
        bool $setSender,
        string $from,
    ) {
        $this->httpClient = $httpClient;
        $this->login = $login;
        $this->password = $password;
        $this->setSender = $setSender;
        $this->from = $from;
    }

    public function sendMessage(string $phoneNumber, string $text): void
    {
        $parameters = [
            'login' => $this->login,
            'psw' => $this->password,
            'phones' => $phoneNumber,
            'mes' => $text,
            'fmt' => 3,
        ];

        if ($this->setSender) {
            $parameters['sender'] = $this->from;
        }

        $response = $this->httpClient->request('POST', self::API_URL, [
            'form_params' => $parameters,
        ]);

        $decodedResponse = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

        if (array_key_exists('error', $decodedResponse)) {
            throw new SmsSendFailedException(sprintf('Error code: %d', $decodedResponse['error_code']));
        }
    }
}
