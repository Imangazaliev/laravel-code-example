<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi\Transport;

class ArrayTransport implements SmsTransportInterface
{
    private string $from;

    private array $messages = [];

    public function __construct(string $from)
    {
        $this->from = $from;
    }

    public function sendMessage(string $phoneNumber, string $text): void
    {
        $this->messages[] = [
            'To: ' . $phoneNumber,
            'From: ' . $this->from,
            'Text: ' . $text,
        ];
    }

    public function getMessages(): array
    {
        return $this->messages;
    }
}
