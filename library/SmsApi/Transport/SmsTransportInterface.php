<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi\Transport;

interface SmsTransportInterface
{
    public function sendMessage(string $phoneNumber, string $text): void;
}
