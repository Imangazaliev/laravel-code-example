<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi\Transport;

use ExampleProject\SmsApi\Exceptions\SmsSendFailedException;
use Vonage\Client;
use Vonage\Client\Credentials\Basic as BasicCredentials;
use Vonage\SMS\Message\SMS;

class VonageTransport implements SmsTransportInterface
{
    private string $apiKey;

    private string $secretKey;

    private string $from;

    public function __construct(string $apiKey, string $secretKey, string $from)
    {
        $this->apiKey = $apiKey;
        $this->secretKey = $secretKey;
        $this->from = $from;
    }

    public function sendMessage(string $phoneNumber, string $text): void
    {
        $basic  = new BasicCredentials($this->apiKey, $this->secretKey);
        $client = new Client($basic);

        $sms = new SMS($phoneNumber, $this->from, $text);

        $response = $client->sms()->send($sms);

        $sentSms = $response->current();

        if ($sentSms->getStatus() !== 0) {
            throw new SmsSendFailedException(sprintf('Message send failed. Error code: %d', $sentSms->getStatus()));
        }
    }
}
