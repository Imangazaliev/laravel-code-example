<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi\Transport;

use GuzzleHttp\ClientInterface;

class D7NetworksTransport implements SmsTransportInterface
{
    private const API_URL = 'https://http-api.d7networks.com/send';

    private ClientInterface $httpClient;

    private string $username;

    private string $password;

    private string $from;

    public function __construct(ClientInterface $httpClient, string $username, string $password, string $from)
    {
        $this->httpClient = $httpClient;
        $this->username = $username;
        $this->password = $password;
        $this->from = $from;
    }

    public function sendMessage(string $phoneNumber, string $text): void
    {
        $parameters = [
            'to' => $phoneNumber,
            'from' => $this->from,
            'content' => mb_convert_encoding($text, 'UTF-16'),
            'username' => $this->username,
            'password' => $this->password,
            'coding' => 8,
        ];

        $this->httpClient->request('POST', self::API_URL, [
            'form_params' => $parameters,
        ]);
    }
}
