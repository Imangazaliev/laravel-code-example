<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi;

interface SmsClientInterface
{
    public function sendMessage(string $recipientPhoneNumber, string $text): void;
}
