<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi;

use ExampleProject\SmsApi\Transport\SmsTransportInterface;

class SmsClient implements SmsClientInterface
{
    private SmsTransportInterface $api;

    public function __construct(SmsTransportInterface $api)
    {
        $this->api = $api;
    }

    public function sendMessage(string $recipientPhoneNumber, string $text): void
    {
        $this->api->sendMessage($recipientPhoneNumber, $text);
    }
}
