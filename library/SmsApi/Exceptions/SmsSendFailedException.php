<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi\Exceptions;

use Exception;

class SmsSendFailedException extends Exception
{
    //
}
