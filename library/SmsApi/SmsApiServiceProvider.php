<?php

declare(strict_types=1);

namespace ExampleProject\SmsApi;

use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use ExampleProject\SmsApi\Transport\ArrayTransport;
use ExampleProject\SmsApi\Transport\D7NetworksTransport;
use ExampleProject\SmsApi\Transport\LogTransport;
use ExampleProject\SmsApi\Transport\SmscRuTransport;
use ExampleProject\SmsApi\Transport\SmsRuTransport;
use ExampleProject\SmsApi\Transport\SmsTransportInterface;
use ExampleProject\SmsApi\Transport\VonageTransport;
use Psr\Log\LoggerInterface;
use RuntimeException;

class SmsApiServiceProvider extends LaravelServiceProvider implements DeferrableProvider
{
    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->app->bind(SmsClientInterface::class, function () {
            $smsTransport = $this->createSmsTransport(config('sms.driver'));

            return new SmsClient($smsTransport);
        });
    }

    private function createSmsTransport(string $driverName): SmsTransportInterface
    {
        $from = config('sms.from');

        switch ($driverName) {
            case 'array':
                return new ArrayTransport($from);
            case 'log':
                return new LogTransport(app(LoggerInterface::class), $from);
            case 'd7networks':
                return new D7NetworksTransport(
                    new GuzzleClient(),
                    config('services.d7networks.username'),
                    config('services.d7networks.password'),
                    $from,
                );
            case 'vonage':
                return new VonageTransport(
                    config('services.vonage.api_key'),
                    config('services.vonage.secret_key'),
                    $from,
                );
            case 'smscru':
                return new SmscRuTransport(
                    new GuzzleClient(),
                    config('services.smscru.login'),
                    config('services.smscru.password'),
                    config('services.smscru.set_sender'),
                    $from,
                );
            case 'smsru':
                return new SmsRuTransport(
                    new GuzzleClient(),
                    config('services.smsru.api_id'),
                    $from,
                );
            default:
                throw new RuntimeException(sprintf('Unknown SMS API transport "%s"', $driverName));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [SmsClientInterface::class];
    }
}
