<?php

declare(strict_types=1);

namespace ExampleProject\Models\Traits;

use Illuminate\Database\Eloquent\Builder as EloquentQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @method $this withTranslation(int|string $language_id = null, array $columns = null)
 * @method $this joinTranslation(int|string $language_id = null, bool $leftJoin = true)
 */
trait Translatable
{
    private $translationModelInstance;

    public function translation(): HasOne
    {
        return $this->hasOne($this->getTranslationModelClass(), $this->getTranslationForeignKeyName());
    }

    public function translations(): HasMany
    {
        return $this->hasMany($this->getTranslationModelClass(), $this->getTranslationForeignKeyName());
    }

    public function fallbackTranslation(): HasOne
    {
        return $this->hasOne($this->getTranslationModelClass(), $this->getTranslationForeignKeyName());
    }

    private function getTranslationModelClass()
    {
        return $this->getTranslationModelInfo()[0];
    }

    private function getTranslationModel(): Model
    {
        if ($this->translationModelInstance === null) {
            $translationModelClass = $this->getTranslationModelClass();

            $this->translationModelInstance = new $translationModelClass();
        }

        return $this->translationModelInstance;
    }

    private function getTranslationTable(): string
    {
        return $this->getTranslationModel()->getTable();
    }

    private function getTranslationForeignKeyName(): string
    {
        return $this->getTranslationModelInfo()[1];
    }

    private function getTranslationQualifiedForeignKeyName(): string
    {
        return $this->getTranslationTable() . '.' . $this->getTranslationModelInfo()[1];
    }

    /**
     * @param EloquentQueryBuilder $query
     * @param int|null $languageId
     * @param string[] $columns
     */
    public function scopeWithTranslation(EloquentQueryBuilder $query, ?int $languageId = null, array $columns = null): void
    {
        $languageId = $languageId ?? language_id();

        $textsTable = $this->getTranslationTable();
        $foreignKeyName = $this->getTranslationQualifiedForeignKeyName();

        if ($columns === null) {
            $selectColumnsStr = "{$textsTable}.*";
        } else {
            $qualifiedColumnNames = array_map(function ($column) use ($textsTable) {
                return "{$textsTable}.{$column}";
            }, $columns);

            // select must include the foreign key name
            if ( ! in_array($foreignKeyName, $qualifiedColumnNames, true)) {
                $qualifiedColumnNames[] = $foreignKeyName;
            }

            $selectColumnsStr = implode(', ', $qualifiedColumnNames);
        }

        $query->with([
            'translation' => function (HasOne $textsQuery) use ($selectColumnsStr, $languageId) {
                $textsQuery->selectRaw($selectColumnsStr);
                $textsQuery->where('language_id', $languageId);
            },
            'fallbackTranslation' => function (HasOne $textsQuery) {
                $textsQuery->where('language_id', app('default_language')->id);
            },
        ]);
    }

    /**
     * @return mixed Related text model.
     */
    public function getTranslation()
    {
        return $this->relations['translation'] ?? $this->relations['fallbackTranslation'] ?? null;
    }

    /**
     * @return mixed Related text model.
     */
    public function getFallbackTranslation()
    {
        return $this->relations['fallbackTranslation'] ?? null;
    }

    /**
     * Join translations (for instance, if we want to sort models by translation column).
     *
     * @param EloquentQueryBuilder $query
     * @param int $languageId
     * @param bool $leftJoin
     */
    public function scopeJoinTranslation(EloquentQueryBuilder $query, ?int $languageId = null, $leftJoin = true): void
    {
        $languageId = $languageId ?? language_id();

        $textsTable = $this->getTranslationTable();
        $foreignKey = $this->getTranslationQualifiedForeignKeyName();

        $query
            ->leftJoin($textsTable, function ($join) use ($textsTable, $foreignKey, $languageId) {
                $join
                    ->on("{$this->table}.id", '=', $foreignKey)
                    ->where("{$textsTable}.language_id", '=', $languageId)
                ;
            })
        ;
    }
}
