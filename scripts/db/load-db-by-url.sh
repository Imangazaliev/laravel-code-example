#!/bin/bash

# Usage:
# ./load-db-by-url.sh <url>

# exit immediately if a command returned non-zero code
set -e

source "$(dirname "$0")/vars.sh"

if [[ -z $1 ]]
then
    echo "No URL provided!"

    exit 1
fi

BACKUP_FILE_PATH="$PROJECT_PATH/tmp/example.dump"

echo "Downloading the database dump"

wget -O "$BACKUP_FILE_PATH" "$1"

"$(dirname $0)/restore-dump.sh" "$BACKUP_FILE_PATH"
