#!/bin/bash

# exit immediately if a command returned non-zero code
set -e

source "$(dirname "$0")/vars.sh"

if [[ -z $1 ]]
then
    echo "No path to the dump file provided!"

    exit 1
fi

# export the vars in .env
export $(egrep -v '^#' "$PROJECT_PATH/.env" | egrep 'DB_' | xargs)

connection="--user=postgres --host=127.0.0.1 --port=$DB_PORT"

PGPASSWORD="$DB_PASSWORD" psql $connection --command "DROP DATABASE IF EXISTS \"$DB_DATABASE\""
PGPASSWORD="$DB_PASSWORD" psql $connection --command "CREATE DATABASE \"$DB_DATABASE\""
PGPASSWORD="$DB_PASSWORD" pg_restore $connection --verbose --clean --if-exists --dbname="$DB_DATABASE" "$1"

echo "Database successfully updated!"