<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Testing\TestResponse;

abstract class HttpTestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function apiGetRequest(string $uri, array $data = [], array $headers = []): TestResponse
    {
        return $this->getJson('/api' . $uri . '?' . http_build_query($data), $headers);
    }

    protected function apiPostRequest(string $uri, array $data = [], array $headers = []): TestResponse
    {
        return $this->postJson('/api' . $uri, $data, $headers);
    }
}
