<?php

namespace Tests\Feature;

use Tests\HttpTestCase;

class LanguagesTest extends HttpTestCase
{
    public function testLanguages(): void
    {
        $response = $this->apiGetRequest('/languages');

        $response->assertOk();

        $response->assertJsonStructure([
            [
                'id',
                'code',
                'name',
                'native_name',
            ],
        ]);
    }
}
