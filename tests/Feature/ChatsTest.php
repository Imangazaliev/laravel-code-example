<?php

namespace Tests\Feature;

use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\ChatRequest;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\ChatRequestFactory;
use Database\Factories\UserFactory;
use Tests\HttpTestCase;

class ChatsTest extends HttpTestCase
{
    public function testChats(): void
    {
        /** @var User $user1 */
        $user1 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user1 */
        $user2 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user3 */
        $user3 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        // load all fields including default values
        $user2->refresh();
        $user3->refresh();

        $chatRequest1 = (new ChatRequestFactory())->create([
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'transaction_id' => 1,
        ]);

        $chatRequest2 = (new ChatRequestFactory())->create([
            'requestor_id' => $user1->id,
            'target_user_id' => $user3->id,
            'transaction_id' => 1,
        ]);

        $currentTime = Carbon::now();

        $chat1 = (new Chat())->create([
            'chat_request_id' => $chatRequest1->id,
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'created_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);

        $chat2 = (new Chat())->create([
            'chat_request_id' => $chatRequest2->id,
            'requestor_id' => $user1->id,
            'target_user_id' => $user3->id,
            'created_at' => $currentTime->addMinute()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);

        $this->actingAs($user1);

        $response = $this->apiGetRequest('/chats');

        $response->assertOk();

        $response->assertExactJson([
            'chats' => [
                [
                    'id' => $chat2->id,
                    'peer' => [
                        'id' => $user3->id,
                        'name' => $user3->name,
                        'sex' => $user3->sex,
                        'status' => $user3->status,
                        'birthday' => $user3->birthday->format(DATE_FORMAT),
                        'photo_url' => null,
                        'presence_status' => $user3->getPresenceStatus(),
                        'was_online_at' => $user3->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                    ],
                    'created_at' => $chat2->created_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'last_activity_at' => null,
                    'last_element' => null,
                    'new_message_count' => 0,
                ],
                [
                    'id' => $chat1->id,
                    'peer' => [
                        'id' => $user2->id,
                        'name' => $user2->name,
                        'sex' => $user2->sex,
                        'status' => $user2->status,
                        'birthday' => $user2->birthday->format(DATE_FORMAT),
                        'photo_url' => null,
                        'presence_status' => $user3->getPresenceStatus(),
                        'was_online_at' => $user2->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                    ],
                    'created_at' => $chat1->created_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'last_activity_at' => null,
                    'last_element' => null,
                    'new_message_count' => 0,
                ],
            ],
            'finished_chats' => [],
        ]);
    }

    public function testChatElements(): void
    {
        /** @var User $user1 */
        $user1 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user1 */
        $user2 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        $chatRequest = (new ChatRequestFactory())->create([
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'transaction_id' => 1,
        ]);

        $currentTime = Carbon::now();

        $chat = (new Chat())->create([
            'chat_request_id' => $chatRequest->id,
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'created_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);

        // outgoing message
        $message1 = (new ChatMessage())->create([
            'chat_id' => $chat->id,
            'user_id' => $user1->id,
            'peer_id' => $user2->id,
            'replied_message_id' => null,
            'replied_message_text' => null,
            'text' => 'Message text',
            'sent_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            'status' => ChatMessage::STATUS_PENDING_MODERATION,
            'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            'transaction_id' => 1,
        ]);

        // incoming message
        $message2 = (new ChatMessage())->create([
            'chat_id' => $chat->id,
            'user_id' => $user2->id,
            'peer_id' => $user1->id,
            'replied_message_id' => $message1->id,
            'replied_message_text' => $message1->text,
            'text' => 'Message text',
            'sent_at' => $currentTime->addMinute()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            'status' => ChatMessage::STATUS_PENDING_MODERATION,
            'status_updated_at' => $currentTime->addMinute()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            'transaction_id' => 2,
        ]);

        // load all fields including default values
        $message1->refresh();
        $message2->refresh();

        $this->actingAs($user1);

        $response = $this->apiGetRequest('/chats/chat-elements', [
            'chat_id' => $chat->id,
        ]);

        $response->assertOk();

        $response->assertJson([
            'elements' => [
                [
                    'id' => $message1->id,
                    'type' => 'message',
                    'direction' => ChatMessage::DIRECTION_OUTGOING,
                    'status' => $message1->status,
                    'sent_at' => $message1->sent_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'seen_at' => null,
                    'replied_message' => null,
                    // do not send the text of a message if it's not moderated
                    'text' => $message1->text,
                ],
                [
                    'id' => $message2->id,
                    'type' => 'message',
                    'direction' => ChatMessage::DIRECTION_INCOMING,
                    'status' => $message2->status,
                    'sent_at' => $message2->sent_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'seen_at' => null,
                    'replied_message' => [
                        'id' => $message1->id,
                        'text' => $message1->text,
                        'status' => $message1->status,
                    ],
                    // do not send the text of a message if it's not moderated
                    'text' => null,
                ],
            ],
            'cancelled_messages' => [],
            'guardian_contacts_request' => null,
            'last_activity_at' => null,
        ]);
    }

    public function testSendMessage(): void
    {
        /** @var User $user1 */
        $user1 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user1 */
        $user2 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        // load all fields including default values
        $user1->refresh();
        $user2->refresh();

        $chatRequest = (new ChatRequestFactory())->create([
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'transaction_id' => 1,
        ]);


        $chat = (new Chat())->create([
            'chat_request_id' => $chatRequest->id,
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'created_at' =>  Carbon::now(),
        ]);

        $this->actingAs($user1);

        $response = $this->apiPostRequest('/chats/send-message', [
            'chat_id' => $chat->id,
            'text' => 'Message text',
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('chat_messages', [
            'chat_id' => $chat->id,
            'user_id' => $user1->id,
            'text' => 'Message text',
            'replied_message_id' => null,
            'replied_message_text' => null,
            'status' => ChatMessage::STATUS_PENDING_MODERATION,
        ]);

        $message = (new ChatMessage())->where('chat_id', $chat->id)->first();

        $this->actingAs($user2);

        $response = $this->apiPostRequest('/chats/send-message', [
            'chat_id' => $chat->id,
            'replied_message_id' => $message->id,
            'text' => 'Message reply',
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('chat_messages', [
            'chat_id' => $chat->id,
            'user_id' => $user2->id,
            'text' => 'Message reply',
            'replied_message_id' => $message->id,
            'replied_message_text' => $message->text,
            'status' => ChatMessage::STATUS_PENDING_MODERATION,
        ]);
    }

    public function testFinishChat(): void
    {
        /** @var User $user1 */
        $user1 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user1 */
        $user2 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        // load all fields including default values
        $user1->refresh();
        $user2->refresh();

        $chatRequest = (new ChatRequestFactory())->create([
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'transaction_id' => 1,
        ]);


        $chat = (new Chat())->create([
            'chat_request_id' => $chatRequest->id,
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'created_at' =>  Carbon::now(),
        ]);

        $this->actingAs($user1);

        $response = $this->apiPostRequest('/chats/finish', [
            'chat_id' => $chat->id,
        ]);

        $response->assertOk();

        $chat->refresh();

        $this->assertNotNull($chat->finished_at);
        $this->assertEquals($user1->id, $chat->finished_by);
    }
}
