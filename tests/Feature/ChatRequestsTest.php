<?php

namespace Tests\Feature;

use App\Models\ChatRequest;
use App\Models\CoinTransaction;
use App\Models\Locality;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\ChatRequestFactory;
use Database\Factories\UserFactory;
use Tests\HttpTestCase;

class ChatRequestsTest extends HttpTestCase
{
    public function testIncomingChatRequests(): void
    {
        $locality = (new Locality())
            ->where('country_code', 'RU')
            ->where('english_name', 'Moscow')
            ->first([
                'id',
                'country_id',
                'subdivision_level_1_id',
                'subdivision_level_2_id',
                'subdivision_level_3_id',
            ])
        ;

        $userData = [
            'status' => User::STATUS_ACCEPTED,
            'country_id' => $locality->country_id,
            'subdivision_level_1_id' => $locality->subdivision_level_1_id,
            'subdivision_level_2_id' => $locality->subdivision_level_2_id,
            'subdivision_level_3_id' => $locality->subdivision_level_3_id,
            'locality_id' => $locality->id,
        ];

        /** @var User $user1 */
        $user1 = (new UserFactory())->create($userData);

        /** @var User $user2 */
        $user2 = (new UserFactory())->create($userData);

        /** @var User $user3 */
        $user3 = (new UserFactory())->create($userData);

        /** @var User $user4 */
        $user4 = (new UserFactory())->create($userData);

        // load all fields including default values
        $user2->refresh();
        $user3->refresh();

        (new ChatRequest())
            ->where([
                'requestor_id' => $user1->id,
                'target_user_id' => $user2->id,
            ])
            ->delete()
        ;

        $currentTime = Carbon::now();

        $chatRequest1 = (new ChatRequestFactory())->create([
            'requestor_id' => $user2->id,
            'target_user_id' => $user1->id,
            'sent_at' =>  $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'transaction_id' => 1,
        ]);

        $chatRequest2 = (new ChatRequestFactory())->create([
            'requestor_id' => $user3->id,
            'target_user_id' => $user1->id,
            'sent_at' =>  $currentTime->addMinute()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'transaction_id' => 1,
        ]);

        // outgoing requests
        (new ChatRequestFactory())->create([
            'requestor_id' => $user1->id,
            'target_user_id' => $user4->id,
            'transaction_id' => 1,
        ]);

        $this->actingAs($user1);

        $response = $this->apiGetRequest('/chat-requests/incoming');

        $response->assertOk();

        // we create two chat requests to check that the order is valid
        $response->assertExactJson([
            'current_page' => 1,
            'page_count' => 1,
            'requests' => [
                [
                    'id' => $chatRequest2->id,
                    'requestor' => [
                        'id' => $user3->id,
                        'name' => $user3->name,
                        'sex' => $user3->sex,
                        'birthday' => $user3->birthday->format(DATE_FORMAT),
                        'photo_url' => null,
                        'country_name' => 'Russia',
                        'locality_name' => 'Moscow',
                        'presence_status' => $user3->getPresenceStatus(),
                        'was_online_at' => $user3->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'is_verified' => false,
                    ],
                    'status' => ChatRequest::STATUS_NEW,
                    'greeting_message' => null,
                    'greeting_message_moderation_status' => null,
                    'is_new' => true,
                    'rejection_comment' => null,
                    'rejection_comment_moderation_status' => null,
                ],
                [
                    'id' => $chatRequest1->id,
                    'requestor' => [
                        'id' => $user2->id,
                        'name' => $user2->name,
                        'sex' => $user2->sex,
                        'birthday' => $user2->birthday->format(DATE_FORMAT),
                        'photo_url' => null,
                        'country_name' => 'Russia',
                        'locality_name' => 'Moscow',
                        'presence_status' => $user3->getPresenceStatus(),
                        'was_online_at' => $user2->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'is_verified' => false,
                    ],
                    'status' => ChatRequest::STATUS_NEW,
                    'greeting_message' => null,
                    'greeting_message_moderation_status' => null,
                    'is_new' => true,
                    'rejection_comment' => null,
                    'rejection_comment_moderation_status' => null,
                ],
            ],
        ]);
    }

    public function testOutgoingChatRequests(): void
    {
        $locality = (new Locality())
            ->where('country_code', 'RU')
            ->where('english_name', 'Moscow')
            ->first([
                'id',
                'country_id',
                'subdivision_level_1_id',
                'subdivision_level_2_id',
                'subdivision_level_3_id',
            ])
        ;

        $userData = [
            'status' => User::STATUS_ACCEPTED,
            'country_id' => $locality->country_id,
            'subdivision_level_1_id' => $locality->subdivision_level_1_id,
            'subdivision_level_2_id' => $locality->subdivision_level_2_id,
            'subdivision_level_3_id' => $locality->subdivision_level_3_id,
            'locality_id' => $locality->id,
        ];

        /** @var User $user1 */
        $user1 = (new UserFactory())->create($userData);

        /** @var User $user1 */
        $user2 = (new UserFactory())->create($userData);

        /** @var User $user3 */
        $user3 = (new UserFactory())->create($userData);

        /** @var User $user4 */
        $user4 = (new UserFactory())->create($userData);

        // load all fields including default values
        $user2->refresh();
        $user3->refresh();

        (new ChatRequest())
            ->where([
                'requestor_id' => $user1->id,
                'target_user_id' => $user2->id,
            ])
            ->delete()
        ;

        $currentTime = Carbon::now();

        $chatRequest1 = (new ChatRequestFactory())->create([
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'sent_at' =>  $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'transaction_id' => 1,
        ]);

        $chatRequest2 = (new ChatRequestFactory())->create([
            'requestor_id' => $user1->id,
            'target_user_id' => $user3->id,
            'sent_at' =>  $currentTime->addMinute()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'transaction_id' => 1,
        ]);

        // incoming requests
        (new ChatRequestFactory())->create([
            'requestor_id' => $user4->id,
            'target_user_id' => $user1->id,
            'transaction_id' => 1,
        ]);

        $this->actingAs($user1);

        $response = $this->apiGetRequest('/chat-requests/outgoing');

        $response->assertOk();

        // we create two chat requests to check that the order is valid
        $response->assertExactJson([
            'current_page' => 1,
            'page_count' => 1,
            'requests' => [
                [
                    'id' => $chatRequest2->id,
                    'target_user' => [
                        'id' => $user3->id,
                        'name' => $user3->name,
                        'sex' => $user3->sex,
                        'birthday' => $user3->birthday->format(DATE_FORMAT),
                        'photo_url' => null,
                        'country_name' => 'Russia',
                        'locality_name' => 'Moscow',
                        'presence_status' => $user3->getPresenceStatus(),
                        'was_online_at' => $user3->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'is_verified' => false,
                    ],
                    'status' => ChatRequest::STATUS_NEW,
                    'greeting_message' => null,
                    'greeting_message_moderation_status' => null,
                    'rejection_comment' => null,
                    'rejection_comment_moderation_status' => null,
                ],
                [
                    'id' => $chatRequest1->id,
                    'target_user' => [
                        'id' => $user2->id,
                        'name' => $user2->name,
                        'sex' => $user2->sex,
                        'birthday' => $user2->birthday->format(DATE_FORMAT),
                        'photo_url' => null,
                        'country_name' => 'Russia',
                        'locality_name' => 'Moscow',
                        'presence_status' => $user3->getPresenceStatus(),
                        'was_online_at' => $user2->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'is_verified' => false,
                    ],
                    'status' => ChatRequest::STATUS_NEW,
                    'greeting_message' => null,
                    'greeting_message_moderation_status' => null,
                    'rejection_comment' => null,
                    'rejection_comment_moderation_status' => null,
                ],
            ],
        ]);
    }

    public function testSendRequest(): void
    {
        /** @var User $user1 */
        $user1 = (new UserFactory())->create([
            'sex' => 'm',
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user2 */
        $user2 = (new UserFactory())->create([
            'sex' => 'f',
            'status' => User::STATUS_ACCEPTED,
        ]);

        $expectedParameters = [
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'greeting_message' => null,
            'greeting_message_moderation_status' => null,
            'status' => ChatRequest::STATUS_NEW,
            'transaction_id' => (new CoinTransaction())->max('id') + 1,
        ];

        // delete outgoing request
        (new ChatRequest())->where([
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
        ])->delete();

        // delete incoming request
        (new ChatRequest())->where([
            'requestor_id' => $user2->id,
            'target_user_id' => $user1->id,
        ])->delete();

        $this->assertDatabaseMissing('chat_requests', $expectedParameters);

        $this->actingAs($user1);

        $response = $this->apiPostRequest('/chat-requests/send', [
            'user_id' => $user2->id,
        ]);

        $response->assertOk();

        $this->assertDatabaseHas('chat_requests', $expectedParameters);
    }

    public function testCancelRequest(): void
    {
        /** @var User $user1 */
        $user1 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user2 */
        $user2 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        // delete outgoing request
        (new ChatRequest())->where([
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
        ])->delete();

        $chatRequest = (new ChatRequestFactory())->create([
            'requestor_id' => $user1->id,
            'target_user_id' => $user2->id,
            'transaction_id' => 1,
        ]);

        $this->actingAs($user1);

        $response = $this->apiPostRequest('/chat-requests/cancel', [
            'user_id' => $user2->id,
        ]);

        $response->assertOk();

        $chatRequest->refresh();

        $this->assertEquals(ChatRequest::STATUS_CANCELED, $chatRequest->status);
        $this->assertNotNull($chatRequest->canceled_at);
        $this->assertNotNull($chatRequest->return_transaction_id);
    }

    public function testAcceptRequest(): void
    {
        /** @var User $user1 */
        $user1 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user2 */
        $user2 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        // delete incoming request
        (new ChatRequest())->where([
            'requestor_id' => $user2->id,
            'target_user_id' => $user1->id,
        ])->delete();

        $chatRequest = (new ChatRequestFactory())->create([
            'requestor_id' => $user2->id,
            'target_user_id' => $user1->id,
            'status' => ChatRequest::STATUS_NEW,
            'transaction_id' => 1,
        ]);

        $this->actingAs($user1);

        $response = $this->apiPostRequest('/chat-requests/accept', [
            'user_id' => $user2->id,
        ]);

        $response->assertOk();

        $chatRequest->refresh();

        $this->assertEquals(ChatRequest::STATUS_ACCEPTED, $chatRequest->status);
        $this->assertNotNull($chatRequest->answered_at);

        $this->assertDatabaseHas('chats', [
            'requestor_id' => $user2->id,
            'target_user_id' => $user1->id,
            'last_activity_at' => null,
            'chat_request_id' => $chatRequest->id,
        ]);
    }

    public function testRejectRequest(): void
    {
        /** @var User $user1 */
        $user1 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user2 */
        $user2 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        // delete incoming request
        (new ChatRequest())->where([
            'requestor_id' => $user2->id,
            'target_user_id' => $user1->id,
        ])->delete();

        $chatRequest = (new ChatRequestFactory())->create([
            'requestor_id' => $user2->id,
            'target_user_id' => $user1->id,
            'transaction_id' => 1,
        ]);

        $this->actingAs($user1);

        $response = $this->apiPostRequest('/chat-requests/reject', [
            'user_id' => $user2->id,
        ]);

        $response->assertOk();

        $chatRequest->refresh();

        $this->assertEquals(ChatRequest::STATUS_REJECTED, $chatRequest->status);
        $this->assertNotNull($chatRequest->answered_at);
    }
}
