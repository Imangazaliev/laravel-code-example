<?php

namespace Tests\Feature\Auth;

use App\Models\PhoneNumberVerificationCode;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\UserFactory;
use Tests\HttpTestCase;

class ConfirmPhoneNumberHttpTest extends HttpTestCase
{
    public function testVerifyPhoneNumber(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'status' => User::STATUS_NOT_MODERATED,
        ]);

        PhoneNumberVerificationCode::create([
            'phone_number' => $user->phone_number,
            'code' => '123456',
            'created_at' => Carbon::now(),
        ]);

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/auth/confirm-phone-number', [
                'code' => '123456',
            ])
        ;

        $response->assertOk();

        $user->refresh();

        $this->assertNotNull($user->phone_number_verified_at);
    }
}
