<?php

namespace Tests\Feature\Auth;

use App\Models\Language;
use App\Models\PhoneNumberVerificationCode;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\UserFactory;
use Tests\HttpTestCase;

class RegistrationTest extends HttpTestCase
{
    public function testRegister(): void
    {
        $user = (new UserFactory())->make();
        $userData = $user->toArray();

        $data = array_only($userData, [
            'name',
            'phone_number',
            'phone_number_country_code',
            'password',
            'sex',
            'birthday',
        ]);

        $data['password'] = 'password';
        $data['fingerprint'] = md5('foobar');

        (new PhoneNumberVerificationCode())->delete();

        $this->assertDatabaseMissing('phone_number_verification_codes', [
            'phone_number' => $userData['phone_number'],
        ]);

        $response = $this->apiPostRequest('/auth/register', $data);

        $response->assertOk();

        $this->assertDatabaseHas('users', [
            'name' => $data['name'],
            'phone_number' => $data['phone_number'],
            'sex' => $data['sex'],
            'birthday' => $data['birthday'],
        ]);

        // TODO: asserts authentication logged
        $this->assertDatabaseHas('phone_number_verification_codes', [
            'phone_number' => $userData['phone_number'],
        ]);

        $user = (new User())->where('phone_number', $userData['phone_number'])->first();

        $this->assertAuthenticatedAs($user);
    }

    public function testStep1(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'phone_number_verified_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'status' => User::STATUS_NOT_MODERATED,
        ])->refresh();

        $this->assertIsNumeric(1, $user->language_id);
        $this->assertNull($user->locality_id);
        $this->assertNull($user->nationality_id);

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/profile/edit', [
                'locality_id' => 3,
                'ethnicity_id' => 3,
            ])
        ;

        $response->assertOk();

        $user->refresh();

        $this->assertEquals(3, $user->locality_id);
        $this->assertEquals(3, $user->ethnicity_id);
    }

    public function testStep2(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'phone_number_verified_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'status' => User::STATUS_NOT_MODERATED,
        ])->refresh();

        $this->assertNull($user->height);
        $this->assertNull($user->weight_from);
        $this->assertNull($user->weight_to);
        $this->assertNull($user->eye_color);
        $this->assertNull($user->hair_color);
        $this->assertNull($user->body_type);
        $this->assertNull($user->facial_hair);

        $eyeColor = array_random(User::getEyeColorOptions());
        $hairColor = array_random(User::getHairColorOptions());
        $bodyType = array_random(User::getBodyTypeOptions());
        $facialHair= array_random(User::getFacialHairOptions());

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/profile/edit', [
                'height' => 170,
                'weight_from' => 65,
                'weight_to' => 65,
                'eye_color' => $eyeColor,
                'hair_color' => $hairColor,
                'body_type' => $bodyType,
                'facial_hair' => $facialHair,
            ])
        ;

        $response->assertOk();

        $user->refresh();

        $this->assertEquals(170, $user->height);
        $this->assertEquals(65, $user->weight_from);
        $this->assertEquals(65, $user->weight_to);
        $this->assertEquals($eyeColor, $user->eye_color);
        $this->assertEquals($hairColor, $user->hair_color);
        $this->assertEquals($bodyType, $user->body_type);
        $this->assertEquals($facialHair, $user->facial_hair);
    }

    public function testStep3(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'phone_number_verified_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'status' => User::STATUS_NOT_MODERATED,
        ])->refresh();

        $this->assertNull($user->smoking);
        $this->assertNull($user->alcohol_consumption);
        $this->assertNull($user->nutrition);
        $this->assertNull($user->self_description);
        $this->assertNull($user->hobby);
        $this->assertNull($user->sport);
        $this->assertNull($user->sport_exercises_frequency);

        $sportExercisesFrequency = array_random(User::getSportExercisesFrequencyOptions());
        $nutrition = array_random(User::getNutritionOptions());
        $smoking = array_random(User::getSmokingOptions());
        $alcoholConsumption = array_random(User::getAlcoholConsumptionOptions());

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/profile/edit', [
                'smoking' => $smoking,
                'alcohol_consumption' => $alcoholConsumption,
                'nutrition' => $nutrition,
                'self_description' => 'Funny, kind',
                'hobbies_and_interests' => 'DIY, snowboarding',
                'sport' => 'Snowboarding',
                'sport_exercises_frequency' => $sportExercisesFrequency,
            ])
        ;

        $response->assertOk();

        $user->refresh();

        $this->assertEquals($smoking, $user->smoking);
        $this->assertEquals($alcoholConsumption, $user->alcohol_consumption);
        $this->assertEquals($nutrition, $user->nutrition);
        $this->assertEquals('Funny, kind', $user->self_description);
        $this->assertEquals('DIY, snowboarding', $user->hobbies_and_interests);
        $this->assertEquals('Snowboarding', $user->sport);
        $this->assertEquals($sportExercisesFrequency, $user->sport_exercises_frequency);
    }

    public function testStep4(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'phone_number_verified_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'status' => User::STATUS_NOT_MODERATED,
        ])->refresh();

        $this->assertNull($user->job_title);
        $this->assertNull($user->employment);
        $this->assertNull($user->financial_status);
        $this->assertNull($user->education);

        $employment = array_random(User::getEmploymentOptions());
        $financialStatus = array_random(User::getFinancialStatusOptions());
        $education = array_random(User::getEducationOptions());

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/profile/edit', [
                'job_title' => 'Engineer',
                'employment' => $employment,
                'financial_status' => $financialStatus,
                'education' => $education,
            ])
        ;

        $response->assertOk();

        $user->refresh();

        $this->assertEquals('Engineer', $user->job_title);
        $this->assertEquals($employment, $user->employment);
        $this->assertEquals($financialStatus, $user->financial_status);
        $this->assertEquals($education, $user->education);
    }

    public function testStep5(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'phone_number_verified_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'status' => User::STATUS_NOT_MODERATED,
        ])->refresh();

        $this->assertNull($user->marital_status);
        $this->assertNull($user->child_count);
        $this->assertNull($user->desired_number_of_children);

        $maritalStatus = array_random(User::getMaritalStatusOptions($user->sex));
        $desiredNumberOfChildren = array_random(User::getDesiredNumberOfChildrenOptions());
        $livingWithParents = array_random(User::getLivingWithParentsOptions());

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/profile/edit', [
                'marital_status' => $maritalStatus,
                'child_count' => 0,
                'desired_number_of_children' => $desiredNumberOfChildren,
                'living_with_parents' => $livingWithParents,
            ])
        ;

        $response->assertOk();

        $user->refresh();

        $this->assertEquals($maritalStatus, $user->marital_status);
        $this->assertEquals(0, $user->child_count);
        $this->assertEquals($desiredNumberOfChildren, $user->desired_number_of_children);
        $this->assertEquals($livingWithParents, $user->living_with_parents);
    }

    public function testStep6(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'phone_number_verified_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'status' => User::STATUS_NOT_MODERATED,
        ])->refresh();

        $this->assertNull($user->future_spouse_age_from);
        $this->assertNull($user->future_spouse_age_to);
        $this->assertNull($user->future_spouse_height_from);
        $this->assertNull($user->future_spouse_height_to);
        $this->assertNull($user->future_spouse_weight_from);
        $this->assertNull($user->future_spouse_weight_to);
        $this->assertNull($user->future_spouse_body_type);
        $this->assertNull($user->future_spouse_aqidah);
        $this->assertNull($user->future_spouse_character);
        $this->assertNull($user->future_spouse_children);
        $this->assertNull($user->future_spouse_description);

        $desiredBodyType = array_random(User::getBodyTypeOptions());
        $desiredSpouseAqidah = array_random(User::getAqidahOptions());
        $desiredSpouseChildren = array_random(User::getFutureSpouseChildrenOptions());

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/profile/edit', [
                'future_spouse_age_from' => 24,
                'future_spouse_age_to' => 35,
                'future_spouse_height_from' => 165,
                'future_spouse_height_to' => 175,
                'future_spouse_weight_from' => 50,
                'future_spouse_weight_to' => 70,
                'future_spouse_body_type' => $desiredBodyType,
                'future_spouse_aqidah' => $desiredSpouseAqidah,
                'future_spouse_character_traits' => 'Kind, modest and smart',
                'future_spouse_children' => $desiredSpouseChildren,
                'future_spouse_description' => 'I am looking for a woman who knows how to cook well',
            ])
        ;

        $response->assertOk();

        $user->refresh();

        $this->assertEquals(24, $user->future_spouse_age_from);
        $this->assertEquals(35, $user->future_spouse_age_to);
        $this->assertEquals(165, $user->future_spouse_height_from);
        $this->assertEquals(175, $user->future_spouse_height_to);
        $this->assertEquals(50, $user->future_spouse_weight_from);
        $this->assertEquals(70, $user->future_spouse_weight_to);
        $this->assertEquals($desiredBodyType, $user->future_spouse_body_type);
        $this->assertEquals($desiredSpouseAqidah, $user->future_spouse_aqidah);
        $this->assertEquals('Kind, modest and smart', $user->future_spouse_character_traits);
        $this->assertEquals('I am looking for a woman who knows how to cook well', $user->future_spouse_description);
        $this->assertEquals($desiredSpouseChildren, $user->future_spouse_children);
    }

    public function testStep7(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'phone_number_verified_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'status' => User::STATUS_NOT_MODERATED,
        ])->refresh();

        $this->assertNull($user->aqidah);
        $this->assertNull($user->religious_practice);
        $this->assertNull($user->religious_practice_duration);
        $this->assertNull($user->madhhab);
        $this->assertNull($user->salah);
        $this->assertNull($user->sawm);
        $this->assertNull($user->hajj_umrah);
        $this->assertNull($user->religious_education);
        $this->assertNull($user->religious_knowledge_source);
        $this->assertNull($user->memorized_surah_count);

        $aqidah = array_random(User::getAqidahOptions());
        $religiousPractice = array_random(User::getReligiousPracticeOptions());
        $religiousPracticeDuration = array_random(User::getReligiousPracticeDurationOptions());
        $madhhab = array_random(User::getMadhhabOptions());
        $salah = array_random(User::getSalahOptions());
        $sawm = array_random(User::getSawmOptions());
        $hajjUmrah = array_random(User::getHajjUmrahOptions());
        $hijabWearing = array_random(User::getHijabWearingOptions());
        $religiousEducation = array_random(User::getReligiousEducationOptions());
        $memorizedQuranSurahCount = array_random(User::getMemorizedSurahCountOptions());

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/profile/edit', [
                'aqidah' => $aqidah,
                'religious_practice' => $religiousPractice,
                'religious_practice_duration' => $religiousPracticeDuration,
                'madhhab' => $madhhab,
                'salah' => $salah,
                'sawm' => $sawm,
                'hajj_umrah' => $hajjUmrah,
                'hijab_wearing' => $hijabWearing,
                'religious_education' => $religiousEducation,
                'religious_knowledge_source' => 'Internet',
                'memorized_surah_count' => $memorizedQuranSurahCount,
            ])
        ;

        $response->assertOk();

        $user->refresh();

        $this->assertEquals($aqidah, $user->aqidah);
        $this->assertEquals($religiousPractice, $user->religious_practice);
        $this->assertEquals($religiousPracticeDuration, $user->religious_practice_duration);
        $this->assertEquals($madhhab, $user->madhhab);
        $this->assertEquals($salah, $user->salah);
        $this->assertEquals($sawm, $user->sawm);
        $this->assertEquals($hajjUmrah, $user->hajj_umrah);
        $this->assertEquals($religiousEducation, $user->religious_education);
        $this->assertEquals('Internet', $user->religious_knowledge_source);
        $this->assertEquals($memorizedQuranSurahCount, $user->memorized_surah_count);
    }

    public function testStep8(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'phone_number_verified_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'status' => User::STATUS_NOT_MODERATED,
        ])->refresh();

        $housingType = array_random(User::getHousingTypeOptions());
        $relocation = array_random(User::getRelocationOptions());
        $childrenAdoption = array_random(User::getChildrenAdoptionOptions());

        $this->assertNull($user->housing_type);
        $this->assertNull($user->relocation);
        $this->assertNull($user->children_adoption);

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/profile/edit', [
                'housing_type' => $housingType,
                'relocation' => $relocation,
                'children_adoption' => $childrenAdoption,
            ])
        ;

        $response->assertOk();

        $user->refresh();

        $this->assertEquals($housingType, $user->housing_type);
        $this->assertEquals($relocation, $user->relocation);
        $this->assertEquals($childrenAdoption, $user->children_adoption);
    }

    public function testRegistrationCompleted(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'phone_number_verified_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'status' => User::STATUS_NOT_MODERATED,
        ])->refresh();

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/auth/registration-completed')
        ;

        $response->assertOk();

        $user->refresh();

        $this->assertNotNull($user->completed_registration_at);
    }
}
