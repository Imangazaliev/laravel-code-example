<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use App\Services\Frontend\JavaScriptVariables;
use Carbon\Carbon;
use Database\Factories\UserFactory;
use Faker\Generator as Faker;
use Tests\HttpTestCase;

class LoginTest extends HttpTestCase
{
    public function testLoginByPhoneNumber(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        // load all fields including default values
        $user->refresh();

        $this->assertGuest();

        $response = $this->apiPostRequest('/auth/login', [
            'phone_number_or_email' => $user->phone_number,
            'password' => 'password',
            'fingerprint' => md5('foobar'),
        ]);

        $response->assertOk();

        $this->assertAuthenticatedAs($user);
    }

    public function testLoginByPhoneNumberWithWrongPassword(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        $this->assertGuest();

        $response = $this->apiPostRequest('/auth/login', [
            'phone_number_or_email' => $user->phone_number,
            'password' => 'wrong_password',
            'fingerprint' => md5('foobar'),
        ]);

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'code',
            'message',
            'validation_errors' => [
                'phone_number_or_email',
            ],
        ]);

        $this->assertGuest();
    }

    public function testLoginByNotExistingPhoneNumber(): void
    {
        $this->assertGuest();

        $response = $this->apiPostRequest('/auth/login', [
            'phone_number_or_email' => '+79999999999',
            'password' => 'wrong_password',
            'fingerprint' => md5('foobar'),
        ]);

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'code',
            'message',
            'validation_errors' => [
                'phone_number_or_email',
            ],
        ]);

        $this->assertGuest();
    }

    public function testLoginByEmail(): void
    {
        $faker = app(Faker::class);

        /** @var User $user */
        $user = (new UserFactory())->create([
            'email' => $faker->email,
            'status' => User::STATUS_ACCEPTED,
        ]);

        $this->assertGuest();

        $response = $this->apiPostRequest('/auth/login', [
            'phone_number_or_email' => $user->phone_number,
            'password' => 'password',
            'fingerprint' => md5('foobar'),
        ]);

        $response->assertOk();

        $this->assertAuthenticatedAs($user);
    }

    public function testLoginByEmailWithWrongPassword(): void
    {
        $faker = app(Faker::class);

        /** @var User $user */
        $user = (new UserFactory())->create([
            'email' => $faker->email,
            'status' => User::STATUS_ACCEPTED,
        ]);

        $this->assertGuest();

        $response = $this->apiPostRequest('/auth/login', [
            'phone_number_or_email' => $user->phone_number,
            'password' => 'wrong_password',
            'fingerprint' => md5('foobar'),
        ]);

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'code',
            'message',
            'validation_errors' => [
                'phone_number_or_email',
            ],
        ]);

        $this->assertGuest();
    }

    public function testLoginByEmailWithNotInvalidEmail(): void
    {
        $this->assertGuest();

        $response = $this->apiPostRequest('/auth/login', [
            'phone_number_or_email' => 'invalid-email',
            'password' => 'password',
            'fingerprint' => md5('foobar'),
        ]);

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'code',
            'message',
            'validation_errors' => [
                'phone_number_or_email',
            ],
        ]);

        $this->assertGuest();
    }

    public function testLoginByEmailWithNotExistingEmail(): void
    {
        $this->assertGuest();

        $response = $this->apiPostRequest('/auth/login', [
            'phone_number_or_email' => 'not-existing-email@gmail.com',
            'password' => 'wrong_password',
            'fingerprint' => md5('foobar'),
        ]);

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'code',
            'message',
            'validation_errors' => [
                'phone_number_or_email',
            ],
        ]);

        $this->assertGuest();
    }

    public function testLogout(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'phone_number_verified_at' => Carbon::now(),
            'status' => User::STATUS_ACCEPTED,
        ]);

        $this->actingAs($user);

        $this->assertAuthenticated();

        $response = $this
            ->actingAs($user)
            ->apiPostRequest('/auth/logout')
        ;

        $response->assertOk();

        $this->assertGuest();
    }
}
