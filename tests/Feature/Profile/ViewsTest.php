<?php

namespace Tests\Feature\Profile;

use App\Models\Locality;
use App\Models\ProfileView;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\UserFactory;
use Tests\HttpTestCase;

class ViewsTest extends HttpTestCase
{
    public function testViews(): void
    {
        $locality = (new Locality())
            ->where('country_code', 'RU')
            ->where('english_name', 'Moscow')
            ->first([
                'id',
                'country_id',
                'subdivision_level_1_id',
                'subdivision_level_2_id',
                'subdivision_level_3_id',
            ])
        ;

        $userData = [
            'status' => User::STATUS_ACCEPTED,
            'country_id' => $locality->country_id,
            'subdivision_level_1_id' => $locality->subdivision_level_1_id,
            'subdivision_level_2_id' => $locality->subdivision_level_2_id,
            'subdivision_level_3_id' => $locality->subdivision_level_3_id,
            'locality_id' => $locality->id,
        ];

        /** @var User $user1 */
        $user1 = (new UserFactory())->create($userData);

        /** @var User $user2 */
        $user2 = (new UserFactory())->create($userData);

        /** @var User $user3 */
        $user3 = (new UserFactory())->create($userData);

        /** @var User $user4 */
        $user4 = (new UserFactory())->create($userData);

        // load all fields including default values
        $user2->refresh();
        $user3->refresh();

        (new ProfileView())
            ->where([
                'visitor_id' => $user1->id,
                'viewed_profile_id' => $user2->id,
            ])
            ->delete()
        ;

        (new ProfileView())
            ->where([
                'visitor_id' => $user2->id,
                'viewed_profile_id' => $user1->id,
            ])
            ->delete()
        ;

        $currentTime = Carbon::now();

        $view1 = (new ProfileView())->create([
            'visitor_id' => $user2->id,
            'viewed_profile_id' => $user1->id,
            'viewed_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'is_incognito' => false,
        ]);

        $view2 = (new ProfileView())->create([
            'visitor_id' => $user3->id,
            'viewed_profile_id' => $user1->id,
            'viewed_at' => $currentTime->addMinute()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'is_incognito' => false,
        ]);

        (new ProfileView())->create([
            'visitor_id' => $user1->id,
            'viewed_profile_id' => $user4->id,
            'viewed_at' => $currentTime->addMinutes(2)->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'is_incognito' => false,
        ]);

        $view1->refresh();
        $view2->refresh();

        $this->actingAs($user1);

        $response = $this->apiGetRequest('/guests');

        $response->assertOk();

        // we create 3 likes to check that the order is valid
        // and outgoing likes are not returned
        $response->assertExactJson([
            'current_page' => 1,
            'page_count' => 1,
            'views' => [
                [
                    'id' => $view2->id,
                    'visitor' => [
                        'id' => $user3->id,
                        'name' => $user3->name,
                        'sex' => $user3->sex,
                        'birthday' => $user3->birthday->format(DATE_FORMAT),
                        'photo_url' => null,
                        'country_name' => 'Russia',
                        'locality_name' => 'Moscow',
                        'presence_status' => $user3->getPresenceStatus(),
                        'was_online_at' => $user3->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'is_verified' => false,
                    ],
                    'viewed_at' => $view2->viewed_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                    'is_new' => true,
                ],
                [
                    'id' => $view1->id,
                    'visitor' => [
                        'id' => $user2->id,
                        'name' => $user2->name,
                        'sex' => $user2->sex,
                        'birthday' => $user2->birthday->format(DATE_FORMAT),
                        'photo_url' => null,
                        'country_name' => 'Russia',
                        'locality_name' => 'Moscow',
                        'presence_status' => $user2->getPresenceStatus(),
                        'was_online_at' => $user2->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'is_verified' => false,
                    ],
                    'viewed_at' => $view1->viewed_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                    'is_new' => true,
                ],
            ],
        ]);
    }
}
