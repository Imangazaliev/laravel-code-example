<?php

namespace Tests\Feature\Profile;

use App\Models\Language;
use App\Models\User;
use Database\Factories\UserFactory;
use Tests\HttpTestCase;

class ProfileTest extends HttpTestCase
{
    public function testChangeLanguage(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        $user->refresh();

        $this->actingAs($user);

        $newLanguageId = (new Language())
            ->where('is_active', true)
            ->where('id', '!=', $user->language_id)
            ->value('id')
        ;

        $response = $this->apiPostRequest('/change-language', [
            'language_id' => $newLanguageId,
        ]);

        $response->assertOk();

        $user->refresh();

        $this->assertEquals($newLanguageId, $user->language_id);
    }
}
