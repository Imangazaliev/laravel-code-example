<?php

namespace Tests\Feature\Profile\Photo;

use App\Models\User;
use App\Models\UserPhoto;
use Database\Factories\UserFactory;
use Database\Factories\UserPhotoFactory;
use Tests\HttpTestCase;

class PhotosTest extends HttpTestCase
{
    public function testPhotos(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        $photo1 = (new UserPhotoFactory())->create([
            'user_id' => $user->id,
            'status' => UserPhoto::STATUS_ACCEPTED,
        ]);

        $photo2 = (new UserPhotoFactory())->create([
            'user_id' => $user->id,
            'status' => UserPhoto::STATUS_PENDING_MODERATION,
        ]);

        $photo1->refresh();
        $photo2->refresh();

        $user->update([
            'main_photo_id' => $photo1->id,
        ]);

        $this->actingAs($user);

        $response = $this->apiGetRequest('/photos');

        $response->assertOk();

        $response->assertExactJson([
            [
                'id' => $photo1->id,
                'url' => $photo1->getImageUrl(),
                'caption' => $photo1->caption,
                'uploaded_at' => $photo1->uploaded_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'status' => $photo1->status,
                'is_being_processed' => false,
                'is_main' => true,
            ],
            [
                'id' => $photo2->id,
                'url' => $photo2->getImageUrl(),
                'caption' => $photo2->caption,
                'uploaded_at' => $photo2->uploaded_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'status' => $photo2->status,
                'is_being_processed' => false,
                'is_main' => false,
            ],
        ]);
    }

    public function testSetMainPhoto(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        $photo1 = (new UserPhotoFactory())->create([
            'user_id' => $user->id,
            'status' => UserPhoto::STATUS_ACCEPTED,
        ]);

        $photo2 = (new UserPhotoFactory())->create([
            'user_id' => $user->id,
            'status' => UserPhoto::STATUS_ACCEPTED,
        ]);

        $user->update([
            'main_photo_id' => $photo1->id,
        ]);

        $this->actingAs($user);

        $response = $this->apiPostRequest('/photos/set-main', [
            'photo_id' => $photo2->id,
        ]);

        $response->assertOk();

        $user->refresh();

        $this->assertEquals($photo2->id, $user->main_photo_id);
    }
}
