<?php

namespace Tests\Feature\Profile\Photo;

use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Http\UploadedFile;
use Tests\HttpTestCase;

class UploadPhotoTest extends HttpTestCase
{
    public function testUploadPhoto(): void
    {
        /** @var User $user1 */
        $user1 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        $this->actingAs($user1);

        $filePath = base_path('tests/stubs/man-photo.jpg');

        $photoFile = new UploadedFile($filePath, 'IMG-2020-01-09.jpg', 'image/jpeg', UPLOAD_ERR_OK, true);

        $response = $this->apiPostRequest('/photos/upload', [
            'photo' => $photoFile,
            'caption' => '',
        ]);

        $response->assertOk();
    }
}
