<?php

namespace Tests\Feature\Profile;

use App\Models\ProfileFavorite;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\UserFactory;
use Tests\HttpTestCase;

class LikesTest extends HttpTestCase
{
    public function testLikes(): void
    {
        /** @var User $user1 */
        $user1 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user2 */
        $user2 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user3 */
        $user3 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        /** @var User $user4 */
        $user4 = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        // load all fields including default values
        $user2->refresh();
        $user3->refresh();

        (new ProfileFavorite())
            ->where([
                'user_id' => $user1->id,
                'target_user_id' => $user2->id,
            ])
            ->delete()
        ;

        (new ProfileFavorite())
            ->where([
                'user_id' => $user2->id,
                'target_user_id' => $user1->id,
            ])
            ->delete()
        ;

        $like1 = (new ProfileFavorite())->create([
            'user_id' => $user2->id,
            'target_user_id' => $user1->id,
            'liked_at' => Carbon::now(),
        ]);

        $like2 = (new ProfileFavorite())->create([
            'user_id' => $user3->id,
            'target_user_id' => $user1->id,
            'liked_at' => Carbon::now(),
        ]);

        (new ProfileFavorite())->create([
            'user_id' => $user1->id,
            'target_user_id' => $user4->id,
            'liked_at' => Carbon::now(),
        ]);

        $like1->refresh();
        $like2->refresh();

        $this->actingAs($user1);

        $response = $this->apiGetRequest('/profile/likes');

        $response->assertOk();

        // we create 3 likes to check that the order is valid
        // and outgoing likes are not returned
        $response->assertExactJson([
            'current_page' => 1,
            'page_count' => 1,
            'likes' => [
                [
                    'id' => $like2->id,
                    'user' => [
                        'id' => $user3->id,
                        'name' => $user3->name,
                        'sex' => $user3->sex,
                        'birthday' => $user3->birthday->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'photo_url' => null,
                        'country_name' => 'Russia',
                        'locality_name' => 'Moscow',
                        'was_online_at' => $user3->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'is_verified' => false,
                    ],
                    'liked_at' => $like2->liked_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                    'is_new' => true,
                ],
                [
                    'id' => $like1->id,
                    'user' => [
                        'id' => $user2->id,
                        'name' => $user2->name,
                        'sex' => $user2->sex,
                        'birthday' => $user2->birthday->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'photo_url' => null,
                        'country_name' => 'Russia',
                        'locality_name' => 'Moscow',
                        'was_online_at' => $user2->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'is_verified' => false,
                    ],
                    'liked_at' => $like1->liked_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                    'is_new' => true,
                ],
            ],
        ]);
    }
}
