<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Database\Factories\UserFactory;
use Tests\HttpTestCase;

class ProfileTest extends HttpTestCase
{
    public function testUsers(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        $response = $this
            ->actingAs($user)
            ->apiGetRequest('/users/profile', [
                'user_id' => $user->id,
            ])
        ;

        $response->assertOk();

        $response->assertJsonStructure([
            'id',
            'name',
            'birthday',
            'was_online_at',
            'is_verified',
            'country_name',
            'locality_name',
            'ethnicity',
            'photos',
        ]);
    }

    public function testUsersAsGuest(): void
    {
        $response = $this->apiGetRequest('/users/profile', [
            'user_id' => 1,
        ]);

        $response->assertUnauthorized();
    }
}
