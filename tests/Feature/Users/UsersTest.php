<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Database\Factories\UserFactory;
use Tests\HttpTestCase;

class UsersTest extends HttpTestCase
{
    public function testUsers(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'status' => User::STATUS_ACCEPTED,
        ]);

        $response = $this->actingAs($user)->apiGetRequest('/users', [
            'sex' => User::SEX_FEMALE,
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            'current_page',
            'page_count',
            'users' => [
                [
                    'id',
                    'name',
                    'birthday',
                    'was_online_at',
                    'is_verified',
                    'country_name',
                    'locality_name',
                    'photo_url',
                    'photo_count',
                    'additional_information' => [
                        [
                            'code',
                            'title',
                        ],
                    ],
                    'is_viewed',
                ],
            ],
        ]);
    }

    public function testUsersPendingModeration(): void
    {
        /** @var User $user */
        $user = (new UserFactory())->create([
            'status' => User::STATUS_PENDING_MODERATION,
        ]);

        $response = $this->actingAs($user)->apiGetRequest('/users', [
            'sex' => User::SEX_FEMALE,
        ]);

        $response->assertForbidden();
    }

    public function testUsersAsGuest(): void
    {
        $response = $this->apiGetRequest('/users', [
            'sex' => User::SEX_FEMALE,
        ]);

        $response->assertUnauthorized();
    }
}
