<?php

namespace Tests\Feature;

use App\Models\Country;
use Tests\HttpTestCase;

class GeoTest extends HttpTestCase
{
    public function testCountries(): void
    {
        $response = $this->apiGetRequest('/countries');

        $response->assertOk();

        $response->assertJsonStructure([
            [
                'id',
                'code',
                'name',
            ],
        ]);
    }

    public function testSubdivisions(): void
    {
        $countryId = (new Country())->where('code_alpha2', 'TR')->value('id');

        $response = $this->apiGetRequest('/subdivisions', [
            'country_id' => $countryId,
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            [
                'id',
                'name',
            ],
        ]);
    }

    public function testLocalities(): void
    {
        $countryId = (new Country())->where('code_alpha2', 'TR')->value('id');

        $response = $this->apiGetRequest('/localities', [
            'country_id' => $countryId,
        ]);

        $response->assertOk();

        $response->assertJsonStructure([
            [
                'id',
                'name',
                'subdivision_level_1' => [
                    'id',
                    'name',
                ],
                'subdivision_level_2' => [
                    'id',
                    'name',
                ],
            ],
        ]);
    }
}
