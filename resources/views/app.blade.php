<!doctype html>
<html lang="{{ language()->code }}" dir="{{ language()->script_direction }}">
    <head>
        <link href="{{ compiled_asset_path('vendors.css') }}" rel="stylesheet">
        <link href="{{ compiled_asset_path('app.css') }}" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1" />
    </head>
    <body>
        <div id="root" class="site-wrapper"></div>

        <script>
            window.ENV = JSON.parse('{!! $jsVariables !!}')

            window.Translations = {}
        </script>

        <script src="{{ route('translations', [$defaultLanguageCode], false) . '?' . $translationsLastUpdateTime  }}"></script>

        @if(language()->code !== $defaultLanguageCode)
            <script src="{{ route('translations', [language()->code], false) . '?' . $translationsLastUpdateTime  }}"></script>
        @endif

        <script src="{{ compiled_asset_path('vendors.js') }}"></script>
        <script src="{{ compiled_asset_path('app.js') }}"></script>
    </body>
</html>
