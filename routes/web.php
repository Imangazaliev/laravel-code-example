<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\OAuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordByEmailController;
use App\Http\Controllers\Auth\ResetPasswordByPhoneNumberController;
use App\Http\Controllers\ChatsController;
use App\Http\Controllers\CoinsController;
use App\Http\Controllers\EthnicitiesController;
use App\Http\Controllers\GeoController;
use App\Http\Controllers\LanguagesController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\Profile\ChangeEmailController;
use App\Http\Controllers\Profile\ChangePhoneNumberController;
use App\Http\Controllers\Profile\ConnectedAccountsController;
use App\Http\Controllers\Profile\NotificationsController;
use App\Http\Controllers\Profile\NotificationSettingsController;
use App\Http\Controllers\Profile\PhotoController;
use App\Http\Controllers\Profile\ProfileController;
use App\Http\Controllers\Seo;
use App\Http\Controllers\TranslationsController;
use App\Http\Controllers\Users\ViewProfileController;
use App\Http\Middleware\CheckProfileAccepted;
use App\Http\Middleware\CheckUserBanned;
use App\Http\Middleware\VerifyCsrfToken;

Route::get('/robots.txt', [Seo::class, 'getRobotsTxtFile']);
Route::get('/sitemap.xml', [Seo::class, 'getSitemap']);
Route::get('/translations/{languageCode}', [TranslationsController::class, 'getTranslations'])->name('translations');

Route::get('/{path}', [MainController::class, 'getIndex'])->where('path', '(?!api)(.*)?');

Route::group([
    'prefix' => '/api',
    'middleware' => CheckUserBanned::class,
], function () {
    // Misc
    Route::get('/languages', [LanguagesController::class, 'languages'])->withoutMiddleware(CheckUserBanned::class);
    Route::get('/ethnicities', [EthnicitiesController::class, 'ethnicities']);
    Route::get('/tawk-data', [TawkController::class, 'getData'])->withoutMiddleware(CheckUserBanned::class);

    Route::get('/csrf-token', [MainController::class, 'csrfToken'])->withoutMiddleware(CheckUserBanned::class);
    Route::post('/change-language', [MainController::class, 'changeLanguage'])->withoutMiddleware(CheckUserBanned::class);

    // Geo
    Route::get('/countries', [GeoController::class, 'countries']);
    Route::get('/subdivisions', [GeoController::class, 'subdivisions']);
    Route::get('/localities', [GeoController::class, 'localities']);

    Route::group(['prefix' => '/auth'], function () {
        Route::post('/register', [RegisterController::class, 'register']);
        Route::post('/fix-phone-number', [RegisterController::class, 'fixPhoneNumber']);
        Route::post('/resend-verification-code', [RegisterController::class, 'resendVerificationCode']);
        Route::get('/verification-code-resend-info', [RegisterController::class, 'verificationCodeResendInfo']);
        Route::post('/confirm-phone-number', [RegisterController::class, 'confirmPhoneNumber']);
        Route::post('/registration-completed', [RegisterController::class, 'registrationCompleted']);

        Route::post('/login', [LoginController::class, 'login']);
        Route::post('/logout', [LoginController::class, 'logout'])->withoutMiddleware(CheckUserBanned::class);

        Route::group(['middleware' => 'throttle:10,1,reset_password'], function () {
            Route::post('/reset-password/by-phone-number/send', [ResetPasswordByPhoneNumberController::class, 'sendResetCode']);
            Route::post('/reset-password/by-phone-number/check-code', [ResetPasswordByPhoneNumberController::class, 'checkCode']);
            Route::post('/reset-password/by-phone-number/reset', [ResetPasswordByPhoneNumberController::class, 'reset']);
            Route::post('/reset-password/by-email/send', [ResetPasswordByEmailController::class, 'sendResetLink']);
            Route::post('/reset-password/by-email/reset', [ResetPasswordByEmailController::class, 'reset']);
        });
    });

    Route::group(['prefix' => '/oauth'], function () {
        Route::post('/login/redirect-url', [OAuthController::class, 'loginRedirectUrl']);
        Route::get('/login/callback', [OAuthController::class, 'loginCallback'])->name('oauth.login.callback');
    });

    Route::group([
        'prefix' => '/profile',
    ], function () {
        Route::get('/', [ProfileController::class, 'profile'])->withoutMiddleware(CheckUserBanned::class);
        Route::get('/extended-information', [ProfileController::class, 'extendedInformation']);
        Route::post('/edit', [ProfileController::class, 'edit']);
        Route::get('/rejection-info', [ProfileController::class, 'rejectionInfo']);
        Route::get('/missed-fields', [ProfileController::class, 'missedFields']);
        Route::get('/ban-info', [ProfileController::class, 'banInfo'])->withoutMiddleware(CheckUserBanned::class);
        Route::get('/updates', [ProfileController::class, 'updates'])->withoutMiddleware(CheckUserBanned::class);
    });

    Route::get('/guests', [ProfileController::class, 'guests']);
    Route::post('/guests/seen', [ProfileController::class, 'markGuestsAsSeen']);
    Route::get('/viewed-profiles', [ProfileController::class, 'viewedProfiles']);

    Route::group(['prefix' => '/favorite-profiles'], function () {
        Route::get('/', [FavoriteProfilesController::class, 'favoriteProfiles']);
        Route::post('/add', [FavoriteProfilesController::class, 'addToFavorites']);
        Route::post('/remove', [FavoriteProfilesController::class, 'removeFromFavorites']);
    });

    Route::group(['prefix' => '/likes'], function () {
        Route::get('/', [LikesController::class, 'likes']);
        Route::post('/seen', [LikesController::class, 'markAsSeen']);
    });

    Route::get('/liked-users', [LikesController::class, 'likedUsers']);

    Route::group([
        'prefix' => '/photos',
    ], function () {
        Route::get('/', [PhotoController::class, 'photos']);
        Route::post('/upload', [PhotoController::class, 'uploadPhoto']);
        Route::post('/set-main', [PhotoController::class, 'setMainPhoto']);
        Route::post('/delete', [PhotoController::class, 'deletePhoto']);
    });

    Route::group([
        'prefix' => '/notifications',
    ], function () {
        Route::get('/', [NotificationsController::class, 'notifications']);
        Route::post('/seen', [NotificationsController::class, 'markAsSeen']);
    });

    Route::group([
        'prefix' => '/tips',
    ], function () {
        Route::get('/', [TipsController::class, 'tips']);
    });

    Route::group(['middleware' => 'throttle:5,10,appeal_moderation'], function () {
        Route::post('/appeal-profile-moderation', [ModerationAppealsController::class, 'appealProfileModeration']);
        Route::post('/appeal-photo-moderation', [ModerationAppealsController::class, 'appealPhotoModeration']);
        Route::post('/appeal-chat-message-moderation', [ModerationAppealsController::class, 'appealChatMessageModeration']);
    });

    Route::group([
        'prefix' => '/change-phone-number',
    ], function () {
        Route::get('/current-request', [ChangePhoneNumberController::class, 'currentRequest']);

        Route::group(['middleware' => 'throttle:10,1,change_phone_number'], function () {
            Route::post('/change', [ChangePhoneNumberController::class, 'change']);
            Route::post('/confirm', [ChangePhoneNumberController::class, 'confirm']);
            Route::post('/cancel', [ChangePhoneNumberController::class, 'cancel']);
        });
    });

    Route::group([
        'prefix' => '/change-email',
    ], function () {
        Route::get('/current-request', [ChangeEmailController::class, 'currentRequest']);

        Route::group(['middleware' => 'throttle:10,1,change_email'], function () {
            Route::post('/change', [ChangeEmailController::class, 'change']);
            Route::post('/confirm', [ChangeEmailController::class, 'confirm']);
            Route::post('/cancel', [ChangeEmailController::class, 'cancel']);
        });
    });

    Route::group([
        'prefix' => '/notification-settings',
    ], function () {
        Route::get('/', [NotificationSettingsController::class, 'notificationSettings']);
        Route::post('/update', [NotificationSettingsController::class, 'update']);
    });

    Route::group([
        'prefix' => '/connected-accounts',
    ], function () {
        Route::get('/', [ConnectedAccountsController::class, 'connectedAccounts']);
        Route::post('/redirect-url', [ConnectedAccountsController::class, 'redirectUrl']);
        Route::get('/callback', [ConnectedAccountsController::class, 'callback'])->name('connected-accounts.callback');
        Route::post('/connect-telegram', [ConnectedAccountsController::class, 'connectTelegram']);
        Route::post('/disconnect', [ConnectedAccountsController::class, 'disconnectAccount']);
    });

    Route::group([
        'prefix' => '/additional-services',
    ], function () {
        Route::get('/', [AdditionalServicesController::class, 'services']);
        Route::post('/toggle-hide-profile', [AdditionalServicesController::class, 'toggleHideProfile'])->middleware('permission:hide-profile');
        Route::post('/toggle-incognito-mode', [AdditionalServicesController::class, 'toggleIncognitoMode'])->middleware('permission:incognito-mode');
    });

    Route::group(['middleware' => 'throttle:1,1,change_password'], function () {
        Route::post('/change-password', [ProfileController::class, 'changePassword']);
    });

    Route::group(['middleware' => 'throttle:5,1,profile_deactivation'], function () {
        Route::post('/deactivate-profile', [ProfileController::class, 'deactivateProfile']);
        Route::post('/reactivate-profile', [ProfileController::class, 'reactivateProfile']);
    });

    Route::group(['middleware' => 'throttle:5,1,profile_deletion'], function () {
        Route::post('/delete-profile', [ProfileController::class, 'deleteProfile']);
        Route::get('/profile-deletion-info', [ProfileController::class, 'profileDeletionInfo'])->withoutMiddleware('throttle:5,1,profile_deletion');
        Route::post('/cancel-profile-deletion', [ProfileController::class, 'cancelProfileDeletion']);
    });

    Route::group([
        'prefix' => '/users',
        'namespace' => 'Users',
    ], function () {
        Route::group(['middleware' => CheckProfileAccepted::class . ':search'], function () {
            Route::get('/', [SearchController::class, 'users'])->middleware('throttle:30,1,search');
            Route::get('/profile', [ViewProfileController::class, 'profile'])->middleware('throttle:30,1,view_profile');
        });

        Route::group(['middleware' => CheckProfileAccepted::class], function () {
            Route::post('/profile-viewed', [ViewProfileController::class, 'profileViewed']);
            Route::post('/make-gift', [ViewProfileController::class, 'makeGift'])->middleware('throttle:5,1,make_gift');
            Route::post('/like', [LikesController::class, 'like'])->middleware('throttle:5,1,like_profile');
            Route::post('/remove-like', [LikesController::class, 'removeLike'])->middleware('throttle:5,1,like_profile');
        });
    });

    Route::group([
        'prefix' => '/saved-filters',
        'namespace' => 'Users',
        'middleware' => CheckProfileAccepted::class,
    ], function () {
        Route::get('/', [SavedFiltersController::class, 'filters'])->withoutMiddleware(CheckProfileAccepted::class);
        Route::get('/search-parameters', [SavedFiltersController::class, 'searchParameters']);
        Route::post('/save', [SavedFiltersController::class, 'saveFilter']);
        Route::post('/delete', [SavedFiltersController::class, 'deleteFilter']);
        Route::post('/enable-notifications', [SavedFiltersController::class, 'enableNotifications']);
        Route::post('/disable-notifications', [SavedFiltersController::class, 'disableNotifications']);
    });

    Route::group([
        'prefix' => '/chat-requests',
    ], function () {
        Route::get('/incoming', [ChatRequestsController::class, 'incomingRequests']);
        Route::get('/outgoing', [ChatRequestsController::class, 'outgoingRequests']);
        Route::post('/seen', [ChatRequestsController::class, 'markAsSeen']);

        Route::group(['middleware' => 'throttle:20,5,chat_requests'], function () {
            Route::post('/send', [ChatRequestsController::class, 'sendRequest'])->middleware(CheckProfileAccepted::class);
            Route::post('/cancel', [ChatRequestsController::class, 'cancelRequest']);
            Route::post('/accept', [ChatRequestsController::class, 'acceptRequest'])->middleware(CheckProfileAccepted::class);
            Route::post('/reject', [ChatRequestsController::class, 'rejectRequest']);
        });
    });

    Route::group([
        'prefix' => '/chats',
    ], function () {
        Route::get('/', [ChatsController::class, 'chats']);
        Route::get('/chat-elements', [ChatsController::class, 'chatElements']);
        Route::post('/send-message', [ChatsController::class, 'sendMessage'])->middleware('throttle:30,5,send_message');
        Route::post('/cancel-message', [ChatsController::class, 'cancelMessage']);
        Route::post('/mark-as-read', [ChatsController::class, 'markAsRead']);
        Route::post('/finish', [ChatsController::class, 'finishChat']);
    });

    Route::group([
        'prefix' => '/support-chat',
    ], function () {
        Route::get('/', [SupportChatController::class, 'chat']);
        Route::post('/create', [SupportChatController::class, 'createChat']);
        Route::get('/chat-elements', [SupportChatController::class, 'chatElements']);
        Route::post('/mark-as-read', [SupportChatController::class, 'markAsRead']);
        Route::post('/set-message-preview', [SupportChatController::class, 'setMessagePreview']);
        Route::post('/send-message', [SupportChatController::class, 'sendMessage']);
    });

    Route::group([
        'prefix' => '/coins',
    ], function () {
        Route::get('/amount-options', [CoinsController::class, 'coinsAmountOptions']);
        Route::get('/calculate-price', [CoinsController::class, 'calculatePrice']);
        Route::post('/purchase', [CoinsController::class, 'purchase'])->middleware('throttle:3,1,coins_purchase');
        Route::post('/complete-purchase/{gateway}', [CoinsController::class, 'completePurchase'])
            ->name('api.coins.complete-purchase')
            ->withoutMiddleware(VerifyCsrfToken::class)
        ;
        Route::get('/history', [CoinsController::class, 'getHistory'])->middleware('throttle:20,1,coins_history');
        Route::get('/prices', [CoinsController::class, 'getPrices'])->middleware('throttle:20,1,coins_history');
    });

    Route::group([
        'prefix' => '/admin',
        'middleware' => 'permission:access-to-admin-panel',
    ], function () {
        Route::group([
            'prefix' => '/users',
        ], function () {
            Route::get('/', [Admin\UsersController::class, 'users'])->middleware('permission:view-users-list');
            Route::get('/details', [Admin\UsersController::class, 'userDetails'])->middleware('permission:view-user-details');
            Route::get('/chats', [Admin\UsersController::class, 'chats'])->middleware('permission:view-user-details');
            Route::get('/photos', [Admin\UsersController::class, 'photos'])->middleware('permission:view-user-details');
            Route::get('/bans', [Admin\UsersController::class, 'bans'])->middleware('permission:view-user-details');
            Route::get('/moderations', [Admin\UsersController::class, 'moderations'])->middleware('permission:view-user-details');
            Route::get('/coin-transactions', [Admin\UsersController::class, 'coinTransactions'])->middleware('permission:view-user-details');
            Route::post('/accept-profile', [Admin\UsersController::class, 'acceptProfile'])->middleware('permission:change-profile-status');
            Route::post('/reject-profile', [Admin\UsersController::class, 'rejectProfile'])->middleware('permission:change-profile-status');
            Route::post('/change-birthday', [Admin\UsersController::class, 'changeBirthday'])->middleware('permission:change-user-birthday');
            Route::post('/change-password', [Admin\UsersController::class, 'changePassword'])->middleware('permission:change-user-password');
            Route::post('/change-coins-balance', [Admin\UsersController::class, 'changeCoinsBalance'])->middleware('permission:change-coins-balance');
            Route::post('/generate-login-url', [Admin\UsersController::class, 'generateLoginUrl'])->middleware('permission:log-in-as-user');
            Route::post('/ban', [Admin\UsersController::class, 'ban'])->middleware('permission:ban-users');
            Route::post('/unban', [Admin\UsersController::class, 'unban'])->middleware('permission:ban-users');
            Route::post('/delete', [Admin\UsersController::class, 'deleteUser'])->middleware('permission:delete-users');
        });
    });
});
