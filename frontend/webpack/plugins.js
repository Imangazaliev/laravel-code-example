const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ManifestPlugin = require('webpack-manifest-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const SimpleProgressWebpackPlugin = require('simple-progress-webpack-plugin')
const WebpackNotifierPlugin = require('webpack-notifier')

module.exports = (isProductionMode, analyze) => {
    const plugins = [
        new CleanWebpackPlugin({
            cleanAfterEveryBuildPatterns: ['!images/**/*', '!fonts/**/*'],
        }),
        new SimpleProgressWebpackPlugin({
            format: 'compact',
        }),
        new ManifestPlugin(),
        new WebpackNotifierPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css',
            chunkFilename: '[name].[contenthash].chunk.css',
        }),
    ]

    if ( ! isProductionMode) {
        // Watcher doesn't work well if you mistype casing in a path so we use
        // a plugin that prints an error when you attempt to do this.
        // See https://github.com/facebookincubator/create-react-app/issues/240
        plugins.push(new CaseSensitivePathsPlugin())
    }

    if (analyze) {
        plugins.push(new BundleAnalyzerPlugin())
    }

    return plugins
}
