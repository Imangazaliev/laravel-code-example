const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const getStyleLoaders = ({
    isProduction,
    modules = false,
}) => {
    const loaders =  [
        MiniCssExtractPlugin.loader,
        {
            loader: 'css-loader',
            options: {
                modules,
                ...(modules ? {
                    localIdentName: '[local]_[hash:base64:6]',
                    camelCase: 'only',
                } : {}),
                sourceMap: false,
            },
        },
    ]

    if (isProduction) {
        loaders.push({
            loader: 'postcss-loader',
            options: {
                plugins: () => [
                    require('autoprefixer')({
                        browsers: ['> 1%', 'last 5 versions'],
                        cascade: false,
                    }),
                ],
                sourceMap: false,
            },
        })
    }

    loaders.push({
        loader: 'sass-loader',
        options: {
            sourceMap: false,
            includePaths: ['frontend'],
        },
    })

    return loaders
}

const jsTsRegex = /\.(ts|tsx|js|jsx)$/
const stylesRegex = /\.(scss|css)$/
const stylesModuleRegex = /\.module\.(scss|css)$/

module.exports = (isProduction) => {
    return {
        rules: [
            // First, run the linter.
            // It's important to do this before Babel processes the JS.
            {
                test: jsTsRegex,
                include: [
                    /resources\/assets\/js\/src/,
                ],
                enforce: 'pre',
                use: {
                    loader: 'eslint-loader',
                },
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            // This is a feature of `babel-loader` for Webpack (not Babel itself).
                            // It enables caching results in ./node_modules/.cache/babel-loader/
                            // directory for faster rebuilds.
                            cacheDirectory: true,
                        },
                    },
                ],
            },
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                use: 'ts-loader',
            },
            {
                test: stylesRegex,
                exclude: stylesModuleRegex,
                use: getStyleLoaders({
                    isProduction,
                }),
            },
            {
                test: stylesModuleRegex,
                use: getStyleLoaders({
                    isProduction,
                    modules: true,
                }),
            },
            {
                test: /\.(eot|ttf|otf|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'fonts',
                        },
                    },
                ],
            },
            {
                test: /\.(png|jpg|gif|jpeg)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: 'images',
                        },
                    },
                ],
            },
            {
                test: /\.svg$/,
                issuer: {
                    test: jsTsRegex,
                },
                use: ['@svgr/webpack', 'file-loader'],
            },
            {
                test: /\.svg$/,
                use: ['file-loader'],
            },
        ],
    }
}
