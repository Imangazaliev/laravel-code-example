module.exports = {
    hash: false,
    version: false,
    timings: false,
    assets: false,
    chunks: false,
    maxModules: 0,
    modules: false,
    reasons: false,
    children: false,
    source: false,
    errors: true,
    errorDetails: false,
    warnings: true,
    publicPath: false,
}
