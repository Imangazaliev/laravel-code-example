import React  from 'react'
import { Route as ReactRoute, Switch } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'
import { StylesProvider, ThemeProvider } from '@material-ui/core/styles'
import { ErrorBoundary } from '@sentry/react'

import { history, store } from './store/configureStore'

import { GoogleAnalytics, Route, Tawk, Toastify, YandexMetrika } from 'app/components'
import { NotFoundError } from './pages/error-pages'

import { routes as publicRoutes } from './routes'
import { routes as adminRoutes } from './routes.admin'

const routes = [...publicRoutes, ...adminRoutes]

import theme from './theme'

const App = () => {
    return <div className="content">
        <Provider store={ store }>
            <ConnectedRouter history={ history }>
                <ThemeProvider theme={ theme }>
                    <StylesProvider injectFirst>
                        <Switch>
                            {
                                routes.map(route => (
                                    <Route key={ route.path } exact={ route.exact } sensitive { ...route }/>
                                ))
                            }
                            <ReactRoute component={ NotFoundError }/>
                        </Switch>
                    </StylesProvider>
                </ThemeProvider>
                <Toastify />
                <ErrorBoundary onError={ console.error }>
                    <GoogleAnalytics />
                </ErrorBoundary>
                <ErrorBoundary onError={ console.error }>
                    <YandexMetrika />
                </ErrorBoundary>
                <ErrorBoundary onError={ console.error }>
                    <Tawk />
                </ErrorBoundary>
            </ConnectedRouter>
        </Provider>
    </div>
}

export default App
