declare module '*.svg' {
    const content: any
    export const ReactComponent: any
    export default content
}

declare module "*.png" {
    const value: string
    export default value
}

declare module '*.module.scss' {
    const classes: {
        readonly [key: string]: string
    }

    export default classes
}

declare type TMapObject<K, V> = {
    [key: string]: V
}
