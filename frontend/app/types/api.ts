export {}

declare global {
    const enum ApiEndpoints {
        // Misc
        LANGUAGES = '/languages',
        ETHNICITIES = '/ethnicities',
        TAWK_DATA = '/tawk-data',

        CSRF_TOKEN = '/csrf-token',
        CHANGE_LANGUAGE = '/change-language',

        // Geo
        COUNTRIES = '/countries',
        SUBDIVISIONS = '/subdivisions',
        LOCALITIES = '/localities',

        // Auth
        REGISTER = '/auth/register',
        FIX_PHONE_NUMBER = '/auth/fix-phone-number',
        RESEND_VERIFICATION_CODE = '/auth/resend-verification-code',
        VERIFICATION_CODE_RESEND_INFO = '/auth/verification-code-resend-info',
        CONFIRM_PHONE_NUMBER = '/auth/confirm-phone-number',
        REGISTRATION_COMPLETED = '/auth/registration-completed',
        LOGIN = '/auth/login',
        LOGOUT = '/auth/logout',
        RESET_PASSWORD_BY_PHONE_NUMBER_SEND = '/auth/reset-password/by-phone-number/send',
        RESET_PASSWORD_BY_PHONE_NUMBER_CHECK_CODE = '/auth/reset-password/by-phone-number/check-code',
        RESET_PASSWORD_BY_PHONE_NUMBER_RESET = '/auth/reset-password/by-phone-number/reset',
        RESET_PASSWORD_BY_EMAIL_SEND = '/auth/reset-password/by-email/send',
        RESET_PASSWORD_BY_EMAIL_RESET = '/auth/reset-password/by-email/reset',

        // Profile
        PROFILE = '/profile',
        PROFILE_EXTENDED_INFORMATION = '/profile/extended-information',
        PROFILE_EDIT = '/profile/edit',
        PROFILE_REJECTION_INFO = '/profile/rejection-info',
        PROFILE_MISSED_FIELDS = '/profile/missed-fields',
        PROFILE_BAN_INFO = '/profile/ban-info',
        PROFILE_UPDATES = '/profile/updates',
    }

    const enum ApiErrors {
        INVALID_CSRF_TOKEN = 'INVALID_CSRF_TOKEN',
        INSUFFICIENT_BALANCE = 'INSUFFICIENT_BALANCE',
    }
}
