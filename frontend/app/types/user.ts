export type TCurrentUser = {
    id: number
    name: string
    sex: TSex
    birthday: string
    phone_number: string
    phone_number_country_code: string
    is_phone_number_verified: boolean
    email: string | null
    is_email_verified: boolean
    completed_registration: boolean
    photo_url:  string | null
    status: TProfileStatus
    last_status: TProfileStatus.ACCEPTED | TProfileStatus.REJECTED | null
    reputation: number
    is_verified: boolean
    coins: number
    profile_completion_percent: number
    required_fields_filled: boolean
    permissions: string[]
    support_chat_exists: boolean
}

export type TUserExtendedInfo = {
    profile_headline: string | null
    height: number | null
    weight_from: number | null
    weight_to: number | null
    body_type: TBodyType | null
    hair_color: THairColor | null
    eye_color: TEyeColor | null
    facial_hair: TFacialHair | null
    hobbies_and_interests: string | null
    sport: string | null
    sport_exercises_frequency: TSportExercisesFrequency | null
    smoking: TSmoking | null
    alcohol_consumption: TAlcoholConsumption | null
    self_description: string | null
    marital_status: TMaritalStatus
    child_count: number | null
    children_in_household: number | null
    living_with_parents: TMaritalStatus
    desired_number_of_children: TDesiredNumberOfChildren | null
    children_adoption: TChildrenAdoption | null
    polygyny: TPolygyny | null
    housing_type: THousingType | null
    relocation: TRelocation | null
    education: TEducation | null
    known_languages: {
        id: number
        name: string
    }[]
    profession_or_occupation: string | null
    employment: TEmployment | null
    job_title: string | null
    financial_status: TFinancialStatus | null
    religion: TReligion | null
    future_spouse_age_from: number | null
    future_spouse_age_to: number | null
    future_spouse_height_from: number | null
    future_spouse_height_to: number | null
    future_spouse_weight_from: number | null
    future_spouse_weight_to: number | null
    future_spouse_body_type: TBodyType | null
    future_spouse_character_traits: string | null
    future_spouse_description: string | null
    future_spouse_children: TFutureSpouseChildren | null
}

export type TCurrentUserExtendedInfo = TUserExtendedInfo & {
    country_id: number | null
    locality_id: number | null
    ethnicity_id: number | null
}

export type TUserBase = TUserExtendedInfo & {
    id: number
    name: string
    sex:  TSex
    birthday: string
    is_verified: boolean
    country_name: string
    locality_name: string
    ethnicity: string
}

export type TUserCard = {
    id: number
    name: string
    sex:  TSex
    birthday: string
    presence_status: TPresenceStatus
    was_online_at: string
    is_verified: boolean
    country_name: string | null
    locality_name: string | null
    photo_url: string | null
    photo_count?: number
}

export type TUserCardInfo = TUserCard & {
    additional_information: {
       code: string
       title: string
    }[]
    is_viewed: boolean
}

export interface OwnProfileView {
    id: number
    visitor: {
        id: number
        name: string
        sex: TSex
        birthday: string
        photo_url: null
        country_name: string
        locality_name: string
        presence_status: TPresenceStatus
        was_online_at: string
        is_verified: boolean
    }
    viewed_at: string
    is_new: boolean
}

export interface ViewedProfile {
    id: number
    profile: {
        id: number
        name: string
        sex: TSex
        birthday: string
        photo_url: null
        country_name: string
        locality_name: string
        presence_status: TPresenceStatus
        was_online_at: string
        is_verified: boolean
    }
    viewed_at: string
}

export interface FavoriteProfile {
    id: number
    name: string
    sex: TSex
    birthday: string
    photo_url: null
    country_name: string
    locality_name: string
    presence_status: TPresenceStatus
    was_online_at: string
    is_verified: boolean
    added_at: string
}

export interface LikedMeUser {
    id: number
    name: string
    sex: TSex
    birthday: string
    photo_url: null
    country_name: string
    locality_name: string
    presence_status: TPresenceStatus
    was_online_at: string
    is_verified: boolean
    liked_at: string
    is_new: boolean
}

export interface LikedUser {
    id: number
    name: string
    sex: TSex
    birthday: string
    photo_url: null
    country_name: string
    locality_name: string
    presence_status: TPresenceStatus
    was_online_at: string
    is_verified: boolean
    liked_at: string
}

export interface ProfileUpdates {
    new_profile_view_count: number
    new_like_count: number
    new_chat_request_count: number
    new_message_count: number
    new_notification_count: number
}

export enum TProfileStatus {
    NOT_MODERATED = 'not-moderated',
    PENDING_MODERATION = 'pending-moderation',
    ACCEPTED = 'accepted',
    REJECTED = 'rejected',
    BANNED = 'banned',
    DEACTIVATED = 'deactivated',
    PENDING_DELETION = 'pending-deletion',
    DELETED = 'deleted',
}

export enum TPresenceStatus {
    ONLINE = 'online',
    IDLE = 'idle',
    OFFLINE = 'offline',
}

export enum TBanReason {
    //
}

export enum TDeletionReason {
    //
}

export enum TSex {
    MALE = 'm',
    FEMALE = 'f',
}

export enum TBodyType {
    //
}

export enum THairColor {
    //
}

export enum TEyeColor {
    //
}

export enum TFacialHair {
    //
}

export enum TSportExercisesFrequency {
    //
}

export enum TSmoking {
    NO = 'no',
    YES = 'yes',
    SOMETIMES = 'sometimes',
}

export enum TAlcoholConsumption {
    NO = 'no',
    YES = 'yes',
    SOMETIMES = 'sometimes',
}

export enum TMaritalStatus {
    //
}

export enum TLivingWithParents {
    LIVING_WITH_PARENT_YES = 'yes',
    LIVING_WITH_PARENT_NO = 'no',
    LIVING_WITH_PARENT_OTHER = 'other',
}

export enum TPolygyny {
    WANT_HAVE_ONE_WIFE = 'want-have-one-wife',
    WANT_HAVE_MULTIPLE_WIVES = 'want-have-multiple-wives',
    READY_FOR_POLYGYNY = 'ready-for-polygyny',
    NOT_READY_FOR_POLYGYNY = 'not-ready-for-polygyny',
    WOULD_BE_ONLY_WIFE = 'would-be-only-wife',
    NOT_SURE = 'not-sure',
}

export enum TDesiredNumberOfChildren {
    NOT_PLANNED = 'not-planned',
    WANT_ONE_CHILD = 'want-one-child',
    WANT_TWO_CHILDREN = 'want-two-children',
    WANT_THREE_CHILDREN = 'want-three-children',
    WANT_MANY_CHILDREN = 'want-many-children',
    NOT_SURE = 'not-sure',
}

export enum TChildrenAdoption {
    POSSIBLE = 'possible',
    NOT_POSSIBLE = 'not-possible',
    NOT_SURE = 'not-sure',
}

export enum THousingType {
    OWN_APARTMENT = 'own-apartment',
    OWN_HOUSE = 'own-house',
    RENTAL_APARTMENT = 'rental-apartment',
    RENTAL_HOUSE = 'rental-house',
    DORMITORY = 'dormitory',
    OTHER = 'other',
}

export enum TRelocation {
    POSSIBLE = 'possible',
    WITHIN_COUNTRY = 'within-country',
    WITHIN_REGION = 'within-region',
    WITHIN_CITY_OR_LOCALITY = 'within-city-or-locality',
    NOT_POSSIBLE = 'not-possible',
    NOT_SURE = 'not-sure',
}

export enum TEducation {
    PRIMARY = 'primary',
    SECONDARY = 'secondary',
    SECONDARY_NON_COMPLETED = 'secondary-non-completed',
    VOCATIONAL_COLLEGE = 'vocational-college',
    HIGHER = 'higher',
    HIGHER_NON_COMPLETED = 'higher-non-completed',
    SEVERAL_HIGHER = 'several-higher',
    DONT_HAVE_EDUCATION = 'dont-have-education',
}

export enum TEmployment {
    FULL = 'full',
    PARTIAL = 'partial',
    NOT_WORKING = 'not-working',
    SELF_EMPLOYMENT = 'self-employment',
    RETIRED = 'retired',
    HOUSEWIFE = 'housewife',
    OTHER = 'other',
}

export enum TFinancialStatus {
    DO_NOT_HAVE_REGULAR_INCOME = 'do-not-have-regular-income',
    LIVING_WAGE = 'living-wage',
    AVERAGE = 'average',
    RICH = 'rich',
}

export enum TReligion {
    ISLAM = 'islam',
    CHRISTIANITY = 'christianity',
    JUDAISM = 'judaism',
    OTHER = 'other',
}

export enum TFutureSpouseChildren {
    POSSIBLE_WITH_CHILDREN = 'possible-with-children',
    WITHOUT_CHILDREN = 'without-children',
    NOT_SURE = 'not-sure',
}

export type TProfileFieldOption<T = string> = {
    code: T
    label: string
}
