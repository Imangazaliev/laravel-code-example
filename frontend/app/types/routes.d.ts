export {}

declare global {
    const enum Routes {
        REGISTER = '/register',
        REGISTER_RULES = '/register/rules',
        REGISTER_VERIFY_PHONE_NUMBER = '/register/verify-phone-number',
        REGISTER_WRONG_PHONE_NUMBER = '/register/wrong-phone-number',
        REGISTRATION_COMPLETED = '/registration-completed',
        LOGIN = '/login',
        PROFILE = '/profile',
        PROFILE_EDIT = '/profile/edit',
        PRIVACY_POLICY = '/privacy-policy',
        TERMS_OF_USE = '/terms-of-use',
    }

    const enum AdminRoutes {
        HOME = '/admin',
        USERS = '/admin/users',
    }
}
