import React from 'react'
import { useDispatch } from 'react-redux'
import i18n from 'i18next'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'
import { FormikErrors, FormikProps } from 'formik'
import { ref as yupRef, string } from 'yup'
import { toast } from 'react-toastify'

import http from 'app/core/axios'
import { fetchUser } from 'app/store/modules/user/user'

import { MainLayout } from 'app/components/layout'
import { Button, Field, Form, FormGroup, Label, Paper, PhoneNumberInput, Result } from 'app/components'

import styles from 'app/styles/styles'

const { classes } = styles

type TFormData = {
    phone_number: string
    phone_number_country_code: string
}

const phoneNumberFormValidationSchema = {
    phone_number: string().required().phoneNumber(yupRef('phone_number_country_code')).label(i18n.t('phone-number')),
    phone_number_country_code: string().required(),
}

const initialValues = {
    phone_number: '',
    phone_number_country_code: '',
}

const WrongPhoneNumber = () => {
    const { t } = useTranslation()
    const dispatch = useDispatch()
    const history = useHistory()

    const handleSubmit = async (
        values: TFormData,
        setErrors: (errors: FormikErrors<TFormData>) => void,
        setSubmitting: (isSubmitting: boolean) => void
    ) => {
        try {
            setSubmitting(true)

            await http.post(ApiEndpoints.FIX_PHONE_NUMBER, values)

            setSubmitting(false)

            dispatch(fetchUser())

            history.push('/register/verify-phone-number')
        } catch (error: any) {
            setSubmitting(false)

            if (error.response !== undefined) {
                if (error.response.status === 422) {
                    setErrors(error.response.data.validation_errors)

                    return
                }

                if (error.response.data.message !== undefined) {
                    toast.error(error.response.data.message)

                    return
                }
            }

            toast.error(t('error'))
        }
    }

    return (
        <MainLayout classes={{ container: classes.container }}>
            <Paper className={ classes.paper } >
                <Result
                    status="phone-2"
                    title={ t('auth:wrong-phone-number') }
                    subTitle={ t('auth:wrong-phone-number.description') }
                    classes={{
                        root: `${classes.wrapper} ${classes.wrapper_center}`,
                        subtitle: classes.block_text,
                    }}
                >
                    <Form
                        autoComplete="off"
                        onSubmit={ handleSubmit }
                        values={ initialValues }
                        validationSchema={ phoneNumberFormValidationSchema }
                    >
                        {
                            ({ setFieldValue }: FormikProps<TFormData>) => (
                                <>
                                    <FormGroup>
                                        <Label htmlFor="phone_number">{ t('phone-number') }</Label>
                                        <Field
                                            component={ PhoneNumberInput }
                                            name="phone_number"
                                            onChange={ (phoneNumber: string, countryCode: string | null) => {
                                                setFieldValue('phone_number', phoneNumber)
                                                setFieldValue('phone_number_country_code', countryCode)
                                            } }
                                        />
                                    </FormGroup>
                                    <Button
                                        color="primary"
                                        fullWidth
                                        type="submit"
                                        variant="contained"
                                    >
                                        { t('actions.change') }
                                    </Button>
                                    <Button
                                        className="mt-8"
                                        color="primary"
                                        fullWidth
                                        href={ Routes.REGISTER_VERIFY_PHONE_NUMBER }
                                        variant="text"
                                    >
                                        { t('navigation.back') }
                                    </Button>
                                </>
                            )
                        }
                    </Form>
                </Result>
            </Paper>
        </MainLayout>
    )
}

export { WrongPhoneNumber }
