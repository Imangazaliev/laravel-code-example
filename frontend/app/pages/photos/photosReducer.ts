import { TUserPhoto } from './types'

export interface TPhotosState {
    photos: TUserPhoto[]
    photoLoading: boolean
    progress: number
    photoErrorOnLoad: boolean
    photoSuccessOnLoad: boolean
}

export const photosState: TPhotosState = {
    photos: [],
    photoLoading: false,
    progress: 0,
    photoErrorOnLoad: false,
    photoSuccessOnLoad: false,
}

export const SET_PHOTOS = 'SET_PHOTOS'
export const PHOTO_LOADING = 'PHOTO_LOADING'
export const SET_PROGRESS = 'SET_PROGRESS'
export const PHOTO_ERROR_ON_LOAD = 'PHOTO_ERROR_ON_LOAD'
export const PHOTO_SUCCESS_ON_LOAD = 'PHOTO_SUCCESS_ON_LOAD'

export function photosReducer(state: TPhotosState, action: any): TPhotosState {
    switch (action.type) {
        case SET_PHOTOS:
            return {
                ...state,
                photos: action.payload,
            }
        case PHOTO_LOADING:
            return {
                ...state,
                photoLoading: action.payload,
            }
        case SET_PROGRESS:
            return {
                ...state,
                progress: action.payload,
            }
        case PHOTO_ERROR_ON_LOAD:
            return {
                ...state,
                photoLoading: false,
                photoErrorOnLoad: action.payload,
            }
        case PHOTO_SUCCESS_ON_LOAD:
            return {
                ...state,
                photoLoading: false,
                photoSuccessOnLoad: action.payload,
            }
        default: return state
    }
}
