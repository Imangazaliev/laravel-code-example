import React, { FC, useCallback, useEffect, useReducer, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { useDropzone } from 'react-dropzone'
import { toast } from 'react-toastify'

import http from 'app/core/axios'
import { fetchUser } from 'app/store/modules/user/user'

import { ProfileLayout } from 'app/components/layout'
import { Paper, Text, PhotoGallery } from 'app/components'
import { PhotoItem } from './PhotoItem'
import { PhotoUploader } from './PhotoUploader'
import { PhotoUploading } from 'app/pages/photos/PhotoUploading'
import { PhotoUploadError } from 'app/pages/photos/PhotoUploadError'
import { PhotoSuccessUpload } from 'app/pages/photos/PhotoSuccessUpload'

import { TUserPhoto } from './types'

import {
    photosReducer,
    photosState,
    SET_PHOTOS,
    SET_PROGRESS,
    PHOTO_LOADING,
    PHOTO_SUCCESS_ON_LOAD,
    PHOTO_ERROR_ON_LOAD,
} from './photosReducer'

import styles from './styles'
import { useFetchData } from 'app/hooks'

const { classes } = styles

const Photos: FC = () => {
    const { t } = useTranslation()

    const dispatch = useDispatch()

    const [state, dispatchState] = useReducer(photosReducer, photosState)
    const [openPhotoIndex, setOpenPhotoIndex] = useState<number | null>(null)
    const [updateList, setUpdateList] = useState(false)

    const { update: updatePhotos } = useFetchData<TUserPhoto[], TUserPhoto[]>(ApiEndpoints.PHOTOS, {
        onLoad: useCallback(response => {
            dispatchState({ type: SET_PHOTOS, payload: response })

            setUpdateList(false)
        }, [dispatchState]),
    })

    useEffect(() => {
        if (updateList) {
            updatePhotos()
        }
    }, [updateList, updatePhotos])

    useEffect(() => {
        for (const photo of state.photos) {
            if (photo.is_being_processed) {
                // update the state after 5 seconds to trigger updating of the photos list
                setTimeout(() => setUpdateList(true), 5000)

                break
            }
        }
    }, [state.photos])

    const handleShowPhoto = useCallback((photoIndex: number) => {
        setOpenPhotoIndex(photoIndex)
    }, [])

    const handlePhotoDropAccepted = useCallback(async (acceptedFiles: File[]) => {
        let config = {
            onUploadProgress: (progressEvent: ProgressEvent) => {
                dispatchState({
                    type: SET_PROGRESS,
                    payload: Math.floor((progressEvent.loaded * 100) / progressEvent.total),
                })
            },
        }

        try {
            const formData = new FormData()

            formData.append('photo', acceptedFiles[0])

            dispatchState({ type: PHOTO_LOADING, payload: true })

            await http.post(ApiEndpoints.PHOTOS_UPLOAD, formData, config)

            dispatchState({ type: PHOTO_SUCCESS_ON_LOAD, payload: true })

            setTimeout(() => dispatchState({ type: PHOTO_SUCCESS_ON_LOAD, payload: false }), 4000)

            updatePhotos()

            dispatch(fetchUser())
        } catch (error: any) {
            dispatchState({ type: PHOTO_ERROR_ON_LOAD, payload: true })

            setTimeout(() => dispatchState({ type: PHOTO_ERROR_ON_LOAD, payload: false }), 4000)

            if (error.response !== undefined) {
                if (error.response.status === 422) {
                    error.response.data.validation_errors.photo.map((errorMessage: string) => toast.error(errorMessage))

                    return
                }

                if (error.response.data.message !== undefined) {
                    toast.error(error.response.data.message)

                    return
                }
            }

            toast.error(t('error'))
        }
    }, [dispatch, dispatchState, t, updatePhotos])

    const handlePhotoDropRejected = useCallback(() => {
        toast.error(t('photo:errors.invalid-size-or-dimensions'))
    }, [t])

    const { getRootProps, getInputProps } = useDropzone({
        accept: 'image/jpeg',
        maxSize: 5 * 1024 * 1024,
        multiple: false,
        onDropAccepted: handlePhotoDropAccepted,
        onDropRejected: handlePhotoDropRejected,
    })

    const handleDeletePhoto = useCallback(async (photoId: number) => {
        if ( ! confirm(t('are-you-sure'))) {
            return
        }

        try {
            await http.post(ApiEndpoints.PHOTOS_DELETE, {
                photo_id: photoId,
            })

            updatePhotos()

            dispatch(fetchUser())
        } catch (error: any) {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        }
    }, [dispatch, t, updatePhotos])

    const handleSetMainPhoto = useCallback(async (photoId: number) => {
        try {
            await http.post(ApiEndpoints.PHOTOS_SET_MAIN, {
                photo_id: photoId,
            })

            updatePhotos()

            dispatch(fetchUser())
        } catch (error: any) {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        }
    }, [dispatch, t, updatePhotos])

    return (
        <ProfileLayout title={ t('photo') }>
            <Paper className={ `${classes.paper}` }>
                <Text bold size="large">Требования к фото:</Text>
                <div className={ classes.list_wrapper }>
                    <ul className={ classes.list }>
                        <li>запрещено загружать чужие фото или другие изображения, кроме вашего фото</li>
                        <li>на фото должны быть только вы</li>
                        <li>аурат должен быть закрыт</li>
                        <li>должно быть полностью видно ваше лицо</li>
                        <li>желательно, чтобы фото было сделано недавно</li>
                    </ul>
                </div>
                <Text bold size="large">Важно:</Text>
                <div className={ classes.list_wrapper }>
                    <ul className={ classes.list }>
                        <li>фото не видны никому до момента знакомства</li>
                        <li>все фото проверяются вручную</li>
                        <li>фото женщин проверяют модераторы-женщины</li>
                    </ul>
                </div>
                {
                    state.photos.length > 0 &&
                    state.photos
                        .map((photo: TUserPhoto, index) => (
                            <PhotoItem
                                key={ photo.id }
                                photo={ photo }
                                onPhotoDelete={ handleDeletePhoto }
                                onSetMainPhoto={ handleSetMainPhoto }
                                onPhotoClick={ () => handleShowPhoto(index + 1) }
                            />
                        ))
                }
                {
                    state.photos.length < ENV.profileRules.photo.maxCount &&
                    <div className="mt-24">
                        <Text bold className="mb-8" size="large">Требования к файлу с фото:</Text>
                        <div className="mb-24">
                            <ul className={ classes.list }>
                                <li>максимальный размер файла: 8 Мб</li>
                                <li>разрешенные форматы изображений: JPEG</li>
                                <li>минимальный размер изображения: 400x400 пикселей</li>
                                <li>максимальный размер изображения: 5000x5000 пикселей</li>
                            </ul>
                        </div>
                        <section>
                            <div { ...getRootProps({ className: classes.photo_dropzone }) }>
                                {
                                    state.photoLoading === true && <PhotoUploading progress={ state.progress } />
                                }
                                {
                                    state.photoErrorOnLoad === true && <PhotoUploadError />
                                }
                                {
                                    state.photoSuccessOnLoad === true && <PhotoSuccessUpload />
                                }
                                {
                                    ! state.photoLoading &&
                                    state.photoSuccessOnLoad === false &&
                                    state.photoErrorOnLoad === false &&
                                    <PhotoUploader getInputProps={ getInputProps } />
                                }
                            </div>
                        </section>
                    </div>
                }
            </Paper>
            <PhotoGallery
                open={ openPhotoIndex !== null }
                handleClose={ () => setOpenPhotoIndex(null) }
                initialPhotoIndex={ openPhotoIndex }
                photos={ state.photos }
            />
        </ProfileLayout>
    )
}

export default Photos
