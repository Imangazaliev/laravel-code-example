import { Styles } from 'jss'

import { jss }  from 'app/core/jss'
import theme from 'app/theme'
import { flexBoxMixin, shapes, colors, mediaQueries, marginStart, marginEnd, paddingStart } from 'app/styles'

const styles: Styles = {
    paper: {
        padding: theme.spacing(3),
        height: 'fit-content',

        [mediaQueries.phoneLarge]: {
            width: '100%',
            maxWidth: '100%',
            height: 'auto',
            padding: theme.spacing(2),
        },
    },

    list_wrapper: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(3),
        paddingBottom: theme.spacing(3),
        borderBottom: `1px solid ${theme.palette.grey[100]}`,
    },

    list: {
        '& li': {
            position: 'relative',
            marginBottom: 4,
            ...flexBoxMixin({
                alignment: 'center',
                flexWrap: 'wrap',
            }),
            [paddingStart]: 15,
            fontSize: 16,
            color: theme.palette.grey[700],
            lineHeight: '24px',

            '&:before': {
                content: '" "',
                position: 'absolute',
                left: 0,
                top: 'calc(0 - 6px)',
                ...flexBoxMixin({
                    alignment: 'center',
                    justifyContent: 'center',
                }),
                [marginEnd]: theme.spacing(1),
                width: 6,
                height: 6,
                borderRadius: '50%',
                backgroundColor: theme.palette.primary.main,
            },

            '& .link': {
                [mediaQueries.phoneSmall]: {
                    fontSize: 14,
                },
            },

            [mediaQueries.phoneSmall]: {
                fontSize: 14,
                lineHeight: '20px',
            },
        },
    },

    photo_item: {
        display: 'flex',

        '& + $photo_item': {
            marginTop: theme.spacing(3),
            paddingTop: theme.spacing(3),
            borderTop: `1px solid ${theme.palette.grey[100]}`,
        },

        [mediaQueries.tablets] : {
            flexDirection: 'column',
            alignItems: 'center',
        },
    },

    photo_image: {
        [marginEnd]: theme.spacing(3),

        '&:hover': {
            cursor: 'pointer',
        },

        [mediaQueries.phoneMedium]: {
            [marginEnd]: 0,
            marginBottom: theme.spacing(2),
        },
    },

    photo: {
        width: 152,
        height: 152,
        borderRadius: 4,
        border: `1px solid #e7eff5`,
        overflow: 'hidden',

        '& img': {
            width: '100%',
            height: '100%',
            'object-fit': 'cover',
        },
    },

    photo_being_processed: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },

    photo_body: {
        width: '100%',
        ...flexBoxMixin({
            direction: 'column',
        }),
    },

    photo_head: {
        ...flexBoxMixin({
            alignment: 'center',
        }),
        marginBottom: theme.spacing(2),
    },

    status_chip: {
        [mediaQueries.phoneSmall]: {
            display: 'flex',
            justifyContent: 'center',
        },
    },

    photo_controls: {
        display: 'flex',
        marginTop: theme.spacing(2),

        [mediaQueries.phoneSmall]: {
            flexDirection: 'column',
        },

        '& > * + *': {
            [marginStart]: theme.spacing(1),

            [mediaQueries.phoneSmall]: {
                width: '100%',
                marginTop: theme.spacing(1),
                [marginStart]: 0,
            },
        },
    },

    star_icon: {
        [marginEnd]: theme.spacing(1),
        fontSize: 22,
        height: 22,
        fill: colors.orange[100],
    },

    set_main_photo_icon: {
        fontSize: 15,
        fill: theme.palette.primary.main,
    },

    trash_icon: {
        fontSize: 16,
        fill: theme.palette.error.main,
    },

    outlined_red: {
        borderColor: colors.red[50],

        '&:hover': {
            borderColor: theme.palette.error.main,
            backgroundColor: theme.palette.error.light,
        },
    },

    outlined_red_label: {
        color: theme.palette.error.main,
    },

    control_label: {
        fontWeight: 500,
        fontSize: 14,
    },

    photo_dropzone: {
        width: '100%',
        height: 198,
        ...flexBoxMixin({
            direction: 'column',
            alignment: 'center',
            justifyContent: 'center',
        }),
        borderRadius: theme.shape.borderRadius,
        backgroundColor: theme.palette.grey[50],
        border: `1px dashed ${ theme.palette.grey[200] }`,

        [mediaQueries.phoneMedium]: {
            padding: theme.spacing(2),
        },
    },

    photo_dropzone_icon: {
        marginBottom: 20,
        height: 64,
        fontSize: 64,
        fill: theme.palette.grey[200],
    },

    photo_success_upload_icon: {
        fill: theme.palette.primary.main,
    },

    photo_error_upload_icon: {
        fill: theme.palette.error.main,
    },

    dropzone_title: {
        marginBottom: theme.spacing(1),
        color: theme.palette.grey[500],
    },

    dropzone_subtitle: {
        color: theme.palette.grey[400],
        textAlign: 'center',

        [mediaQueries.phoneMedium]: {
            fontSize: 14,
            lineHeight: '21px',
            textAlign: 'center',
        },
    },

    modal: {
        width: '100%',
        maxWidth: 520,
        margin: 'auto',
        ...flexBoxMixin({
            alignment: 'center',
            justifyContent: 'center',
        }),
    },

    modalPaper: {
        flex: '1 1 auto',
        backgroundColor: theme.palette.common.white,
        boxShadow: '0px 4px 12px rgba(0, 0, 0, 0.1), 0px 0px 3px rgba(0, 0, 0, 0.06)',
        borderRadius: shapes.small,
    },

    modalHead: {
        position: 'relative',
        padding: [0, theme.spacing(3)],
        height: theme.spacing(6),
        borderBottom: `1px solid ${ theme.palette.grey[100] }`,
        ...flexBoxMixin({
            alignment: 'center',
        }),
    },

    modalClose: {
        position: 'absolute',
        right: 0,
        top: '50%',
        transform: 'translate(-50%, -50%)',
        height: 20,
        fontSize: 20,
        fill: theme.palette.grey[400],
    },

    modalBody: {
        padding: theme.spacing(3),
    },

    modalFooter: {
        ...flexBoxMixin({
            alignment: 'center',
            justifyContent: 'flex-end',
        }),
    },

    modalCancel: {
        width: 89,
    },

    modalApply: {
        [marginStart]: theme.spacing(1),
        width: 120,
    },

    question_icon: {
        [marginStart]: theme.spacing(1),
        height: 20,
        fontSize:  20,
        fill: theme.palette.grey[400],
    },

    photo_loading: {
        width: '100%',
        maxWidth: 340,
        ...flexBoxMixin({
            direction: 'column',
            alignment: 'center',
        }),
    },

    progress: {
        marginTop: theme.spacing(2),
        width: '100%',
        borderRadius: 2,
        backgroundColor: theme.palette.grey[200],
    },
}

export default jss.createStyleSheet(styles).attach()
