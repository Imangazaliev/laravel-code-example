import React from 'react'
import classNames from 'classnames'

import { Icon, Text } from 'app/components'

import styles from './styles'

const { classes } = styles

const PhotoUploadError = () => {
    return <>
        <div className={ classNames(classes.photo_dropzone_icon, classes.photo_error_upload_icon) }>
            <Icon name="upload-error" />
        </div>
        <div>
            <Text bold className={ classes.dropzone_title }>Ошибка загрузки</Text>
        </div>
    </>
}

export { PhotoUploadError }
