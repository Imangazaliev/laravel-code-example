import React from 'react'
import classNames from 'classnames'

import { Icon, Text } from 'app/components'

import styles from './styles'

const { classes } = styles

const PhotoSuccessUpload = () => {
    return <>
        <div className={ classNames(classes.photo_dropzone_icon, classes.photo_success_upload_icon) }>
            <Icon name="upload-success" />
        </div>
        <div>
            <Text bold className={ classes.dropzone_title }>Фотография загружена</Text>
        </div>
    </>
}

export { PhotoSuccessUpload }
