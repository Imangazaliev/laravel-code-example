import React, { FC } from 'react'

import { Icon, LinearProgress, Text } from 'app/components'

import styles from './styles'

const { classes } = styles

interface PhotoLoadingProps {
    progress: number
}

const PhotoUploading: FC<PhotoLoadingProps> = ({
    progress,
}) => {
    return <>
        <div className={ classes.photo_dropzone_icon }>
            <Icon name="uploading" />
        </div>
        <div className={ classes.photo_loading }>
            <Text bold className={ classes.dropzone_title }>Загрузка фотографии:</Text>
            <LinearProgress
                variant="determinate"
                value={ progress  }
                color="primary"
                className={ classes.progress }
            />
        </div>
    </>
}

export { PhotoUploading }
