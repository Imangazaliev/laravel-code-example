export enum TUserPhotoStatus {
    PENDING_MODERATION = 'pending-moderation',
    ACCEPTED = 'accepted',
    REJECTED = 'rejected',
}

export type TUserPhoto = {
    id: number
    url: string
    caption: string
    uploaded_at: string
    status: TUserPhotoStatus
    is_main: boolean
    is_being_processed: boolean
}
