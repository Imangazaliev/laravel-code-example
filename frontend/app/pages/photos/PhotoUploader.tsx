import React, { FC } from 'react'

import { Icon, Text } from 'app/components'

import styles from './styles'

const { classes } = styles

interface PhotoUploaderProps {
    getInputProps: any
}

const PhotoUploader: FC<PhotoUploaderProps> = ({
    getInputProps,
}) => {
    return <>
        <input type="file" { ...getInputProps() }  hidden/>
        <div className={ classes.photo_dropzone_icon }>
            <Icon name="upload-image" />
        </div>
        <Text bold className={ classes.dropzone_title }>Загрузить фотографию:</Text>
        <Text className={ classes.dropzone_subtitle }>Перетащите фото в эту область, либо нажмите на нее и выберите картинку.</Text>
    </>
}

export { PhotoUploader }
