import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import classNames from 'classnames'

import { Button, Icon, Spinner, Text } from 'app/components'
import { StatusChip } from './StatusChip'

import { TUserPhoto, TUserPhotoStatus } from './types'

import styles from './styles'

const { classes } = styles

interface PhotoItemProps {
    photo: TUserPhoto
    onPhotoDelete: (photo_id: number) => void
    onSetMainPhoto: (photo_id: number) => void
    onPhotoClick: () => void
}

const PhotoItem: FC<PhotoItemProps> = ({
    photo,
    onPhotoDelete,
    onSetMainPhoto,
    onPhotoClick,
}) => {
    const { t } = useTranslation()

    return (
        <div className={ classes.photo_item }>
            <div className={ classes.photo_image } onClick={ onPhotoClick }>
                <div
                    className={ classNames(classes.photo, {
                        [classes.photo_being_processed]: photo.is_being_processed,
                    }) }
                >
                    {
                        photo.is_being_processed &&
                        <Spinner />
                    }
                    {
                        ! photo.is_being_processed &&
                        <img src={ photo.url as string } alt="Фотография" />
                    }
                </div>
            </div>
            <div className={ classes.photo_body }>
                {
                    photo.is_main &&
                    <div className={ classes.photo_head }>
                        <div className={ classes.star_icon }>
                            <Icon name="star"/>
                        </div>
                        <Text bold size="large">{ t('photo:main-photo') }</Text>
                    </div>
                }
                <div className={ classes.status_chip }>
                    <StatusChip isBeingProcessed={ photo.is_being_processed } status={ photo.status } />
                </div>
                <div className={ classes.photo_controls }>
                    {
                        ! photo.is_being_processed &&
                        <Button
                            size="small"
                            variant="outlined"
                            color="secondary"
                            className={ classes.button_remove }
                            classes={{
                                root: classes.outlined_red,
                                label: classNames(classes.outlined_red_label, classes.control_label),
                            }}
                            startIcon={ <Icon name="trash" className={ classes.trash_icon } /> }
                            onClick={ () => onPhotoDelete(photo.id) }
                        >
                            { t('actions.delete') }
                        </Button>
                    }
                    {
                        ! photo.is_main &&
                        ! photo.is_being_processed &&
                        photo.status === TUserPhotoStatus.ACCEPTED &&
                        <Button
                            size="small"
                            variant="outlined"
                            color="primary"
                            className={ classes.button_set_main_photo }
                            classes={{
                                label: classes.control_label,
                            }}
                            startIcon={ <Icon name="star" className={ classes.set_main_photo_icon } /> }
                            onClick={ () => onSetMainPhoto(photo.id) }
                        >
                            { t('photo:main-photo') }
                        </Button>
                    }
                </div>
            </div>
        </div>
    )
}

export { PhotoItem }
