import React, { FC } from 'react'

import { Chip } from 'app/components'

import { TUserPhotoStatus } from './types'

const colors: any = {
    [TUserPhotoStatus.PENDING_MODERATION]: 'orange',
    [TUserPhotoStatus.ACCEPTED]: 'green',
    [TUserPhotoStatus.REJECTED]: 'red',
}

const titles: any = {
    [TUserPhotoStatus.PENDING_MODERATION]: 'Ожидает модерации',
    [TUserPhotoStatus.ACCEPTED]: 'Принято',
    [TUserPhotoStatus.REJECTED]: 'Отклонено',
}

interface StatusChipProps {
    isBeingProcessed: boolean
    status: TUserPhotoStatus
}

const StatusChip: FC<StatusChipProps> = ({
    isBeingProcessed,
    status,
}) => {
    let color, label

    if (isBeingProcessed) {
        color = 'orange'
        label = 'Обрабатывается'
    } else {
        color = colors[status]
        label = titles[status]
    }

    return <Chip
        color={ color }
        label={ label }
    />
}

export { StatusChip }
