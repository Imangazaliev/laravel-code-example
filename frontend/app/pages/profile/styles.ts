import { createUseStyles } from 'react-jss'

import theme from 'app/theme'
import { flexBoxMixin, mediaQueries } from 'app/styles'

export const useStyles = createUseStyles({
    paper: {
        padding: theme.spacing(3),
        minHeight: 600,

        [mediaQueries.phoneMedium]: {
            padding: theme.spacing(2),
        },
    },

    center: {
        ...flexBoxMixin({
            alignment: 'center',
            justifyContent: 'center',
        }),
    },

    head: {
        marginBottom: theme.spacing(3),
    },

    requests: {
        display: 'grid',
        gridTemplateColumns: '1fr',
        gridTemplateRows: 'repeat(auto-fit, 120px)',
        gap: `${ theme.spacing(2)}px`,

        [mediaQueries.phoneLarge]: {
            gridTemplateRows: 'none',
        },
    },

    button_red: {
        border: `1px solid ${ theme.palette.error.main }`,

        '&:hover': {
            backgroundColor: theme.palette.error.main,

            '& $button_label': {
                color: theme.palette.common.white,
            },
        },
    },

    button_label: {
        color: theme.palette.error.main,
    },

    action_buttons: {
        display: 'grid',
        gridTemplateColumns: '115px 115px',
        gap: `${ theme.spacing(1) }px`,

        [mediaQueries.phoneLarge]: {
            gridTemplateColumns: '1fr 1fr',
        },
    },

    result_subtitle: {
        maxWidth: 552,
        lineHeight: '24px',
    },
})
