import React, { FC, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'

import { useCurrentUser, useMediaQuery } from 'app/hooks'

import { ProfileLayout } from 'app/components/layout'
import { Alerts, Tips } from 'app/components/profile'
import { ProfileCompletion } from './components'

import { TCurrentUser } from 'app/types/user'

const Profile: FC = () => {
    const { t } = useTranslation()

    const isPhone = useMediaQuery('phoneLarge')

    const user = useCurrentUser() as TCurrentUser

    useEffect(() => {
        if (sessionStorage.getItem('permission_denied') !== null) {
            toast.error(t('errors.permission-denied'))

            sessionStorage.removeItem('permission_denied')
        }
    }, [t])

    return (
        <ProfileLayout hideSidebarOnMobile={ false } title={ t('profile') }>
            <section>
                <ProfileCompletion user={ user }/>

                {
                    ! isPhone &&
                    <>
                        <Tips />
                        <Alerts user={ user }/>
                    </>
                }
            </section>
        </ProfileLayout>
    )
}

export { Profile }
