import React from 'react'
import { Link } from 'react-router-dom'

import { Result, Text } from 'app/components'
import { ErrorPageLayout } from 'app/components/layout'

import styles from './styles'

const InternalServerError = () => {
    return (
        <ErrorPageLayout>
            <Result
                classes={{
                    root: styles.classes.result_root,
                    title: styles.classes.errorNumber,
                    subtitle: styles.classes.subtitle,
                    content: styles.classes.result_content,
                }}
                title="403"
                subTitle="Отказано в доступе"
            >
                <Text className={ styles.classes.text }>Вы запросили страницу, доступ к которой ограничен специальными правами. Возможно, это закрытая страница или личные файлы пользователя.</Text>
                <Link to="/" className="link" >Перейти на главную страницу</Link>
            </Result>
        </ErrorPageLayout>
    )
}

export { InternalServerError }
