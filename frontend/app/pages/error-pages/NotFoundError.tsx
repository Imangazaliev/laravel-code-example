import React from 'react'
import classNames from 'classnames'
import { Link } from 'react-router-dom'

import { Result, Text } from 'app/components'
import { ErrorPageLayout } from 'app/components/layout'

import styles from './styles'

const NotFoundError = () => {

    const heroFemaleClasses = classNames(styles.classes.hero_img, styles.classes.hero_img_female)
    const heroMaleClasses = classNames(styles.classes.hero_img, styles.classes.hero_img_male)

    return (
        <ErrorPageLayout>
            <div className={ heroFemaleClasses }>
                <img src="/images/male.svg"/>
            </div>
            <Result
                classes={{
                    root: styles.classes.result_root,
                    title: styles.classes.errorNumber,
                    subtitle: styles.classes.subtitle,
                    content: styles.classes.result_content,
                }}
                title="404"
                subTitle="Страница не найдена"
            >
                <Text className={ styles.classes.text }>Страница, на которую вы хотели перейти, не найдена. Возможно, введен некоректный адрес или страница была удалена.</Text>
                <Link to="/" className="link" >Перейти на главную страницу</Link>
            </Result>
            <div className={ heroMaleClasses }>
                <img src="/images/female.svg"/>
            </div>
        </ErrorPageLayout>
    )
}

export { NotFoundError }
