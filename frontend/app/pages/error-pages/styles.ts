import { Styles } from 'jss'

import { jss }  from 'app/core/jss'
import { flexBoxMixin, mediaQueries } from 'app/styles'
import theme from 'app/theme'

const styles: Styles = {
    result_root: {
        margin: `0 ${theme.spacing(3)}`,
    },

    hero_img: {
        '& img': {
            display: 'block',
            height: 'auto',
            width: '100%',
        },
    },

    hero_img_female: {
        maxWidth: 325,
        minWidth: 325,
        maxHeight:  553,

        [mediaQueries.tablets]: {
            minWidth:  'inherit',
        },

        [mediaQueries.phoneLarge]: {
            position: 'absolute',
            bottom: - 154,
            maxWidth: 110,
            height: 348,
            left: 130,
        },

        [mediaQueries.phoneMedium]: {
            left: "15%",
        },

        '@media (max-width: 320px)': {
            bottom: - 200,
        },
    },

    hero_img_male: {
        maxWidth: 415,
        minWidth: 415,
        maxHeight:  553,

        [mediaQueries.tablets]:{
            minWidth:  'inherit',
        },

        [mediaQueries.phoneLarge]: {
            position: 'absolute',
            bottom: - 165,
            maxWidth: 120,
            height: 353,
            right: 130,
        },

        [mediaQueries.phoneMedium]: {
            right: '15%',
        },

        '@media (max-width: 320px)': {
            bottom: - 207,
        },
    },

    errorNumber: {
        marginBottom: theme.spacing(3),
        fontSize: 156,
        fontWeight: 800,
        lineHeight: 1,
        color: theme.palette.grey[600],
    },

    subtitle: {
        maxWidth: '100%',
        fontSize: 36,
        fontWeight: 'bold',
        color: theme.palette.grey[800],
    },

    result_content: {
        ...flexBoxMixin({
            direction: 'column',
            alignment: 'center',
            justifyContent: 'center',
        }),
    },

    text: {
        marginBottom: theme.spacing(2),
        textAlign: 'center',
    },
}

export default jss.createStyleSheet(styles).attach()
