import { Styles } from 'jss'

import { jss }  from 'app/core/jss'
import { flexBoxMixin, mediaQueries } from 'app/styles'
import theme from 'app/theme'

const styles: Styles = {
    paper: {
        width: '100%',
        maxWidth: 744,
        padding: `${theme.spacing(4)}px`,
        margin: `${theme.spacing(5)}px auto`,

        [mediaQueries.phoneMedium]: {
            padding: `${theme.spacing(3)}px ${theme.spacing(2)}px`,
        },
    },

    button: {
        width: 195,
        fontSize: 16,
    },

    result_content: {
        ...flexBoxMixin({
            alignment: 'center',
            direction: 'column',
        }),
    },
}

export default jss.createStyleSheet(styles).attach().classes
