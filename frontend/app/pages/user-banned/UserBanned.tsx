import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'

import http from 'app/core/axios'
import { useCurrentUser, useFormatTime } from 'app/hooks'

import { MainLayout } from 'app/components/layout'
import { Button, Text, Paper, Result, Spinner, MultilineParagraph } from 'app/components'

import { TCurrentUser } from 'app/types/user'

import { DATE_TIME_HUMAN_FORMAT } from 'app/constants/dates'

import classes from './styles'

type TBanInfo = {
    reasons: string[]
    banned_at: string
    unban_at: string | null
    comment: string | null
}

const UserBanned = () => {
    const { t } = useTranslation()

    const currentUser = useCurrentUser() as TCurrentUser

    const [banInfo, setBanInfo] = useState<TBanInfo | null>(null)

    useEffect(() => {
        loadBanInfo()

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const loadBanInfo = async () => {
        try {
            const response = await http.get<TBanInfo>(ApiEndpoints.PROFILE_BAN_INFO)

            setBanInfo(response.data)
        } catch (error: any) {
            toast.error(t('errors.error-loading-data'))
        }
    }

    const bannedAt = useFormatTime(banInfo?.banned_at, DATE_TIME_HUMAN_FORMAT)
    const unbanAt = useFormatTime(banInfo?.unban_at, DATE_TIME_HUMAN_FORMAT)

    return (
        <MainLayout>
            <Paper className={ classes.paper }>
                <Result
                    status="user-block"
                    title={ unbanAt !== null ? t('errors.profile-temporary-banned') : t('errors.profile-banned') }
                    classes={{
                        content: classes.result_content,
                    }}
                >
                    <Text align="center" bold>
                        {
                            t('profile:your-id', {
                                id: currentUser.id,
                            })
                        }
                    </Text>
                    {
                        banInfo === null &&
                        <Spinner />
                    }
                    {
                        banInfo !== null &&
                        <>
                            <div className="mt-16 d-flex flex-column align-items-center">
                                <Text align="center" bold>{ t('ban-reasons') }</Text>
                                <ul className="list">
                                    {
                                        banInfo.reasons.map(reason => (
                                            <li>{ reason }</li>
                                        ))
                                    }
                                </ul>
                            </div>
                            <div className="mb-8 d-flex flex-column align-items-center">
                                <Text align="center" bold>{ t('ban-date') }</Text>
                                <Text align="center">{ bannedAt }</Text>
                            </div>
                            {
                                unbanAt !== null &&
                                <div className="mb-8 d-flex flex-column align-items-center">
                                    <Text align="center" bold>{ t('unban-date') }</Text>
                                    <Text align="center">{ unbanAt }</Text>
                                </div>
                            }
                            {
                                banInfo.comment !== null &&
                                <div className="mb-8 d-flex flex-column align-items-center">
                                    <Text align="center" bold>Комментарий</Text>
                                    <Text align="center">{ banInfo.comment }</Text>
                                </div>
                            }
                        </>
                    }
                    <MultilineParagraph content={ t('ban-description') } />
                    <Button
                        className={ classes.button }
                        color="primary"
                        onClick={ window.contactSupport }
                        variant="contained"
                        weight="bold"
                    >
                        { t('support') }
                    </Button>
                </Result>
            </Paper>
        </MainLayout>
    )
}

export { UserBanned }
