import React, { FC, useState, useEffect } from 'react'
import i18n from 'i18next'
import { useTranslation, Trans } from 'react-i18next'
import { Link } from 'react-router-dom'
import { FormikProps, FormikErrors } from 'formik'
import { string } from 'yup'
import classNames from 'classnames'
import { toast } from 'react-toastify'

import http from 'app/core/axios'
import { getBrowserFingerprint } from 'app/helpers'
import { useQueryParameters } from 'app/hooks'

import { InnerWrapper, MainLayout } from 'app/components/layout'
import {
    Input,
    Label,
    Paper,
    InputAdornment,
    FormControlLabel,
    FormGroup,
    Checkbox,
    Button,
    Form,
    Field,
    Title,
} from 'app/components'

import { TCurrentUser } from 'app/types/user'

import styles from 'app/styles/styles'

type LoginFormData = {
    phone_number_or_email: string
    password: string
    remember: boolean
}

const validationSchema = {
    phone_number_or_email: string().required().label(i18n.t('auth:phone-number-or-email')),
    password: string().required().label(i18n.t('password')),
}

const Login: FC = () => {
    const { t } = useTranslation()

    let [showPassword, togglePassword] = useState<Boolean>(false)
    let [fingerprint, setFingerprint] = useState<string | null>(null)

    useEffect(() => {
        getBrowserFingerprint()
            .then(fingerprint => {
                setFingerprint(fingerprint)
            })
    }, [])

    const queryParameters = useQueryParameters()

    const handleTogglePassword = () => {
        togglePassword(prevState => showPassword = ! prevState)
    }

    const handleLoginSubmit = async (
        values: LoginFormData,
        setErrors: (errors: FormikErrors<LoginFormData>) => void,
        setSubmitting: (isSubmitting: boolean) => void
    ) => {
        try {
            setSubmitting(true)

            await http.post<TCurrentUser>(ApiEndpoints.LOGIN, {
                ...values,
                by_token: queryParameters.token !== undefined ? true : undefined,
                fingerprint,
            })

            setSubmitting(false)

            location.href = '/profile'
        } catch (error: any) {
            setSubmitting(false)

            if (error.response !== undefined) {
                if (error.response.status === 422) {
                    setErrors(error.response.data.validation_errors)

                    return
                }

                if (error.response.data.message !== undefined) {
                    toast.error(error.response.data.message)

                    return
                }
            }

            toast.error(t('error'))
        }
    }

    const initialSchema: LoginFormData = {
        phone_number_or_email: queryParameters.phone_number ?? '',
        password: queryParameters.token ?? '',
        remember: false,
    }

    return (
        <MainLayout title={ t('auth:log-in') }>
            <InnerWrapper>
                <Paper className={ classNames(styles.classes.paper, "d-flex flex-column align-items-center justify-content-center") }>
                    <div className={ classNames(styles.classes.wrapper, styles.classes.wrapper_center) }>
                        <Title component="h4" noMarginTop>{ t('auth:log-in') }</Title>
                        <div className="paper__text mb-32">
                            <Trans
                                components={ [
                                    <Link to={ Routes.REGISTER } className="link" />,
                                ] }
                                i18nKey="auth:do-not-have-account"
                            />
                        </div>
                        <Form
                            className="d-flex flex-column"
                            onSubmit={ handleLoginSubmit }
                            values={ initialSchema }
                            validationSchema={ validationSchema }
                        >
                            {
                                ({ handleChange, isSubmitting, setFieldValue, values }: FormikProps<LoginFormData>) => (
                                    <div className="d-flex flex-column">
                                        <FormGroup>
                                            <Label htmlFor="phone_number_or_email">{ t('auth:phone-number-or-email') }</Label>
                                            <Field
                                                id="phone_number_or_email"
                                                name="phone_number_or_email"
                                                type="text"
                                                onChange={ handleChange }
                                                placeholder={ t('auth:phone-number-or-email') }
                                                component={ Input }
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Label htmlFor="password">{ t('password') }</Label>
                                            <Field
                                                id="password"
                                                name="password"
                                                type={ showPassword ? 'text' : 'password' }
                                                onChange={ handleChange }
                                                placeholder={ t('password') }
                                                endAdornment={
                                                    <InputAdornment position="end">
                                                        <button
                                                            type="button"
                                                            className="link input__button"
                                                            onClick={ handleTogglePassword }
                                                        >
                                                            { showPassword ? t('auth:password.hide') : t('auth:password.show') }
                                                        </button>
                                                    </InputAdornment>
                                                }
                                                component={ Input }
                                            />
                                        </FormGroup>
                                        <div className="mb-24 d-flex">
                                            <FormControlLabel
                                                name="remember"
                                                onChange={ () => setFieldValue('remember', ! values.remember) }
                                                control={
                                                    <Checkbox
                                                        inputProps={{
                                                            id: "remember",
                                                            name: "remember",
                                                        }}
                                                        checked={ values.remember }
                                                    />
                                                }
                                                label={ t('auth:remember-me') }
                                            />
                                        </div>
                                        <div>
                                            <Button
                                                color="primary"
                                                disabled={ isSubmitting }
                                                fullWidth
                                                size="large"
                                                textTransform="uppercase"
                                                type="submit"
                                                variant="contained"
                                                weight={ 500 }
                                            >{ t('auth:log-in') }</Button>
                                        </div>
                                        <Link to={ Routes.RESET_PASSWORD_BY_PHONE_NUMBER_PAGE } className="link mt-24">{ t('auth:forgot-password') }</Link>
                                        {/*<div className="paper__text mb-16">{ t('auth:or-log-in-with') }</div>*/}
                                    </div>
                                )
                            }
                        </Form>
                    </div>
                </Paper>
            </InnerWrapper>
        </MainLayout>
    )
}

export { Login }
