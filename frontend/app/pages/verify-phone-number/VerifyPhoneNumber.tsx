import React, { FC, useCallback, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import i18n from 'i18next'
import { useTranslation, Trans } from 'react-i18next'
import { RouteComponentProps } from 'react-router'
import { Link } from 'react-router-dom'
import { FormikErrors } from 'formik'
import { differenceInSeconds, parseISO } from 'date-fns'
import { string } from 'yup'
import { toast } from 'react-toastify'
import classNames from 'classnames'

import http from 'app/core/axios'
import { useCurrentUser } from 'app/hooks'
import { setUser } from 'app/store/modules/user'

import { MainLayout } from 'app/components/layout'
import {
    Input,
    Label,
    Paper,
    FormGroup,
    Form,
    Field,
    Text,
    Button,
    Result,
    SupportChatLink,
} from 'app/components'

import { TCurrentUser } from 'app/types/user'

import globalStyles from 'app/styles/styles'

import styles from './styles'

const { classes } = styles

const validationSchema = {
    code: string().required().label(i18n.t('auth:verification-code')),
}

const initialValues = {
    code: '',
}

type TVerificationCodeResendInfo = {
    available_at: string | null
}

type TVerifyPhoneNumberForm = {
    code: string
}

const VerifyPhoneNumber: FC<RouteComponentProps> = ({
    history,
}) => {
    const { t } = useTranslation()
    const dispatch = useDispatch()

    const user = useCurrentUser() as TCurrentUser

    const [resendRemainingTime, setResendRemainingTime] = useState(0)
    const [resendTimerExecuted, setResendTimerExecuted] = useState(false)

    useEffect(() => {
        if (resendRemainingTime > 0 && ! resendTimerExecuted) {
            setResendTimerExecuted(true)
        }
    }, [resendRemainingTime, resendTimerExecuted])

    useEffect(() => {
        if ( ! resendTimerExecuted) {
            return
        }

        const interval = setInterval(() => {
            setResendRemainingTime(value => {
                if (value === 1) {
                    setResendTimerExecuted(false)
                }

                return value - 1
            })
        }, 1000)

        return () => clearInterval(interval)
    }, [resendTimerExecuted])

    const loadVerificationCodeResendInfo = useCallback(async () => {
        try {
            const response = await http.get<TVerificationCodeResendInfo>(ApiEndpoints.VERIFICATION_CODE_RESEND_INFO)

            if (response.data.available_at === null) {
                setResendRemainingTime(0)

                return
            }

            const availableAt = parseISO(response.data.available_at)
            const currentTime = new Date()

            if (availableAt > currentTime) {
                const remainingTime = differenceInSeconds(availableAt, currentTime)

                setResendRemainingTime(remainingTime)
            }
        } catch (error: any) {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        }
    }, [t])

    useEffect(() => {
        loadVerificationCodeResendInfo()
    }, [loadVerificationCodeResendInfo])

    const handleConfirmPhoneNumberSubmit = useCallback(async (
        values: TVerifyPhoneNumberForm,
        setErrors: (errors: FormikErrors<TVerifyPhoneNumberForm>) => void,
        setSubmitting: (isSubmitting: boolean) => void
    ) => {
        try {
            setSubmitting(true)

            await http.post(ApiEndpoints.CONFIRM_PHONE_NUMBER, values)

            setSubmitting(false)

            const profileResponse = await http.get(ApiEndpoints.PROFILE)

            dispatch(setUser(profileResponse.data))

            history.push('/register/rules')
        } catch (error: any) {
            setSubmitting(false)

            if (error.response !== undefined && error.response.status === 422) {
                setErrors(error.response.data.validation_errors)

                return
            }

            toast.error(t('error'))
        }
    }, [dispatch, history, t])

    const resendVerificationCode = useCallback(async () => {
        try {
            await http.post(ApiEndpoints.RESEND_VERIFICATION_CODE)

            toast.success(t('auth:code-sent'))
        } catch (error: any) {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        } finally {
            loadVerificationCodeResendInfo()
        }
    }, [loadVerificationCodeResendInfo, t])

    return (
        <MainLayout>
            <Paper className={ globalStyles.classes.paper }>
                <Result
                    title={ t('auth:phone-number-verification') }
                    classes={{
                        root: globalStyles.classes.wrapper,
                        title: classes.root_paper_title,
                        content: globalStyles.classes.content,
                    }}
                >
                    <Form
                        className={ classes.paper_form }
                        onSubmit={ handleConfirmPhoneNumberSubmit }
                        values={ initialValues }
                        validationSchema={ validationSchema }
                    >
                        {
                            ({ handleChange, isSubmitting }) => (
                                <>
                                    <div className="mb-32">
                                        <Text align="center" className="mb-8">
                                            <Trans
                                                i18nKey="auth:phone-number-verification.description"
                                                values={{
                                                    phoneNumber: user.phone_number,
                                                }}
                                            />
                                        </Text>
                                        <Text align="center">
                                            <Link to={ Routes.REGISTER_WRONG_PHONE_NUMBER } className="link">
                                                { t('auth:entered-wrong-phone-number') }
                                            </Link>
                                        </Text>
                                    </div>
                                    <FormGroup>
                                        <Label htmlFor="code">{ t('auth:verification-code') }</Label>
                                        <Field
                                            autoFocus
                                            type="text"
                                            id="code"
                                            name="code"
                                            placeholder={ t('auth:verification-code.input-placeholder') }
                                            autoComplete="off"
                                            component={ Input }
                                            onChange={ handleChange }
                                        />
                                    </FormGroup>
                                    <div>
                                        <Button
                                            color="primary"
                                            disabled={ isSubmitting }
                                            fullWidth
                                            size="large"
                                            type="submit"
                                            variant="contained"
                                            weight={ 500 }
                                        >
                                            { t('auth:confirm-button-label') }
                                        </Button>
                                    </div>
                                    <div className="mt-16">
                                        <Button
                                            color="primary"
                                            disabled={ isSubmitting || resendRemainingTime > 0 }
                                            fullWidth
                                            onClick={ resendVerificationCode }
                                            size="large"
                                            variant="outlined"
                                            weight={ 500 }
                                        >
                                            { t('auth:resend-verification-code') }
                                        </Button>
                                    </div>
                                    {
                                        resendRemainingTime > 0 &&
                                        <Text align="center" className="mt-16" color="gray" size="small">
                                            { t('auth:resend-verification-code.resend-time', {
                                                seconds: resendRemainingTime,
                                            }) }
                                        </Text>
                                    }
                                </>
                            )
                        }
                    </Form>
                </Result>
            </Paper>
            <Paper
                className={ classNames(globalStyles.classes.paper, 'mt-16') }
                padding="sm"
            >
                <Text align="center">
                    <Trans
                        components={ [
                            <SupportChatLink />,
                        ] }
                        i18nKey="need-help"
                    />
                </Text>
            </Paper>
        </MainLayout>
    )
}

export { VerifyPhoneNumber }

