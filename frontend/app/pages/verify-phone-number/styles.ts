import { Styles } from 'jss'

import { jss } from 'app/core/jss'
import theme from 'app/theme'
import { flexBoxMixin, marginStart, mediaQueries } from 'app/styles'

const styles: Styles = {
    root_paper: {
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(6),
    },

    paper_form: {
        width: '100%',
        maxWidth: 360,
        margin: 'auto',
        ...flexBoxMixin({
            direction: 'column',
        }),
    },

    root_paper_title: {
        marginBottom: theme.spacing(3),
        textAlign: 'center',

        [mediaQueries.phoneLarge]: {
            fontSize: 24,
        },
    },

    modal: {
        width: '100%',
        maxWidth: 490,
        height: 170,
        margin: 'auto',
        borderRadius: 4,
        ...flexBoxMixin({
            alignment: 'center',
            justifyContent: 'center',
        }),
    },

    modal_paper: {
        position: 'relative',
        padding: theme.spacing(3),
        boxShadow: `0px 4px 12px rgba(0, 0, 0, 0.1), 0px 0px 3px rgba(0, 0, 0, 0.06)`,
    },

    modalFooter: {
        marginTop: theme.spacing(3),
        ...flexBoxMixin({
            alignment: 'center',
            justifyContent: 'flex-end',
        }),
    },

    modalClose: {
        position: 'absolute',
        right: 0,
        top: '15%',
        transform: 'translate(-50%, -50%)',
        height: 20,
        fontSize: 20,
        fill: theme.palette.grey[400],
        cursor: 'pointer',
    },

    modalParagraph: {
        marginTop: theme.spacing(1),
        color: theme.palette.grey[600],
    },

    cancelButton: {
        padding: `0px ${theme.spacing(2)}px`,
        width: 80,
    },

    sendButton: {
        padding: `0px ${theme.spacing(1)}px`,
        width: 93,
        [marginStart]: theme.spacing(2),
    },
}

export default jss.createStyleSheet(styles).attach()
