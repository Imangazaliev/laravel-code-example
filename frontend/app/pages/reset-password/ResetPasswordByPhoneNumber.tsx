import React, { FC, useMemo } from 'react'
import { Link, Redirect, useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import classNames from 'classnames'
import { FormikErrors, FormikProps } from 'formik'
import { string } from 'yup'
import { toast } from 'react-toastify'
import { History } from 'history'

import http from 'app/core/axios'

import { MainLayout } from 'app/components/layout'
import { Paragraph, Field, Form, FormGroup, Input, Label, Button, Paper, PhoneNumberInput, Result } from 'app/components'

import styles from 'app/styles/styles'

const { classes } = styles

type THistoryState = {
    phone_number?: string
    phone_number_country_code?: string
    code?: string
}

const ResetPasswordByPhoneNumber = () => {
    const history = useHistory<THistoryState>()

    // redirect from result pages to the index page if the page was opened directly or by "Back" button click
    if (
        history.location.pathname !== Routes.RESET_PASSWORD_BY_PHONE_NUMBER_PAGE &&
        history.action !== 'PUSH'
    ) {
        return <Redirect to={ Routes.RESET_PASSWORD_BY_PHONE_NUMBER_PAGE } />
    }

    return (
        <MainLayout classes={{ container: classes.container}}>
            <Paper className={ classes.paper } >
                {
                    history.location.pathname === Routes.RESET_PASSWORD_BY_PHONE_NUMBER_PAGE &&
                    <ResetPasswordForm history={ history } />
                }
                {
                    history.location.pathname === Routes.RESET_PASSWORD_BY_PHONE_NUMBER_ENTER_CODE_PAGE &&
                    <EnterCode history={ history } />
                }
                {
                    history.location.pathname === Routes.RESET_PASSWORD_BY_PHONE_NUMBER_FAILED_PAGE &&
                    <ResultFailed />
                }
                {
                    history.location.pathname === Routes.RESET_PASSWORD_BY_PHONE_NUMBER_ENTER_NEW_PASSWORD_PAGE &&
                    <EnterNewPasswordForm history={ history } />
                }
                {
                    history.location.pathname === Routes.RESET_PASSWORD_BY_PHONE_NUMBER_PASSWORD_CHANGE_FAILED_PAGE &&
                    <PasswordChangeFailed />
                }
            </Paper>
        </MainLayout>
    )
}

type TResetPasswordFormData = {
    phone_number: string
    phone_number_country_code: string
}

interface ResetPasswordFormProps {
    history: History<THistoryState>
}

const ResetPasswordForm: FC<ResetPasswordFormProps> = ({
    history,
}) => {
    const validationSchema = useMemo(() => ({
        phone_number: string().required(),
        phone_number_country_code: string().required(),
    }), [])

    const initialValues: TResetPasswordFormData = {
        phone_number: '',
        phone_number_country_code: '',
    }

    const handleSubmit = async (
        values: TResetPasswordFormData,
        setErrors: (errors: FormikErrors<TResetPasswordFormData>) => void,
        setSubmitting: (isSubmitting: boolean) => void
    ) => {
        setSubmitting(true)

        try {
            await http.post(ApiEndpoints.RESET_PASSWORD_BY_PHONE_NUMBER_SEND, {
                phone_number: values.phone_number,
                phone_number_country_code: values.phone_number_country_code,
            })

            setSubmitting(false)

            history.push(Routes.RESET_PASSWORD_BY_PHONE_NUMBER_ENTER_CODE_PAGE, {
                phone_number: values.phone_number,
                phone_number_country_code: values.phone_number_country_code,
            })
        } catch (error: any) {
            setSubmitting(false)

            if (error.response !== undefined) {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors)

                    return
                }

                if (error.response.status === 400) {
                    setTimeout(() => {
                        toast.error(error.response.data.message)
                    }, 200)
                }

                if (error.response.status === 403) {
                    setTimeout(() => {
                        toast.error(error.response.data.message)
                    }, 200)
                }
            }

            history.push(Routes.RESET_PASSWORD_BY_PHONE_NUMBER_FAILED_PAGE)
        }
    }

    return (
        <Result
            status="forgot-icon"
            title="Забыли пароль?"
            classes={{
                root: `${classes.wrapper} ${classes.wrapper_center}`,
                subtitle: classes.block_text,
                icon: classes.icon,
            }}
        >
            <Paragraph className={ classes.block_text }>Укажите ваш номер телефона, под которым вы зарегистрированы на сайте и на него будет отправлена информацию о сбросе пароля.</Paragraph>
            <Form
                autoComplete="off"
                className={ classes.form }
                onSubmit={ handleSubmit }
                validationSchema={ validationSchema }
                values={ initialValues }
            >
                {
                    ({ isSubmitting , setFieldValue }: FormikProps<TResetPasswordFormData>) => (
                        <>
                            <FormGroup>
                                <Label htmlFor="phone_number">Номер телефона</Label>
                                <Field
                                    component={ PhoneNumberInput }
                                    name="phone_number"
                                    onChange={ (phoneNumber: string, countryCode: string | null) => {
                                        setFieldValue('phone_number', phoneNumber)
                                        setFieldValue('phone_number_country_code', countryCode)
                                    } }
                                />
                            </FormGroup>
                            <Button
                                disabled={ isSubmitting }
                                textTransform="uppercase"
                                type="submit"
                                color="primary"
                                variant="contained"
                            >
                                Отправить
                            </Button>
                        </>
                    )
                }
            </Form>
            <div>
                <Link to={ Routes.RESET_PASSWORD_BY_EMAIL_PAGE } className={ classes.link }>Сбросить по Email</Link>
            </div>
            <div className="mt-16">
                <Link to={ Routes.LOGIN } className={ classes.link }>Войти</Link>
            </div>
        </Result>
    )
}

type TEnterCodeFormData = {
    code: string
}

interface EnterCodeProps {
    history: History<THistoryState>
}

const EnterCode: FC<EnterCodeProps> = ({
    history,
}) => {
    const validationSchema = useMemo(() => ({
        code: string().required(),
    }), [])

    const initialValues: TEnterCodeFormData = {
        code: '',
    }

    const handleSubmit = async (
        values: TEnterCodeFormData,
        setErrors: (errors: FormikErrors<TEnterCodeFormData>) => void,
        setSubmitting: (isSubmitting: boolean) => void
    ) => {
        setSubmitting(true)

        try {
            await http.post(ApiEndpoints.RESET_PASSWORD_BY_PHONE_NUMBER_CHECK_CODE, {
                phone_number: history.location.state.phone_number,
                phone_number_country_code: history.location.state.phone_number_country_code,
                code: values.code,
            })

            setSubmitting(false)

            history.push(Routes.RESET_PASSWORD_BY_PHONE_NUMBER_ENTER_NEW_PASSWORD_PAGE, {
                phone_number: history.location.state.phone_number,
                phone_number_country_code: history.location.state.phone_number_country_code,
                code: values.code,
            })
        } catch (error: any) {
            setSubmitting(false)

            if (error.response !== undefined) {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors)

                    return
                }

                if (error.response.status === 400) {
                    setTimeout(() => {
                        toast.error(error.response.data.message)
                    }, 200)
                }
            }

            history.push(Routes.RESET_PASSWORD_BY_PHONE_NUMBER_PASSWORD_CHANGE_FAILED_PAGE)
        }
    }

    return (
        <Result
            status="forgot-icon"
            title="Забыли пароль?"
            classes={{
                root: `${classes.wrapper} ${classes.wrapper_center}`,
                subtitle: classes.block_text,
                icon: classes.icon,
            }}
        >
            <Paragraph className={ classes.block_text }>Мы отправили код для сброса пароля на номер { history.location.state.phone_number }.</Paragraph>
            <Form
                autoComplete="off"
                className={ classes.form }
                onSubmit={ handleSubmit }
                validationSchema={ validationSchema }
                values={ initialValues }
            >
                {
                    ({ isSubmitting }: FormikProps<TEnterCodeFormData>) => (
                        <>
                            <FormGroup>
                                <Label htmlFor="code">Код</Label>
                                <Field
                                    component={ Input }
                                    name="code"
                                    placeholder="Введите код"
                                    type="text"
                                />
                            </FormGroup>
                            <Button
                                disabled={ isSubmitting }
                                textTransform="uppercase"
                                type="submit"
                                color="primary"
                                variant="contained"
                            >
                                Отправить
                            </Button>
                        </>
                    )
                }
            </Form>
        </Result>
    )
}

const ResultFailed: FC = () => {
    return (
        <Result
            classes={{
                root: `${classes.wrapper} ${classes.wrapper_center}`,
                subtitle: classes.block_text,
                icon: classNames(classes.icon, classes.icon_failed),
                content: classes.content,
            }}
            status="error-circle"
            subTitle="При отправке кода на указанный номер возникла ошибка. Проверьте правильность написания номера или обратитесь в службу поддержки."
            title="Ошибка отправки"
        >
            <Button
                color="primary"
                href={ Routes.RESET_PASSWORD_BY_PHONE_NUMBER_PAGE }
                size="large"
                textTransform="uppercase"
                type="button"
                variant="outlined"
            >
                Другой номер телефона
            </Button>
        </Result>
    )
}

type TEnterNewPasswordFormData = {
    new_password: string
    new_password_confirmation: string
}

interface EnterNewPasswordFormProps {
    history: History<THistoryState>
}

const EnterNewPasswordForm: FC<EnterNewPasswordFormProps> = ({
    history,
}) => {
    const { t } = useTranslation()

    const validationSchema = useMemo(() => ({
        new_password: string().required().min(ENV.profileRules.minPasswordLength),
        new_password_confirmation: string().required(),
    }), [])

    const initialValues: TEnterNewPasswordFormData = {
        new_password: '',
        new_password_confirmation: '',
    }

    const handleSubmit = async (
        values: TEnterNewPasswordFormData,
        setErrors: (errors: FormikErrors<TEnterNewPasswordFormData>) => void,
        setSubmitting: (isSubmitting: boolean) => void
    ) => {
        setSubmitting(true)

        try {
            await http.post(ApiEndpoints.RESET_PASSWORD_BY_PHONE_NUMBER_RESET, {
                phone_number: history.location.state.phone_number,
                phone_number_country_code: history.location.state.phone_number_country_code,
                code: history.location.state.code,
                new_password: values.new_password,
                new_password_confirmation: values.new_password_confirmation,
            })

            setSubmitting(false)

            toast.info(t('auth:password.password-changed-alert'))

            history.push('/login')
        } catch (error: any) {
            setSubmitting(false)

            if (error.response !== undefined) {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors)

                    return
                }

                if (error.response.status === 400) {
                    setTimeout(() => {
                        toast.error(error.response.data.message)
                    }, 200)
                }
            }

            history.push(Routes.RESET_PASSWORD_BY_EMAIL_PASSWORD_CHANGE_FAILED_PAGE)
        }
    }

    return (
        <Result
            title="Сброс пароля"
            subTitle=""
            classes={{
                root: `${classes.wrapper} ${classes.wrapper_center}`,
                subtitle: classes.block_text,
                icon: classes.icon,
            }}
        >
            <Form
                autoComplete="off"
                className={ classes.form }
                onSubmit={ handleSubmit }
                validationSchema={ validationSchema }
                values={ initialValues }
            >
                {
                    ({ isSubmitting }: FormikProps<TEnterNewPasswordFormData>) => (
                        <>
                            <FormGroup>
                                <Label htmlFor="new_password">Пароль</Label>
                                <Field
                                    component={ Input }
                                    name="new_password"
                                    placeholder="Введите новый пароль"
                                    type="password"
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="new_password_confirmation">Пароль еще раз</Label>
                                <Field
                                    component={ Input }
                                    name="new_password_confirmation"
                                    placeholder="Повторите новый пароль"
                                    type="password"
                                />
                            </FormGroup>
                            <Button
                                disabled={ isSubmitting }
                                textTransform="uppercase"
                                type="submit"
                                color="primary"
                                variant="contained"
                            >
                                Сбросить
                            </Button>
                        </>
                    )
                }
            </Form>
        </Result>
    )
}

const PasswordChangeFailed: FC = () => {
    return (
        <Result
            classes={{
                root: `${classes.wrapper} ${classes.wrapper_center}`,
                subtitle: classes.block_text,
                icon: classNames(classes.icon, classes.icon_failed),
                content: classes.content,
            }}
            status="error-circle"
            subTitle="При сбросе пароля возникла ошибка. Попробуйте еще раз или обратитесь в службу поддержки."
            title="Ошибка при сбросе пароля"
        >
            <Button
                color="primary"
                href={ Routes.RESET_PASSWORD_BY_PHONE_NUMBER_PAGE }
                size="large"
                textTransform="uppercase"
                type="button"
                variant="outlined"
            >
                Попробовать еще раз
            </Button>
        </Result>
    )
}

export { ResetPasswordByPhoneNumber }
