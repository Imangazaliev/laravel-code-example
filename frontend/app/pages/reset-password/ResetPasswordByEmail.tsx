import React, { FC, useMemo } from 'react'
import { Link, Redirect, useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import classNames from 'classnames'
import { FormikErrors, FormikProps } from 'formik'
import { string } from 'yup'
import { toast } from 'react-toastify'
import { History } from 'history'

import http from 'app/core/axios'
import { useQueryParameters } from 'app/hooks'

import { MainLayout } from 'app/components/layout'
import { Button, Field, Form, FormGroup, Input, Label, Paper, Paragraph, Result } from 'app/components'

import styles from 'app/styles/styles'

const { classes } = styles

type THistoryState = {
    email?: string
}

const ResetPasswordByEmail = () => {
    const history = useHistory<THistoryState>()

    const queryParameters = useQueryParameters()

    // redirect from result pages to the index page if the page was opened directly or by "Back" button click
    if (
        history.location.pathname !== Routes.RESET_PASSWORD_BY_EMAIL_PAGE &&
        history.action !== 'PUSH'
    ) {
        return <Redirect to={ Routes.RESET_PASSWORD_BY_EMAIL_PAGE } />
    }

    return (
        <MainLayout classes={{ container: classes.container}}>
            <Paper className={ classes.paper } >
                {
                    history.location.pathname === Routes.RESET_PASSWORD_BY_EMAIL_PAGE &&
                    queryParameters.token === undefined &&
                    <ResetPasswordForm history={ history } />
                }
                {
                    history.location.pathname === Routes.RESET_PASSWORD_BY_EMAIL_EMAIL_SENT_PAGE &&
                    <EmailSent history={ history } />
                }
                {
                    history.location.pathname === Routes.RESET_PASSWORD_BY_EMAIL_EMAIL_SEND_FAILED_PAGE &&
                    <EmailSendFailed />
                }
                {
                    history.location.pathname === Routes.RESET_PASSWORD_BY_EMAIL_PAGE &&
                    queryParameters.token !== undefined &&
                    <EnterNewPasswordForm history={ history } token={ queryParameters.token } />
                }
                {
                    history.location.pathname === Routes.RESET_PASSWORD_BY_EMAIL_PASSWORD_CHANGE_FAILED_PAGE &&
                    <PasswordChangeFailed />
                }
            </Paper>
        </MainLayout>
    )
}

type TResetPasswordFormData = {
    email: string
}

interface ResetPasswordFormProps {
    history: History<THistoryState>
}

const ResetPasswordForm: FC<ResetPasswordFormProps> = ({
    history,
}) => {
    const validationSchema = useMemo(() => ({
        email: string().required().email(),
    }), [])

    const initialValues: TResetPasswordFormData = {
        email: '',
    }

    const handleSubmit = async (
        values: TResetPasswordFormData,
        setErrors: (errors: FormikErrors<TResetPasswordFormData>) => void,
        setSubmitting: (isSubmitting: boolean) => void
    ) => {
        setSubmitting(true)

        try {
            await http.post(ApiEndpoints.RESET_PASSWORD_BY_EMAIL_SEND, {
                email: values.email,
            })

            setSubmitting(false)

            history.push(Routes.RESET_PASSWORD_BY_EMAIL_EMAIL_SENT_PAGE, {
                email: values.email,
            })
        } catch (error: any) {
            setSubmitting(false)

            if (error.response !== undefined) {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors)

                    return
                }

                if (error.response.status === 400) {
                    setTimeout(() => {
                        toast.error(error.response.data.message)
                    }, 200)
                }

                if (error.response.status === 403) {
                    setTimeout(() => {
                        toast.error(error.response.data.message)
                    }, 200)
                }
            }

            history.push(Routes.RESET_PASSWORD_BY_EMAIL_EMAIL_SEND_FAILED_PAGE)
        }
    }

    return (
        <Result
            status="forgot-icon"
            title="Забыли пароль?"
            classes={{
                root: `${classes.wrapper} ${classes.wrapper_center}`,
                subtitle: classes.block_text,
                icon: classes.icon,
            }}
        >
            <Paragraph className={ classes.block_text }>Укажите ваш Email, под которым вы зарегистрированы на сайте и на него будет отправлена информацию о сбросе пароля.</Paragraph>
            <Form
                autoComplete="off"
                className={ classes.form }
                onSubmit={ handleSubmit }
                validationSchema={ validationSchema }
                values={ initialValues }
            >
                {
                    ({ isSubmitting }: FormikProps<TResetPasswordFormData>) => (
                        <>
                            <FormGroup>
                                <Label htmlFor="email">Email</Label>
                                <Field
                                    component={ Input }
                                    name="email"
                                    placeholder="Введите ваш Email"
                                    type="text"
                                />
                            </FormGroup>
                            <Button
                                disabled={ isSubmitting }
                                textTransform="uppercase"
                                type="submit"
                                color="primary"
                                variant="contained"
                            >
                                Отправить
                            </Button>
                        </>
                    )
                }
            </Form>
            <div>
                <Link to={ Routes.RESET_PASSWORD_BY_PHONE_NUMBER_PAGE } className={ classes.link }>Сбросить по номеру телефона</Link>
            </div>
            <div className="mt-16">
                <Link to={ Routes.LOGIN } className={ classes.link }>Войти</Link>
            </div>
        </Result>
    )
}

interface EmailSentProps {
    history: History<THistoryState>
}

const EmailSent: FC<EmailSentProps> = ({
    history,
}) => {
    return (
        <Result
            status="check-circle"
            title="Письмо отправлено"
            subTitle={
                `Мы отправили электронное письмо с инструкциями по сбросу пароля на адрес ${ history.location.state.email }. Пожалуйста, проверьте ваш почтовый ящик и следуйте инструкциям в письме.`
            }
            classes={{
                root: `${classes.wrapper} ${classes.wrapper_center}`,
                subtitle: classes.block_text,
                icon: classNames(classes.icon, classes.icon_success),
                content: classes.content,
            }}
        >
            <Button
                color="primary"
                href="/"
                size="large"
                textTransform="uppercase"
                type="button"
                variant="outlined"
            >
                На главную
            </Button>
        </Result>
    )
}

const EmailSendFailed: FC = () => {
    return (
        <Result
            classes={{
                root: `${classes.wrapper} ${classes.wrapper_center}`,
                subtitle: classes.block_text,
                icon: classNames(classes.icon, classes.icon_failed),
                content: classes.content,
            }}
            status="error-circle"
            subTitle="При отправке письма на указанный Email возникла ошибка. Проверьте правильность написания Email или обратитесь в службу поддержки."
            title="Ошибка отправки"
        >
            <Button
                color="primary"
                href={ Routes.RESET_PASSWORD_BY_EMAIL_PAGE }
                size="large"
                textTransform="uppercase"
                type="button"
                variant="outlined"
            >
                Другой Email
            </Button>
        </Result>
    )
}

type TEnterNewPasswordFormData = {
    new_password: string
    new_password_confirmation: string
}

interface EnterNewPasswordFormProps {
    history: History<THistoryState>
    token: string
}

const EnterNewPasswordForm: FC<EnterNewPasswordFormProps> = ({
    history,
    token,
}) => {
    const { t } = useTranslation()

    const validationSchema = useMemo(() => ({
        new_password: string().required().min(ENV.profileRules.minPasswordLength),
        new_password_confirmation: string().required(),
    }), [])

    const initialValues: TEnterNewPasswordFormData = {
        new_password: '',
        new_password_confirmation: '',
    }

    const handleSubmit = async (
        values: TEnterNewPasswordFormData,
        setErrors: (errors: FormikErrors<TEnterNewPasswordFormData>) => void,
        setSubmitting: (isSubmitting: boolean) => void
    ) => {
        setSubmitting(true)

        try {
            await http.post(ApiEndpoints.RESET_PASSWORD_BY_EMAIL_RESET, {
                token: token,
                new_password: values.new_password,
                new_password_confirmation: values.new_password_confirmation,
            })

            setSubmitting(false)

            toast.info(t('auth:password.password-changed-alert'))

            history.push('/login')
        } catch (error: any) {
            setSubmitting(false)

            if (error.response !== undefined) {
                if (error.response.status === 422) {
                    setErrors(error.response.data.errors)

                    return
                }

                if (error.response.status === 400) {
                    setTimeout(() => {
                        toast.error(error.response.data.message)
                    }, 200)
                }
            }

            history.push(Routes.RESET_PASSWORD_BY_EMAIL_PASSWORD_CHANGE_FAILED_PAGE)
        }
    }

    return (
        <Result
            title="Сброс пароля"
            subTitle=""
            classes={{
                root: `${classes.wrapper} ${classes.wrapper_center}`,
                subtitle: classes.block_text,
                icon: classes.icon,
            }}
        >
            <Form
                autoComplete="off"
                className={ classes.form }
                onSubmit={ handleSubmit }
                validationSchema={ validationSchema }
                values={ initialValues }
            >
                {
                    ({ isSubmitting }: FormikProps<TEnterNewPasswordFormData>) => (
                        <>
                            <FormGroup>
                                <Label htmlFor="new_password">Пароль</Label>
                                <Field
                                    component={ Input }
                                    name="new_password"
                                    placeholder="Введите новый пароль"
                                    type="password"
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="new_password_confirmation">Пароль еще раз</Label>
                                <Field
                                    component={ Input }
                                    name="new_password_confirmation"
                                    placeholder="Повторите новый пароль"
                                    type="password"
                                />
                            </FormGroup>
                            <Button
                                disabled={ isSubmitting }
                                textTransform="uppercase"
                                type="submit"
                                color="primary"
                                variant="contained"
                            >
                                Сбросить
                            </Button>
                        </>
                    )
                }
            </Form>
        </Result>
    )
}

const PasswordChangeFailed: FC = () => {
    return (
        <Result
            classes={{
                root: `${classes.wrapper} ${classes.wrapper_center}`,
                subtitle: classes.block_text,
                icon: classNames(classes.icon, classes.icon_failed),
                content: classes.content,
            }}
            status="error-circle"
            subTitle="При сбросе пароля возникла ошибка. Попробуйте еще раз или обратитесь в службу поддержки."
            title="Ошибка при сбросе пароля"
        >
            <Button
                color="primary"
                href={ Routes.RESET_PASSWORD_BY_EMAIL_PAGE }
                size="large"
                textTransform="uppercase"
                type="button"
                variant="outlined"
            >
                Попробовать еще раз
            </Button>
        </Result>
    )
}

export { ResetPasswordByEmail }
