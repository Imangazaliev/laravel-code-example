import React, { FC, useCallback, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'

import http from 'app/core/axios'
import { generateUrl } from 'app/helpers'
import { openAddCoinsModal } from 'app/store/modules/addCoinsModalOpen'

import { ActionButton } from './ActionButton/ActionButton'
import { Icon } from 'app/components/common/Icon'

import { TCurrentUser } from 'app/types/user'
import { TUserProfile } from './types'
import { Text } from 'app/components'

interface ActionsProps {
    currentUser: TCurrentUser
    updateUser: () => void
    user: TUserProfile
}

const Actions: FC<ActionsProps> = ({
    currentUser,
    updateUser,
    user,
}) => {
    const { t } = useTranslation()

    const dispatch = useDispatch()
    const history = useHistory()

    const sendChatRequest = useCallback(async () => {
        const amount = ENV.prices.chat_request

        if ( ! confirm(t('profile:chat-requests.confirmation-alert', { amount }))) {
            return
        }

        try {
            await http.post(ApiEndpoints.CHAT_REQUESTS_SEND, {
                user_id: user.id,
            })

            updateUser()
        } catch (error: any) {
            if (error.response !== undefined) {
                if (error.response.data.code === ApiErrors.INSUFFICIENT_BALANCE) {
                    dispatch(openAddCoinsModal())

                    return
                }

                if (error.response.data.message !== undefined) {
                    toast.error(error.response.data.message)

                    return
                }
            }

            toast.error(t('error'))
        }
    }, [user.id, updateUser, dispatch, t])

    const acceptChatRequest = useCallback(async () => {
        const amount = ENV.prices.chat_request_acceptance

        if ( ! confirm(t('profile:chat-requests.acceptance-alert', { amount }))) {
            return
        }

        try {
            await http.post(ApiEndpoints.CHAT_REQUESTS_ACCEPT, {
                user_id: user.id,
            })

            updateUser()
        } catch (error: any) {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        }
    }, [user.id, updateUser, t])

    const cancelChatRequest = useCallback(async () => {
        if ( ! confirm(t('profile:chat-requests.cancellation-alert'))) {
            return
        }

        try {
            await http.post(ApiEndpoints.CHAT_REQUESTS_CANCEL, {
                user_id: user.id,
            })

            updateUser()
        } catch (error: any) {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        }
    }, [user.id, updateUser, t])

    const finishChat = useCallback(async () => {
        if ( ! confirm(t('are-you-sure'))) {
            return
        }

        try {
            await http.post(ApiEndpoints.CHATS_FINISH, {
                chat_id: user.chat_id,
            })

            updateUser()
        } catch (error: any) {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        }
    }, [user.chat_id, updateUser, t])

    const toggleFavorite = useCallback(async () => {
        try {
            const url = user.is_favorite ? ApiEndpoints.FAVORITE_PROFILES_REMOVE : ApiEndpoints.FAVORITE_PROFILES_ADD

            await http.post(url, {
                user_id: user.id,
            })

            updateUser()
        } catch (error: any) {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        }
    }, [updateUser, t, user.id, user.is_favorite])

    const toggleLike = useCallback(async () => {
        try {
            const url = user.is_liked ? ApiEndpoints.USERS_REMOVE_LIKE : ApiEndpoints.USERS_LIKE

            await http.post(url, {
                user_id: user.id,
            })

            updateUser()
        } catch (error: any) {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        }
    }, [updateUser, t, user.id, user.is_liked])

    const makeGift = useCallback(async () => {
        const amount = ENV.prices.gift

        if ( ! confirm(t('profile:make-gift.confirmation-alert', { amount }))) {
            return
        }

        try {
            await http.post(ApiEndpoints.USERS_MAKE_GIFT, {
                user_id: user.id,
            })

            toast.success(t('profile:make-gift.success-message'))
        } catch (error: any) {
            if (error.response !== undefined) {
                if (error.response.data.code === ApiErrors.INSUFFICIENT_BALANCE) {
                    dispatch(openAddCoinsModal())

                    return
                }

                if (error.response.data.message !== undefined) {
                    toast.error(error.response.data.message)

                    return
                }
            }

            toast.error(t('error'))
        }
    }, [dispatch, t, user.id])

    const chatUrl = useMemo(() => {
        if (user.chat_id === null) {
            return null
        }

        return generateUrl(Routes.CHAT, {
            id: user.chat_id,
        })
    }, [user.chat_id])

    const goToChat = useCallback(() => {
        if (chatUrl === null) {
            return
        }

        history.push(chatUrl)
    }, [history, chatUrl])

    return <div className="d-flex flex-column">
        {
            user.sex !== currentUser.sex &&
            user.chat_id === null &&
            user.incoming_chat_request_id === null &&
            user.outgoing_chat_request_id === null &&
            <>
                <div className="mb-16">
                    <ActionButton
                        color="primary"
                        subTitle={ t('profile:chat-requests.send-request.subtitle') }
                        title={ t('profile:chat-requests.send-request') }
                        onClick={ sendChatRequest }
                    >
                        <Icon name="user-chat" />
                    </ActionButton>
                </div>
                {
                    user.sent_chat_request_previously &&
                    <Text className="mb-16" color="gray">
                        { t('profile:profile-info.previously-sent-request') }
                    </Text>
                }
            </>
        }
        {
            user.incoming_chat_request_id !== null &&
            <div className="mb-16">
                <ActionButton
                    color="primary"
                    title={ t('profile:chat-requests.accept-request') }
                    onClick={ acceptChatRequest }
                >
                    <Icon name="user-chat" />
                </ActionButton>
            </div>
        }
        {
            user.outgoing_chat_request_id !== null &&
            <div className="mb-16">
                <ActionButton
                    color="primary"
                    title={ t('profile:chat-requests.cancel-request') }
                    onClick={ cancelChatRequest }
                >
                    <Icon name="user-chat" />
                </ActionButton>
            </div>
        }
        {
            user.chat_id !== null &&
            <>
                <div className="mb-16">
                    <ActionButton
                        color="primary"
                        title={ t('profile:go-to-chat') }
                        onClick={ goToChat }
                    >
                        <Icon name="user-chat" />
                    </ActionButton>
                </div>
                <div className="mb-16">
                    <ActionButton
                        color="red"
                        title={ t('chat:finish-chat') }
                        onClick={ finishChat }
                    >
                        <Icon name="x-circle-thin" />
                    </ActionButton>
                </div>
            </>
        }
        {
            user.sex !== currentUser.sex &&
            <div className="mb-16">
                <ActionButton
                    color="orange"
                    subTitle={ user.is_favorite ? undefined : t('profile:favorites.add-to-favorites.subtitle') }
                    title={ user.is_favorite ? t('profile:favorites.remove-from-favorites') : t('profile:favorites.add-to-favorites') }
                    onClick={ toggleFavorite }
                >
                    <Icon name="star" />
                </ActionButton>
            </div>
        }
        {
            user.sex !== currentUser.sex &&
            <div className="mb-16">
                <ActionButton
                    color="red"
                    subTitle={ user.is_liked ? undefined : t('profile:likes.like-button.subtitle') }
                    title={ user.is_liked ? t('profile:likes.remove-like') : t('profile:likes.like-button.label') }
                    onClick={ toggleLike }
                >
                    <Icon name="heart" />
                </ActionButton>
            </div>
        }
        {
            user.sex !== currentUser.sex &&
            <div className="mb-16">
                <ActionButton
                    color="blue"
                    title={ t('profile:make-gift') }
                    subTitle={ t('profile:make-gift.description') }
                    onClick={ makeGift }
                >
                    <Icon name="gift" />
                </ActionButton>
            </div>
        }
    </div>
}

export { Actions }
