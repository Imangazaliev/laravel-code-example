import { Styles } from 'jss'

import { jss } from 'app/core/jss'
import theme from 'app/theme'
import { marginEnd, marginStart, mediaQueries } from 'app/styles'

const styles: Styles = {
    view_profile_paper: {
        padding: theme.spacing(4),
        maxWidth: 'inherit',

        [mediaQueries.phoneLarge]: {
            [marginStart]: 'auto',
            [marginEnd]: 'auto',
        },

        [mediaQueries.phoneMedium]: {
            padding: theme.spacing(2),
        },
    },

    user_card_location: {
        color: 'var(--grey-400)',
        fontSize: 16,
        fontWeight: 'normal',
        lineHeight: '24px',
    },
}

export default jss.createStyleSheet(styles).attach()
