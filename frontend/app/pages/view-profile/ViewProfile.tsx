import React, { FC, useCallback, useMemo } from 'react'
import { useHistory, useRouteMatch } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'

import { getAge } from 'app/helpers'

import { MainLayout } from 'app/components/layout'
import { Paper } from 'app/components'
import { ViewProfileContent } from './ViewProfileContent'

import { TUserProfile } from './types'

import styles from 'app/pages/view-profile/styles'
import { useFetchData } from 'app/hooks'

const ViewProfile: FC = () => {
    const { t } = useTranslation()

    const history = useHistory()
    const match = useRouteMatch<any>()

    const {
        data: user,
        update: updateUser,
    } = useFetchData<TUserProfile, TUserProfile>(apiUrl, {
        params: {
            user_id: parseInt(match.params.id),
        },
        onError: useCallback(error => {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        }, [history, mode, t]),
    })

    const title = useMemo(() => {
        if (user === null) {
            return undefined
        }

        const age = getAge(user.birthday)

        return t('name-with-age', {
            name: user.name,
                age: age,
        })
    }, [user, t])

    return (
        <MainLayout title={ title }>
            <Paper className={ styles.classes.view_profile_paper }>
                {
                    user === null &&
                    <div>Loading...</div>
                }
                {
                    user !== null &&
                    <ViewProfileContent
                        mode={ mode }
                        updateUser={ updateUser }
                        user={ user }
                    />
                }
            </Paper>
        </MainLayout>
    )
}

export default ViewProfile
