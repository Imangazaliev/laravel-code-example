import React, { FC, useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import memoizeOne from 'memoize-one'
import { toast } from 'react-toastify'

import http from 'app/core/axios'
import { getAge, getLocationStr } from 'app/helpers'
import { useCurrentUser, usePermission } from 'app/hooks'
import { getSections } from 'app/helpers/profile'

import { Avatar, Icon, PhotoCount, PhotoGallery, Title, UserPresenceStatus } from 'app/components'
import { Actions } from './Actions'
import { ViewProfileSection } from './ViewProfileSection'

import { TUserPhoto, TUserProfile } from './types'

import styles from './styles'
import { generatePath, Link } from 'react-router-dom'

const THUMBNAIL_COUNT = 3

interface ViewProfileContentProps {
    updateUser: () => void
    user: TUserProfile
}

const ViewProfileContent: FC<ViewProfileContentProps> = ({
    mode,
    updateUser,
    user,
}) => {
    const { t } = useTranslation()
    const { hasPermission } = usePermission()

    const currentUser = useCurrentUser()

    const [photoGalleryOpen, setPhotoGalleryOpen] = useState(false)
    const [currentPhotoIndex, setCurrentPhotoIndex] = useState(1)
    const [profileMarkedAsViewed, setProfileMarkedAsViewed] = useState<boolean>(false)

    const sendViewedRequest = useCallback(async () => {
        try {
            await http.post(ApiEndpoints.USERS_PROFILE_VIEWED, {
                user_id: user.id,
            })
        } catch (error: any) {
            if (error.response !== undefined && error.response.data.message !== undefined) {
                toast.error(error.response.data.message)

                return
            }

            toast.error(t('error'))
        }

        setProfileMarkedAsViewed(true)
    }, [t, user.id])

    useEffect(() => {
        if (currentUser !== null && user.id === currentUser.id) {
            return
        }

        if (profileMarkedAsViewed) {
            return
        }

        if (mode === 'user') {
            sendViewedRequest()
        }
    }, [currentUser, mode, profileMarkedAsViewed, sendViewedRequest, user.id])

    const openPhoto = useCallback(memoizeOne((index: number) => () => {
        if (user.photos.length === 0) {
            return
        }

        setCurrentPhotoIndex(index)
        setPhotoGalleryOpen(true)
    }), [user.photos.length])

    const age = useMemo(() => getAge(user.birthday), [user])

    const location = useMemo(() => {
        return getLocationStr(user.country_name, user.locality_name)
    }, [user.country_name, user.locality_name])

    const sections = useMemo(() => {
        return getSections(user)
    }, [user])

    const mainPhoto = useMemo<TUserPhoto | null>(() => {
        return user.photos.filter(photo => photo.is_main)[0] ?? null
    }, [user.photos])

    const thumbnails = useMemo<TUserPhoto[]>(() => {
        const otherPhoto = user.photos.filter(photo => ! photo.is_main)

        return Object.assign(new Array(THUMBNAIL_COUNT).fill(null), otherPhoto.slice(0, THUMBNAIL_COUNT))
    }, [user.photos])

    const userAdminUrl = generatePath(AdminRoutes.USERS_DETAILS, {
        id: user.id,
    })

    return (
        <>
            <div className="view-profile">
                <div className="view-profile__main-info mb-40">
                    <div className="view-profile__main-block view-profile__photo">
                        <div className="view-profile__avatar" onClick={ openPhoto(1) }>
                            <Avatar gender={ user.sex } shape="rounded" src={ mainPhoto?.url ?? null } bordered />
                            <PhotoCount position="left">{ user.photos.length }</PhotoCount>
                        </div>
                        <div className="view-profile__photo-thumbs">
                            {
                                thumbnails.length > 0 &&
                                thumbnails.map((photo, index) => {
                                    const key = photo === null ? `placeholder-${index}` : photo.id

                                    return (
                                        <div key={ key } className="view-profile__photo-thumb" onClick={ openPhoto(index + 2) }>
                                            <Avatar gender={ user.sex } src={ photo?.url ?? null } />
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                    <div className="view-profile__main-block">
                        <div className="d-flex align-items-center justify-content-between mb-8">
                            <Title component="h4" noMargin>
                                {
                                    t('name-with-age', {
                                        name: user.name,
                                        age: age,
                                    })
                                }
                            </Title>
                            {
                                hasPermission('view-user-details') &&
                                <Link className="view-profile__more-icon" to={ userAdminUrl }>
                                    <Icon name="more" />
                                </Link>
                            }
                        </div>
                        <div className="d-flex mb-8">
                            <span className={ styles.classes.user_card_location }>{ location }</span>
                        </div>
                        <div className="d-flex mb-16">
                            <UserPresenceStatus
                                bigText
                                lastOnlineTime={ user.was_online_at }
                                sex={ user.sex }
                                status={ user.presence_status }
                            />
                        </div>
                        {
                            user.profile_headline !== null &&
                            <div className="view-profile__message mb-16">
                                <span className="view-profile__message-text">{ user.profile_headline }</span>
                            </div>
                        }

                        {
                            mode === 'user' &&
                            currentUser !== null &&
                            <Actions
                                currentUser={ currentUser }
                                updateUser={ updateUser }
                                user={ user }
                            />
                        }
                    </div>
                </div>
                <div className="view-profile__sections">
                    {
                        sections !== null &&
                        sections.map((item: any) => (
                            <ViewProfileSection
                                key={ item.title }
                                title={ item.title }
                                data={ item.data }
                            />
                        ))
                    }
                </div>
            </div>
            <PhotoGallery
                canViewPhoto={ user.can_view_photo }
                initialPhotoIndex={ currentPhotoIndex }
                handleClose={ () => setPhotoGalleryOpen(false) }
                open={ photoGalleryOpen }
                photos={ user.photos }
            />
        </>
    )
}

export { ViewProfileContent }
