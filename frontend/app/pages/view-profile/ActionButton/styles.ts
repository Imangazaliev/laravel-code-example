import { createUseStyles } from 'react-jss'

import { colors, spacing, mediaQueries, paddingStart, paddingEnd } from 'app/styles'

export const useStyles = createUseStyles({
    root: {
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        maxWidth: 334,
        height: 72,
        padding: `12px ${ spacing.medium }px`,
        backgroundColor: colors.white,
        border: `2px solid ${ colors.gray[100] }`,
        borderRadius: 8,
        transition: 'all .25s ease',
        cursor: 'pointer',
        outline: 'none',

        [mediaQueries.phoneSmall]: {
            width: '100%',
            [paddingStart]: spacing.small,
            [paddingEnd]: spacing.small,
        },
    },

    icon: {
        fontSize: '28px',
        'transition': 'all .25s ease',
    },

    title: {
        fontWeight: 500,
        fontSize: 18,
        lineHeight: '24px',
        transition: 'all .25s ease',
    },

    subtitle: {
        fontWeight: 500,
        fontSize: 14,
        lineHeight: '21px',
        color: `${ colors.gray[600] }`,
        transition: "all .25s ease",
    },

    color_primary: {
        borderColor: colors.primary,

        '&:hover, &:focus': {
            backgroundColor: colors.primary,

            '& .icon': {
                fill: colors.white,
            },

            '& $title': {
                color: colors.white,
            },

            '& $subtitle': {
                color: colors.white,
            },
        },

        '& .icon': {
            fill: colors.primary,
        },

        '& $title': {
            color: colors.primary,
        },
    },

    color_blue: {
        '&:hover, &:focus': {
            borderColor: colors.secondary,
        },

        '& .icon ': {
            fill: colors.secondary,
        },

        '& $title': {
            color: colors.secondary,
        },
    },

    color_orange: {
        '&:hover, &:focus': {
            borderColor: colors.orange[100],
        },

        '& .icon ': {
            fill: colors.orange[100],
        },

        '& $title': {
            color: colors.orange[100],
        },
    },

    color_red: {
        '&:hover, &:focus': {
            borderColor: colors.error,
        },

        '& .icon': {
            fill: colors.error,
        },

        '& $title': {
            color: colors.error,
        },
    },
})
