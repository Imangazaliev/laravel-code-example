import React, { FC, MouseEventHandler, ReactNode } from 'react'
import classNames from 'classnames'

import { useStyles } from './styles'

export interface ActionButtonProps {
    children: ReactNode
    title: string
    subTitle?: string
    color: 'primary' | 'blue' | 'orange' | 'red'
    onClick?: MouseEventHandler
}

const ActionButton: FC<ActionButtonProps> = ({
    children,
    title,
    subTitle,
    color,
    onClick,
}) => {
    const classes = useStyles()

    const rootCls = classNames(classes.root, {
        [classes.color_primary]: color === 'primary',
        [classes.color_blue]: color === 'blue',
        [classes.color_orange]: color === 'orange',
        [classes.color_red]: color === 'red',
    })

    return (
        <button
            type="button"
            className={ rootCls }
            onClick={ onClick }
        >
            <div className={ classes.icon }>
                { children }
            </div>
            <div className="d-flex flex-column align-items-start ms-16">
                <span className={ classes.title }>{ title }</span>
                {
                    subTitle !== undefined &&
                    <span className={ classes.subtitle }>{ subTitle }</span>
                }
            </div>
        </button >
    )
}

export { ActionButton }
