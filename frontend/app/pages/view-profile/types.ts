import { TUserBase, TPresenceStatus } from 'app/types/user'

export type TViewProfileSection = {
    title: string
    data: any[]
}

export type TUserProfile = TUserBase & {
    presence_status: TPresenceStatus
    was_online_at: string
    photos: TUserPhoto[]
    incoming_chat_request_id: number | null
    outgoing_chat_request_id: number | null
    chat_id: number | null
    is_favorite: boolean
    is_liked: boolean
    can_view_photo: boolean
    sent_chat_request_previously: boolean
}

export type TUserPhoto = {
    id: number
    url: string
    caption: string
    is_main: boolean
}
