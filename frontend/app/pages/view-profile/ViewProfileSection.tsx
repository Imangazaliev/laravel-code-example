import React, { FC } from 'react'
import { chunk } from 'lodash'

import { TViewProfileSection } from './types'

const ViewProfileSection: FC<TViewProfileSection> = ({
    title,
    data,
}) => {
    data = data.filter(item => item !== null)

    return (
        <>
            <div className="view-profile__section-head mb-16">{ title }</div>
            {
                chunk(data, 2).map((columns, index) => {
                    return <div className="view-profile__section-table" key={ index }>
                        <div className="d-flex flex-column mb-16" key={ columns[0].title }>
                            <div className="view-profile__section-key">{ columns[0].title }</div>
                            <div className="view-profile__section-value">{ columns[0].value }</div>
                        </div>
                        {
                            columns[1] !== undefined &&
                            <div className="d-flex flex-column mb-16" key={ columns[1].title }>
                                <div className="view-profile__section-key">{ columns[1].title }</div>
                                <div className="view-profile__section-value">{ columns[1].value }</div>
                            </div>
                        }
                    </div>
                })
            }
        </>
    )
}

export { ViewProfileSection }
