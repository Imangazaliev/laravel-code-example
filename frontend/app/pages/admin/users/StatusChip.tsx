import React, { FC, useMemo } from 'react'

import { Chip } from 'app/components'

import { TProfileStatus } from 'app/types/user'

import { getProfileStatuses } from 'app/constants/profile'

const colors: any = {
    [TProfileStatus.NOT_MODERATED]: 'gray',
    [TProfileStatus.PENDING_MODERATION]: 'green',
    [TProfileStatus.ACCEPTED]: 'green',
    [TProfileStatus.REJECTED]: 'red',
    [TProfileStatus.BANNED]: 'red',
    [TProfileStatus.DEACTIVATED]: 'red',
    [TProfileStatus.DELETED]: 'red',
}

interface StatusChipProps {
    status: TProfileStatus
}

const StatusChip: FC<StatusChipProps> = ({
    status,
}) => {
    const profileStatuses = useMemo(() => getProfileStatuses(), [])

    return <Chip
        color={ colors[status] }
        label={ profileStatuses[status] }
    />
}

export { StatusChip }
