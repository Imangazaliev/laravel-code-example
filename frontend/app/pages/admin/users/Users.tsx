import React, { FC, useCallback, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'

import http from 'app/core/axios'
import { useDebouncedEffect, useQueryState } from 'app/hooks'

import { AdminLayout } from 'app/components/layout'
import { Title, Pagination } from 'app/components'
import { SearchInput } from 'app/components/admin'
import { UsersTable } from './UsersTable'

import { TUserItem } from './types'

const Users: FC = () => {
    const { t } = useTranslation()

    const [state, setState] = useState<{
        users: TUserItem[]
        totalCount: number
        pageCount: number
    } | null>(null)

    const {
        parameters: {
            page,
            searchQuery,
        },
        setParameters,
        updateQueryString,
    } = useQueryState({
        initialState: {
            page: 1,
            searchQuery: '',
        },
        updateQueryStringAutomatically: false,
    })

    const loadUsers = useCallback(async () => {
        updateQueryString()

        try {
            const response = await http.get(ApiEndpoints.ADMIN_USERS, {
                params: {
                    page: page,
                    query: searchQuery,
                },
            })

            setState({
                users: response.data.users,
                totalCount: response.data.total_count,
                pageCount: response.data.page_count,
            })
        } catch (error: any) {
            toast.error(t('errors.error-loading-data'))
        }
    }, [page, searchQuery, updateQueryString, t])

    useDebouncedEffect(loadUsers, 500, {
        maxWait: 1000,
    })

    const handleSearchInputChange = useCallback((query: string) => {
        setParameters({
            searchQuery: query,
        })
    }, [setParameters])

    const goToPage = useCallback(page => setParameters({ page }), [setParameters])

    return (
        <AdminLayout>
            <Title component="h5" noMargin>Пользователи</Title>
            {
                state !== null &&
                <>
                    <h3>Найдено анкет: { state.totalCount }</h3>
                    <SearchInput
                        onChange={ handleSearchInputChange }
                        query={ searchQuery }
                    />
                    <Pagination
                        className="mb-16"
                        currentPage={ page }
                        pageCount={ state.pageCount }
                        goToPage={ goToPage }
                    />
                    <UsersTable users={ state.users } />
                    <Pagination
                        className="mt-16"
                        currentPage={ page }
                        pageCount={ state.pageCount }
                        goToPage={ goToPage }
                    />
                </>
            }
        </AdminLayout>
    )
}

export { Users }
