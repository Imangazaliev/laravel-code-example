import { TProfileStatus, TSex } from 'app/types/user'

export type TUserItem = {
    id: number
    name: string
    sex: TSex
    phone_number: string | null
    status: TProfileStatus
}
