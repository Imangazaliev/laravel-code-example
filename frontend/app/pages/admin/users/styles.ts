import { Styles } from 'jss'

import { jss }  from 'app/core/jss'
import { shapes, flexBoxMixin } from 'app/styles'
import theme from 'app/theme'

const styles: Styles = {
    breadcrumbs: {
        marginBottom: theme.spacing(3),
    },

    paper: {
        borderRadius: shapes.base,
        backgroundColor: theme.palette.common.white,
    },

    row: {
        ...flexBoxMixin({ alignment: 'center' }),
    },

    more: {
        fill: theme.palette.grey[400],
        transform: 'rotate(90deg)',

        '&:hover': {
            borderColor: theme.palette.grey[400],
        },
    },

    more_cell: {
        width: 105,
    },

    name_cell: {
        padding: '0px !important',
    },

    cell_link: {
        width: '100%',
        height: '100%',
        padding: '6px 24px 6px 16px',

        ...flexBoxMixin({
            justifyContent: 'space-between',
            alignment: 'center',
        }),
        '&:hover': {
            '& .text': {
                color: theme.palette.primary.main,
            },

            '& .icon': {
                fill: theme.palette.primary.main,
            },
        },
    },

    user_icon_male: {
        fill: 'var(--blue)',
    },

    user_icon_female: {
        fill: 'var(--red)',
    },

    link_icon: {
        height: 20,
        fontSize: 20,
        fill: theme.palette.grey[300],

        '&:hover': {
            fill: theme.palette.grey[800],
        },
    },

    popper: {
        width: 210,
    },

    list_icon: {
        fontSize: 21,
        height: 21,
        fill: theme.palette.grey[300],
    },

    list_icon_red: {
        fill: theme.palette.error.main,
    },
}

export default jss.createStyleSheet(styles).attach()
