import React, { FC } from 'react'
import { generatePath, Link } from 'react-router-dom'

import {
    Icon,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Text,
} from 'app/components'

import { TSex } from 'app/types/user'
import { StatusChip } from './StatusChip'
import { TUserItem } from './types'

import styles from './styles'
import { usePermission } from 'app/hooks'

const { classes } = styles

interface UsersTableProps {
    users: TUserItem[]
}

const UsersTable: FC<UsersTableProps> = ({
    users,
}) => {
    const { hasPermission } = usePermission()

    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell size="small" align="center">ID</TableCell>
                        <TableCell size="small" align="left">Имя</TableCell>
                        {
                            hasPermission('view-user-contacts') &&
                            <TableCell size="small" align="left">Номер телефона</TableCell>
                        }
                        <TableCell size="small" align="center">Статус</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        users.map(user => (
                            <TableRow key={ user.id }>
                                <TableCell align="center" size="small">
                                    <Text>{ user.id }</Text>
                                </TableCell>
                                <TableCell className={ classes.name_cell } size="small">
                                    <Link
                                        className="d-flex align-items-baseline"
                                        to={ generatePath(AdminRoutes.USERS_DETAILS, {
                                            id: user.id,
                                        }) }
                                    >
                                        <Icon className={ user.sex === TSex.MALE ? classes.user_icon_male : classes.user_icon_female } name="user" />
                                        <Text className="ms-8" bold>{ user.name }</Text>
                                    </Link>
                                </TableCell>
                                {
                                    hasPermission('view-user-contacts') &&
                                    <TableCell align="left" size="small">
                                        <Text>{ user.phone_number }</Text>
                                    </TableCell>
                                }
                                <TableCell align="center" size="small">
                                    <StatusChip status={ user.status } />
                                </TableCell>
                            </TableRow>
                        ))
                    }
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export { UsersTable }
