import { Styles } from 'jss'

import { jss }  from 'app/core/jss'
import { flexBoxMixin, paddingEnd } from 'app/styles'
import theme from 'app/theme'

const tableStyles: Styles = {
    more: {
        fill: theme.palette.grey[400],
        transform: 'rotate(90deg)',

        '&:hover': {
            borderColor: theme.palette.grey[400],
        },
    },

    more_cell: {
        width: 105,
    },

    name_cell: {
        ...flexBoxMixin({
            justifyContent: 'space-between',
            alignment: 'center',
        }),
    },

    link_icon: {
        [paddingEnd]: theme.spacing(2),
        height: 20,
        fontSize: 20,
        fill: theme.palette.grey[300],

        '&:hover': {
            fill: theme.palette.grey[800],
        },
    },
}

export default jss.createStyleSheet(tableStyles).attach()
