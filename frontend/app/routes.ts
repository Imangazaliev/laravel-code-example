import { createRouteFunction, TAccess } from './core/routing'

import Landing from './pages/landing/Landing'

// Auth
import { Registration, Rules as RegistrationRules } from './pages/register'
import { VerifyPhoneNumber } from './pages/verify-phone-number'
import { WrongPhoneNumber } from './pages/wrong-phone-number'
import { RegistrationSteps, RegistrationCompleted } from './pages/registration-steps'
import { Login } from './pages/login'

// Profile
import { Profile } from './pages/profile'

import { PrivacyPolicy } from 'app/pages/privacy-policy'
import { TermsOfUse } from 'app/pages/terms-of-use'

export const routes: any[] = []

const route = createRouteFunction(routes)

route('/', Landing, { access: TAccess.GUEST })

// Auth

route(Routes.REGISTER, Registration, { access: TAccess.GUEST })
route(Routes.REGISTER_RULES, RegistrationRules, { access: TAccess.AUTHENTICATED, registrationPage: true })
route(Routes.REGISTER_VERIFY_PHONE_NUMBER, VerifyPhoneNumber, { access: TAccess.AUTHENTICATED, registrationPage: true })
route(Routes.REGISTER_WRONG_PHONE_NUMBER, WrongPhoneNumber, { access: TAccess.AUTHENTICATED, registrationPage: true })
route('/register/step-:stepNumber(\\d+)', RegistrationSteps, { exact: false, access: TAccess.AUTHENTICATED, registrationPage: true })
route(Routes.REGISTRATION_COMPLETED, RegistrationCompleted, { access: TAccess.AUTHENTICATED, registrationPage: true })
route(Routes.LOGIN, Login, { access: TAccess.GUEST })

// Profile

route(Routes.PROFILE, Profile, { access: TAccess.AUTHENTICATED })
route(Routes.PRIVACY_POLICY, PrivacyPolicy, { access: TAccess.ALL })
route(Routes.TERMS_OF_USE, TermsOfUse, { access: TAccess.ALL })
