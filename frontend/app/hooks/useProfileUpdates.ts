import { useSelector } from 'react-redux'

import { RootState } from 'app/store/configureStore'
import { ProfileUpdates } from 'app/types/user'

export const useProfileUpdates = () => {
    return useSelector<RootState, ProfileUpdates>(state => state.profileUpdates)
}
