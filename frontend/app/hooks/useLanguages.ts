import { useCallback, useState } from 'react'

import { localStorageCache } from 'app/helpers'

import { TLanguage } from 'app/types/common'
import { useFetchData } from 'app/hooks/useFetchData'

const cacheEntry = localStorageCache<TLanguage[]>('languages', 60 * 60 * 24)

export const useLanguages = () => {
    const [languages, setLanguages] = useState<TLanguage[] | null>(() => cacheEntry.get())

    useFetchData<TLanguage[]>(ApiEndpoints.LANGUAGES, {
        load: languages === null,
        onLoad: useCallback(response => {
            setLanguages(response)

            cacheEntry.put(response)
        }, []),
    })

    return languages
}
