import { useCallback, useState } from 'react'

import { sessionStorageCache } from 'app/helpers'
import { useFetchData } from 'app/hooks'

import { TCountry } from 'app/types/common'

const cacheEntry = sessionStorageCache<TCountry[]>('countries', 60 * 60 * 24)

export const useCountries = () => {
    const [countries, setCountries] = useState(() => cacheEntry.get())

    useFetchData<TCountry[], TCountry[]>(ApiEndpoints.COUNTRIES, {
        load: countries === null,
        onLoad: useCallback(response => {
            setCountries(response)

            cacheEntry.put(response)
        }, []),
    })

    return countries
}
