import { useCallback, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'

import http from 'app/core/axios'

import { TMessageStatus } from 'app/types/chat'

type TModerationResponse = {
    new_status: TMessageStatus
}

export const useChangeMessageStatus = () => {
    const { t } = useTranslation()

    const acceptMessage = useCallback((messageId: number) => {
        return new Promise<TModerationResponse>(async (resolve, reject) => {
            try {
                const response = await http.post<TModerationResponse>(ApiEndpoints.ADMIN_CHAT_ACCEPT_MESSAGE, {
                    message_id: messageId,
                })

                resolve(response.data)
            } catch (error: any) {
                reject()

                if (error.response !== undefined && error.response.data.message !== undefined) {
                    toast.error(error.response.data.message)

                    return
                }

                toast.error(t('error'))
            }
        })
    }, [t])

    const rejectMessage = useCallback(({
        messageId,
        rejectionReasons,
        comment,
        commentForUser,
    }: {
        messageId: number
        rejectionReasons: string[]
        comment: string | null
        commentForUser: string | null
    }) => {
        return new Promise<TModerationResponse>(async (resolve, reject) => {
            try {
                const response = await http.post<TModerationResponse>(ApiEndpoints.ADMIN_CHAT_REJECT_MESSAGE, {
                    message_id: messageId,
                    rejection_reasons: rejectionReasons,
                    comment: comment,
                    comment_for_user: commentForUser,
                })

                resolve(response.data)
            } catch (error: any) {
                reject()

                if (error.response !== undefined && error.response.data.message !== undefined) {
                    toast.error(error.response.data.message)

                    return
                }

                toast.error(t('error'))
            }
        })
    }, [t])

    return useMemo(() => ({
        acceptMessage,
        rejectMessage,
    }), [acceptMessage, rejectMessage])
}
