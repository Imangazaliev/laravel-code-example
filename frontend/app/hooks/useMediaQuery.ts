import { useState, useEffect } from 'react'

import { breakpoints } from 'app/styles/breakpoints'

const mediaQueries = new Map()

type TBreakpoint = keyof typeof breakpoints

(Object.keys(breakpoints) as TBreakpoint[]).map(key => {
    mediaQueries.set(key, `(max-width: ${ breakpoints[key] }px)`)
})

export function useMediaQuery(query: TBreakpoint): boolean {
    const mediaQuery = mediaQueries.get(query)
    const mediaQueryList = window.matchMedia(mediaQuery)

    const [value, setValue] = useState(mediaQueryList.matches)

    useEffect(() => {
        const handler = (mql: any) => setValue(mql.matches)

        mediaQueryList.addListener(handler)

        return () => mediaQueryList.removeListener(handler)
    }, [mediaQueryList])

    return value
}
