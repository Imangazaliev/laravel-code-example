import { useSelector } from 'react-redux'

import { TCurrentUser} from 'app/types/user'
import { RootState } from 'app/store/configureStore'

const usePermission = () => {
    const currentUser: TCurrentUser|null = useSelector((state: RootState) => state.user)

    const hasPermission = (permission: string) => {
        if (currentUser === null) {
            return false
        }

        return currentUser.permissions.includes(permission)
    }

    return {
        hasPermission,
    }

}

export { usePermission }
