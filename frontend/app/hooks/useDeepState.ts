import { Dispatch, SetStateAction, useCallback, useState } from 'react'
import { dequal } from 'dequal'

export const useDeepState = <S>(initialState: S | (() => S)): [S, Dispatch<SetStateAction<S>>] => {
    const [state, setState] = useState(initialState)

    const memoizedSetState = useCallback((value: SetStateAction<S>) => {
        const newState = typeof value === 'function' ? (value as any)(state) : value

        if (dequal(state, newState)) {
            return
        }

        setState(newState)
    }, [state])

    return [state, memoizedSetState]
}
