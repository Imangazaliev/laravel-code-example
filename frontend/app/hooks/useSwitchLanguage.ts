import { useCallback, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { find } from 'lodash'
import { toast } from 'react-toastify'

import http from 'app/core/axios'
import { localStorageCache } from 'app/helpers'
import { useFetchData } from 'app/hooks'

import { TLanguage } from 'app/types/common'

const cacheEntry = localStorageCache<TLanguage[]>('supported-languages', 60 * 60 * 24)

export function useSwitchLanguage() {
    const { t } = useTranslation()

    const [languages, setLanguages] = useState<TLanguage[] | null>(null)
    const [currentLanguageCode, setCurrentLanguageCode] = useState(ENV.currentLanguage.code)

    useFetchData<TLanguage[]>(ApiEndpoints.LANGUAGES, {
        load: languages === null,
        onLoad: useCallback(response => {
            setLanguages(response)

            cacheEntry.put(response)
        }, []),
        params: {
            supported_only: 1,
        },
    })

    const switchLanguage = useCallback(async (languageCode: string) => {
        try {
            setCurrentLanguageCode(languageCode)

            const languageId = find(languages, {
                code: languageCode,
            })?.id

            if (languageId === undefined) {
                toast.error(t('errors.language-not-found', {
                    languageCode: currentLanguageCode,
                }))

                throw new Error(`Language with code ${languageCode} not found`)
            }

            await http.post(ApiEndpoints.CHANGE_LANGUAGE, {
                language_id: languageId,
            })

            location.reload()
        } catch (error: any) {
            toast.error(t('error'))
        }
    }, [languages, currentLanguageCode, t])

    return {
        languages: languages ?? [],
        switchLanguage,
        currentLanguageCode,
    }
}
