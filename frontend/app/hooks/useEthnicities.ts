import { useCallback, useState } from 'react'

import { sessionStorageCache } from 'app/helpers'
import { useFetchData } from 'app/hooks'

import { TEthnicity } from 'app/types/common'
import { TSex } from 'app/types/user'

const cacheEntry = sessionStorageCache<TEthnicity[]>('ethnicities', 60 * 60 * 24)

export const useEthnicities = (sex: TSex) => {
    const [ethnicities, setEthnicities] = useState(() => cacheEntry.get())

    useFetchData<TEthnicity[]>(ApiEndpoints.ETHNICITIES, {
        load: ethnicities === null,
        onLoad: useCallback(response => {
            setEthnicities(response)

            cacheEntry.put(response)
        }, []),
        params: {
            sex,
        },
    })

    return ethnicities
}
