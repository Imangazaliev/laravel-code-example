import { useEffect, useState } from 'react'
import { debounce } from 'lodash'


function useKeyPress(targetKey: any) {
    const [keyPressed, setKeyPressed] = useState(false)

    // If pressed key is our target key then set to true
    const downHandler = ({ key }: any) => {
        if (key === targetKey) {
            setKeyPressed(true)
        }
    }

    // If released key is our target key then set to false
    const upHandler = ({ key }: any) => {
        if (key === targetKey) {
            setKeyPressed(false)
        }
    }

    useEffect(() => {
        window.addEventListener('keydown', debounce(downHandler, 200))
        window.addEventListener('keyup', debounce(upHandler, 200))

        return () => {
            window.removeEventListener('keydown', downHandler)
            window.removeEventListener('keyup', upHandler)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return keyPressed
}

export { useKeyPress}