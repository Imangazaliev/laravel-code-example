import { useCallback, useEffect, useState } from 'react'

import { sessionStorageCache } from 'app/helpers'
import { useFetchData, usePrevious } from 'app/hooks'

import { TLocality } from 'app/types/common'

const cacheEntry = sessionStorageCache<TLocality[]>('localities', 60 * 60 * 24)

export const useLocalities = (countryId: number | null, subdivisionId: number | null = null) => {
    const [localities, setLocalities] = useState(() => cacheEntry.get())

    useFetchData<TLocality[]>(ApiEndpoints.LOCALITIES, {
        load: countryId !== null && localities === null,
        onLoad: useCallback(response => {
            setLocalities(response)

            cacheEntry.put(response)
        }, []),
        params: {
            country_id: countryId,
            subdivision_id: subdivisionId,
        },
    })

    const prevCountryId = usePrevious(countryId)

    useEffect(() => {
        if (prevCountryId !== null && countryId !== prevCountryId) {
            setLocalities(null)

            cacheEntry.remove()
        }
    }, [countryId, prevCountryId])

    return localities
}
