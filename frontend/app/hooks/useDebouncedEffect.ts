import { useEffect } from 'react'
import { debounce } from 'lodash'

import { useDeepCallback } from 'app/hooks'

interface DebounceSettings {
    maxWait?: number
}

export const useDebouncedEffect = (callback: () => void, wait: number, options?: DebounceSettings) => {
    const callbackDebounced = useDeepCallback(debounce(callback, wait, options), [callback, options, wait])

    useEffect(() => {
        callbackDebounced()

        return callbackDebounced.cancel
    }, [callbackDebounced])
}
