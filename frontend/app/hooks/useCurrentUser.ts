import { useSelector } from 'react-redux'

import { RootState } from 'app/store/configureStore'
import { TCurrentUser } from 'app/types/user'

export const useCurrentUser = () => {
    return useSelector<RootState, TCurrentUser | null>(state => state.user)
}
