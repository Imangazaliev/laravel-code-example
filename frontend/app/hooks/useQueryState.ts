import { useCallback, useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'

import { objectToQueryString } from 'app/helpers'
import { useDeepCallback } from 'app/hooks/useDeepCallback'

type TState = Record<string, any>
type TValueType = 'string' | 'number' | 'string[]' | 'number[]'

const castValue = (value: string, type: TValueType) => {
    if (type === 'number') {
        return parseInt(value, 10)
    }

    if (type === 'string[]') {
        return value.split(',')
    }

    if (type === 'number[]') {
        return value.split(',').map((value: string) => parseInt(value, 10))
    }

    return value
}

export const useQueryState = <T extends TState>({
    afterDeserialization,
    initialState,
    types,
    updateQueryStringAutomatically = true,
}: {
    afterDeserialization?: (initialState: T, queryParameters: any) => Partial<T>
    initialState: T
    types?: Partial<Record<keyof T, TValueType>>
    updateQueryStringAutomatically?: boolean
}) => {
    const history = useHistory()

    const location = history.location

    const getQueryParameters = useCallback(() => {
        const queryParameters: any = Object.fromEntries(new URLSearchParams(location.search))

        const result: any = {}

        Object.keys(initialState).map(paramName => {
            const value = queryParameters[paramName]

            if (value === undefined) {
                return
            }

            if (types !== undefined && types[paramName] !== undefined) {
                const type = types[paramName] as TValueType

                if (type !== undefined) {
                    result[paramName] = castValue(value, type)
                }
            } else {
                result[paramName] = /^\d+$/.test(value) ? parseInt(value, 10) : value
            }
        })

        return afterDeserialization === undefined ? result : afterDeserialization(initialState, result)
    }, [afterDeserialization, initialState, location.search, types])

    // TODO: replace useRef because updating of the state updates updateQueryStringPublic function
    // that causes cascading call of dependent methods on every state change
    const [state, setState] = useState<T>(() => ({
        ...initialState,
        ...getQueryParameters(),
    }))

    const [queryString, setQueryString] = useState(location.search)

    const serializeQueryString = useDeepCallback((parameters: T) => {
        const changedParameters: any = {}

        Object.keys(parameters).forEach(paramName => {
            const parameterValue = (parameters as any)[paramName]
            const initialParameterValue = (initialState as any)[paramName]

            if (Array.isArray(parameterValue) && parameterValue.length === 0) {
                return
            }

            if (parameterValue !== initialParameterValue) {
                changedParameters[paramName] = parameterValue
            }
        })

        const queryString = objectToQueryString(changedParameters)

        return queryString === '' ? '' : `?${queryString}`
    }, [initialState])

    useEffect(() => {
        if (location.search !== queryString) {
            setState({
                ...initialState,
                ...getQueryParameters(),
            })
            setQueryString(location.search)
        }
    }, [location.search, initialState, getQueryParameters, queryString])

    const updateQueryString = useCallback((parameters: T) => {
        const queryString = serializeQueryString(parameters)

        if (location.search === queryString) {
            return
        }

        setQueryString(queryString)

        history.push({
            pathname: location.pathname,
            search: queryString,
        })
    }, [history, location.pathname, location.search, serializeQueryString])

    const setParameters = useCallback((parameters: Partial<T> | ((prevParameters: T) => Partial<T>)) => {
        setState(state => {
            if (typeof parameters === 'function') {
                parameters = parameters(state) as T
            }

            const newState = {
                ...state,
                ...parameters,
            }

            if (updateQueryStringAutomatically) {
                updateQueryString(newState)
            }

            return newState
        })
    }, [updateQueryStringAutomatically, updateQueryString])

    const reset = useCallback(() => setState(initialState), [initialState])
    const updateQueryStringPublic = useCallback(() => updateQueryString(state), [state, updateQueryString])

    return {
        parameters: state,
        reset,
        setParameters,
        queryString,
        updateQueryString: updateQueryStringPublic,
    }
}
