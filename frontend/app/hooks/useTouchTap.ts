import { TouchEvent, useCallback, useState } from 'react'

export type TTouchInfo = {
    x: number
    y: number
    startedAt: number
}

export const useTouchTap = ({
    handleTouch,
    startTouch,
}: {
    handleTouch: (event: TouchEvent<HTMLElement>, touchInfo: TTouchInfo) => void
    startTouch?: (event: TouchEvent<HTMLElement>, touchInfo: TTouchInfo) => boolean | undefined
}) => {
    const [touchInfo, setTouchInfo] = useState<TTouchInfo | null>(null)

    const handleTouchStart = useCallback((event: TouchEvent<HTMLElement>) => {
        const touch = event.touches.item(0)

        const touchInfo: TTouchInfo = {
            x: touch.clientX,
            y: touch.clientY,
            startedAt: Date.now(),
        }

        const start = startTouch === undefined || (startTouch(event, touchInfo) ?? true)

        setTouchInfo(start ? touchInfo : null)
    }, [startTouch])

    const handleTouchMove = useCallback(() => {
        // ignore swipes
        setTouchInfo(null)
    }, [])

    const handleTouchEnd = useCallback((event: TouchEvent<HTMLElement>) => {
        // ignore swipes
        if (touchInfo === null) {
            return
        }

        handleTouch(event, touchInfo)
    }, [touchInfo, handleTouch])

    return {
        handleTouchStart,
        handleTouchMove,
        handleTouchEnd,
        touchInfo,
    }
}
