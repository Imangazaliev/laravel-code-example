import { useMemo } from 'react'
import { format as formatFunc, parseISO } from 'date-fns'

function useFormatTime(time: string, format: string, defaultValue?: string): string;
function useFormatTime(time: string | null | undefined, format: string, defaultValue: string): string;
function useFormatTime(time: string | null | undefined, format: string, defaultValue?: string): string | null;
function useFormatTime(time: string | null | undefined, format: string, defaultValue?: string): string | null {
    return useMemo(() => {
        if (time === undefined || time === null) {
            return defaultValue ?? null
        }

        return formatFunc(parseISO(time), format)
    },[defaultValue, format, time])
}

export { useFormatTime }
