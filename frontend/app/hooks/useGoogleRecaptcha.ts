import { useEffect, useState } from 'react'

declare global {
    interface Window {
        grecaptcha: any
    }
}

export const useGoogleRecaptcha = (
    siteKey: string,
    onRecaptchaResponse: (key: string) => void
) => {
    const [recaptchaId, setRecaptchaId] = useState(null)

    useEffect(() => {
        const scriptEl = document.createElement('script')

        let intervalId: any

        const attemptToRenderRecaptcha = () => {
            if(window.grecaptcha && window.grecaptcha.render) {
                const recaptchaId =  window.grecaptcha.render('g-recaptcha', {
                    sitekey: siteKey,
                    callback: (key: string) => onRecaptchaResponse(key),
                })

                setRecaptchaId(recaptchaId)
                clearInterval(intervalId)
            }
        }

        scriptEl.async = false
        scriptEl.src = 'https://www.google.com/recaptcha/api.js?render=explicit'
        scriptEl.onload = () => {
            if (recaptchaId !== null) {
                return
            }

            intervalId = setInterval(attemptToRenderRecaptcha, 500)
        }

        document.body.appendChild(scriptEl)

        return () => {
            clearInterval(intervalId)

            scriptEl.remove()
        }
    }, [onRecaptchaResponse, recaptchaId, siteKey])

    return recaptchaId
}
