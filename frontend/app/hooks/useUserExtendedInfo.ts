import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'

import http from 'app/core/axios'

import { TCurrentUserExtendedInfo } from 'app/types/user'

export const useUserExtendedInfo = (load: boolean = true): TCurrentUserExtendedInfo | null => {
    const { t } = useTranslation()

    const [isLoading, setIsLoading] = useState(false)
    const [extendedInfo, setExtendedInfo] = useState<TCurrentUserExtendedInfo | null>(null)

    useEffect(() => {
        if (load && ! isLoading && extendedInfo === null) {
            loadExtendedInfo()
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [load, extendedInfo])

    const loadExtendedInfo = async () => {
        try {
            setIsLoading(true)

            const response = await http.get(ApiEndpoints.PROFILE_EXTENDED_INFORMATION)

            setIsLoading(false)
            setExtendedInfo(response.data)
        } catch (error: any) {
            if (error.response !== undefined) {
                if (error.response.data.message !== undefined) {
                    toast.error(error.response.data.message)

                    return
                }
            }

            toast.error(t('errors.error-loading-data'))
        }
    }

    return extendedInfo
}
