import { useMemo } from 'react'
import isTouchDevice from 'is-touch-device'

export const useIsTouchDevice = () => {
    return useMemo(() => isTouchDevice(), [])
}
