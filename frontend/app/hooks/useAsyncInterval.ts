import { useEffect, useState } from 'react'

export const useAsyncInterval = ({
    callback,
    enabled = true,
    interval,
}: {
    callback: () => Promise<any>
    enabled?: boolean
    interval: number
}) => {
    const [needCall, setNeedCall] = useState(enabled)

    useEffect(() => {
        if ( ! enabled) {
            return
        }

        if ( ! needCall) {
            const timerId: any = setTimeout(() => {
                setNeedCall(true)
            }, interval)

            return () => clearTimeout(timerId)
        }

        callback().then(() => setNeedCall(false))
    }, [callback, enabled, interval, needCall])
}
