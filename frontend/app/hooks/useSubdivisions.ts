import { useCallback, useEffect, useState } from 'react'

import { sessionStorageCache } from 'app/helpers'
import { useFetchData, usePrevious } from 'app/hooks'

import { TSubdivision } from 'app/types/common'

const cacheEntry = sessionStorageCache<TSubdivision[]>('subdivisions', 60 * 60 * 24)

export const useSubdivisions = (countryId: number | null) => {
    const [subdivisions, setSubdivisions] = useState(() => cacheEntry.get())

    useFetchData<TSubdivision[]>(ApiEndpoints.SUBDIVISIONS, {
        load: countryId !== null && subdivisions === null,
        onLoad: useCallback(response => {
            setSubdivisions(response)

            cacheEntry.put(response)
        }, []),
        params: {
            country_id: countryId,
        },
    })

    const prevCountryId = usePrevious(countryId)

    useEffect(() => {
        if (prevCountryId !== null && countryId !== prevCountryId) {
            setSubdivisions(null)

            cacheEntry.remove()
        }
    }, [countryId, prevCountryId])

    return subdivisions
}
