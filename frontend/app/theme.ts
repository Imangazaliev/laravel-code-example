import { createMuiTheme } from '@material-ui/core/styles'

import {
    colors,
    fonts,
    shapes,
    height,
    fontSize,
    spacing,
    mediaQueries,
    marginStart,
    marginEnd,
    paddingStart,
    paddingEnd,
} from 'app/styles'

const MuiButton = {
    root: {
        borderRadius: shapes.base,
        height: height.medium,
        fontSize: 16,
    },
    label: {
        fontFamily: fonts.base,
        fontWeight: 500,
    },
    contained: {
        color: colors.white,
        boxShadow: 'none',
        backgroundColor: colors.gray[50],

        '&:hover': {
            boxShadow: 'none',
            backgroundColor: colors.gray[50],
        },

        '&:active': {
            boxShadow: 'none',
        },

        '& $label': {
            color: colors.black,
        },
    },
    containedPrimary: {
        color: colors.white,
        backgroundColor: colors.primary,

        [mediaQueries.phoneMedium]: {
            fontSize: 14,
        },

        '&:hover': {
            backgroundColor: colors.primary,
        },

        '& $label': {
            color: colors.white,
        },
    },

    containedSecondary: {
        '& $label': {
            color: colors.white,
        },
    },

    outlinedPrimary: {
        color: colors.primary,
        border: `1px solid  ${ colors.primary }`,

        [mediaQueries.phoneMedium]: {
            fontSize: 14,
        },

        '&:hover': {
            border: `1px solid  currentColor`,
            backgroundColor: 'transparent',
        },
    },
    textPrimary: {
        '& $label': {
            color: colors.primary,
        },
    },
    textSizeLarge: {
        padding: `6px ${spacing.small}px`,
        fontSize: fontSize.large,
    },
    textSizeSmall: {
        padding: `6px 12px`,
        fontSize: fontSize.small,
    },
    outlinedSizeSmall: {
        [paddingStart]: `16px`,
        [paddingEnd]: `16px`,
    },
    sizeLarge: {
        height: height.large,
        fontSize: fontSize.large,
    },
    sizeSmall: {
        height: height.small,
        borderRadius: shapes.small,
        fontSize: 14,
    },
}

const MuiInputBase = {
    root: {
        width: '100%',
        border: `1px solid ${ colors.gray[200] }`,
        borderRadius: shapes.base,

        '&$focused': {
            borderColor: colors.primary,
        },

        '&.Mui-error': {
            borderColor: colors.error,
        },
    },

    input: {
        padding: `0 ${spacing.small}px`,
        fontSize: fontSize.large,

        '&:placeholder' :  {
            color: colors.gray[400],
        },
    },

    multiline: {
        display: 'flex',
        alignItems: 'flex-start',
        padding: 0,
    },

    inputMultiline: {
        padding: `14px ${spacing.small}px`,
    },

}

const MuiInput = {
    input: {
        height: '100%',
    },

    underline: {
        '&::before': {
            display: 'none',
        },

        '&::after': {
            display: 'none',
        },
    },
}

const MuiSelect  = {
    select: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',

        '&:focus': {
            backgroundColor: 'transparent',
        },
    },

    selectMenu: {
        height: '100%',
        [paddingStart]: 14,

        '& .MuiMenuItem-root': {
            padding: 0,
            width: '100%',
        },

        '& .MuiListItem-button': {
            '&:hover': {
                backgroundColor: 'transparent',
            },
        },
    },

    icon: {
        color: colors.gray[300],
        right: 8,
    },
}

const MuiMenu = {
    paper: {
        maxHeight: 300,
        marginTop: 10,
    },
}

const MuiListItem = {
    root: {
        '&.Mui-selected': {
            backgroundColor: colors.gray[50],

            '&:hover': {
                backgroundColor: colors.gray[50],
            },
        },
    },
    button: {
        '&:hover': {
            backgroundColor: colors.gray[50],
        },
    },
}

const MuiCheckbox = {
    root: {
        padding: 5,
    },
    colorPrimary: {
        minWidth: 20,
        minHeight: 20,
        '&$checked': {
            color: colors.primary,
        },
        '&:hover': {
            backgroundColor: colors.gray[50],
        },
    },
}

const MuiFormControlLabel = {
    root: {
        [marginStart]: 0,
        [marginEnd]: 0,
    },

    labelPlacementStart: {
        [marginStart]: 0,
    },
}

const MuiFab = {
    root: {
        boxShadow: 'none',
        backgroundColor: 'transparent',

        '&:hover': {
            backgroundColor: 'transparent',
        },

        '&:active': {
            boxShadow: 'none',
        },

        '&.Mui-disabled': {
            backgroundColor: 'transparent',
            fill: colors.gray[300],
        },
    },

    sizeSmall: {
        width:  24,
        height: 24,
    },

    sizeMedium: {
        width:  40,
        height: 40,
    },
}

const MuiSwitch = {
    root: {
        width: 36,
        height: 22,
        padding: 0,
    },
    track: {
        borderRadius: 24,
    },
    thumb: {
        width: 16,
        height: 16,
        backgroundColor: colors.white,
    },
    switchBase: {
        padding: 3,
        transition: 'all .4s cubic-bezier(.54,1.85,.5,1)',

        '&.Mui-checked': {
            transform: 'translateX(64%)',

            '& + .MuiSwitch-track': {
                opacity: 1,
            },
        },
    },
}

const MuiTableContainer = {
    root: {
        backgroundColor: colors.white,
        borderRadius: shapes.base,
        boxShadow: '0 0 2px 1px rgba(100, 100, 100, .3)',
    },
}

const MuiTable = {
    root: {
        minWidth: 700,

        [mediaQueries.phoneLarge]: {
            minWidth: 'inherit',
        },
    },
}

const MuiTableHead = {
    root: {
        backgroundColor: colors.gray[100],
    },
}

const MuiTableRow = {
    root: {
        '&:nth-of-type(even)': {
            backgroundColor: colors.gray[50],
        },
    },
}

const MuiTableCell = {
    root: {
        height: 56,
        padding: 8,
        border: 0,
        fontSize: '1em',

        [mediaQueries.phoneLarge]: {
            whiteSpace: 'nowrap',
        },
    },
}

const MuiRadio = {
    root: {
        padding: 2,
        color: colors.gray[300],

        '& .MuiSvgIcon-root': {
            fontSize: 20,
        },
    },
}

const MuiTab = {
    root: {
        borderBottom: `1px solid ${ colors.gray[200] }`,
        fontWeight: 500,
        fontSize: 16,
        lineHeight: '24px',
        color: colors.gray[500],

        '&.Mui-selected': {
            border: `1px solid ${ colors.gray[200] }`,
            borderBottom: 'none',
            borderRadius: `${shapes.small}px ${shapes.small}px 0px 0px`,
            zIndex: 1,
            color:  colors.primary,
        },
    },
}

const MuiTabs = {
    indicator: {
        backgroundColor: colors.white,
        transition: 'none',
    },

    flexContainer: {
        borderBottom: `1px solid ${ colors.gray[200] }`,
    },
}

const MuiPopover = {
    paper: {
        boxShadow: '0px 10px 20px rgba(50, 60, 69, 0.1)',
        borderRadius: 4,
    },
}

export default createMuiTheme({
    palette: {
        primary: {
            light: '#F1FEF7',
            main: colors.primary,
        },
        secondary: {
            main: colors.secondary,
        },
        error: {
            light: '#FFF2F2',
            main: colors.error,
        },
        grey: colors.gray,
    },
    props: {
        MuiButtonBase: {
            disableRipple: true,
        },
    },
    spacing: spacing.extraSmall,
    shape: {
        borderRadius: shapes.small,
    },
    typography: {
        button: {
            textTransform: 'none',
        },
    },
    overrides: {
        MuiButton,
        MuiInput,
        MuiInputBase,
        MuiMenu,
        MuiListItem,
        MuiSelect,
        MuiCheckbox,
        MuiFormControlLabel,
        MuiFab,
        MuiSwitch,
        MuiTable,
        MuiTableCell,
        MuiTableContainer,
        MuiTableHead,
        MuiTableRow,
        MuiRadio,
        MuiTab,
        MuiTabs,
        MuiPopover,
    },
})
