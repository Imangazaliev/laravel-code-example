import i18n from 'i18next'

import { TMessageStatus } from 'app/types/chat'

export const chatMessageStatuses = {
    [TMessageStatus.ACCEPTED]: i18n.t('chat:message-status.delivered'),
    [TMessageStatus.CANCELED]: i18n.t('chat:message-status.canceled'),
    [TMessageStatus.PENDING_MODERATION]: i18n.t('chat:message-status.pending-moderation'),
    [TMessageStatus.REJECTED_BY_MODERATOR]: i18n.t('chat:message-status.rejected-by-moderator'),
}
