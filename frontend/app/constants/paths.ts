export const getFlagIconPath = (name: string, square: boolean = false) => {
    const directory = square ? '1x1' : '4x3'

    return `/images/flags/${directory}/${name.toLowerCase()}.svg`
}
