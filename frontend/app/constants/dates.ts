import i18n from 'i18next'

import { range } from 'app/helpers/range'

export const DATE_FORMAT = 'Y-MM-dd'
export const DATE_TIME_FORMAT = 'Y-MM-dd HH:mm:ss'
export const DATE_TIME_WITH_TIMEZONE_FORMAT = 'Y-MM-dd HH:mm:ssxxx'
export const DATE_TIME_HUMAN_FORMAT = 'Y-MM-dd — kk:mm'
export const DATE_TIME_HUMAN_WITH_SECONDS_FORMAT = 'Y-MM-dd — kk:mm:ss'

const monthNames = [
    i18n.t('months.january'),
    i18n.t('months.february'),
    i18n.t('months.march'),
    i18n.t('months.april'),
    i18n.t('months.may'),
    i18n.t('months.june'),
    i18n.t('months.july'),
    i18n.t('months.august'),
    i18n.t('months.september'),
    i18n.t('months.october'),
    i18n.t('months.november'),
    i18n.t('months.december'),
]

export const months = range(1, 12).map(month => ({
    value: month,
    label: monthNames[month - 1],
}))
