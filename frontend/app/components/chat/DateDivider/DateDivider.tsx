import React, { FC } from 'react'

import { Text } from 'app/components'

import styles from './styles'

const { classes } = styles

interface DateDividerProps {
    date: string
}

export const DateDivider: FC<DateDividerProps> = ({ date }) => {
    return (
        <div className={ classes.divider }>
            <Text bold>{ date }</Text>
        </div>
    )
}
