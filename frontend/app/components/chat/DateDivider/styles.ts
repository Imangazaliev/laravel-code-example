import { Styles } from 'jss'

import { jss }  from 'app/core/jss'
import theme from 'app/theme'
import { flexBoxMixin } from 'app/styles'

const styles: Styles = {
    divider: {
        height: 32,
        ...flexBoxMixin({
            justifyContent: 'center',
            alignment: 'center',
        }),
        width: 'fit-content',
        padding: `0px ${ theme.spacing(2) }px`,
        margin: 'auto',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        backgroundColor: theme.palette.common.white,
        border: `1px solid ${ theme.palette.grey[100] }`,
        borderRadius: 16,

        '& .text': {
            fontSize: 12,
        },
    },
}

export default jss.createStyleSheet(styles).attach()
