import React, { FC, useMemo } from 'react'
import { useTranslation } from 'react-i18next'

import { ChatMessageBase } from 'app/components/chat'

import { TChatMessage, TChatElementDirection, TMessageStatus } from 'app/types/chat'
import { ChatMessageBaseProps } from 'app/components/chat/ChatMessageBase/ChatMessageBase'
import { TStatusIcon } from 'app/components/chat/ChatMessageBase/types'

import { getMessageText } from 'app/helpers/chat'

export interface ChatMessageProps extends Omit<
    ChatMessageBaseProps<TChatMessage>,
    'hideText'
> {
    respectStatus?: boolean
}

const ChatMessage: FC<ChatMessageProps> = ({
    message,
    respectStatus = true,
    ...props
}) => {
    const { t } = useTranslation()

    const hideText = (
        respectStatus &&
        message.status === TMessageStatus.PENDING_MODERATION &&
        message.direction === TChatElementDirection.INCOMING
    )

    const statusIcon = useMemo(() => {
        const map = {
            [TMessageStatus.PENDING_MODERATION]: TStatusIcon.SHIELD,
            [TMessageStatus.CANCELED]: TStatusIcon.NO_SIGN,
            [TMessageStatus.ACCEPTED]: message.seen_at === null ? TStatusIcon.SENT : TStatusIcon.SEEN,
            [TMessageStatus.REJECTED_BY_MODERATOR]: TStatusIcon.NO_SIGN,
        }

        const tooltipText = (
            message.status === TMessageStatus.ACCEPTED &&
            message.direction === TChatElementDirection.OUTGOING &&
            message.seen_at !== null
        ) ? t('chat:message-status.seen') : null

        return {
            name: map[message.status],
            tooltipText,
        }
    }, [message.status, message.direction, message.seen_at, t])

    const newMessage = useMemo(() => ({
        ...message,
        text: getMessageText(message, respectStatus),
        replied_message: message.replied_message === null ? null : {
            ...message.replied_message,
            text: getMessageText(message.replied_message, respectStatus),
        },
    }), [message, respectStatus])

    return <ChatMessageBase<TChatMessage>
        hideText={ hideText }
        message={ newMessage }
        statusIcon={ statusIcon }
        { ...props }
    />
}

export { ChatMessage }
