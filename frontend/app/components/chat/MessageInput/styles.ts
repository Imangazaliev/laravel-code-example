import { createUseStyles } from 'react-jss'

import theme from 'app/theme'
import { marginStart, mediaQueries, paddingStart, shapes } from 'app/styles'

export const useStyles = createUseStyles({
    send_button: {
        [marginStart]: theme.spacing(2),
        width: 113,
        minWidth: 113,
        borderRadius:  shapes.base,
        fontWeight: 500,
        lineHeight: `24px`,

        [mediaQueries.phoneLarge]: {
            display: 'none',
        },
    },

    message_input: {
        minHeight: '100%',

        [mediaQueries.phoneLarge]: {
            border: 'none',
        },
    },

    send_button_mobile: {
        display: 'none',
        alignItems: 'center',
        height: '100%',
        padding: theme.spacing(1),
        [paddingStart]: theme.spacing(2),

        [mediaQueries.phoneLarge]: {
            display: 'flex',
        },
    },

    send_button_mobile_icon: {
        fill: theme.palette.primary.main,
    },

    send_button_mobile_icon_disabled: {
        fill: 'rgba(0, 0, 0, .38)',
    },
})
