import React, { ChangeEvent, KeyboardEvent, FC, useCallback, useState, useEffect, useRef } from 'react'
import i18n from 'i18next'
import classNames from 'classnames'

import { mergeRefs, moveCursorToEnd } from 'app/helpers'
import { useMediaQuery, usePrevious } from 'app/hooks'
import { useSaveInputValue } from 'app/hooks/chat'

import { Input, Button, IconButton } from 'app/components'

import { useStyles } from './styles'

interface MessageInputProps {
    autoFocus?: boolean
    defaultValue?: string | null
    disabled?: boolean
    inputRef?: any
    maxLength: number
    onChange?: (messageText: string) => void
    onFocus?: () => void
    onSend: (messageText: string) => Promise<unknown>
    saveInputValueKey: string
    value?: string | null
}

const MessageInput: FC<MessageInputProps> = React.memo(({
    autoFocus = true,
    defaultValue = null,
    disabled: disabledProp = false,
    inputRef: inputRefProp = null,
    maxLength,
    onChange,
    onFocus,
    onSend,
    saveInputValueKey,
    value: valueProp = null,
}) => {
    const classes = useStyles()

    const inputRef = useRef<HTMLTextAreaElement | null>(null)

    const {
        getInputValue,
        saveInputValue,
    } = useSaveInputValue(saveInputValueKey)

    const isPhone = useMediaQuery('phoneMedium')

    const [disabled, setInputDisabled] = useState(disabledProp)
    const [messageText, setMessageText] = useState(() => {
        return defaultValue === null ? getInputValue() : defaultValue
    })

    useEffect(() => {
        setInputDisabled(disabledProp)
    }, [disabledProp])

    const prevValueProp = usePrevious(valueProp)

    useEffect(() => {
        if (valueProp === null) {
            if (prevValueProp !== null) {
                setMessageText('')
            }

            return
        }

        setMessageText(valueProp)
    }, [prevValueProp, valueProp])

    useEffect(() => {
        if (valueProp !== null) {
            return
        }

        saveInputValue(messageText)
    }, [saveInputValue, messageText, valueProp])

    useEffect(() => {
        // focus on message text change (after reset for example)
        if (
            autoFocus &&
            inputRef.current !== null &&
            document.activeElement !== inputRef.current
        ) {
            inputRef.current.focus()

            moveCursorToEnd(inputRef.current)
        }
    }, [autoFocus])

    useEffect(() => {
        if (onChange !== undefined) {
            onChange(messageText)
        }
    }, [messageText, onChange])

    const handleChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        setMessageText(event.target.value)
    }, [])

    const handleSendMessageButtonClick = useCallback(() => {
        if (disabled || messageText.length === 0) {
            return
        }

        setInputDisabled(true)

        onSend(messageText)
            .then(() => {
                // clear the input after a message was sent
                setMessageText('')
            })
            .finally(() => {
                setInputDisabled(false)

                if (inputRef.current !== null) {
                    inputRef.current.focus()
                }
            })
    }, [disabled, messageText, onSend])

    const handleKeyPress = useCallback((event: KeyboardEvent) => {
        // if enter pressed and it's not a combination
        const enterPressed =  ! event.ctrlKey && ! event.shiftKey && event.keyCode === 13

        // send a message on press Enter
        if (enterPressed && ! isPhone) {
            event.preventDefault()

            handleSendMessageButtonClick()
        }
    }, [handleSendMessageButtonClick, isPhone])

    const mergedInputRef = mergeRefs([inputRef, inputRefProp])

    return (
        <div className="message-input-content">
            <div className="message-input">
                <Input
                    classes={{
                        root: classes.message_input,
                    }}
                    autoComplete="off"
                    placeholder="Сообщение"
                    disabled={ disabled }
                    inputProps={{
                        autoCapitalize: 'on',
                        maxLength: maxLength,
                    }}
                    multiline
                    name="message"
                    onChange={ handleChange }
                    onFocus={ onFocus }
                    onKeyDown={ handleKeyPress }
                    ref={ mergedInputRef }
                    type="text"
                    size="medium"
                    value={ messageText }
                />

                <Button
                    size="medium"
                    color="primary"
                    variant="contained"
                    classes={{
                        root: classes.send_button,
                    }}
                    onClick={ handleSendMessageButtonClick }
                    disabled={ disabled }
                >
                    { i18n.t('chat:send') }
                </Button>

                <div className={ classes.send_button_mobile } onClick={ handleSendMessageButtonClick }>
                    <IconButton
                        className={ classNames(classes.send_button_mobile_icon, {
                            [classes.send_button_mobile_icon_disabled]: disabled,
                        }) }
                        disabled={ disabled }
                        icon="send-arrow"
                        size={ 24 }
                    />
                </div>
            </div>
        </div>
    )
})

export { MessageInput }
