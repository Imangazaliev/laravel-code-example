import React, { useCallback } from 'react'
import classNames from 'classnames'

import { useTouchTap } from 'app/hooks'

import { Icon, Tooltip } from 'app/components'

import { TStatusIcon } from './types'

import { iconsMap } from './constants'

import styles from './ChatMessageBase.module.scss'

interface MessageStatusIconProps {
    name: TStatusIcon
    setTooltipOpen: (value: boolean) => void
    tooltipOpen: boolean
    tooltipText: string | null
}

const MessageStatusIcon = React.forwardRef<any, MessageStatusIconProps>(({
    name,
    setTooltipOpen,
    tooltipOpen,
    tooltipText,
}, ref) => {
    const iconTouchTap = useTouchTap({
        handleTouch: () => {
            setTooltipOpen(true)
        },
    })

    const className = classNames(styles.messageInfoIcon, {
        [styles.messageStatusIconSeen]: name === TStatusIcon.SEEN,
        [styles.messageStatusIconNoSign]: name === TStatusIcon.NO_SIGN,
    })

    const RootComponent = useCallback(({ children }: any) => {
        if (tooltipText === null) {
            return <>
                { children }
            </>
        }

        return <Tooltip
            children={ children }
            open={ tooltipOpen }
            placement="top"
            title={ tooltipText }
        />
    }, [tooltipOpen, tooltipText])

    return <RootComponent>
        <div
            ref={ ref }
            className={ className }
            onMouseOver={ () => setTooltipOpen(true) }
            onMouseLeave={ () => setTooltipOpen(false) }
            onTouchStart={ iconTouchTap.handleTouchStart }
            onTouchMove={ iconTouchTap.handleTouchMove }
            onTouchEnd={ iconTouchTap.handleTouchEnd }
        >
            {
                <Icon name={ iconsMap[name] } />
            }
        </div>
    </RootComponent>
})

export { MessageStatusIcon }
