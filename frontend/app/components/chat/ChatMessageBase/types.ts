export enum TStatusIcon {
    SHIELD = 'shield',
    SENT = 'sent',
    SEEN = 'seen',
    NO_SIGN = 'no-sign',
}

export type TMessageMenuItem<T> = {
    code: string
    title: string
    onClick: (message: T) => void
}
