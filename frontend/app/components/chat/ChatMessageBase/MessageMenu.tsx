import React, { useCallback } from 'react'
import { Menu, MenuItem } from '@material-ui/core'
import memoizeOne from 'memoize-one'

import { Text } from 'app/components'

import { TMessageMenuItem } from './types'

interface MessageMenuProps<T> {
    anchorEl?: Element | null
    items: TMessageMenuItem<T>[]
    message: T
    onClose: () => void
    open: boolean
}

function MessageMenu<T>({
    anchorEl,
    items,
    message,
    onClose,
    open,
}: MessageMenuProps<T>) {
    const handleClick = useCallback(memoizeOne((item) => () => {
        item.onClick(message)

        onClose()
    }), [message, onClose])

    return (
        <Menu
            open={ open }
            anchorEl={ anchorEl }
            anchorOrigin={{
                horizontal: 'center',
                vertical: 'center',
            }}
            anchorReference="anchorEl"
            getContentAnchorEl={ null }
            onClose={ onClose }
        >
            {
                items.map(item => (
                    <MenuItem key={ item.code } onClick={ handleClick(item) }>
                        <Text>{ item.title }</Text>
                    </MenuItem>
                ))
            }
        </Menu>
    )
}

export { MessageMenu }
