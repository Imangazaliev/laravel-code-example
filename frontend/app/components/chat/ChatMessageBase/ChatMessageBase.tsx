import React, { useCallback, useRef, useState } from 'react'
import classNames from 'classnames'

import { useFormatTime, useOnClickOutside } from 'app/hooks'

import { IconButton, Text } from 'app/components'
import { MessageStatusIcon } from './MessageStatusIcon'
import { MessageMenu } from './MessageMenu'

import { TChatElementDirection, TChatMessageBase } from 'app/types/chat'
import { TMessageMenuItem, TStatusIcon } from './types'

import styles from './ChatMessageBase.module.scss'

export interface ChatMessageBaseProps<T extends TChatMessageBase> {
    clickable?: boolean | ((message: T) => boolean)
    hideText: boolean
    highlighted?: boolean | ((message: T) => boolean)
    menuItems?: TMessageMenuItem<T>[] | ((message: T) => TMessageMenuItem<T>[])
    message: T
    onClick?: (message: T) => void
    selected?: boolean | ((message: T) => boolean)
    showStatusForIncoming?: boolean
    statusIcon?: {
        name: TStatusIcon
        tooltipText: string | null
    }
}

function ChatMessageBase<T extends TChatMessageBase>({
    clickable = false,
    hideText,
    highlighted = false,
    menuItems = [],
    message,
    onClick,
    selected = false,
    showStatusForIncoming = false,
    statusIcon,
}: ChatMessageBaseProps<T>) {
    const statusIconRef = useRef<HTMLDivElement>(null)
    const menuToggleRef = useRef<HTMLButtonElement>(null)
    const contextMenuRef = useRef(null)

    const [menuOpen, setMenuOpen] = useState(false)
    const [statusTooltipOpen, setStatusTooltipOpen] = useState(false)

    clickable = typeof clickable === 'function' ? clickable(message) : clickable

    const handleClick = useCallback((event: any) => {
        if (menuToggleRef.current !== null && menuToggleRef.current.contains(event.target)) {
            return
        }

        if (clickable && onClick !== undefined) {
            onClick(message)
        }
    }, [clickable, message, onClick])

    const handleOpenMenuClick = useCallback(() => {
        setMenuOpen(state => ! state)
    }, [])

    const handleCloseMenu = useCallback(() => {
        setMenuOpen(false)
    }, [])

    useOnClickOutside(statusIconRef, useCallback((event: any) => {
        if (statusIconRef.current !== null && ! statusIconRef.current.contains(event.target)) {
            return setStatusTooltipOpen(false)
        }
    }, []))

    const messageTextCls = classNames(styles.messageText, {
        [styles.messageTextHidden]: hideText,
    })

    const repliedMessage = message.replied_message

    const repliedMessageCls = classNames(styles.repliedMessage, {
        [styles.repliedMessageIncoming]: message.direction === TChatElementDirection.INCOMING,
        [styles.repliedMessageOutgoing]: message.direction === TChatElementDirection.OUTGOING,
    })

    selected = typeof selected === 'function' ? selected(message) : selected
    highlighted = typeof highlighted === 'function' ? highlighted(message) : highlighted

    const messageRowCls = classNames(styles.messageRow, {
        [styles.messageRowOutgoing]: message.direction === TChatElementDirection.OUTGOING,
        [styles.messageRowClickable]: clickable,
        [styles.messageRowSelected]: selected,
    })

    const messageCls = classNames(styles.message, {
        [styles.messageIncoming]: message.direction === TChatElementDirection.INCOMING,
        [styles.messageOutgoing]: message.direction === TChatElementDirection.OUTGOING,
        [styles.messageHighlighted]: highlighted,
    })

    menuItems = typeof menuItems === 'function' ? menuItems(message) : menuItems

    const sentAt = useFormatTime(message.sent_at, 'HH:mm')

    return (
        <>
            <div className={ messageRowCls } onClick={ handleClick }>
                <div className={ messageCls }>
                    {
                        repliedMessage !== null &&
                        <div className={ repliedMessageCls }>
                            <Text className={ styles.repliedMessageText }>{ repliedMessage.text }</Text>
                        </div>
                    }
                    <div className={ messageTextCls }>
                        <Text>{ message.text }</Text>
                    </div>
                    <span className={ styles.messageInfo }>
                        {
                            statusIcon !== undefined &&
                            (message.direction === TChatElementDirection.OUTGOING || showStatusForIncoming) &&
                            <MessageStatusIcon
                                name={ statusIcon.name }
                                ref={ statusIconRef }
                                setTooltipOpen={ setStatusTooltipOpen }
                                tooltipOpen={ statusTooltipOpen }
                                tooltipText={ statusIcon.tooltipText }
                            />
                        }
                        {
                            sentAt !== null &&
                            <span className={ styles.messageTime }>{ sentAt }</span>
                        }
                    </span>
                </div>

                {
                    menuItems.length > 0 &&
                    <IconButton
                        className={ styles.messageMenuToggle }
                        icon="more"
                        onClick={ handleOpenMenuClick }
                        ref={ menuToggleRef }
                        size={ 24 }
                    />
                }
            </div>
            <div ref={ contextMenuRef }>
                <MessageMenu
                    anchorEl={ menuToggleRef.current }
                    items={ menuItems }
                    message={ message }
                    onClose={ handleCloseMenu }
                    open={ menuOpen }
                />
            </div>
        </>
    )
}

export { ChatMessageBase }
