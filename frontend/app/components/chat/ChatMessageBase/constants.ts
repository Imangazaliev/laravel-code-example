import { TIconName } from 'app/components/common/Icon'
import { TStatusIcon } from './types'

export const iconsMap: Record<TStatusIcon, TIconName> = {
    [TStatusIcon.SHIELD]: 'shield',
    [TStatusIcon.SENT]: 'check-double',
    [TStatusIcon.SEEN]: 'check-double',
    [TStatusIcon.NO_SIGN]: 'no-sign',
}
