import React, { FC } from 'react'

import { InnerWrapper, Page } from 'app/components/layout'
import { Header } from 'app/components'

import jss from 'jss'

import { flexBoxMixin } from 'app/styles'

const styles = jss.createStyleSheet({
    main: {
        display: 'flex',
        flex: '1 1 auto',
    },

    center: {
        margin: 'auto',
        ...flexBoxMixin({
            alignment: 'center',
            justifyContent: 'center',
        }),
        height: 350,
        width: '100%',
        maxWidth: 1300,
    },

}).attach()

const ErrorPageLayout: FC = ({
    children,
}) => {
    return (
        <Page>
            <Header />
            <InnerWrapper>
                <main className={ styles.classes.main }>
                    <div className={ styles.classes.center }>
                        { children }
                    </div>
                </main>
            </InnerWrapper>
        </Page>
    )
}

export { ErrorPageLayout }
