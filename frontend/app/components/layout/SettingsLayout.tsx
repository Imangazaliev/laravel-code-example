import React, { ReactNode, FC, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { NavLink, useHistory } from 'react-router-dom'

import { usePermission } from 'app/hooks'

import { MainLayout } from 'app/components/layout'
import { Title, Text, Spoiler } from 'app/components'

type TSection = {
    title: string
    url: string
    children?: string[]
}

interface SettingsLayoutProps {
    children: ReactNode
    title?: string
}

const SettingsLayout: FC<SettingsLayoutProps> = ({
    children,
    title,
}) => {
    const { t } = useTranslation()
    const { hasPermission } = usePermission()
    const history  = useHistory()

    const sections = useMemo((): TSection[] => ([
        {
            title: t('settings.sections.common'),
            url: Routes.SETTINGS,
            children: [
                t('auth:phone-number-change'),
                t('auth:email-change'),
                t('auth:password.password-change'),
                t('profile:profile-deletion'),
            ],
        },
        {
            title: t('notifications'),
            url: Routes.SETTINGS_NOTIFICATIONS,
        },
        {
            title: t('connected-accounts'),
            url: Routes.SETTINGS_CONNECTED_ACCOUNTS,
        },
        {
            title: t('transaction-history'),
            url: Routes.SETTINGS_COINS_TRANSACTION_HISTORY,
        },
        ...(hasPermission('hide-profile') || hasPermission('incognito-mode') ? [{
            title: t('additional-services'),
            url: Routes.SETTINGS_ADDITIONAL_SERVICES,
        }] : []),
    ]), [hasPermission, t])

    const spoilerSections = useMemo(() => sections.reduce<Record<string, string>>((result, item) => {
        result[item.url] = item.title

        return result
    }, {}), [sections])

    return (
        <MainLayout title={ title }>
            <div className="settings">
                <Title component="h3" noMarginTop>{ t('settings') }</Title>
                <div className="settings--grid">
                    { children }
                    <div className="settings-navigation">
                        <Spoiler
                            className="settings-navigation__list"
                            currentSection={ history.location.pathname }
                            sections={ spoilerSections }
                        >
                            {
                                sections.map(section => (
                                    <React.Fragment key={ section.url }>
                                        <NavLink
                                            key={ section.url }
                                            activeClassName="active"
                                            className="settings-navigation__item"
                                            exact
                                            to={ section.url }
                                        >
                                            <Text bold>{ section.title }</Text>
                                        </NavLink>
                                        {
                                            section.children?.map((subSection, index) => (
                                                <div key={ index } className="settings-navigation__step">
                                                    <Text size="small">{ subSection }</Text>
                                                </div>
                                            ))
                                        }
                                    </React.Fragment>
                                ))
                            }
                        </Spoiler>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export { SettingsLayout }
