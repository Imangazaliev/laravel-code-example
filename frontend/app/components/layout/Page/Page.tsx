import React, { FC, ReactNode, useMemo } from 'react'
import { Helmet  } from 'react-helmet'
import { useTranslation } from 'react-i18next'

interface PageProps {
    children: ReactNode
    title?: string
}

const Page: FC<PageProps> = ({
    children,
    title,
}) => {
    const { t } = useTranslation()

    const description = useMemo(() => `${ ENV.siteName } — ${ t('seo.description') }`, [t])

    const ldJson: any = {
        '@context' : 'http://schema.org',
        '@type' : 'Organization',
        'name' : ENV.siteName,
        'url' : ENV.siteUrl,
        'description': description,
        'sameAs' : [],
    }

    return (
        <>
            <Helmet>
                <meta charSet="utf-8" />

                <title>{ title !== undefined ? `${ title } - ${ ENV.siteName }` : ENV.siteName }</title>

                <meta name="description" content={ description } />

                <link rel="canonical" href={ ENV.siteUrl } />
                <link rel="shortcut icon" href="/favicons/favicon.ico" />

                <script type="application/ld-json">
                    { JSON.stringify(ldJson) }
                </script>
            </Helmet>

            { children }
        </>
    )
}

export { Page }
