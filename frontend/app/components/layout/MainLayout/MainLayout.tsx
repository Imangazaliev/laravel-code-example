import React, { FC, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { closeHelpModal } from 'app/store/modules/helpModal'
import { closeAddCoinsModal } from 'app/store/modules/addCoinsModalOpen'

import { InnerWrapper, Page } from 'app/components/layout'
import { Container, Footer, Header } from 'app/components'
import { AddCoinsModal } from './AddCoinsModal/AddCoinsModal'
import { HelpModal } from './HelpModal/HelpModal'
import { MobileNavigation } from './MobileNavigation/MobileNavigation'
import { Navigation } from './Navigation/Navigation'

import { TCurrentUser, TProfileStatus, TSex } from 'app/types/user'
import { RootState } from 'app/store/configureStore'

interface MainLayoutProps {
    classes?: {
        container?: any
    }
    title?: string
}

const MainLayout: FC<MainLayoutProps> = ({
    children,
    classes,
    title,
}) => {
    const dispatch = useDispatch()

    const {
        addCoinsModalOpen,
        helpModal,
        user,
    } = useSelector((state: RootState): {
        addCoinsModalOpen: boolean
        user: TCurrentUser | null
        helpModal: {
            open: boolean
            messagePreset: string | null
        }
    } => {
        return {
            addCoinsModalOpen: state.addCoinsModalOpen,
            helpModal: state.helpModal,
            user: state.user,
        }
    })

    const showNavigation = (
        user !== null &&
        user.completed_registration &&
        ! [
            TProfileStatus.DEACTIVATED,
            TProfileStatus.PENDING_DELETION,
            TProfileStatus.BANNED,
        ].includes(user.status)
    )

    const handleCloseHelpModal = useCallback(() => dispatch(closeHelpModal()), [dispatch])
    const handleCloseAddCoinsModal = useCallback(() => dispatch(closeAddCoinsModal()), [dispatch])

    return (
        <Page title={ title }>
            <Header />

            {
                showNavigation &&
                <>
                    <Navigation />
                    <MobileNavigation/>
                </>
            }

            <main className="main">
                <InnerWrapper>
                    <Container className={ classes?.container ?? '' }>
                        { children }
                    </Container>
                </InnerWrapper>
            </main>

            <Footer/>

            <HelpModal
                messagePreset={ helpModal.messagePreset }
                open={ helpModal.open }
                onClose={ handleCloseHelpModal }
            />

            {
                user !== null &&
                <AddCoinsModal
                    open={ addCoinsModalOpen }
                    onClose={ handleCloseAddCoinsModal }
                />
            }
        </Page>
    )
}

export { MainLayout }
