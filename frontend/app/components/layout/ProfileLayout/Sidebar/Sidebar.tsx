import React, { FC } from 'react'
import classNames from 'classnames'

import { useCurrentUser, useMediaQuery, useProfileUpdates } from 'app/hooks'

import { Alerts, Tips } from 'app/components/profile'
import { EditProfileMobile } from './EditProfileMobile/EditProfileMobile'
import { Menu } from './Menu/Menu'
import { UserInfo } from './UserInfo/UserInfo'
import { UserInfoMobile } from './UserInfoMobile/UserInfoMobile'

import { TCurrentUser, ProfileUpdates } from 'app/types/user'

import styles from './Sidebar.module.scss'

interface SidebarProps {
    hideOnMobile: boolean
}

const Sidebar: FC<SidebarProps> = ({
    hideOnMobile,
}) => {
    const isPhone = useMediaQuery('phoneLarge')

    const user = useCurrentUser() as TCurrentUser
    const profileUpdates = useProfileUpdates() as ProfileUpdates

    return (
        <aside
            className={ classNames({
                [styles.hideOnMobile]: hideOnMobile,
            }) }
        >
            {
                ! isPhone &&
                <UserInfo user={ user } />
            }

            {
                isPhone &&
                <>
                    <UserInfoMobile user={ user } />
                    <EditProfileMobile
                        profileCompletionPercent={ user.profile_completion_percent }
                    />
                    <Tips />
                    <Alerts user={ user } />
                </>
            }

            <Menu profileUpdates={ profileUpdates } user={ user } />
        </aside>
    )
}

export { Sidebar }
