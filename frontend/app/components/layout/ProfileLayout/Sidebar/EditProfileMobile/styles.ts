import { Styles } from 'jss'

import { jss }  from 'app/core/jss'
import { colors, marginStart } from 'app/styles'
import theme from 'app/theme'

const styles: Styles = {
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: theme.spacing(2),
        marginTop: theme.spacing(2),
    },

    text: {
        color: colors.primary,
        fontSize: 16,
    },

    icon: {
        [marginStart]: 12,
        fontSize: 12,
        fill: colors.primary,
        transform: 'rotate(-90deg)',
    },

    completion_percent: {
        color: colors.primary,
        fontSize: 18,
        fontWeight: 'bold',
    },
}

export default jss.createStyleSheet(styles).attach().classes
