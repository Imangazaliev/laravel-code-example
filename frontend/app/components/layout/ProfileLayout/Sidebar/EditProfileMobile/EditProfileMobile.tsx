import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import classNames from 'classnames'

import { Icon, Paper } from 'app/components'

import classes from './styles'

interface EditProfileMobileProps {
    profileCompletionPercent: number
}

const EditProfileMobile: FC<EditProfileMobileProps> = ({
    profileCompletionPercent,
}) => {
    const { t } = useTranslation()

    return <Link to="/profile/edit">
        <Paper className={ classNames('profile__paper', classes.root) }>
            <div className={ classes.text }>
                { t('profile:edit-profile.action') }

                <Icon className={ classes.icon } name="chevron-down" />
            </div>

            <div className={ classes.completion_percent }>
                { `${  profileCompletionPercent}%` }
            </div>
        </Paper>
    </Link>
}

export { EditProfileMobile }
