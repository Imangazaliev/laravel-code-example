import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import classNames from 'classnames'

import { Icon } from 'app/components'

import styles from './Coins.module.scss'

interface CoinsProps {
    coins: number
}

export const Coins: FC<CoinsProps> = ({
    coins,
}) => {
    const { t } = useTranslation()

    return (
        <Link className={ styles.coins } to={ Routes.ADD_COINS }>
            <div className={ styles.icon }>
                <Icon name="coins"/>
            </div>
            <div  className="d-flex flex-column">
                <div className={ styles.amount }>
                    <span>
                        { t('coins-x', {
                            amount: coins,
                        }) }
                    </span>
                </div>
                <div className={ classNames('link', styles.topUpLink) }>{ t('coins:top-up') }</div>
            </div>
            <div className={ styles.chevronIcon }>
                <Icon name="chevron-down"/>
            </div>
        </Link>
    )
}
