import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { Trans, useTranslation } from 'react-i18next'

import { Text } from 'app/components'

import styles from './Reputation.module.scss'

interface ReputationProps {
    reputation: number
}

const Reputation: FC<ReputationProps> = ({
    reputation,
}) => {
    const { t } = useTranslation()

    return (
        <div>
            <div className={ styles.text }>
                <Text bold size="small" sizeMd="large" sizeSm="small">
                    <Trans
                        components={ [
                            <span className="text--primary" />,
                        ] }
                        i18nKey="profile:reputation-x"
                        values={{
                            reputation: reputation,
                        }}
                    />
                </Text>
            </div>
            <div className={ styles.line }>
                <div className={ styles.inner }>
                    <div className={ styles.bg } style={{ 'width': `${ reputation }%` }}/>
                </div>
            </div>
            <Link className="profile-user-info__link link" to={ Routes.FAQ }>
                { t('profile:what-is-reputation') }
            </Link>
        </div>
    )
}

export { Reputation }
