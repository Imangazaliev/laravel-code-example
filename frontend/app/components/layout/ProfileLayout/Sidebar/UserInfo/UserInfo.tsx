import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

import { Avatar, Paper, Text } from 'app/components'
import { Coins } from './Coins'
import { Reputation } from './Reputation'

import { TCurrentUser } from 'app/types/user'

import styles from './UserInfo.module.scss'

interface UserInfoMobileProps {
    user: TCurrentUser
}

const UserInfo: FC<UserInfoMobileProps> = ({
    user,
}) => {
    const { t } = useTranslation()

    return (
        <Paper className="profile__paper" overflow="hidden">
            <div className="p-16">
                <Link className={ styles.avatar } to={ Routes.PHOTO }>
                    <Avatar gender={ user.sex } src={ user.photo_url } />
                    <div className={ styles.name }>
                        <Text bold className="d-flex align-items-center" size="large">
                            { user.name }
                        </Text>
                    </div>
                </Link>
            </div>
            <div className={ styles.reputationAndCoins }>
                <div className="profile-user-info__reputation">
                    <Reputation reputation={ user.reputation } />
                </div>
                <div className="profile-user-info__user-id">
                    <div className="d-flex d-flex align-items-center">
                        <Text bold>
                            { t('profile:your-id', {
                                id: user.id,
                            }) }
                        </Text>
                    </div>
                </div>
                <Coins coins={ user.coins } />
            </div>
        </Paper>
    )
}

export { UserInfo }
