import React, { FC } from 'react'
import { useTranslation } from 'react-i18next'

import { Paper } from 'app/components'

import { TCurrentUser, ProfileUpdates } from 'app/types/user'
import { MenuItem } from './MenuItem'

interface MenuProps {
    profileUpdates: ProfileUpdates
    user: TCurrentUser
}

const Menu: FC<MenuProps> = ({
    profileUpdates,
    user,
}) => {
    const { t } = useTranslation()

    return <Paper className="profile__paper profile-menu">
        <MenuItem
            badgeCount={ profileUpdates.new_profile_view_count }
            icon="eye"
            label={ t('profile:profile-views.guests') }
            url={ Routes.GUESTS }
        />
        <MenuItem
            icon="eye"
            label={ t('profile:profile-views.viewed-profiles') }
            url={ Routes.VIEWED_PROFILES }
        />
        <MenuItem
            icon="star"
            label={ t('profile:favorites') }
            url={ Routes.FAVORITE_PROFILES }
        />
        <MenuItem
            badgeCount={ profileUpdates.new_like_count }
            icon="heart"
            label={ t('profile:likes') }
            url={ Routes.LIKES }
        />
        <MenuItem
            badgeCount={ profileUpdates.new_chat_request_count }
            icon="user-plus"
            label={ t('profile:chat-requests.incoming-requests') }
            url={ Routes.INCOMING_REQUESTS }
        />
        <MenuItem
            icon="chat-upload"
            label={ t('profile:chat-requests.outgoing-requests') }
            url={ Routes.OUTGOING_REQUESTS }
        />
        <MenuItem
            icon="user"
            label={ t('profile:show-my-profile') }
            url={ `/user/${user.id}` }
        />
    </Paper>
}

export { Menu }
