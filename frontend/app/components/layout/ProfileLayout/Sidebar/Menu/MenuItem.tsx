import { Icon, TIconName } from 'app/components/common/Icon'
import React, { FC } from 'react'
import { Badge } from 'app/components/common/Badge'
import { Text } from 'app/components'
import { NavLink } from 'react-router-dom'

interface MenuItemProps {
    badgeCount?: number
    icon: TIconName
    onClick?: () => void
    label: string
    url?: string
}

const MenuItem: FC<MenuItemProps> = ({
    badgeCount,
    icon,
    onClick,
    label,
    url,
}) => {
    const className = 'profile-menu__item d-flex align-items-center'

    const children = <>
        <div className="profile-menu__icon">
            <Icon name={ icon } />
        </div>
        <Badge className="profile-menu__badge" count={ badgeCount } >
            <Text>{ label }</Text>
        </Badge>
    </>

    if (url !== undefined) {
        return <NavLink activeClassName="active" to={ url } className={ className }>{ children }</NavLink>
    }

    return <div className={ className } onClick={ onClick }>{ children }</div>
}

export { MenuItem }
