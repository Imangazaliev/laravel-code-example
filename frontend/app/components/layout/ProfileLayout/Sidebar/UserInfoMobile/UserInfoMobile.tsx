import React, { FC } from 'react'
import { Trans, useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'

import { Avatar, Icon, Paper, Text } from 'app/components'

import { CircleProgressBar } from '../CircleProgressBar'

import { TCurrentUser } from 'app/types/user'

import styles from './UserInfoMobile.module.scss'

interface UserInfoMobileProps {
    user: TCurrentUser
}

const UserInfoMobile: FC<UserInfoMobileProps> = ({
    user,
}) => {
    const { t } = useTranslation()

    return (
        <>
            <Paper className="d-flex align-items-center" padding="sm">
                <Link to={ Routes.PHOTO }>
                    <Avatar
                        className={ styles.avatar }
                        gender={ user.sex }
                        shape="rounded"
                        src={ user.photo_url }
                    />
                </Link>
                <div className="ms-16">
                    <Text bold size="large">{ user.name }</Text>
                    <Text bold className="mt-8" color="gray" size="small">
                        { `ID: ${ user.id }` }
                    </Text>
                </div>
            </Paper>
            <Paper className={ styles.reputationAndCoins } padding="sm">
                <div className={ styles.column }>
                    <CircleProgressBar barWidth={ 2 } progress={ user.reputation } radius={ 20 } />
                    <div className={ styles.title }>
                        <Trans
                            components={ [
                                <span className="text--primary" />,
                            ] }
                            i18nKey="profile:reputation-x"
                            values={{
                                reputation: user.reputation,
                            }}
                        />
                    </div>
                    <Link className={ styles.hint } to={ Routes.FAQ }>{ t('what-is-it') }</Link>
                </div>
                <Link to={ Routes.ADD_COINS } className={ styles.column }>
                    <div className={ styles.coinsIcon }>
                        <Icon name="coins" size={ 22 }/>
                    </div>
                    <div className={ styles.title }>
                        <span>
                            { t('coins-x', {
                                amount: user.coins,
                            }) }
                        </span>
                    </div>
                    <div className={ styles.hint }>{ t('coins:top-up') }</div>
                </Link>
            </Paper>
        </>
    )
}

export { UserInfoMobile }
