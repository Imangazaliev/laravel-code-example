import React, { FC } from 'react'

interface CircleProgressBarProps {
    barWidth: number
    progress: number
    // the radius including bar width
    radius: number
}

const CircleProgressBar: FC<CircleProgressBarProps> = ({
    barWidth,
    progress,
    radius,
}) => {
    const circumference = 2 * Math.PI * (radius - barWidth / 2)
    const dashoffset = circumference * (1 - progress / 100)

    return <div className="circle-progress">
        <svg
            className="circle-progress__bar"
            width={ radius * 2 } height={ radius * 2 } viewBox={ `0 0 ${ radius * 2 } ${ radius * 2 }` }
        >
            <circle
                className="circle-progress__meter"
                cx={ radius } cy={ radius } r={ radius - barWidth / 2 } fill="none" strokeWidth={ barWidth }
            />
            <circle
                className="circle-progress__fill"
                cx={ radius } cy={ radius } r={ radius - barWidth / 2 } fill="none"
                strokeWidth={ barWidth } strokeDasharray={ circumference } strokeDashoffset={ dashoffset }
            />
        </svg>
    </div>
}

export { CircleProgressBar }
