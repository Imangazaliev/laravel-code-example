import React, { FC } from 'react'

import { MainLayout } from 'app/components/layout'
import { Sidebar } from './Sidebar/Sidebar'

interface ProfileLayoutProps {
    hideSidebarOnMobile?: boolean
    title?: string
}

const ProfileLayout: FC<ProfileLayoutProps> = ({
    children,
    hideSidebarOnMobile = true,
    title,
}) => {
    return (
        <MainLayout classes={{ container: 'profile' }} title={ title }>
            <Sidebar hideOnMobile={ hideSidebarOnMobile }/>

            { children }
        </MainLayout>
    )
}

export { ProfileLayout }
