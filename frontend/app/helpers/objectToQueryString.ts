type ParamsType = {
    [key: string]: any
}

export function objectToQueryString(params: ParamsType): string {
    return Object.keys(params)
        .filter(key => Boolean(params[key]))
        .map((key) => encodeURIComponent(key) + '=' + encodeURIComponent(params[key]))
        .join('&')
}
