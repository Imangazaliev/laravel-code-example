import React, { Suspense } from 'react'
import { Route } from 'react-router'

import { Spinner } from 'app/components'

const renderRoutes = ({
    routes,
    extraProps,
}: {
    routes: {
        component: React.ElementType
        path: string
        exact?: boolean
    }[]
    extraProps?: Record<string, any>
}) => {
    if (routes.length < 0) {
        return null
    }

    const fallback = (
        <div className="d-flex justify-content-center pt-16">
            <Spinner />
        </div>
    )

    return routes.map((route) => (
        <Route
            key={ route.path }
            path={ route.path }
            exact={ route.exact ?? true }
            render={ props => (
                <Suspense fallback={ fallback }>
                    <route.component { ...props } { ...extraProps } />
                </Suspense>
            ) }
        />
    ))
}

export { renderRoutes }
