import { differenceInYears } from 'date-fns'

export const getAge = (birthday: string): number => {
    return differenceInYears(new Date(), new Date(birthday))
}
