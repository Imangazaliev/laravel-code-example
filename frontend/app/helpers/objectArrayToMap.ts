import { mapValues, keyBy } from 'lodash'

export const objectArrayToMap = (array: any[], key: string, value: string) => mapValues(keyBy(array, key), value)
