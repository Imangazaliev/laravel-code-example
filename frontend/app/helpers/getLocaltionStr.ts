import i18next from 'i18next'

export const getLocationStr = (countryName: string | null, localityName: string | null): string => {
    if (countryName === null && localityName === null) {
        return i18next.t('not-specified')
    }

    if (localityName === null) {
        return countryName as string
    }

    return `${countryName}, ${localityName}`
}
