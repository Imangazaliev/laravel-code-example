import storage from 'local-storage-fallback'

import { makeStorageCache } from './makeStorageCache'

export const localStorageCache = makeStorageCache(storage)
