import { parsePhoneNumberFromString } from 'libphonenumber-js'

export const getPhoneNumberCountryCode = (phoneNumber: string): string | null => {
    const phoneNumberObj = parsePhoneNumberFromString(phoneNumber)

    if (phoneNumberObj === undefined) {
        return null
    }

    return phoneNumberObj.country as string
}
