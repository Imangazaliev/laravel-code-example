import { addSeconds, format, isAfter, parseISO } from 'date-fns'

import { DATE_TIME_FORMAT } from 'app/constants/dates'

type TStorage = {
    getItem: (key: string) => string | null
    setItem: (key: string, value: string) => void
    removeItem: (key: string) => void
}

type TGetFunction<T> = () => T | null
type TPutFunction<T> = (data: T) => void
type TRememberFunction<T> = (callback: () => T | Promise<T>, async?: boolean) => T | Promise<T>
type TRemoveFunction = () => void

type TCacheEntry<T> = {
    get: TGetFunction<T>
    put: TPutFunction<T>
    remember: TRememberFunction<T>
    remove: TRemoveFunction
}

type TCacheEntryData = {
    version: string
    data: any
    createdAt: string
    expiredAt: string
}

export const makeStorageCache = (storage: TStorage) => {
    return function<T = any>(key: string, ttl: number | Date): TCacheEntry<T> {
        key = `cache.${key}`

        const get: TGetFunction<T> = () => {
            const entrySerialized = storage.getItem(key)

            if (entrySerialized === null) {
                return null
            }

            const entry = JSON.parse(entrySerialized) as TCacheEntryData

            const expiredAt = parseISO(entry.expiredAt)

            if (entry.version !== ENV.version || isAfter(new Date(), expiredAt)) {
                storage.removeItem(key)

                return null
            }

            return entry.data
        }

        const put: TPutFunction<T> = (data) => {
            const expireAt = typeof ttl === 'number' ? addSeconds(new Date(), ttl) : ttl

            const languagesJson = JSON.stringify({
                version: ENV.version,
                data: data,
                createdAt: format(new Date(), DATE_TIME_FORMAT),
                expireAt: format(expireAt, DATE_TIME_FORMAT),
            })

            storage.setItem(key, languagesJson)
        }

        const remember: TRememberFunction<T> = (callback, async= false) => {
            let data = get()

            if (data !== null) {
                return async ? new Promise((resolve) => {
                    resolve(data as T)
                }) : data
            }

            if (async) {
                return new Promise((resolve) => {
                    const result = callback()

                    if ( ! (result instanceof Promise)) {
                        throw new Error('The callback must return a Promise in the asynchronous mode')
                    }

                    result
                        .then((data: any) => {
                            put(data)

                            resolve(data)
                        })
                })
            }

            const result = callback()

            if (result instanceof Promise) {
                throw new Error('The callback returned a Promise, should you need enable the asynchronous mode?')
            }

            put(result)

            return result
        }

        const remove = () => {
            storage.removeItem(key)
        }

        return {
            get,
            put,
            remember,
            remove,
        }
    }
}
