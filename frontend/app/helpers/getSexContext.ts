import { TSex } from 'app/types/user'

export const getSexContext = (sex: TSex) => sex === TSex.MALE ? 'male' : 'female'
