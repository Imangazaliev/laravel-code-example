import * as Fingerprint2 from 'fingerprintjs2'

const options = {
    excludes: {
        plugins: true,
        localStorage: true,
        adBlock: true,
        screenResolution: true,
        availableScreenResolution: true,
        enumerateDevices: true,
        pixelRatio: true,
        doNotTrack: true,
    },
}

export const getBrowserFingerprint = () => {
    return new Promise<string>((resolve, reject) => {
        async function getHash() {
            await Fingerprint2.getPromise(options)
                .then(components => {
                    const values = components.map((component: any) => component.value)

                    resolve(String(Fingerprint2.x64hash128(values.join(''), 31)))
                })
                .catch(reject)
        }

        if (window.requestIdleCallback !== undefined) {
            requestIdleCallback(getHash)
        } else {
            setTimeout(getHash, 500)
        }
    })
}
