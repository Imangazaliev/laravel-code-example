export const moveCursorToEnd = (el: HTMLInputElement | HTMLTextAreaElement) => {
    if (typeof el.selectionStart == 'number') {
        el.selectionStart = el.selectionEnd = el.value.length

        return
    }

    if (typeof el.setSelectionRange != 'undefined') {
        el.setSelectionRange(el.value.length, el.value.length)
    }
}
