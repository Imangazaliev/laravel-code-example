import { CookieStorage, MemoryStorage, isSupported } from 'local-storage-fallback'

import { makeStorageCache } from './makeStorageCache'

type StorageFallback = Pick<Storage, 'clear' | 'getItem' | 'setItem' | 'removeItem'>;

let storage: Storage | StorageFallback

if (isSupported('sessionStorage')) {
    storage = window.sessionStorage
} else if (isSupported('cookieStorage')) {
    storage = new CookieStorage()
} else {
    storage = new MemoryStorage()
}

export const sessionStorageCache = makeStorageCache(storage)
