import { range as lodashRange } from 'lodash'

/**
 * A wrapper around Lodash range() helper to make it inclusive.
 */
export function range(start: number, end: number, step: number = 1): number[] {
    return lodashRange(start, end + 1, step)
}
