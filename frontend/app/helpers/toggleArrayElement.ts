export const toggleArrayElement = <T>(array: T[], element: T): T[] => {
    if (array.includes(element)) {
        const optionIndex = array.indexOf(element)
        const valuesBefore = array.slice(0, optionIndex)
        const valuesAfter = array.slice(optionIndex + 1)

        return [...valuesBefore, ...valuesAfter]
    }

    return [...array, element]
}
