import { range } from 'app/helpers/range'
import { getLastDayOfMonth } from './getLastDayOfMonth'

export const getDaysOfMonth = (year: number, month: number) => {
    return range(1, getLastDayOfMonth(year, month))
}
