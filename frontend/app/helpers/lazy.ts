import { lazy as ReactLazy} from 'react'

export const lazy = (resolver: any, name: string = 'default') => {
    return ReactLazy(async () => {
        const resolved = await resolver()

        return {default: resolved[name]}
    })
}
