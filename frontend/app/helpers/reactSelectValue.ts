import { TOptionValue } from 'app/components/common/Select/types'

export const reactSelectValue = (options: any[], getOptionValue: (option: any) => TOptionValue, value: TOptionValue | null) => {
    const valueOption = options.find(option => getOptionValue(option) === value)

    if (valueOption === undefined) {
        return ''
    }

    return value === null || value === undefined ? '' : valueOption
}
