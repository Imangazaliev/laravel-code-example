export const redirectViaPost = (redirectUrl: string, redirectData: Record<string, any>) => {
    const form = document.createElement('form')

    form.method = 'POST'
    form.action = redirectUrl

    for (let name in redirectData) {
        if ( ! redirectData.hasOwnProperty(name)) {
            continue
        }

        const input = document.createElement('input')

        input.type = 'hidden'
        input.name = name
        input.value = redirectData[name]

        form.appendChild(input)
    }

    document.body.appendChild(form)

    form.submit()
}
