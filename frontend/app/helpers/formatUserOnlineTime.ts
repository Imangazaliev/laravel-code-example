import i18next from 'i18next'
import {
    differenceInMinutes,
    format,
    formatDistanceStrict,
    formatRelative,
    isToday,
    isYesterday,
    parseISO,
} from 'date-fns'
import ruLocale from 'date-fns/locale/ru'

import { getSexContext } from 'app/helpers'

import { TSex } from 'app/types/user'

const TWENTY_HOURS_IN_MINUTES = 60 * 12

export const formatUserOnlineTime = (dateStr: string, sex: TSex) => {
    const date = parseISO(dateStr)
    const currentTime = new Date()
    const context = getSexContext(sex)

    if (differenceInMinutes(currentTime, date) < TWENTY_HOURS_IN_MINUTES) {
        return i18next.t(`profile:was-online-relative`, {
            context,
            time: formatDistanceStrict(date, new Date(), {
                locale: ruLocale,
            }),
        })
    }

    // TODO: use dynamic locale
    if (isToday(date) || isYesterday(date)) {
        return i18next.t(`profile:was-online-at`, {
            context,
            time: formatRelative(date, currentTime, {
                locale: ruLocale,
            }),
        })
    }

    return i18next.t(`profile:was-online-at`, {
        context,
        time: format(date, 'dd.MM.yy'),
    })
}
