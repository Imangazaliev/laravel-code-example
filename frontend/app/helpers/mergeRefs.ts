import { MutableRefObject, Ref } from 'react'

const mergeRefs = (refs: Ref<any>[]) => {
    return (value: any) => {
        refs.forEach(ref => {
            if (typeof ref === 'function') {
                ref(value)

                return
            }

            if (ref != null) {
                (ref as MutableRefObject<any>).current = value
            }
        })
    }
}

export { mergeRefs }
