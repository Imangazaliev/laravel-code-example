import React from 'react'
import ReactDOM from 'react-dom'

import './core/polyfills'
import './core/i18next'
import './core/yup'

import { initSentry } from './core/initSentry'

import App from './App'

import '../styles/app.scss'

initSentry()

ReactDOM.render(React.createElement(App), document.getElementById('root') )
