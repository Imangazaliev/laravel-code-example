import { combineEpics } from 'redux-observable'

import {
    countriesEpic,
    ethnicitiesEpic,
    subdivisionsEpic,
    localitiesEpic,
} from 'app/store/epics/misc'

//TODO write an interface for epic
export const epics: any = combineEpics(
    countriesEpic,
    ethnicitiesEpic,
    subdivisionsEpic,
    localitiesEpic,
)
