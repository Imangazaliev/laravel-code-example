import { History } from 'history'

// reloads a page on switch between the public side and the admin panel
// it's necessary to disable services analytics (from Google and Yandex) and the support widget (Tawk)
// when a user goes to admin panel and vice versa
export const reloadPageOnSwitchBetweenPublicSideAndAdminPanel = (history: History) => {
    const getQueryStr = (query: string) => query === '' ? '' : `?${ query }`

    let prevLocation = `${ location.pathname }${ getQueryStr(location.search) }${ location.hash }`

    history.block(location => {
        const locationStr = `${ location.pathname }${ getQueryStr(location.search) }${ location.hash }`

        // ignore identical link clicks
        if (prevLocation === null || prevLocation === locationStr) {
            return
        }

        // if user navigation from from the public side to the admin panel or vice versa
        // then navigate to it using a normal browser navigation
        if (
            (locationStr.startsWith('/admin') && ! prevLocation.startsWith('/admin'))
            || (prevLocation.startsWith('/admin') && ! locationStr.startsWith('/admin'))
        ) {
            window.location.href = locationStr
        }

        prevLocation = locationStr
    })
}
