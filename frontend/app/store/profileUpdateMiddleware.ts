import { Middleware, AnyAction, Dispatch, MiddlewareAPI } from 'redux'
import { isEqual } from 'lodash'

import http from 'app/core/axios'
import { setProfileUpdates } from 'app/store/modules/profileUpdates'
import { setUser } from 'app/store/modules/user'

import { TCurrentUser } from 'app/types/user'

import { FETCH_USER, SET_USER } from 'app/store/modules/user'
import { CLEAR_USER } from 'app/store/modules/user/user'

const UPDATES_UPDATE_INTERVAL = 5000
const PROFILE_UPDATE_INTERVAL = 10000

const MAX_REQUEST_ERROR_COUNT = 3

export const profileUpdateMiddleware: Middleware = (store: MiddlewareAPI) => {
    let updatesTimer: any = null
    let profileTimer: any = null

    let updatesRequestErrorCount = 0
    let profileRequestErrorCount = 0

    const loadUpdates = async () => {
        if (updatesTimer !== null) {
            clearTimeout(updatesTimer)
        }

        try {
            const response = await http.get(ApiEndpoints.PROFILE_UPDATES)

            if ( ! isEqual(response.data, store.getState().profileUpdates)) {
                store.dispatch(setProfileUpdates(response.data))
            }

            updatesTimer = setTimeout(loadUpdates, UPDATES_UPDATE_INTERVAL)

            updatesRequestErrorCount = 0
        } catch (error: any) {
            updatesRequestErrorCount++

            if (updatesRequestErrorCount < MAX_REQUEST_ERROR_COUNT) {
                updatesTimer = setTimeout(loadUpdates, UPDATES_UPDATE_INTERVAL)

                return
            }

            console.error('Profile update error')
            console.error(error)
        }
    }

    const loadProfile = async () => {
        if (updatesTimer !== null) {
            clearTimeout(profileTimer)
        }

        try {
            const response = await http.get(ApiEndpoints.PROFILE)

            if ( ! isEqual(response.data, store.getState().user)) {
                store.dispatch(setUser(response.data))
            }

            profileRequestErrorCount = 0

            // if a user has not completed registration don't update profile in real-time
            if ( ! (response.data as TCurrentUser).completed_registration) {
                return
            }

            profileTimer = setTimeout(loadProfile, PROFILE_UPDATE_INTERVAL)
        } catch (error: any) {
            profileRequestErrorCount++

            if (profileRequestErrorCount < MAX_REQUEST_ERROR_COUNT) {
                profileTimer = setTimeout(loadProfile, PROFILE_UPDATE_INTERVAL)

                return
            }

            console.error('Profile update error')
            console.error(error)
        }
    }

    const currentUser = store.getState().user as TCurrentUser | null

    if (currentUser !== null && currentUser.completed_registration) {
        loadUpdates()

        profileTimer = setTimeout(loadProfile, PROFILE_UPDATE_INTERVAL)
    }

    return (next: Dispatch) => {
        return (action: AnyAction) => {
            if (action.type === SET_USER) {
                // if a user is not completed registration then do nothing
                if ( ! (action.payload as TCurrentUser).completed_registration) {
                    return next(action)
                }

                // elsewhere start timers
                if (store.getState().user === null) {
                    loadUpdates()

                    profileTimer = setTimeout(loadProfile, PROFILE_UPDATE_INTERVAL)
                } else {
                    // if it's just a profile update
                    clearTimeout(profileTimer)

                    profileTimer = setTimeout(loadProfile, PROFILE_UPDATE_INTERVAL)
                }
            }

            if (action.type === CLEAR_USER) {
                clearTimeout(updatesTimer)
                clearTimeout(profileTimer)
            }

            if (action.type === FETCH_USER) {
                clearTimeout(profileTimer)

                loadProfile()
            }

            return next(action)
        }
    }
}
