import { ProfileUpdates } from 'app/types/user'

export const moduleName = 'profileUpdates'

export const SET_PROFILE_UPDATES = `${moduleName}/SET_PROFILE_UPDATES`
export const RESET_PROFILE_VIEW_COUNT = `${moduleName}/RESET_PROFILE_VIEW_COUNT`
export const RESET_LIKE_COUNT = `${moduleName}/RESET_LIKE_COUNT`
export const UPDATE_PROFILE = `${moduleName}/UPDATE_PROFILE`

type TState = ProfileUpdates

const initialState: TState = {
    new_profile_view_count: 0,
    new_like_count: 0,
    new_chat_request_count: 0,
    new_message_count: 0,
    new_notification_count: 0,
}

export const profileUpdatesReducer = (state: TState = initialState, action: any): TState => {
    const { type, payload } = action

    switch (type) {
        case SET_PROFILE_UPDATES:
            return payload
        case RESET_PROFILE_VIEW_COUNT:
            return {
                ...state,
                new_profile_view_count: 0,
            }
        case RESET_LIKE_COUNT:
            return {
                ...state,
                new_like_count: 0,
            }
        default:
            return state
    }
}

export function setProfileUpdates(payload: ProfileUpdates) {
    return {
        type: SET_PROFILE_UPDATES,
        payload,
    }
}

export function resetProfileViewCount() {
    return {
        type: RESET_PROFILE_VIEW_COUNT,
    }
}

export function resetLikeCount() {
    return {
        type: RESET_LIKE_COUNT,
    }
}
