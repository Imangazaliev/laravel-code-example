import { TCountry } from 'app/types/common'

export const moduleName = 'countries'

export const GET_COUNTRIES = `${moduleName}/GET_COUNTRIES`
export const SET_COUNTRIES = `${moduleName}/SET_COUNTRIES`
export const SET_COUNTRIES_ERROR = `${moduleName}/SET_COUNTRIES_ERROR`

type TState = TCountry[] | null

const initialState = null

export const countriesReducer = (state = initialState, action: any): TState => {
    const { type, payload } = action

    switch (type) {
        case SET_COUNTRIES:
            return payload
        default:
            return state
    }
}

export function getCountries() {
    return {
        type: GET_COUNTRIES,
    }
}

export function setCountries(payload: TCountry[]) {
    return {
        type: SET_COUNTRIES,
        payload,
    }
}

export function setCountriesError() {
    return {
        type: SET_COUNTRIES_ERROR,
    }
}
