export {
    ethnicitiesReducer,
    moduleName,
    GET_ETHNICITIES,
    getEthnicities,
    setEthnicities,
} from './ethnicities'
