import { TEthnicity } from 'app/types/common'

export const moduleName = 'ethnicities'

export const GET_ETHNICITIES = `${moduleName}/GET_ETHNICITIES`
export const SET_ETHNICITIES = `${moduleName}/SET_ETHNICITIES`

type TState = TEthnicity[] | null

const initialState = null

export const ethnicitiesReducer = (state = initialState, action: any): TState => {
    const { type, payload } = action

    switch (type) {
        case SET_ETHNICITIES:
            return payload
        default:
            return state
    }
}

export function getEthnicities(sex: string) {
    return {
        type: GET_ETHNICITIES,
        payload: {
            sex,
        },
    }
}

export function setEthnicities(payload: any) {
    return {
        type: SET_ETHNICITIES,
        payload,
    }
}
