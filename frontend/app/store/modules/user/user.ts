import { TCurrentUser } from 'app/types/user'

export const moduleName = 'user'

export const SET_USER = `${moduleName}/SET_USER`
export const CLEAR_USER = `${moduleName}/CLEAR_USER`
export const FETCH_USER = `${moduleName}/FETCH_USER`

type TState = typeof ENV.user

const initialState = ENV.user

export const userReducer = (state = initialState, action: any): TState => {
    const { type, payload } = action

    switch (type) {
        case SET_USER:
            return payload
        case CLEAR_USER:
            return null
        default:
            return state
    }
}

export function setUser(payload: TCurrentUser) {
    return {
        type: SET_USER,
        payload,
    }
}

export function clearUser() {
    return {
        type: CLEAR_USER,
    }
}

export function fetchUser() {
    return {
        type: FETCH_USER,
    }
}
