export {
    moduleName,
    userReducer,
    SET_USER,
    FETCH_USER,
    setUser,
    clearUser,
    fetchUser,
} from './user'
