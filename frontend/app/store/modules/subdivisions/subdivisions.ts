import { TSubdivision } from 'app/types/common'

import { SET_LOCALITIES } from 'app/store/modules/localities'

export const moduleName = 'subdivisions'

export const GET_SUBDIVISIONS = `${moduleName}/GET_SUBDIVISIONS`
export const SET_SUBDIVISIONS = `${moduleName}/SET_SUBDIVISIONS`

type TState = TSubdivision[] | null

const initialState = null

export const subdivisionsReducer = (state = initialState, action: any): TState => {
    const { type, payload } = action

    switch (type) {
        case SET_SUBDIVISIONS:
            return payload
        default:
            return state
    }
}

export function getSubdivisions(payload: number | null) {
    if (payload === null) {
        return {
            type: SET_LOCALITIES,
            payload: null,
        }
    }

    return {
        type: GET_SUBDIVISIONS,
        payload,
    }
}

export function setSubdivisions(payload: any) {

    return {
        type: SET_SUBDIVISIONS,
        payload,
    }
}
