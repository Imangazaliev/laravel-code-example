export {
    subdivisionsReducer,
    moduleName,
    GET_SUBDIVISIONS,
    getSubdivisions,
    setSubdivisions,
} from './subdivisions'
