export {
    moduleName,
    localitiesReducer,
    SET_LOCALITIES,
    GET_LOCALITIES,
    setLocalities,
    getLocalities,
} from './localities'