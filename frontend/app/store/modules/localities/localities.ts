import { TLocality } from 'app/types/common'

export const moduleName = 'localities'

export const GET_LOCALITIES = `${moduleName}/GET_LOCALITIES`
export const SET_LOCALITIES = `${moduleName}/SET_LOCALITIES`

type TState = TLocality[] | null

const initialState = null

export const localitiesReducer = (state = initialState, action: any): TState => {
    const { type, payload } = action

    switch (type) {
        case SET_LOCALITIES:
            return payload
        default:
            return state
    }
}

export function getLocalities(payload: number | null) {
    if (payload === null) {
        return {
            type: SET_LOCALITIES,
            payload: null,
        }
    }

    return {
        type: GET_LOCALITIES,
        payload,
    }
}

export function setLocalities(payload: any) {

    return {
        type: SET_LOCALITIES,
        payload,
    }
}
