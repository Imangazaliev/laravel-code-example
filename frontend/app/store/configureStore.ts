import { createBrowserHistory } from 'history'
import { createStore, applyMiddleware } from 'redux'
import { routerMiddleware } from 'connected-react-router'

import { epicMiddleware } from './createEpicMiddleware'
import { composeWithDevTools } from './composeWithDevTools'
import { createRootReducer } from './reducer'
import { epics } from './epics'
import { reloadPageOnSwitchBetweenPublicSideAndAdminPanel } from './reloadPageOnSwitchBetweenPublicSideAndAdminPanel'
import { profileUpdateMiddleware } from './profileUpdateMiddleware'

export const history = createBrowserHistory()

history.listen(() => {
    window.scrollTo(0, 0)
})

reloadPageOnSwitchBetweenPublicSideAndAdminPanel(history)

const enhancers = composeWithDevTools(
    applyMiddleware(epicMiddleware, routerMiddleware(history), profileUpdateMiddleware),
)
const reducer = createRootReducer(history)

export type RootState = ReturnType<typeof reducer>

export const store = createStore(reducer, enhancers)

epicMiddleware.run(epics)
