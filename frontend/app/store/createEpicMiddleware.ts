import { createEpicMiddleware } from 'redux-observable'

import axios from 'app/core/axios'

const makeRequestFunction = (http: any) => {
    return async (url: string, data: object = {}) => {
        return await http(url, data)
    }
}

export const epicMiddleware = createEpicMiddleware({
    dependencies: {
        getRequest: makeRequestFunction((url: string, data: object = {}) => {
            return axios.get(url, {
                params: data,
            })
        }),
        postRequest: makeRequestFunction((url: string, data: object = {}) => {
            return axios.post(url, data)
        }),
    },
})
