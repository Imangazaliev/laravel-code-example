
import { ofType, ActionsObservable, StateObservable } from 'redux-observable'
import { of, from } from 'rxjs'
import { switchMap } from 'rxjs/operators'
import { Action } from 'redux'

import {
    GET_LOCALITIES,
    setLocalities,
} from 'app/store/modules/localities'

export const localitiesEpic = (
    action$: ActionsObservable<Action>,
    state$: StateObservable<any>,
    { getRequest }: any
): any => action$.pipe(
    ofType(GET_LOCALITIES),
    switchMap(
        ({ payload }: any) => from(
            getRequest(ApiEndpoints.LOCALITIES, {
                country_id: payload,
            })
        ).pipe(
            switchMap((response: any) => of(setLocalities(response.data)))
        )
    )
)

