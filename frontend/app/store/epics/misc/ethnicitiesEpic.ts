import { ofType, ActionsObservable, StateObservable } from 'redux-observable'
import { of, from } from 'rxjs'
import { switchMap } from 'rxjs/operators'
import { Action } from 'redux'

import {
    GET_ETHNICITIES,
    setEthnicities,
} from 'app/store/modules/ethnicities'

export const ethnicitiesEpic = (
    action$: ActionsObservable<Action>,
    state$: StateObservable<any>,
    { getRequest }: any
): any => action$.pipe(
    ofType(GET_ETHNICITIES),
    switchMap(
        ({ payload }: any) => from(
            getRequest(ApiEndpoints.ETHNICITIES, {
                sex: payload.sex,
            })
        ).pipe(
            switchMap((response: any) => of(setEthnicities(response.data)))
        )
    )
)

