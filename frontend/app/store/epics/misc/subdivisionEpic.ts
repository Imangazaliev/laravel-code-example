import { ofType, ActionsObservable, StateObservable } from 'redux-observable'
import { of, from } from 'rxjs'
import { switchMap } from 'rxjs/operators'

import { setSubdivisions } from 'app/store/modules/subdivisions'

import { GET_SUBDIVISIONS } from 'app/store/modules/subdivisions'

type SubdivisionsAction = {
    type: string
    payload: number
}

export const subdivisionsEpic = (
    action$: ActionsObservable<SubdivisionsAction>,
    state$: StateObservable<any>,
    { getRequest }: any
): any => action$.pipe(
    ofType(GET_SUBDIVISIONS),
    switchMap(
        ({ payload }) => from(
            getRequest(`${ ApiEndpoints.SUBDIVISIONS }?country_id=${ payload }`)
        ).pipe(
            switchMap((response: any) => of(setSubdivisions(response.data)))
        )
    )
)
