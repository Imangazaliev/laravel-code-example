import { ofType, ActionsObservable, StateObservable } from 'redux-observable'
import { of, from } from 'rxjs'
import { switchMap } from 'rxjs/operators'
import { Action } from 'redux'

import {
    GET_COUNTRIES,
    setCountries,
} from 'app/store/modules/countries'

export const countriesEpic = (
    action$: ActionsObservable<Action>,
    state$: StateObservable<any>,
    { getRequest }: any
): any => action$.pipe(
    ofType(GET_COUNTRIES),
    switchMap(
        () => from(
            getRequest(ApiEndpoints.COUNTRIES)
        ).pipe(
            switchMap((response: any) => of(setCountries(response.data)))
        )
    )
)
