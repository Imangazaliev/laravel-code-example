import { compose } from 'redux'

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

export const composeWithDevTools =  (config: any) => {
    if (process.env.NODE_ENV === 'production' || window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === undefined) {
        return compose(config)
    }

    return window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(config)
}
