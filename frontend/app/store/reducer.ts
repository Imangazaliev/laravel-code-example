import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { History } from 'history'

import { addCoinsModalOpenReducer, moduleName as addCoinsModalOpenModule } from './modules/addCoinsModalOpen'
import { countriesReducer, moduleName as countriesModule } from './modules/countries'
import { ethnicitiesReducer, moduleName as ethnicitiesModule } from './modules/ethnicities'
import { helpModalReducer, moduleName as helpModalModule } from './modules/helpModal'
import { localitiesReducer, moduleName as localitiesModule } from './modules/localities'
import { openMenuItemCodeReducer, moduleName as openAdminMenuItemCodeModule } from './modules/openAdminMenuItemCode'
import { profileUpdatesReducer, moduleName as profileUpdatesModule } from './modules/profileUpdates'
import { subdivisionsReducer, moduleName as subdivisionsModule } from './modules/subdivisions'
import { userReducer, moduleName as userModule } from './modules/user'

export const createRootReducer = (history: History) => {
    return combineReducers({
        router: connectRouter(history),
        [addCoinsModalOpenModule]: addCoinsModalOpenReducer,
        [countriesModule]: countriesReducer,
        [ethnicitiesModule]: ethnicitiesReducer,
        [helpModalModule]: helpModalReducer,
        [localitiesModule]: localitiesReducer,
        [openAdminMenuItemCodeModule]: openMenuItemCodeReducer,
        [profileUpdatesModule]: profileUpdatesReducer,
        [subdivisionsModule]: subdivisionsReducer,
        [userModule]: userReducer,
    })
}
