import { FC } from 'react'
import { ConnectedComponent } from 'react-redux'
import { RouteProps } from 'app/components/common/Route/Route'

export enum TAccess {
    GUEST = 'guest',
    AUTHENTICATED = 'auth',
    ALL = 'all',
}

type TRouteParameters = Omit<RouteProps, 'component' | 'path'>

const defaultRouteParameters: TRouteParameters = {
    exact: true,
    access: TAccess.ALL,
    registrationPage: false,
    permissions: [],
    allowBanned: false,
    allowDeactivated: false,
    allowPendingDeletion: false,
}

const createRouteFunction = (routes: any[]) => {
    return (
        path: string | string[],
        component: FC<any> | ConnectedComponent<FC<any>, any>,
        parameters: Partial<TRouteParameters> = defaultRouteParameters,
    ) => {
        const mergeParameters = { ...defaultRouteParameters, ...parameters }

        const props: RouteProps = {
            path,
            component,
            ...mergeParameters,
        }

        routes.push(props)
    }
}

export { createRouteFunction }
