import 'core-js/features/object/from-entries'
import 'core-js/features/promise/finally'
// see support: https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollTo
import 'element-scroll-polyfill'
