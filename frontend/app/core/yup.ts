import { addMethod, Ref, setLocale, string }  from 'yup'
import { CountryCode, isValidNumberForRegion } from 'libphonenumber-js'
import i18n from 'i18next'

declare module "yup" {
    interface StringSchema {
        phoneNumber(countryCodeFieldRef: Ref, strict?: boolean, message?: string): StringSchema
    }
}

addMethod(string, 'phoneNumber', function (countryCodeFieldRef: Ref) {
    return this.test('phoneNumber', (params) => {
        return i18n.t('validation:phone', { field: params.label ?? params.path })
    }, function (value) {
        if (typeof value !== 'string') {
            return false
        }

        const countryCode = this.resolve(countryCodeFieldRef)

        if (typeof countryCode !== 'string') {
            // @ts-ignore
            console.warn(`Country code at ${ countryCodeFieldRef.path } must be a string, ${ typeof countryCode } given`)

            return false
        }

        return isValidNumberForRegion(value, countryCode as CountryCode)
    })
})

setLocale({
    mixed: {
        required: params => {
            return i18n.t('validation:required', { field: params.label ?? params.path })
        },
    },
    string: {
        min: params => {
            return i18n.t('validation:min.string', { field: params.label ?? params.path, min: params.min })
        },
        max: params => {
            return i18n.t('validation:max.string', { field: params.label ?? params.path, max: params.max })
        },
    },
})
