import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

const currentLanguageCode = ENV.currentLanguage.code

const resources: any = {
    en: Translations.en,
}

if (currentLanguageCode !== 'en') {
    resources[currentLanguageCode] = Translations[currentLanguageCode]
}

i18n
    .use(initReactI18next)
    .init({
        resources: resources,
        lng: currentLanguageCode,
        fallbackLng: 'en',
        defaultNS: 'common',
        interpolation: {
            escapeValue: false,
        },
        keySeparator: false,
    })

i18n.options.react = {
    transKeepBasicHtmlNodesFor: ['b', 'br', 'u'],
}
