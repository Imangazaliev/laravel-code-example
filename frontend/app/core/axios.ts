import axios from 'axios'

const http = axios.create({
	baseURL: `${ ENV.siteUrl }/api/`,
	headers: {
		Accept: 'application/json',
        'Content-Type': 'application/json',
	},
    xsrfCookieName: 'XSRF-TOKEN',
    xsrfHeaderName: 'X-XSRF-TOKEN',
})

// refreshing of expired CSRF token
http.interceptors.response.use(response => response, async error => {
    if (
        error.response !== undefined &&
        error.response.status === 400
        && error.response.data.code === ApiErrors.INVALID_CSRF_TOKEN
    ) {
        await http.get(ApiEndpoints.CSRF_TOKEN)

        return http.request(error.config)
    }

    return Promise.reject(error)
})

export default http
