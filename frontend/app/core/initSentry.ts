import * as Sentry from '@sentry/react'

const SAME_ERRORS_INTERVAL = 1000 * 60 * 10

export const initSentry = () => {
    const errors: any = {}

    Sentry.init({
        dsn: ENV.services.sentry.dsn,
        environment: ENV.environment,
        beforeSend(event, hint) {
            // filter non-error events
            if (hint === undefined || hint.originalException === undefined || hint.originalException === null) {
                return event
            }

            const originalException = hint.originalException

            const key = typeof originalException === 'string' ? originalException : originalException.message

            if (key in errors) {
                return null
            }

            errors[key] = true

            setTimeout(() => {
                delete errors[key]
            }, SAME_ERRORS_INTERVAL)

            return event
        },
    })

    const user = ENV.user

    Sentry.setUser({
        user: user !== null ? {
            id: user.id,
            name: user.name,
        } : null,
    })
}
