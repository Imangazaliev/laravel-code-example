import { createRouteFunction, TAccess } from './core/routing'

import { Users } from './pages/admin/users'
import { Main } from './pages/admin/main'

export const routes: any[] = []

const route = createRouteFunction(routes)

route(AdminRoutes.HOME, Main, { access: TAccess.AUTHENTICATED, permissions: ['access-to-admin-panel'] })
route(AdminRoutes.USERS, Users, { access: TAccess.AUTHENTICATED, permissions: ['view-users-list'] })
