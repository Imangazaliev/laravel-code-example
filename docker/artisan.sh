#!/bin/bash

# exit immediately if a command returned non-zero code
set -e

# this variable is required
# because we can't use $@ in "sh -c" directly
COMMAND="php /app/artisan ${@}"

docker exec -it example_php_1 sh -c "${COMMAND}"
