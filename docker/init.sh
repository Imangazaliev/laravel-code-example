#!/bin/bash

# exit immediately if a command returned non-zero code
set -e

# go to the project root
CURRENT_DIR=`dirname $0`
PROJECT_PATH=`cd ${CURRENT_DIR}/..; pwd`

cd $PROJECT_PATH

# load params from .env
source "${PROJECT_PATH}/.env"

# get container and check that it is running
PHP_CONTAINER_NAME="${COMPOSE_PROJECT_NAME}_php_1"
PHP_CONTAINER_ID=$(docker ps -q -f status=running -f name=^/${PHP_CONTAINER_NAME}$)

if [ ! "${PHP_CONTAINER_ID}" ]; then
  echo "Container ${PHP_CONTAINER_NAME} doesn't exist"

  exit 1
fi

echo "Installing Composer dependencies..."

docker exec -it $PHP_CONTAINER_NAME sh -c "composer install"

echo "Configuring the project..."

docker exec -it $PHP_CONTAINER_NAME sh -c "php artisan key:generate"
docker exec -it $PHP_CONTAINER_NAME sh -c "php artisan ui-strings:cache ar"
docker exec -it $PHP_CONTAINER_NAME sh -c "php artisan ui-strings:cache en"
docker exec -it $PHP_CONTAINER_NAME sh -c "php artisan ui-strings:cache ru"

echo "Frontend assets building..."

docker-compose run --rm node npm install
docker-compose run --rm node gulp build-dev
docker-compose run --rm node npm run dev
