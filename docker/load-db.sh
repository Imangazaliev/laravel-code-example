#!/bin/bash

# exit immediately if a command returned non-zero code
set -e

# go to the project root
CURRENT_DIR=`dirname $0`
PROJECT_PATH=`cd ${CURRENT_DIR}/..; pwd`

cd $PROJECT_PATH

# load params from .env
source "${PROJECT_PATH}/.env"

# get container and check that it is running
CONTAINER_NAME="${COMPOSE_PROJECT_NAME}_postgres_1"
CID=$(docker ps -q -f status=running -f name=^/${CONTAINER_NAME}$)

if [ ! "${CID}" ]; then
  echo "Container ${CONTAINER_NAME} doesn't exist"

  exit 1
fi

PG_CONNECTION="-h \"\$POSTGRES_PORT_5432_TCP_ADDR\" -p \"\$POSTGRES_PORT_5432_TCP_PORT\" -U $DB_USERNAME"

docker cp "${PROJECT_PATH}/tmp/example.dump" ${CONTAINER_NAME}:/var/lib/postgresql/data
docker exec -it $CONTAINER_NAME sh -c "psql ${PG_CONNECTION} --command 'DROP DATABASE IF EXISTS \"${DB_DATABASE}\"' "
docker exec -it $CONTAINER_NAME sh -c "psql ${PG_CONNECTION} --command 'CREATE DATABASE \"${DB_DATABASE}\"' "
docker exec -it $CONTAINER_NAME sh -c "pg_restore ${PG_CONNECTION} --dbname=\"${DB_DATABASE}\" /var/lib/postgresql/data/example.dump"
