<?php

declare(strict_types=1);

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Defaults
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard".
    |
    */

    'defaults' => [
        'guard' => 'user',
    ],

    /*
    |--------------------------------------------------------------------------
    | Authentication Guards
    |--------------------------------------------------------------------------
    |
    | Next, you may define every authentication guard for your application.
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | Supported: "session", "token"
    |
    */

    'guards' => [
        'user' => [
            'driver' => 'session',
            'provider' => 'users',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | User Providers
    |--------------------------------------------------------------------------
    |
    | All authentication drivers have a user provider. This defines how the
    | users are actually retrieved out of your database or other storage
    | mechanisms used by this application to persist your user's data.
    |
    | If you have multiple user tables or models you may configure multiple
    | sources which represent each model / table. These sources may then
    | be assigned to any extra authentication guards you have defined.
    |
    | Supported: "database", "eloquent"
    |
    */

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\Models\User::class,
        ],
    ],

    'minimum_password_length' => 8,

    'phone_number_verification' => [
        'code_length' => 6,

        /*
         | Verification code lifetime in minutes.
         */

        'code_lifetime' => 10,

        /*
         | Maximum count of message in the decay time.
         */

        'max_attempts' => 3,

        /*
         | Interval in that the specified count will be reset (in minutes).
         */

        'decay_time' => 15,

        /*
         | Interval between sending a message (in seconds).
         */

        'resend_interval' => 180,
    ],

    /*
    |--------------------------------------------------------------------------
    | Resetting Passwords
    |--------------------------------------------------------------------------
    |
    | The lifetime the number of minutes that the reset token should be
    | considered valid. This security feature keeps tokens short-lived so
    | they have less time to be guessed.
    |
    */

    'password_reset' => [
        'by_phone_number' => [
            'lifetime' => 5,
            'throttle_timeout' => 60,
        ],

        'by_email' => [
            'lifetime' => 30,
            'throttle_timeout' => 60,
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Password Confirmation Timeout
    |--------------------------------------------------------------------------
    |
    | Here you may define the amount of seconds before a password confirmation
    | times out and the user is prompted to re-enter their password via the
    | confirmation screen. By default, the timeout lasts for three hours.
    |
    */

    'password_timeout' => 10800,

    /*
    |--------------------------------------------------------------------------
    | The lifetime of an authentication token (in hours)
    |--------------------------------------------------------------------------
    */

    'auth_token_lifetime' => 24,
];
