<?php

declare(strict_types=1);

return [
    /*
    |--------------------------------------------------------------------------
    | Notification delay (in minutes).
    |--------------------------------------------------------------------------
    |
    | A time after that a notification will be sent.
    */

    'delay' => [
        'new_messages' => 10,
    ],
];
