<?php

declare(strict_types=1);

return [
    /*
    |--------------------------------------------------------------------------
    | Profile moderation delay.
    |--------------------------------------------------------------------------
    |
    | A delay in minutes after which an edited profile will be sent for moderation.
    */

    'profile_moderation_delay' => 2,

    /*
    |--------------------------------------------------------------------------
    | Moderation lifetime.
    |--------------------------------------------------------------------------
    |
    | The number of seconds that you wish the session to be allowed to remain idle before it expires.
    */

    'session_lifetime' => 60 * 20,

    /*
    |--------------------------------------------------------------------------
    | Moderation lifetime.
    |--------------------------------------------------------------------------
    |
    | The number of seconds after that profiles, photos and chats pending moderation by a moderator will be released.
    */

    'moderation_lifetime' => 60 * 5,
];
