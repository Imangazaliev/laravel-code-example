<?php

declare(strict_types=1);

return [
    'version' => '2006-03-01',

    'user_photos' => [
        'key'    => env('WASABI_USER_PHOTOS_ACCESS_KEY_ID', 'key_id'),
        'secret' => env('WASABI_USER_PHOTOS_SECRET_ACCESS_KEY', 'secret'),
        'region' => env('WASABI_USER_PHOTOS_REGION', 'eu-central-1'),
        'bucket' => env('WASABI_USER_PHOTOS_BUCKET', 'example-photos'),
        'public_url' => 'https://' . env('WASABI_USER_PHOTOS_REGION', 'eu-central-1') . '.wasabisys.com/' . env('WASABI_USER_PHOTOS_BUCKET', 'example-photos'),
    ],
];
