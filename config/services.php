<?php

declare(strict_types=1);

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun and others. This file provides a sane default
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'd7networks' => [
        'username' => env('D7_NETWORKS_API_USERNAME', 'username'),
        'password' => env('D7_NETWORKS_API_PASSWORD', 'password'),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_APP_ID', '123456'),
        'client_secret' => env('FACEBOOK_APP_SECRET', 'secret'),
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID', '123456'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET', 'secret'),
    ],

    'google_analytics' => [
        'enabled' => env('GOOGLE_ANALYTICS_ENABLED', false),
    ],

    'google_recaptcha' => [
        'enabled' => env('GOOGLE_RECAPTCHA_ENABLED'),
        'site_key' => env('GOOGLE_RECAPTCHA_SITE_KEY', '123456'),
        'secret_key' => env('GOOGLE_RECAPTCHA_SECRET_KEY', 'secret'),
    ],

    'interkassa' => [
        'checkout_id' => env('INTERKASSA_CHECKOUT_ID'),
        'sign_key' => env('INTERKASSA_SIGN_KEY'),
        'test_key' => env('INTERKASSA_TEST_KEY'),
    ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'odnoklassniki' => [
        'application_key' => env('ODNOKLASSNIKI_APPLICATION_KEY', 'key'),
        'client_id' => env('ODNOKLASSNIKI_CLIENT_ID', '123456'),
        'client_secret' => env('ODNOKLASSNIKI_CLIENT_SECRET', 'secret'),
    ],

    'smscru' => [
        'login' => env('SMSCRU_LOGIN'),
        'password' => env('SMSCRU_PASSWORD'),
        'set_sender' => env('SMSCRU_SET_SENDER', false),
    ],

    'smsru' => [
        'api_id' => env('SMSRU_API_ID'),
    ],

    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'tawk' => [
        'enabled' => env('TAWK_ENABLED', false),
        'chat_id' => env('TAWK_CHAT_ID'),
        'api_key' => env('TAWK_API_KEY'),
        'guest_email' => 'guest@example.com',
    ],

    'telegram' => [
        'bot_login' => env('TELEGRAM_BOT_LOGIN'),
        'bot_token' => env('TELEGRAM_BOT_TOKEN'),
    ],

    'telegram-bot-api' => [
        'token' => env('TELEGRAM_BOT_TOKEN'),
    ],

    'tinkoff' => [
        'terminal_key' => env('TINKOFF_TERMINAL_KEY'),
        'terminal_password' => env('TINKOFF_TERMINAL_PASSWORD'),
    ],

    'vkontakte' => [
        'client_id' => env('VK_CLIENT_ID', '123456'),
        'client_secret' => env('VK_CLIENT_SECRET', 'secret'),
        'code' => env('VK_CODE', 'code'),
    ],

    'vonage' => [
        'api_key' => env('VONAGE_API_KEY'),
        'secret_key' => env('VONAGE_SECRET_KEY'),
    ],

    'yandex_metrika' => [
        'enabled' => env('YANDEX_METRIKA_ENABLED', false),
    ],

    'yookassa' => [
        'shop_id' => env('YOOKASSA_SHOP_ID', false),
        'secret_key' => env('YOOKASSA_SECRET_KEY', false),
    ],

];
