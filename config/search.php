<?php

return [
    // the number of days before the profile is hidden from the search
    'last_online_timeout' => env('SEARCH_LAST_ONLINE_TIMEOUT', 14),
];
