<?php

declare(strict_types=1);

return [
    'min_age' => 18,
    'max_age' => 120,

    'max_height' => 250,

    'min_weight' => 35,
    'max_weight' => 180,
    'weight_step' => 5,

    'registration_gift_coins_amount' => 15,

    'gift_coins_amount' => 10,

    'photo' => [
        'max_count' => 5,

         /*
         | The maximum size of a photo file in kilobytes.
         */

        'max_file_size' => 1024 * 8,

        'allowed_mime_types' => ['image/jpeg'],

        'max_caption_length' => 64,

        'thumbnail_sizes' => [100, 200, 300, 400],
    ],

    /*
     | The interval between updates of saving last online time to the database (in seconds).
     */

    'online_status_update_interval' => 30,

    'idle_timeout' => 60,
    'offline_timeout' => 60,

    /*
     | The time after a profile pending deletion will be deleted (in hours).
     */

    'deletion_timeout' => 24,
];
