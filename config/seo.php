<?php

declare(strict_types=1);

return [
    'robots_access' => env('ROBOTS_ACCESS', false),
];
