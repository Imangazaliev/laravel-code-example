<?php

declare(strict_types=1);

return [
    'test_mode' => env('PAYMENTS_TEST_MODE', false),

    /*
    |--------------------------------------------------------------------------
    | The price of a coin in USD cents
    |--------------------------------------------------------------------------
    */

    'coin_price' => 6,

    'supported_currencies' => ['EUR', 'RUB', 'UAH', 'USD'],

    /*
    |--------------------------------------------------------------------------
    | The lifetime of an invoice (in hours)
    |--------------------------------------------------------------------------
    */

    'invoice_lifetime' => 24,
];
