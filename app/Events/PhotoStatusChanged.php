<?php

declare(strict_types=1);

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PhotoStatusChanged
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private int $photoId;

    public function __construct(int $photoId)
    {
        $this->photoId = $photoId;
    }

    public function getPhotoId(): int
    {
        return $this->photoId;
    }
}
