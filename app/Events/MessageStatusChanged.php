<?php

declare(strict_types=1);

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MessageStatusChanged
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private int $messageId;
    private Carbon $time;

    public function __construct(int $messageId, Carbon $time)
    {
        $this->messageId = $messageId;
        $this->time = $time;
    }

    public function getMessageId(): int
    {
        return $this->messageId;
    }

    public function getTime(): Carbon
    {
        return $this->time;
    }
}
