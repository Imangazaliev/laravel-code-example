<?php

declare(strict_types=1);

namespace App\Console;

use App\Console\Commands\Chats\CancelInactiveUsersContactsRequests;
use App\Console\Commands\Chats\FinishExpiredChatRequests;
use App\Console\Commands\Moderation\FinishIdleSessions;
use App\Console\Commands\Moderation\ReleaseHangingModerations;
use App\Console\Commands\Moderation\SendProfilesToModeration;
use App\Console\Commands\Notifications\SendNotifications;
use App\Console\Commands\Payments\FinishExpiredInvoices;
use App\Console\Commands\Users\DeleteProfilesPendingDeletion;
use App\Console\Commands\Users\UnbanTemporaryBannedUsers;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('telescope:prune --hours=48')->daily();

        $schedule->command(SendProfilesToModeration::class)->everyMinute();
        $schedule->command(SendNotifications::class)->everyMinute()->withoutOverlapping(10);
        $schedule->command(FinishExpiredChatRequests::class)->hourly();
        $schedule->command(FinishExpiredInvoices::class)->everyTenMinutes();
        $schedule->command(FinishIdleSessions::class)->everyMinute();
        $schedule->command(ReleaseHangingModerations::class)->everyMinute();
        $schedule->command(DeleteProfilesPendingDeletion::class)->everyFiveMinutes();
        $schedule->command(UnbanTemporaryBannedUsers::class)->everyMinute()->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');
    }
}
