<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\Language;
use App\Models\BadWord;
use DB;
use Illuminate\Console\Command;

class LoadBadWords extends Command
{
    protected $signature = 'load-bad-words';

    protected $description = 'Load bad words';

    public function handle(): void
    {
        DB::statement('TRUNCATE bad_words RESTART IDENTITY CASCADE');

        $words = json_decode(file_get_contents(base_path('tmp/bad-words.json')), true);

        $russianLanguageId = (new Language())->where('code', 'ru')->value('id');

        foreach ($words as $word) {
            (new BadWord())->create([
                'language_id' => $russianLanguageId,
                'text' => $word['text'],
                'description' => $word['description'] ?? null,
            ]);
        }

        $this->info('Loaded!');
    }
}
