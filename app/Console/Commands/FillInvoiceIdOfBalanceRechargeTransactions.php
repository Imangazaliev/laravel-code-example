<?php

declare(strict_types=1);

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class FillInvoiceIdOfBalanceRechargeTransactions extends Command
{
    protected $signature = 'fill-invoice-id-of-balance-recharge-transactions';

    protected $description = 'Fill invoice ID of balance recharge transactions';

    public function handle(): void
    {
        DB::update("
            UPDATE coin_transactions ct
            SET invoice_id = i.id
            FROM invoices i
            WHERE ct.user_id = i.user_id
            AND ct.type = 'balance-recharge'
            AND DATE_TRUNC('second', ct.transaction_time::timestamp) = DATE_TRUNC('second', i.finished_at::timestamp)
            AND i.status = 'success'
        ");

        DB::update("
            UPDATE coin_transactions ct
            SET invoice_id = i.id
            FROM invoices i
            WHERE ct.user_id = i.user_id
            AND ct.type = 'balance-recharge'
            AND DATE_TRUNC('second', ct.transaction_time::timestamp + interval '1 second') = DATE_TRUNC('second', i.finished_at::timestamp)
            AND i.status = 'success'
        ");

        $this->info('Invoice ID field filled!');
    }
}
