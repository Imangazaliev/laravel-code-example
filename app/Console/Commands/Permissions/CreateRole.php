<?php

declare(strict_types=1);

namespace App\Console\Commands\Permissions;

use App\Models\Permission;
use App\Models\Role;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use ExampleProject\Permissions\PermissionRegistrar;
use Throwable;

class CreateRole extends Command
{
    protected $signature = 'permission:create-role';

    protected $description = 'Create a role';

    public function handle(PermissionRegistrar $permissionRegistrar): void
    {
        while (true) {
            $code = $this->ask('Enter the code of a role');

            // the role code may contain latin alphabet letters, dots, and hyphens
            // and must start and end with a letter
            if (preg_match('/^[a-z][a-z.\-]+[a-z]$/', $code) === 1) {
                break;
            }

            $this->error('Invalid role code format');
        }

        $roleExists = (new Role())
            ->where('code', $code)
            ->exists()
        ;

        if ($roleExists) {
            $this->error(sprintf('Role with code %s already exists', $code));

            return;
        }

        $title = $this->ask('Enter the title of a role');
        $description = $this->ask('Enter the description of a role');

        $availablePermissions = (new Permission())
            ->orderBy('code')
            ->get(['code', 'title'])
            ->map(fn (Permission $permission) => "{$permission->code} - {$permission->title}")
            ->all()
        ;

        if (count($availablePermissions) === 0) {
            $this->warn('There\'s no available permissions. ');

            return;
        }

        $this->info('Available permissions:');

        $this->line('');
        $this->line(implode("\n", $availablePermissions));
        $this->line('');

        $permissionCodesStr = $this->ask('Enter permission codes with a comma that you want to attach to the created role');

        $permissionCodes = explode(',', $permissionCodesStr);

        $permissions = (new Permission())
            ->whereIn('code', $permissionCodes)
            ->get()
        ;

        $missedPermissionCodes = array_diff($permissionCodes, $permissions->pluck('code')->all());

        if (count($missedPermissionCodes) > 0) {
            $this->error(sprintf('Permissions with the following codes not found: %s', implode(', ', $missedPermissionCodes)));

            return;
        }

        DB::beginTransaction();

        try {
            $role = (new Role())->create([
                'code' => $code,
                'title' => $title,
                'description' => $description,
                'created_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);

            $role->permissions()->sync($permissions->pluck('id'));

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }

        $permissionRegistrar->clearCachedPermissions();

        $this->info('Role created!');
    }
}
