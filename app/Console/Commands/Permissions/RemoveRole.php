<?php

declare(strict_types=1);

namespace App\Console\Commands\Permissions;

class RemoveRole extends BaseRolesAndPermissionsCommand
{
    protected $signature = 'permission:remove-role';

    protected $description = 'Remove roles from a user';

    public function handle(): void
    {
        $user = $this->askUser();

        if ($user->roles->count() === 0) {
            $this->info('There\'s no any role assigned to the user');

            return;
        }

        $this->showAssignedRoles($user);

        $roleCodes = $this->askRoles('Enter role codes with a comma that you want to remove from the user');
        $languageIds = $this->askLanguages();

        if ($languageIds === null) {
            $user->removeRole($roleCodes, null);
        } else {
            foreach ($languageIds as $languageId) {
                $user->removeRole($roleCodes, $languageId);
            }
        }

        $this->info('Roles updated!');
    }
}
