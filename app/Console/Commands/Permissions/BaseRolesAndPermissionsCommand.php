<?php

declare(strict_types=1);

namespace App\Console\Commands\Permissions;

use App\Models\Language;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

abstract class BaseRolesAndPermissionsCommand extends Command
{
    protected function askUser(): User
    {
        while (true) {
            $idOrPhoneNumber = $this->ask('Enter the ID or a phone number of a user');

            $user = (new User())
                ->with([
                    'roles' => function (BelongsToMany $query) {
                        $query->withPivot(['language_id']);
                    },
                ])
                ->with([
                    'permissions' => function (BelongsToMany $query) {
                        $query->withPivot(['language_id']);
                    },
                ])
                ->where('id', $idOrPhoneNumber)
                ->orWhere('phone_number', $idOrPhoneNumber)
                ->first()
            ;

            if ($user !== null) {
                break;
            }

            $this->error('User with the specified ID or phone number not found');
        }

        $this->line('<info>ID:</info> ' . $user->id);
        $this->line('<info>Name:</info> ' . $user->name);
        $this->line('<info>Phone number:</info> ' . $user->phone_number);
        $this->line('');

        return $user;
    }

    protected function askRoles(string $message): array
    {
        while (true) {
            $roleCodesStr = $this->ask($message);

            $roleCodes = $roleCodesStr === null ? [] : explode(',', $roleCodesStr);
            $roleCodes = array_map('trim', $roleCodes);

            $existingRoleCodes = (new Role())
                ->whereIn('code', $roleCodes)
                ->pluck('code')
                ->all()
            ;

            $missedRoleCodes = array_diff($roleCodes, $existingRoleCodes);

            if (count($missedRoleCodes) === 0) {
                return $roleCodes;
            }

            $this->error(sprintf('Roles with the following codes not found: %s', implode(', ', $missedRoleCodes)));
        }
    }

    protected function askPermissions(string $message): array
    {
        while (true) {
            $permissionCodesStr = $this->ask($message);

            $permissionCodes = $permissionCodesStr === null ? [] : explode(',', $permissionCodesStr);
            $permissionCodes = array_map('trim', $permissionCodes);

            $existingRoleCodes = (new Permission())
                ->whereIn('code', $permissionCodes)
                ->pluck('code')
                ->all()
            ;

            $missedPermissionCodes = array_diff($permissionCodes, $existingRoleCodes);

            if (count($missedPermissionCodes) === 0) {
                return $permissionCodes;
            }

            $this->error(sprintf('Permissions with the following codes not found: %s', implode(', ', $missedPermissionCodes)));
        }
    }

    protected function showAssignedRoles(User $user): void
    {
        if ($user->roles->count() === 0) {
            $this->line('<info>Assigned roles:</info> (empty)');
            $this->line('');
        } else {
            $this->line('<info>Assigned roles:</info>');
            $this->line('');

            $languageCodesById = (new Language())
                ->pluck('code', 'id')
                ->all()
            ;

            $rolesByLanguage = $user
                ->roles
                ->groupBy(fn (Role $role) => $role->pivot->language_id ?? '')
                ->sortBy(function (Collection $rolesInLanguage, $languageId) {
                    return $languageCodesById[$languageId] ?? '';
                })
            ;

            foreach ($rolesByLanguage as $languageId => $rolesInLanguage) {
                $languageCode = $languageId === '' ? '*' : $languageCodesById[(int) $languageId];

                $this->line(sprintf('<comment>[%s]</comment>: %s', $languageCode, implode(', ', $rolesInLanguage->pluck('code')->all())));
            }

            $this->line('');
        }
    }

    protected function showGivenPermissions(User $user): void
    {
        if ($user->permissions->count() === 0) {
            $this->line('<info>Assigned permissions:</info> (empty)');
            $this->line('');
        } else {
            $this->line('<info>Assigned permissions:</info>');
            $this->line('');

            $languageCodesById = (new Language())
                ->pluck('code', 'id')
                ->all()
            ;

            $permissionsByLanguage = $user
                ->permissions
                ->groupBy(fn (Permission $permission) => $permission->pivot->language_id ?? '')
                ->sortBy(function (Collection $permissionsInLanguage, $languageId) {
                    return $languageCodesById[$languageId] ?? '';
                })
            ;

            foreach ($permissionsByLanguage as $languageId => $permissionsInLanguage) {
                $languageCode = $languageId === '' ? '*' : $languageCodesById[(int) $languageId];

                $this->line(sprintf('<comment>[%s]</comment>: %s', $languageCode, implode(', ', $permissionsInLanguage->pluck('code')->all())));
            }

            $this->line('');
        }
    }

    protected function askLanguages(): ?array
    {
        while (true) {
            $languageCodesStr = $this->ask('Enter the code of languages with a comma you want to attach the permissions');

            if ($languageCodesStr === '*') {
                return null;
            }

            $languageCodes = explode(',', $languageCodesStr);
            $languageCodes = array_map('trim', $languageCodes);

            $languages = (new Language())
                ->whereIn('code', $languageCodes)
                ->where('is_active', true)
                ->get(['id', 'code'])
            ;

            $missedLanguageCodes = array_diff($languageCodes, $languages->pluck('code')->all());

            if (count($missedLanguageCodes) === 0) {
                return $languages->pluck('id')->all();
            }

            $this->error(sprintf('Languages with the following codes not found or inactive: %s', implode(', ', $missedLanguageCodes)));

            exit;
        }
    }
}
