<?php

declare(strict_types=1);

namespace App\Console\Commands\Permissions;

use App\Models\Permission;

class GivePermission extends BaseRolesAndPermissionsCommand
{
    protected $signature = 'permission:give-permission';

    protected $description = 'Give permissions to a user';

    public function handle(): void
    {
        $user = $this->askUser();

        $this->showGivenPermissions($user);

        $permissions = (new Permission())
            ->orderBy('code')
            ->get(['code', 'title'])
        ;

        if ($permissions->count() === 0) {
            $this->warn('There\'s no available permissions.');

            return;
        }

        $this->info('Available permissions:');

        $this->line('');
        $permissions->map(fn (Permission $permission) => $this->line("{$permission->code} - {$permission->title}"));
        $this->line('');

        $permissionCodes = $this->askPermissions('Enter permission codes with a comma that you want to give to the user');
        $languageIds = $this->askLanguages();

        if ($languageIds === null) {
            $user->givePermissionTo($permissionCodes, null);
        } else {
            foreach ($languageIds as $languageId) {
                $user->givePermissionTo($permissionCodes, $languageId);
            }
        }

        $this->info('Permissions updated!');
    }
}
