<?php

declare(strict_types=1);

namespace App\Console\Commands\Permissions;

use App\Models\Permission;
use Carbon\Carbon;
use Illuminate\Console\Command;
use ExampleProject\Permissions\PermissionRegistrar;

class CreatePermission extends Command
{
    protected $signature = 'permission:create-permission';

    protected $description = 'Create a permission';

    public function handle(PermissionRegistrar $permissionRegistrar): void
    {
        while (true) {
            $code = $this->ask('Enter the code of a permission');

            // the permission code may contain latin alphabet letters, dots, and hyphens
            // and must start and end with a letter
            if (preg_match('/^[a-z][a-z.\-]+[a-z]$/', $code) === 1) {
                break;
            }

            $this->error('Invalid permission code format');
        }

        $permissionExists = (new Permission())
            ->where('code', $code)
            ->exists()
        ;

        if ($permissionExists) {
            $this->error(sprintf('Permission with code %s already exists', $code));

            return;
        }

        $title = $this->ask('Enter the title of a permission');
        $description = $this->ask('Enter the description of a permission');

        (new Permission())->create([
            'code' => $code,
            'title' => $title,
            'description' => $description,
            'created_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);

        $permissionRegistrar->clearCachedPermissions();

        $this->info('Permission created!');
    }
}
