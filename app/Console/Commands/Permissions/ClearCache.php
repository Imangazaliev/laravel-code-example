<?php

declare(strict_types=1);

namespace App\Console\Commands\Permissions;

use Illuminate\Console\Command;
use ExampleProject\Permissions\PermissionRegistrar;

class ClearCache extends Command
{
    protected $signature = 'permission:clear-cache';

    protected $description = 'Clear the permission cache';

    public function handle(PermissionRegistrar $permissionRegistrar): void
    {
        $permissionRegistrar->clearCachedPermissions();

        $this->info('Cache cleared!');
    }
}
