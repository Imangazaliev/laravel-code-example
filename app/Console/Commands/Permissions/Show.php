<?php

declare(strict_types=1);

namespace App\Console\Commands\Permissions;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Console\Command;

class Show extends Command
{
    protected $signature = 'permission:show';

    protected $description = 'List roles and permissions';

    public function handle(): void
    {
        $roles = (new Role())
            ->with('permissions')
            ->orderBy('code')
            ->get()
        ;

        $permissions = (new Permission())
            ->orderBy('code')
            ->get()
        ;

        $this->info('Roles:');
        $this->line('');

        foreach ($roles as $role) {
            $this->warn("$role->title ($role->code)");
            $this->line($role->description !== '' ? $role->description : '(no description)');
            $this->line('');

            if ($role->permissions->count() === 0) {
                $this->line('No permissions attached');
            } else {
                $permissionsStr = implode(', ', $role->permissions->pluck('code')->all());

                $this->line("<info>Permissions:</info> {$permissionsStr}");
            }

            $this->line('');
        }

        $this->info('Permissions:');
        $this->line('');

        foreach ($permissions as $permission) {
            $this->warn("$permission->title ($permission->code)");
            $this->line($permission->description !== '' ? $permission->description : '(no description)');
            $this->line('');
        }
    }
}
