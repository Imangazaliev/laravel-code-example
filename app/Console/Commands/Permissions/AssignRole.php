<?php

declare(strict_types=1);

namespace App\Console\Commands\Permissions;

use App\Models\Role;

class AssignRole extends BaseRolesAndPermissionsCommand
{
    protected $signature = 'permission:assign-role';

    protected $description = 'Assign roles to a user';

    public function handle(): void
    {
        $user = $this->askUser();

        $this->showAssignedRoles($user);

        $roles = (new Role())
            ->orderBy('code')
            ->get(['code', 'title'])
        ;

        if ($roles->count() === 0) {
            $this->warn('There\'s no available roles. ');

            return;
        }

        $this->info('Available roles:');

        $this->line('');
        $roles->map(fn (Role $role) => $this->line("{$role->code} - {$role->title}"));
        $this->line('');

        $roleCodes = $this->askRoles('Enter role codes with a comma that you want to attach to the user');
        $languageIds = $this->askLanguages();

        if ($languageIds === null) {
            $user->assignRole($roleCodes, null);
        } else {
            foreach ($languageIds as $languageId) {
                $user->assignRole($roleCodes, $languageId);
            }
        }

        $this->info('Roles updated!');
    }
}
