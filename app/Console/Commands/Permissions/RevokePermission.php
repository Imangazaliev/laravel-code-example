<?php

declare(strict_types=1);

namespace App\Console\Commands\Permissions;

class RevokePermission extends BaseRolesAndPermissionsCommand
{
    protected $signature = 'permission:revoke-permission';

    protected $description = 'Revoke permissions from a user';

    public function handle(): void
    {
        $user = $this->askUser();

        if ($user->permissions->count() === 0) {
            $this->info('There\'s no any permission assigned to the user');

            return;
        }

        $this->showGivenPermissions($user);

        $permissionCodes = $this->askPermissions('Enter permission codes with a comma that you want to remove from the user');
        $languageIds = $this->askLanguages();

        if ($languageIds === null) {
            $user->revokePermissionTo($permissionCodes, null);
        } else {
            foreach ($languageIds as $languageId) {
                $user->revokePermissionTo($permissionCodes, $languageId);
            }
        }

        $this->info('Permissions updated!');
    }
}
