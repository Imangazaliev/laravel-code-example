<?php

declare(strict_types=1);

namespace App\Console\Commands\Moderation;

use App\Models\Chat;
use App\Models\User;
use App\Models\UserPhoto;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ReleaseHangingModerations extends Command
{
    protected $signature = 'moderation:release-hanging-moderations';

    protected $description = 'Release hanging moderations';

    public function handle(): void
    {
        $lifetime = config('moderation.moderation_lifetime');

        $time = Carbon::now()->subSeconds($lifetime);

        // release pending users
        $releasedUserCount = (new User())
            ->where('moderation_claimed_at', '<=', $time)
            ->update([
                'moderation_moderator_id' => null,
                'moderation_claimed_at' => null,
            ])
        ;

        // release pending photos
        $releasedPhotoCount = (new UserPhoto())
            ->where('moderation_claimed_at', '<=', $time)
            ->update([
                'moderation_moderator_id' => null,
                'moderation_claimed_at' => null,
            ])
        ;

        // release pending chats
        $releasedChatCount = (new Chat())
            ->where('moderation_claimed_at', '<=', $time)
            ->update([
                'moderation_moderator_id' => null,
                'moderation_claimed_at' => null,
            ])
        ;

        $this->info("{$releasedUserCount} user(s) released!");
        $this->info("{$releasedPhotoCount} photo(s) released!");
        $this->info("{$releasedChatCount} chat(s) released!");
    }
}
