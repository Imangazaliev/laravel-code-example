<?php

declare(strict_types=1);

namespace App\Console\Commands\Moderation;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendProfilesToModeration extends Command
{
    protected $signature = 'moderation:send-profiles-to-moderation';

    protected $description = 'Send profiles that are not moderated to the moderation';

    public function handle(): void
    {
        $profileModerationDelay = config('moderation.profile_moderation_delay');

        $sentProfileCount = (new User())
            ->where('required_fields_filled', true)
            ->where('status', User::STATUS_NOT_MODERATED)
            ->where('status_updated_at', '<=', Carbon::now()->subMinutes($profileModerationDelay))
            ->update([
                'status' => User::STATUS_PENDING_MODERATION,
                'status_updated_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ])
        ;

        $this->info("{$sentProfileCount} profile(s) sent to moderation!");
    }
}
