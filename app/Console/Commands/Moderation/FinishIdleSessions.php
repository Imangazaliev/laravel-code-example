<?php

declare(strict_types=1);

namespace App\Console\Commands\Moderation;

use App\Models\Chat;
use App\Models\ModerationSession;
use App\Models\User;
use App\Models\UserPhoto;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FinishIdleSessions extends Command
{
    protected $signature = 'moderation:finish-idle-sessions';

    protected $description = 'Finish all idle moderation sessions';

    public function handle(): void
    {
        $lifetime = config('moderation.session_lifetime');

        $moderationSessions = (new ModerationSession())
            ->where('last_activity_at', '<=', Carbon::now()->subSeconds($lifetime))
            ->get(['id', 'moderator_id', 'type'])
        ;

        $moderationSessionsByType = $moderationSessions->groupBy('type');

        if ($moderationSessionsByType->has(ModerationSession::TYPE_PROFILES)) {
            $moderatorIds = $moderationSessionsByType->get(ModerationSession::TYPE_PROFILES)->pluck('moderator_id')->all();

            // release claimed users
            (new User())
                ->whereIn('moderation_moderator_id', $moderatorIds)
                ->update([
                    'moderation_moderator_id' => null,
                    'moderation_claimed_at' => null,
                ])
            ;
        }

        if ($moderationSessionsByType->has(ModerationSession::TYPE_PHOTOS)) {
            $moderatorIds = $moderationSessionsByType->get(ModerationSession::TYPE_PROFILES)->pluck('moderator_id')->all();

            // release claimed photos
            (new UserPhoto())
                ->whereIn('moderation_moderator_id', $moderatorIds)
                ->update([
                    'moderation_moderator_id' => null,
                    'moderation_claimed_at' => null,
                ])
            ;
        }

        if ($moderationSessionsByType->has(ModerationSession::TYPE_CHAT_MESSAGES)) {
            $moderatorIds = $moderationSessionsByType->get(ModerationSession::TYPE_PROFILES)->pluck('moderator_id')->all();

            // release claimed chats
            (new Chat())
                ->whereIn('moderation_moderator_id', $moderatorIds)
                ->update([
                    'moderation_moderator_id' => null,
                    'moderation_claimed_at' => null,
                ])
            ;
        }

        (new ModerationSession())
            ->whereIn('id', $moderationSessions->pluck('id')->all())
            ->update([
                'finished_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'is_finished_automatically' => true,
            ])
        ;

        $this->info("{$moderationSessions->count()} session(s) finished!");
    }
}
