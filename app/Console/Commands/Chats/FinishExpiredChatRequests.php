<?php

declare(strict_types=1);

namespace App\Console\Commands\Chats;

use App\Models\ChatRequest;
use App\Models\CoinTransaction;
use App\Models\Notification;
use App\Models\User;
use App\Services\Coins\Coins;
use App\Services\Coins\CoinTransactionDto;
use App\Services\PhotoAccessGate\PhotoAccessGate;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Throwable;

class FinishExpiredChatRequests extends Command
{
    protected $signature = 'chats:finish-expired-chat-requests';

    protected $description = 'Finish all expired chat requests';

    private Coins $coins;

    private PhotoAccessGate $photoAccessGate;

    private int $lifetime;

    public function __construct(Coins $coins, PhotoAccessGate $photoAccessGate)
    {
        parent::__construct();

        $this->coins = $coins;
        $this->photoAccessGate = $photoAccessGate;
        $this->lifetime = config('chats.chat_requests_lifetime');
    }

    public function handle(): void
    {
        $beforeTime = Carbon::now()->subDays($this->lifetime)->format(DATE_TIME_WITH_TIME_ZONE_FORMAT);

        $canceledChatRequestCount = 0;

        while (true) {
            $chatRequests = (new ChatRequest())
                ->with('transaction')
                ->where('status', ChatRequest::STATUS_NEW)
                ->where('sent_at', '<=', $beforeTime)
                ->limit(50)
                ->get()
            ;

            if ($chatRequests->count() === 0) {
                break;
            }

            $this->finishChatRequests($chatRequests);

            $canceledChatRequestCount += $chatRequests->count();
        }

        $this->info("{$canceledChatRequestCount} chat request(s) canceled!");
    }

    private function finishChatRequests(Collection $chatRequests): void
    {
        foreach ($chatRequests as $chatRequest) {
            DB::beginTransaction();

            try {
                $returnCoinsAmount = abs($chatRequest->transaction->amount);

                $coinTransactionDto = (new CoinTransactionDto())
                    ->setType(CoinTransaction::TYPE_CHAT_REQUEST_AUTO_CANCELLATION)
                    ->setAmount($returnCoinsAmount)
                    ->setUserId($chatRequest->requestor_id)
                    ->setDescription(sprintf('Automatically cancel chat request #%d to user #%d', $chatRequest->id, $chatRequest->target_user_id))
                ;

                $transaction = $this->coins->changeBalance($coinTransactionDto);

                $currentTime = Carbon::now();

                $chatRequest->update([
                    'status' => ChatRequest::STATUS_CANCELED_AUTOMATICALLY,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'return_transaction_id' => $transaction->id,
                ]);

                $targetUser = $chatRequest->targetUser()->first(['id', 'name', 'sex']);

                (new Notification())->create([
                    'user_id' => $chatRequest->requestor_id,
                    'type' => Notification::TYPE_CHAT_REQUEST_CANCELED_AUTOMATICALLY,
                    'title' => 'notifications:chat-request-canceled-automatically.title',
                    'body' => 'notifications:chat-request-canceled-automatically.body',
                    'parameters' => [
                        'userId' => $targetUser->id,
                        'userName' => $targetUser->name,
                        'lifetime' => $this->lifetime,
                    ],
                    'context' => $targetUser->sex === User::SEX_MALE ? 'male' : 'female',
                    'additional_data' => [
                        'chat_request_id' => $chatRequest->id,
                    ],
                    'is_hidden' => false,
                    'created_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                ]);

                DB::commit();

                $this->photoAccessGate->checkAccess($chatRequest->target_user_id, $chatRequest->requestor_id);

                cache_info()->usersIncomingChatRequestId($chatRequest->target_user_id, $chatRequest->requestor_id)->forget();
                cache_info()->usersOutgoingChatRequestId($chatRequest->requestor_id, $chatRequest->target_user_id)->forget();
                cache_info()->newChatRequestCount($chatRequest->target_user_id)->forget();
            } catch (Throwable $exception) {
                DB::rollBack();

                throw $exception;
            }
        }
    }
}
