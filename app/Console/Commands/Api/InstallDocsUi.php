<?php

declare(strict_types=1);

namespace App\Console\Commands\Api;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use RuntimeException;

class InstallDocsUi extends Command
{
    protected $signature = 'api:docs:install-ui {--force : Force the operation to run when in production}';

    protected $description = 'Install a UI for the API documentation';

    public function handle(): void
    {
        if (app()->environment() === 'production' && ! $this->option('force')) {
            $this->error('Can not install the API documentation in production because it will be available publicly. Use --force option to bypass this warning.');

            return;
        }

        $this->cleanApiDocsDirectory();
        $this->copySwaggerFiles();
        $this->createSymlink();

        $this->info(sprintf('Done! Now you can open the URL %s', url('/api-docs/index.html')));
    }

    private function cleanApiDocsDirectory(): void
    {
        $files = File::files(public_path('api-docs'));

        File::delete($files);
    }

    private function copySwaggerFiles(): void
    {
        $files = File::files(resource_path('swagger-ui-3.24.2'));

        foreach ($files as $file) {
            File::copy($file->getPathname(), public_path('api-docs/' . $file->getBasename()));
        }
    }

    private function createSymlink(): void
    {
        // remove the symlink if it already exists
        if (File::exists(public_path('api-docs/docs')) && ! File::delete(public_path('api-docs/docs'))) {
            throw new RuntimeException(sprintf('Cannot remove a symlink to the docs directory (%s)', public_path('api-docs/docs')));
        }

        File::link(base_path('frontend/docs'), public_path('api-docs/docs'));
    }
}
