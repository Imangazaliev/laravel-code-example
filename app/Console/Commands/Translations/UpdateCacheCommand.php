<?php

declare(strict_types=1);

namespace App\Console\Commands\Translations;

use Illuminate\Console\Command;
use ExampleProject\Translations\UiStringsCacheUpdater;
use Symfony\Component\Console\Input\InputArgument;

class UpdateCacheCommand extends Command
{
    protected $name = 'translations:cache';

    protected $description = 'Update translation strings cache';

    public function handle(UiStringsCacheUpdater $cacheUpdater): void
    {
        $this->info('Updating UI strings cache...');

        $afterUpdateCallback = function (string $languageCode) {
            $this->info($languageCode);
        };

        $languageCodes = $this->argument('language');

        if (count($languageCodes) === 0) {
            $cacheUpdater->updateCache(null, $afterUpdateCallback);
        } else {
            foreach ($languageCodes as $languageCode) {
                $cacheUpdater->updateCache($languageCode, $afterUpdateCallback);
            }
        }

        $this->info('UI strings cache updated!');
    }

    protected function getArguments(): array
    {
        return [
            ['language', InputArgument::IS_ARRAY, 'Language code'],
        ];
    }
}
