<?php

declare(strict_types=1);

namespace App\Console\Commands\Notifications;

use App\Models\Notification as NotificationModel;
use App\Models\User;
use App\Notifications\Notification;
use App\Services\NotificationDelivery\NotificationDelivery;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendNotifications extends Command
{
    protected $signature = 'notifications:send';

    protected $description = 'Send notifications';

    public function handle(): void
    {
        $afterTime = Carbon::now()->subDay(3)->format(DATE_TIME_FORMAT);

        $notifications = (new NotificationModel())
            // set the limit to don't send too old notifications
            ->where('created_at', '>', $afterTime)
            ->whereNull('seen_at')
            ->whereNull('sent_at')
            ->orderByDesc('created_at')
            ->limit(5)
            ->get([
                'id',
                'user_id',
                'title',
                'body',
                'parameters',
                'context',
            ])
        ;

        if ($notifications->count() === 0) {
            $this->info('Done.');

            return;
        }

        $notificationsByUserId = $notifications->groupBy('user_id');

        $users = (new User())
            ->whereIn('id', $notificationsByUserId->keys())
            ->get(['id', 'notification_settings'])
        ;

        $usersById = $users->pluck('id')->combine($users);

        $this->info(sprintf('Sending %d notifications...', $notifications->count()));

        $progress = $this->output->createProgressBar($notifications->count());

        foreach ($notificationsByUserId as $userId => $userNotifications) {
            /**
             * @var User $user
             */
            $user = $usersById[$userId];

            foreach ($userNotifications as $notification) {
                $send = true;

                if (
                    $notification->type === NotificationModel::TYPE_NEW_CHAT_REQUEST &&
                    ! $user->notificationSettings()->isNotificationTypeEnabled(NotificationDelivery::TYPE_INCOMING_CHAT_REQUEST)
                ) {
                    $send = false;
                }

                if (
                    in_array($notification->type, [
                        NotificationModel::TYPE_CHAT_REQUEST_ACCEPTED,
                        NotificationModel::TYPE_CHAT_REQUEST_REJECTED,
                    ], true) &&
                    ! $user->notificationSettings()->isNotificationTypeEnabled(NotificationDelivery::TYPE_CHAT_REQUEST_ANSWER)
                ) {
                    $send = false;
                }

                if ($send) {
                    $user->notify(new Notification(
                        $notification->title,
                        $notification->body,
                        $notification->parameters,
                        $notification->context,
                    ));
                }

                $notification->update([
                    'sent_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ]);

                $progress->advance();
            }
        }

        $progress->finish();

        $this->line('');
        $this->info('Done!');
    }
}
