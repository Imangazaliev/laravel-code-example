<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\User;
use App\Services\RatingCalculator\ReputationCalculator;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class RecalculateUsersReputation extends Command
{
    protected $signature = 'recalculate-users-reputation {user-id? : The ID of a user}';

    protected $description = 'Recalculate users reputation';

    public function handle(ReputationCalculator $reputationCalculator): void
    {
        $query = (new User())->select(['id']);

        if ($this->argument('user-id') !== null) {
            $query->where('id', $this->argument('user-id'));
        }

        $query->chunk(500, function (Collection $users) use ($reputationCalculator) {
            foreach ($users as $user) {
                $reputation = $reputationCalculator->calculateReputation($user->id);

                $user->update([
                    'reputation' => $reputation,
                    'reputation_updated_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ]);
            }
        });
    }
}
