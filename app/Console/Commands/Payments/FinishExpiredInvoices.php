<?php

declare(strict_types=1);

namespace App\Console\Commands\Payments;

use App\Models\Invoice;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FinishExpiredInvoices extends Command
{
    protected $signature = 'payments:finish-expired-invoices';

    protected $description = 'Finish expired invoices';

    public function handle(): void
    {
        $lifetime = config('payments.invoice_lifetime');

        $currentTime = Carbon::now();

        $finishedInvoiceCount = (new Invoice())
            ->where('status', Invoice::STATUS_NEW)
            ->where('created_at', '<=', $currentTime->subHours($lifetime))
            ->update([
                'status' => Invoice::STATUS_EXPIRED,
                'finished_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ])
        ;

        $this->info("{$finishedInvoiceCount} invoice(s) finished!");
    }
}
