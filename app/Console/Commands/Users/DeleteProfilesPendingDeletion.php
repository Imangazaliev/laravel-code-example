<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Throwable;

class DeleteProfilesPendingDeletion extends Command
{
    protected $signature = 'users:delete-profile-pending-deletion';

    protected $description = 'Delete profiles pending deletion';

    public function handle(): void
    {
        $profileDeletionTimeout = config('profile.deletion_timeout');

        $userIds = (new User())
            ->where('status', User::STATUS_PENDING_DELETION)
            ->where('status_updated_at', '<=', Carbon::now()->subHours($profileDeletionTimeout)->format(DATE_TIME_WITH_TIME_ZONE_FORMAT))
            ->pluck('id')
        ;

        foreach ($userIds as $userId) {
            DB::beginTransaction();

            try {
                $currentTime = Carbon::now();

                $user = (new User())
                    ->where('id', $userId)
                    ->firstOrFail([
                        'id',
                        'status_updated_at',
                        'name',
                    ])
                ;

                (new User())
                    ->where('id', $userId)
                    ->where('status_updated_at', $user->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                    ->update([
                        'name' => 'DELETED USER',
                        'phone_number' => sprintf('DELETED #%d', $userId),
                        'phone_number_country_code' => null,
                        'phone_number_verified_at' => null,
                        'phone_number_in_national_format' => sprintf('DELETED #%d', $userId),
                        'email' => sprintf('DELETED #%d', $userId),
                        'email_verified_at' => null,
                        'password' => bcrypt(str_random(config('auth.minimum_password_length'))),
                        'password_updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'status' => User::STATUS_DELETED,
                        'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                        'remember_token' => null,
                    ])
                ;

                (new Notification())->create([
                    'user_id' => $userId,
                    'type' => Notification::TYPE_PROFILE_DELETED,
                    'title' => 'notifications:profile-deleted.title',
                    'body' => 'notifications:profile-deleted.body',
                    'is_hidden' => true,
                    'created_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                ]);

                DB::commit();

                cache_info()->userProfile($user->id)->forget();
                cache_info()->userLastModerationStatus($user->id)->forget();
            } catch (Throwable $exception) {
                DB::rollBack();

                throw $exception;
            }
        }

        $this->info(sprintf('%d profile(s) deleted!', $userIds->count()));
    }
}
