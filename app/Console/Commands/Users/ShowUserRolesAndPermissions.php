<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Console\Commands\Permissions\BaseRolesAndPermissionsCommand;

class ShowUserRolesAndPermissions extends BaseRolesAndPermissionsCommand
{
    protected $signature = 'users:show-roles-and-permissions';

    protected $description = 'List roles and permissions of a user';

    public function handle(): void
    {
        $user = $this->askUser();

        $this->showAssignedRoles($user);
        $this->showGivenPermissions($user);
    }
}
