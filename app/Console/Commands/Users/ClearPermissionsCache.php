<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Models\Language;
use DB;
use Illuminate\Console\Command;

class ClearPermissionsCache extends Command
{
    protected $signature = 'users:clear-permission-cache';

    protected $description = 'Clear user permission cache';

    public function handle(): void
    {
        $userIds = array_merge(
            DB::table('permissions_users')->pluck('user_id')->all(),
            DB::table('roles_users')->pluck('user_id')->all(),
        );

        $userIds = array_unique($userIds);

        $languageIds = (new Language())
            ->where('is_active', true)
            ->pluck('id')
            ->all()
        ;

        foreach ($userIds as $userId) {
            foreach ($languageIds as $languageId) {
                cache_info()->userPermissions($userId, $languageId)->forget();
            }
        }

        $this->info('Permissions cache cleared!');
    }
}
