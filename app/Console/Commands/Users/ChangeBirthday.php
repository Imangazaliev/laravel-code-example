<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ChangeBirthday extends Command
{
    protected $signature = 'users:change-birthday';

    protected $description = 'Change the birthday of a user';

    public function handle(): void
    {
        while (true) {
            $idOrPhoneNumber = $this->ask('Enter the ID or a phone number of a user');

            $user = (new User())
                ->with('roles')
                ->where('id', $idOrPhoneNumber)
                ->orWhere('phone_number', $idOrPhoneNumber)
                ->first()
            ;

            if ($user !== null) {
                break;
            }

            $this->error('User with the specified ID or phone number not found!');
        }

        $this->line('<info>ID:</info> ' . $user->id);
        $this->line('<info>Name:</info> ' . $user->name);
        $this->line('<info>Phone number:</info> ' . $user->phone_number);
        $this->line('<info>Birthday:</info> ' . $user->birthday->format(DATE_FORMAT));
        $this->line('');

        $newBirthday = $this->ask('Enter a new birthday');

        $user->update([
            'birthday' => Carbon::parse($newBirthday)->format(DATE_FORMAT),
        ]);

        cache_info()->userProfile($user->id)->forget();

        $this->info('Birthday changed!');
    }
}
