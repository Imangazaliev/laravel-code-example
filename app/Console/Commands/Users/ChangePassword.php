<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ChangePassword extends Command
{
    protected $signature = 'users:change-password';

    protected $description = 'Change the password of a user';

    public function handle(): void
    {
        while (true) {
            $idOrPhoneNumber = $this->ask('Enter the ID or a phone number of a user');

            $user = (new User())
                ->with('roles')
                ->where('id', $idOrPhoneNumber)
                ->orWhere('phone_number', $idOrPhoneNumber)
                ->first()
            ;

            if ($user !== null) {
                break;
            }

            $this->error('User with the specified ID or phone number not found');
        }

        $this->line('<info>ID:</info> ' . $user->id);
        $this->line('<info>Name:</info> ' . $user->name);
        $this->line('<info>Phone number:</info> ' . $user->phone_number);
        $this->line('');

        $newPassword = $this->ask('Enter a new password (keep empty to generate it automatically)');

        if ($newPassword === null) {
            $newPassword = str_random(8);

            $this->line('<info>New password:</info> ' . $newPassword);
            $this->line('');
        }

        $user->update([
            'password' => bcrypt($newPassword),
            'password_updated_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'remember_token' => str_random(60),
        ]);

        $this->info('Password changed!');
    }
}
