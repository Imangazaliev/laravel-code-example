<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Jobs\GenerateUserPhotoVariants;
use App\Models\UserPhoto;
use Illuminate\Console\Command;

class GeneratePhotoVariants extends Command
{
    protected $signature = 'users:generate-photo-variants';

    public function handle(): void
    {
        $query = (new UserPhoto())
            ->withTrashed()
            ->select('id')
            ->where('is_being_processed', true)
            ->orderBy('user_id')
        ;

        $progress = $this->output->createProgressBar($query->count());

        while (true) {
            $photos = $query->limit(50)->get();

            if ($photos->count() === 0) {
                break;
            }

            foreach ($photos as $photo) {
                dispatch(new GenerateUserPhotoVariants($photo->id))->onConnection('sync');

                $progress->advance();
            }
        }

        $progress->finish();

        $this->line('');
    }
}
