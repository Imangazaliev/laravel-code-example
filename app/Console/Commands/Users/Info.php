<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class Info extends Command
{
    protected $signature = 'users:info';

    protected $description = 'Show information about a user';

    public function handle(): void
    {
        while (true) {
            $idOrPhoneNumber = $this->ask('Enter the ID or a phone number of a user');

            $user = (new User())
                ->with('roles')
                ->where('id', $idOrPhoneNumber)
                ->orWhere('phone_number', $idOrPhoneNumber)
                ->first()
            ;

            if ($user !== null) {
                break;
            }

            $this->error('User with the specified ID or phone number not found');
        }

        $this->line('<info>ID:</info> ' . $user->id);
        $this->line('<info>Name:</info> ' . $user->name);
        $this->line('<info>Sex:</info> ' . ($user->sex === User::SEX_MALE ? 'Male' : 'Female'));
        $this->line('<info>Age:</info> ' . Carbon::today()->diffInYears($user->birthday));
        $this->line('<info>Phone number:</info> ' . $user->phone_number);
        $this->line('<info>Email:</info> ' . ($user->email ?? '(not specified)'));
        $this->line('<info>Registered at:</info> ' . $user->registered_at->format(DATE_TIME_FORMAT));
        $this->line('<info>Was online at at:</info> ' . $user->was_online_at->format(DATE_TIME_FORMAT));
        $this->line('<info>Status:</info> ' . $user->status);
        $this->line('<info>Coins:</info> ' . $user->coins);
    }
}
