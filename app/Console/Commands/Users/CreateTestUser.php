<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use RuntimeException;

class CreateTestUser extends Command
{
    protected $signature = 'users:create-test-user {count=1 : The count of users} {--password= : The password to set}';

    protected $description = 'Create a test user';

    public function handle(): void
    {
        if ( ! is_numeric($this->argument('count'))) {
            throw new RuntimeException('Count of user must be an integer');
        }

        $count = $this->argument('count');

        $defaultPassword = $this->option('password');

        for ($i = 0; $i < $count; $i++) {
            $password = $defaultPassword ?? str_random(8);
            $currentTime = Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT);

            $user = factory(User::class)->create([
                'password' => bcrypt($password),
                'password_updated_at' => $currentTime,
                'is_test_user' => true,
                'phone_number_verified_at' => $currentTime,
                'completed_registration_at' => $currentTime,
            ]);

            $sexMap = [
                User::SEX_MALE => 'Male',
                User::SEX_FEMALE => 'Female',
            ];

            // TODO: generate login link
            $this->info('User successfully created!');
            $this->line('');
            $this->info("ID: {$user->id}");
            $this->info("Name: {$user->name}");
            $this->info("Sex: {$sexMap[$user->sex]}");
            $this->info("Phone number: {$user->phone_number}");
            $this->info("Password: {$password}");

            // add a separator between users
            if ($i < $count - 1) {
                $this->line('');
                $this->line(str_repeat('=', 10));
                $this->line('');
            }
        }
    }
}
