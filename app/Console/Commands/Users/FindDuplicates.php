<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Models\User;
use App\Services\Profile\ProfileDuplicatesDetector;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FindDuplicates extends Command
{
    protected $signature = 'users:find-duplicates';

    protected $description = 'Find profile duplicates of a user';

    public function handle(): void
    {
        while (true) {
            $idOrPhoneNumber = $this->ask('Enter the ID or a phone number of a user');

            $user = (new User())
                ->with('roles')
                ->where('id', $idOrPhoneNumber)
                ->orWhere('phone_number', $idOrPhoneNumber)
                ->first()
            ;

            if ($user !== null) {
                break;
            }

            $this->error('User with the specified ID or phone number not found');
        }

        $duplicatesDetector = (new ProfileDuplicatesDetector());
        $profileDuplicates = $duplicatesDetector->getDuplicates($user->id);

        $titles = ['ID', 'Name', 'Sex', 'Age', 'Registered at', 'Was online at'];

        $this->info('By fingerprint and IP');


        $this->table(
            $titles,
            $this->getUsersData($profileDuplicates['fingerprint_and_ip']),
        );

        $this->info('By fingerprint');

        $this->table(
            $titles,
            $this->getUsersData($profileDuplicates['fingerprint']),
        );

        $this->info('By IP');

        $this->table(
            $titles,
            $this->getUsersData($profileDuplicates['ip']),
        );
    }

    private function getUsersData(array $userIds): array
    {
        return (new User())
            ->whereIn('id', $userIds)
            ->orderBy('id')
            ->get([
                'id',
                'name',
                'sex',
                'birthday',
                'registered_at',
                'was_online_at',
            ])
            ->map(function (User $user) {
                return [
                    $user->id,
                    $user->name,
                    $user->sex === User::SEX_MALE ? 'Male' : 'Female',
                    Carbon::today()->diffInYears($user->birthday),
                    $user->registered_at->format(DATE_TIME_FORMAT),
                    $user->was_online_at->format(DATE_TIME_FORMAT),
                ];
            })
            ->all()
        ;
    }
}
