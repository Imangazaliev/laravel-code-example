<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Models\User;
use App\Services\UserLoginUrlGenerator\UserLoginUrlGenerator;
use Illuminate\Console\Command;

class LoginUrl extends Command
{
    protected $signature = 'users:login-url';

    protected $description = 'Generate a login URL for a user';

    public function handle(): void
    {
        while (true) {
            $idOrPhoneNumber = $this->ask('Enter the ID or a phone number of a user');

            $user = (new User())
                ->with('roles')
                ->where('id', $idOrPhoneNumber)
                ->orWhere('phone_number', $idOrPhoneNumber)
                ->first(['id', 'status'])
            ;

            if ($user !== null) {
                break;
            }

            $this->error('User with the specified ID or phone number not found');
        }

        if (in_array($user->status, [
            User::STATUS_BANNED,
            User::STATUS_DELETED,
        ], true)) {
            $this->error('User banned or deleted!');

            return;
        }

        $userLoginUrlGenerator = new UserLoginUrlGenerator();

        $this->info($userLoginUrlGenerator->generateUrl($user->id, null));
    }
}
