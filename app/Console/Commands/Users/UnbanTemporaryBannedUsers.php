<?php

declare(strict_types=1);

namespace App\Console\Commands\Users;

use App\Models\UserBan;
use App\Services\BanUsers\BanUsers;
use App\Services\BanUsers\UserUnbanDto;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class UnbanTemporaryBannedUsers extends Command
{
    protected $signature = 'users:unban-temporary-banned-users';

    protected $description = 'Unban temporary banned users';

    private ?BanUsers $banUsers;

    public function handle(BanUsers $banUsers): void
    {
        $this->banUsers = $banUsers;

        while (true) {
            $bans = (new UserBan())
                ->whereNotNull('unban_at')
                ->where('unban_at', '<=', Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT))
                ->whereNull('unbanned_at')
                ->limit(100)
                ->get(['id', 'user_id'])
            ;

            if ($bans->count() === 0) {
                $this->info('Done!');

                break;
            }

            $this->unbanUsers($bans);

            $this->info(sprintf('%d user(s) unbanned!', $bans->count()));
        }
    }

    private function unbanUsers(Collection $bans): void
    {
        /**
         * @var UserBan $ban
         */
        foreach ($bans as $ban) {
            $userUnbanDto = (new UserUnbanDto())->setUserId($ban->user_id);

            $this->banUsers->unban($userUnbanDto);
        }
    }
}
