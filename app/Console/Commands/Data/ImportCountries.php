<?php

declare(strict_types=1);

namespace App\Console\Commands\Data;

use App\Models\Country;
use App\Models\CountryText;
use App\Models\Language;
use DB;
use Illuminate\Console\Command;

class ImportCountries extends Command
{
    protected $signature = 'import-countries {--dev : Import only a part of countries for development purposes}';

    protected $description = 'Import a list of countries from a JSON file to the database';

    public function handle(): void
    {
        DB::transaction(function () {
            DB::table('country_texts')->truncate();
            DB::table('countries')->truncate();

            $countriesFilePath = base_path('resources/data/countries.json');
            $countriesInfo = json_decode(file_get_contents($countriesFilePath), true, 512, JSON_THROW_ON_ERROR);

            $devCountryCodesFilePath = base_path('resources/data/dev-country-codes.json');
            $devCountryCodes = json_decode(file_get_contents($devCountryCodesFilePath), true, 512, JSON_THROW_ON_ERROR);

            $this->info('Importing countries to the database...');

            $countryCodeToIdMap = [];

            $progress = $this->output->createProgressBar(count($countriesInfo));

            foreach ($countriesInfo as $countryInfo) {
                if ($this->option('dev') && ! in_array($countryInfo['code_alpha2'], $devCountryCodes, true)) {
                    $progress->advance();

                    continue;
                }

                $country = Country::create([
                    'code_alpha2' => $countryInfo['code_alpha2'],
                    'code_alpha3' => $countryInfo['code_alpha3'],
                    'code_numeric' => $countryInfo['code_numeric'],
                    'native_name' => $countryInfo['native_name'],
                    'english_name' => $countryInfo['english_name'],
                    'continent_code' => $countryInfo['continent_code'],
                    'is_independent' => $countryInfo['is_independent'],
                    'show_in_list' => $countryInfo['show_in_list'],
                    'is_in_european_union' => $countryInfo['is_in_european_union'],
                    'currencies' => $countryInfo['currencies'],
                ]);

                $countryCodeToIdMap[$countryInfo['code_alpha2']] = $country->id;

                $progress->advance();
            }

            $progress->finish();

            $progress = $this->output->createProgressBar(count($countriesInfo));

            foreach ($countriesInfo as $countryInfo) {
                if ($this->option('dev') && ! in_array($countryInfo['code_alpha2'], $devCountryCodes, true)) {
                    $progress->advance();

                    continue;
                }

                if ($countryInfo['sovereign_state_country_code'] !== null) {
                    (new Country())->where('code_alpha2', $countryInfo['code_alpha2'])->update([
                        'sovereign_state_country_code' => $countryInfo['sovereign_state_country_code'],
                    ]);
                }

                $progress->advance();
            }

            $progress->finish();

            $this->line('');

            $this->info('Loading translations of countries...');

            $supportedLanguageCodes = json_decode(file_get_contents(base_path('resources/data/supported-language-codes.json')), true);
            $supportedLanguageCodes = array_flip($supportedLanguageCodes);

            $languageCodeToIdMap = (new Language())->pluck('id', 'code')->all();

            $progress = $this->output->createProgressBar(count($countriesInfo));


            foreach ($countriesInfo as $countryInfo) {
                if ($this->option('dev') && ! in_array($countryInfo['code_alpha2'], $devCountryCodes, true)) {
                    $progress->advance();

                    continue;
                }

                foreach ($countryInfo['translations'] as $languageCode => $countryNameTranslation) {
                    if ( ! array_key_exists($languageCode, $supportedLanguageCodes)) {
                        continue;
                    }

                    $countryId = $countryCodeToIdMap[$countryInfo['code_alpha2']];
                    $languageId = $languageCodeToIdMap[$languageCode];

                    CountryText::updateOrCreate([
                        'country_id' => $countryId,
                        'language_id' => $languageId,
                    ], [
                        'name' => $countryNameTranslation,
                    ]);
                }

                $progress->advance();
            }

            $progress->finish();

            $this->line('');
        });
    }
}
