<?php

declare(strict_types=1);

namespace App\Console\Commands\Data;

use App\Models\Country;
use App\Models\Language;
use App\Models\Locality;
use App\Models\LocalityText;
use App\Models\Subdivision;
use DB;
use Illuminate\Console\Command;

class ImportLocalities extends Command
{
    protected $signature = 'import-localities {--dev : Import only localities of a part of countries for development purposes}';

    protected $description = 'Import a list of localities from a JSON file to the database';

    public function handle(): void
    {
        DB::transaction(function () {
            DB::table('locality_texts')->truncate();
            DB::table('localities')->truncate();

            $localitiesFilePath = base_path('resources/data/localities.json');
            $localities = json_decode(file_get_contents($localitiesFilePath), true, 512, JSON_THROW_ON_ERROR);

            $devCountryCodesFilePath = base_path('resources/data/dev-country-codes.json');
            $devCountryCodes = json_decode(file_get_contents($devCountryCodesFilePath), true, 512, JSON_THROW_ON_ERROR);

            $this->info('Importing localities to the database...');

            $progress = $this->output->createProgressBar(count($localities));

            $countryCodeToIdMap = (new Country())->pluck('id', 'code_alpha2')->all();
            $subdivisionCodeToIdMap = (new Subdivision())
                ->get(['id', db_raw('CONCAT(country_code, \'.\', code) AS code')])
                ->pluck('id', 'code')
                ->all()
            ;

            $localityIndexToIdMap = [];

            foreach ($localities as $index => $localityInfo) {
                if ($this->option('dev') && ! in_array($localityInfo['country_code'], $devCountryCodes, true)) {
                    $progress->advance();

                    continue;
                }

                $countryCode = $localityInfo['country_code'];
                $subdivisionLevel1Code = $localityInfo['subdivision_level_1_code'];
                $subdivisionLevel2Code = $localityInfo['subdivision_level_2_code'];

                $locality = Locality::create([
                    'english_name' => $localityInfo['english_name'],
                    'country_id' => $countryCodeToIdMap[$countryCode],
                    'country_code' => $countryCode,
                    'subdivision_level_1_id' => $subdivisionLevel1Code === null ? null : $subdivisionCodeToIdMap["$countryCode.$subdivisionLevel1Code"],
                    'subdivision_level_1_code' => $subdivisionLevel1Code,
                    'subdivision_level_2_id' => $subdivisionLevel2Code === null ? null : $subdivisionCodeToIdMap["$countryCode.$subdivisionLevel2Code"],
                    'subdivision_level_2_code' => $subdivisionLevel2Code,
                    'geoname_id' => $localityInfo['geoname_id'],
                ]);

                $localityIndexToIdMap[$index] = $locality->id;

                $progress->advance();
            }

            $progress->finish();

            $this->line('');

            $this->info('Loading translations of localities...');

            $supportedLanguageCodes = json_decode(file_get_contents(base_path('resources/data/supported-language-codes.json')), true);
            $supportedLanguageCodes = array_flip($supportedLanguageCodes);

            $languageCodeToIdMap = (new Language())->pluck('id', 'code');

            $progress = $this->output->createProgressBar(count($localities));

            foreach ($localities as $index => $localityInfo) {
                if ($this->option('dev') && ! in_array($localityInfo['country_code'], $devCountryCodes, true)) {
                    $progress->advance();

                    continue;
                }

                foreach ($localityInfo['translations'] as $languageCode => $localityNameTranslation) {
                    if ( ! array_key_exists($languageCode, $supportedLanguageCodes)) {
                        continue;
                    }

                    $localityId = $localityIndexToIdMap[$index];
                    $languageId = $languageCodeToIdMap[$languageCode];

                    LocalityText::updateOrCreate([
                        'locality_id' => $localityId,
                        'language_id' => $languageId,
                    ], [
                        'name' => $localityNameTranslation,
                    ]);
                }

                $progress->advance();
            }

            $progress->finish();

            $this->line('');
        });
    }
}
