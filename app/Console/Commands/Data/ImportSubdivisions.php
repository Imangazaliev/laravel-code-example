<?php

declare(strict_types=1);

namespace App\Console\Commands\Data;

use App\Models\Country;
use App\Models\Language;
use App\Models\Subdivision;
use App\Models\SubdivisionText;
use DB;
use Illuminate\Console\Command;

class ImportSubdivisions extends Command
{
    protected $signature = 'import-subdivisions {--dev : Import only subdivisions of a part of countries for development purposes}';

    protected $description = 'Import a list of subdivisions from a JSON file to the database';

    public function handle(): void
    {
        DB::transaction(function () {
            DB::table('subdivision_texts')->truncate();
            DB::table('subdivisions')->truncate();

            $subdivisionsFilePath = base_path('resources/data/subdivisions.json');
            $subdivisionsByCountry = json_decode(file_get_contents($subdivisionsFilePath), true, 512, JSON_THROW_ON_ERROR);

            $countriesFilePath = base_path('resources/data/countries.json');
            $countries = json_decode(file_get_contents($countriesFilePath), true, 512, JSON_THROW_ON_ERROR);
            $countriesByCode = array_combine(array_column($countries, 'code_alpha2'), $countries);

            $devCountryCodesFilePath = base_path('resources/data/dev-country-codes.json');
            $devCountryCodes = json_decode(file_get_contents($devCountryCodesFilePath), true, 512, JSON_THROW_ON_ERROR);

            $this->info('Importing subdivisions to the database...');

            $countryCodeToIdMap = (new Country())
                ->pluck('id', 'code_alpha2')
                ->all()
            ;

            $subdivisionCodeToIdMap = [];
            $subdivisionsWithParent = [];

            $progress = $this->output->createProgressBar(count($subdivisionsByCountry));

            foreach ($subdivisionsByCountry as $countryCode => $countrySubdivisions) {
                if ($this->option('dev') && ! in_array($countryCode, $devCountryCodes, true)) {
                    $progress->advance();

                    continue;
                }

                foreach ($countrySubdivisions as $subdivisionCode => $subdivisionInfo) {
                    $subdivision = Subdivision::create([
                        'code' => $subdivisionCode,
                        'english_name' => $subdivisionInfo['translations']['en'],
                        'country_id' => $countryCodeToIdMap[$countryCode],
                        'country_code' => $countryCode,
                        'parent_subdivision_code' => $subdivisionInfo['parent_subdivision_code'],
                    ]);

                    $subdivisionCodeToIdMap["$countryCode.$subdivisionCode"] = $subdivision->id;

                    if ($subdivisionInfo['parent_subdivision_code'] !== null) {
                        $subdivisionsWithParent[$subdivision->id] = $countryCode . '.' . $subdivisionInfo['parent_subdivision_code'];
                    }
                }

                $progress->advance();
            }

            $progress->finish();

            foreach ($subdivisionsWithParent as $subdivisionId => $parentSubdivisionCode) {
                $parentSubdivisionId = $subdivisionCodeToIdMap[$parentSubdivisionCode];

                (new Subdivision())->where('id', $subdivisionId)->update([
                    'parent_subdivision_id' => $parentSubdivisionId,
                ]);

                (new Subdivision())->where('id', $parentSubdivisionId)->update([
                    'has_child_subdivisions' => true,
                ]);
            }

            $this->line('');

            $this->info('Loading translations of subdivisions...');

            $supportedLanguageCodes = json_decode(file_get_contents(base_path('resources/data/supported-language-codes.json')), true);
            $supportedLanguageCodes = array_flip($supportedLanguageCodes);

            $languageCodeToIdMap = (new Language())->pluck('id', 'code');

            $progress = $this->output->createProgressBar(count($subdivisionsByCountry));

            foreach ($subdivisionsByCountry as $countryCode => $countrySubdivisions) {
                if ($this->option('dev') && ! in_array($countryCode, $devCountryCodes, true)) {
                    $progress->advance();

                    continue;
                }

                if ( ! $countriesByCode[$countryCode]['is_independent']) {
                    continue;
                }

                foreach ($countrySubdivisions as $subdivisionCode => $subdivisionInfo) {
                    foreach ($subdivisionInfo['translations'] as $languageCode => $subdivisionNameTranslation) {
                        if ( ! array_key_exists($languageCode, $supportedLanguageCodes)) {
                            continue;
                        }

                        $subdivisionId = $subdivisionCodeToIdMap["$countryCode.$subdivisionCode"];
                        $languageId = $languageCodeToIdMap[$languageCode];

                        SubdivisionText::updateOrCreate([
                            'subdivision_id' => $subdivisionId,
                            'language_id' => $languageId,
                        ], [
                            'name' => $subdivisionNameTranslation,
                        ]);
                    }
                }

                $progress->advance();
            }

            $progress->finish();

            $this->line('');
        });
    }
}
