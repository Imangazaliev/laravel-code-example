<?php

declare(strict_types=1);

namespace App\Console\Commands\Data;

use App\Models\Language;
use App\Models\LanguageText;
use DB;
use Illuminate\Console\Command;

class ImportLanguages extends Command
{
    protected $signature = 'import-languages';

    protected $description = 'Import a list of languages from a JSON file to the database';

    public function handle(): void
    {
        $languagesFilePath = base_path('resources/data/languages.json');
        $languagesInfo = json_decode(file_get_contents($languagesFilePath), true, 512, JSON_THROW_ON_ERROR);

        DB::transaction(function () use ($languagesInfo) {
            DB::table('language_texts')->truncate();
            DB::table('languages')->truncate();

            $this->info('Importing languages to the database...');

            $activeLanguages = ['en', 'ru'];

            $progress = $this->output->createProgressBar(count($languagesInfo));

            $languageCodeToIdMap = [];

            foreach ($languagesInfo as $languageInfo) {
                $language = Language::create([
                    'code' => $languageInfo['code'],
                    'code_iso_639_1' => $languageInfo['code_iso_639_1'],
                    'code_iso_639_2b' => $languageInfo['code_iso_639_2b'],
                    'code_iso_639_2t' => $languageInfo['code_iso_639_2t'],
                    'code_iso_639_3' => $languageInfo['code_iso_639_3'],
                    'script_direction' => $languageInfo['script_direction'],
                    'native_name' => $languageInfo['native_name'],
                    'english_name' => $languageInfo['english_name'],
                    'is_active' => in_array($languageInfo['code'], $activeLanguages, true),
                ]);

                $languageCodeToIdMap[$languageInfo['code']] = $language->id;

                $progress->advance();
            }

            $progress->finish();

            $this->line('');

            $this->info('Loading translations of languages...');

            $supportedLanguageCodes = json_decode(file_get_contents(base_path('resources/data/supported-language-codes.json')), true);
            $supportedLanguageCodes = array_flip($supportedLanguageCodes);

            $progress = $this->output->createProgressBar(count($languagesInfo));

            foreach ($languagesInfo as $languageInfo) {
                foreach ($languageInfo['translations'] as $languageCode => $languageNameTranslation) {
                    if ( ! array_key_exists($languageCode, $supportedLanguageCodes)) {
                        continue;
                    }

                    $languageEntityId = $languageCodeToIdMap[$languageInfo['code']];
                    $languageId = $languageCodeToIdMap[$languageCode];

                    LanguageText::updateOrCreate([
                        'language_entity_id' => $languageEntityId,
                        'language_id' => $languageId,
                    ], [
                        'name' => $languageNameTranslation,
                    ]);
                }

                $progress->advance();
            }

            $progress->finish();

            $this->line('');
        });
    }
}
