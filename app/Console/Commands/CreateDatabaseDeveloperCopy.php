<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class CreateDatabaseDeveloperCopy extends Command
{
    protected $signature = 'create-database-developer-copy';

    protected $description = 'Create a developer copy of the database';

    public function handle(): void
    {
        if (app()->environment() === 'production') {
            $this->error('Can\'t create developer copy of the database in production.');

            return;
        }

        $connection = config('database.default');
        $dbConfig = config("database.connections.{$connection}");

        $dbName = $dbConfig['database'];

        $dbCopyName = "{$dbName}-dev";

        $this->info('Cleaning Telescope tables...');

        DB::statement('TRUNCATE telescope_entries RESTART IDENTITY CASCADE');

        $this->info('Creating the database copy...');

        DB::statement("DROP DATABASE IF EXISTS \"{$dbCopyName}\"");

        DB::statement("CREATE DATABASE \"{$dbCopyName}\" WITH TEMPLATE \"{$dbName}\" OWNER postgres");

        $connectionName = 'developer-copy-database';

        $connectionConfig = array_merge(array_only($dbConfig, [
            'driver',
            'host',
            'port',
            'username',
            'password',
            'schema',
        ]), [
            'database' => $dbCopyName,
        ]);

        config()->set(sprintf('database.connections.%s', $connectionName), $connectionConfig);

        /**
         * @var \Illuminate\Database\Connection $connection
         */
        $connection = DB::connection($connectionName);

        $this->info('Removing sensitive data...');

        $connection->table('connected_accounts')->delete();
        $connection->table('email_change_requests')->delete();
        $connection->table('email_verification_tokens')->delete();
        $connection->table('failed_jobs')->delete();
        $connection->table('payment_gateway_notifications')->delete();
        $connection->table('phone_number_change_requests')->delete();
        $connection->table('phone_number_verification_codes')->delete();
        $connection->table('user_login_tokens')->delete();
        $connection->table('user_profile_revisions')->delete();

        $connection->table('users')->update([
            'phone_number' => DB::raw("CONCAT('+7999', LPAD(id::text, 7, '0'))"),
            'phone_number_country_code' => 'RU',
            'phone_number_in_national_format' => DB::raw("CONCAT('8999', LPAD(id::text, 7, '0'))"),
            'email' => null,
            'email_verified_at' => null,
            'password ' => bcrypt('123456'),
            'remember_token' => null,
        ]);

        $connection->table('user_photos')->update([
            'original_file_name' => DB::raw("CONCAT(id, '.jpg')"),
            'original_file_name_transliterated' => DB::raw("CONCAT(id, '.jpg')"),
            'original_image_file_name' => DB::raw("CONCAT(id, '.jpg')"),
            'original_image_path' => DB::raw("CONCAT(id, '.jpg')"),
            'file_name' => DB::raw('blurred_image_file_name'),
            'path' => DB::raw('blurred_image_path'),
            'thumbnails' => null,
        ]);

        $connection->table('user_deletions')->update([
            'phone_number' => DB::raw("CONCAT('+7495', LPAD(user_id::text, 7, '0'))"),
            'email' => null,
        ]);

        // TODO: clean chat_messages, invoices and support chat messages tables

        $this->info('Running vacuum...');

        $connection->statement('VACUUM(FULL)');

        $this->info('Creating a dump...');

        $dumpFilePath = sprintf('/tmp/%s-%s.dump', $dbCopyName, Carbon::now()->format('Y-m-d_H-i-s'));

        $command = sprintf(
            'PGPASSWORD=%s pg_dump --clean --host=%s --port=%s --username=%s --format=c %s --file %s',
            escapeshellarg($dbConfig['password']),
            escapeshellarg($dbConfig['host']),
            escapeshellarg($dbConfig['port']),
            escapeshellarg($dbConfig['username']),
            escapeshellarg($dbCopyName),
            escapeshellarg($dumpFilePath),
        );

        shell_exec($command);

        $this->info("Dump created at {$dumpFilePath}!");
    }
}
