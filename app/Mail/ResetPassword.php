<?php

declare(strict_types=1);

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private string $userName;

    private string $email;

    private string $verificationToken;

    public function __construct(string $userName, string $email, string $verificationToken)
    {
        $this->userName = $userName;
        $this->email = $email;
        $this->verificationToken = $verificationToken;
    }

    public function build()
    {
        return $this
            ->to($this->email)
            ->subject('Password reset')
            ->markdown('emails.auth.reset-password', [
                'userName' => $this->userName,
                'token' => $this->verificationToken,
            ])
        ;
    }
}
