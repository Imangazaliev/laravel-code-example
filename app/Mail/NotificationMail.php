<?php

declare(strict_types=1);

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificationMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private string $title;

    private string $body;

    private string $userName;

    public function __construct(int $userId, string $title, string $body)
    {
        $this->title = $title;
        $this->body = $body;

        $user = (new User())->findOrFail($userId, ['name']);

        $this->userName = $user->name;
    }

    public function build(): self
    {
        return $this
            ->subject($this->title)
            ->markdown('emails.notification', [
                'userName' => $this->userName,
                'body' => $this->body,
            ])
        ;
    }
}
