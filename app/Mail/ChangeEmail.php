<?php

declare(strict_types=1);

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ChangeEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private string $userName;

    private string $verificationCode;

    private string $newEmail;

    public function __construct(string $userName, string $verificationCode, string $newEmail)
    {
        $this->userName = $userName;
        $this->verificationCode = $verificationCode;
        $this->newEmail = $newEmail;
    }

    public function build()
    {
        return $this
            ->to($this->newEmail)
            ->subject('Email change')
            ->markdown('emails.auth.change-email', [
                'userName' => $this->userName,
                'code' => $this->verificationCode,
                'newEmail' => $this->newEmail,
            ])
        ;
    }
}
