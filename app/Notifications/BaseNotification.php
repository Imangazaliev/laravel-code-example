<?php

declare(strict_types=1);

namespace App\Notifications;

use App\Mail\NotificationMail;
use App\Models\User;
use App\Services\ConnectedAccounts\ConnectedAccounts;
use App\Services\NotificationDelivery\NotificationDelivery;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification as LaravelNotification;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

abstract class BaseNotification extends LaravelNotification implements ShouldQueue
{
    use Queueable;

    public function via($notifiable): array
    {
        $connectedAccounts = app(ConnectedAccounts::class);

        $user = (new User())
            ->where('id', $notifiable->id)
            ->firstOrFail(['email_verified_at', 'notification_settings'])
        ;

        $settings = $user->notificationSettings();

        $channels = [];

        if (
            $this->useChannel(NotificationDelivery::CHANNEL_EMAIL) &&
            $settings->isChannelEnabled(NotificationDelivery::CHANNEL_EMAIL) &&
            $user->isEmailVerified()
        ) {
            $channels[] = 'mail';
        }

        if (
            $this->useChannel(NotificationDelivery::CHANNEL_TELEGRAM) &&
            $settings->isChannelEnabled(NotificationDelivery::CHANNEL_TELEGRAM) &&
            $connectedAccounts->isConnected($notifiable->id, 'telegram')
        ) {
            $channels[] = TelegramChannel::class;
        }

        return $channels;
    }

    public function useChannel(string $channel): bool
    {
        $channels = $this->getChannels();

        if (count($channels) === 0) {
            return true;
        }

        return in_array($channel, $channels);
    }

    public function toMail($notifiable): NotificationMail
    {
        return (new NotificationMail($notifiable->id, $this->getTitle(), $this->getBody()))
            ->to($notifiable->email, $notifiable->name)
        ;
    }

    public function toTelegram($notifiable): ?TelegramMessage
    {
        $connectedAccounts = app(ConnectedAccounts::class);

        $account = $connectedAccounts->getAccount($notifiable->id, 'telegram');

        if ($account === null) {
            return null;
        }

        return TelegramMessage::create()
            ->to($account->provider_user_id)
            ->content($this->getBody())
        ;
    }

    protected function getChannels(): array
    {
        return [];
    }

    abstract protected function getTitle(): string;

    abstract protected function getBody(): string;
}
