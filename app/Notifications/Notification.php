<?php

declare(strict_types=1);

namespace App\Notifications;

class Notification extends BaseNotification
{
    private string $title;

    private string $body;

    private array $parameters;

    private ?string $context;

    private array $channels;

    public function __construct(string $title, string $body, array $parameters = [], ?string $context = null, array $channels = [])
    {
        $this->title = $title;
        $this->body = $body;
        $this->parameters = $parameters;
        $this->context = $context;
        $this->channels = $channels;
    }

    protected function getTitle(): string
    {
        return __($this->title, $this->parameters, [
            'context' => $this->context,
        ]);
    }

    protected function getBody(): string
    {
        return __($this->body, $this->parameters, [
            'context' => $this->context,
        ]);
    }

    protected function getChannels(): array
    {
        return $this->channels;
    }
}
