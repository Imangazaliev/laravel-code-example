<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\MessageStatusChanged;
use App\Jobs\CheckChatMessage;
use App\Models\ChatMessage;
use App\Services\Chat\ChatsService;

class HandleMessageStatusChanged
{
    public function handle(MessageStatusChanged $event): void
    {
        $message = (new ChatMessage())
            ->with('chat:id,last_activity_at')
            ->findOrFail($event->getMessageId(), [
                'id',
                'chat_id',
                'user_id',
                'peer_id',
                'status',
            ])
        ;
        $chat = $message->chat;

        $time = $event->getTime();

        ChatsService::updateChatLastActivityTime($chat->id, $chat->last_activity_at, $time);

        if ($message->status === ChatMessage::STATUS_ACCEPTED) {
            dispatch(new CheckChatMessage($message->id));
        }

        if (
            $message->status === ChatMessage::STATUS_ACCEPTED ||
            $message->status === ChatMessage::STATUS_REJECTED_BY_MODERATOR
        ) {
            cache_info()->newMessageModerationCount($message->user_id)->forget();
            cache_info()->newMessageCount($message->peer_id)->forget();
        }
    }
}
