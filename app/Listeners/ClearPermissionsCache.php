<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Models\Language;
use ExampleProject\Permissions\Events\PermissionsUpdated;

class ClearPermissionsCache
{
    public function handle(PermissionsUpdated $event): void
    {
        $languageIds = $event->getLanguageId() !== null ? [$event->getLanguageId()] : (new Language())
            ->where('is_active', true)
            ->pluck('id')
            ->all()
        ;

        foreach ($languageIds as $languageId) {
            cache_info()->userPermissions($event->getUserId(), $languageId)->forget();
        }
    }
}
