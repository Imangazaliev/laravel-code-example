<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\PhotoStatusChanged;
use App\Models\UserPhoto;

class SetMainPhoto
{
    public function handle(PhotoStatusChanged $event): void
    {
        $photo = (new UserPhoto())
            ->with('user:id,main_photo_id')
            ->withTrashed()
            ->findOrFail($event->getPhotoId(), ['id', 'user_id', 'status'])
        ;

        if (
            $photo->status === UserPhoto::STATUS_ACCEPTED &&
            $photo->user->main_photo_id === null
        ) {
            $photo->user->update([
                'main_photo_id' => $photo->id,
            ]);
        }

        if (
            $photo->status === UserPhoto::STATUS_REJECTED &&
            $photo->user->main_photo_id === $photo->id
        ) {
            $photo->user->update([
                'main_photo_id' => null,
            ]);
        }

        if (
            $photo->status === UserPhoto::STATUS_DELETED &&
            $photo->user->main_photo_id === $photo->id
        ) {
            $lastPhotoId = $photo
                ->user
                ->photos()
                ->where('status', UserPhoto::STATUS_ACCEPTED)
                ->where('is_being_processed', false)
                ->orderBy('uploaded_at', 'desc')
                ->value('id')
            ;

            $photo->user->update([
                'main_photo_id' => $lastPhotoId,
            ]);
        }

        cache_info()->mainPhotoUrl($photo->user_id)->forget();
        cache_info()->userProfile($photo->user_id)->forget();
        cache_info()->userPhotos($photo->user_id)->forget();
        cache_info()->newPhotoModerationCount($photo->user_id)->forget();
    }
}
