<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\ChatFinished;
use App\Models\Chat;
use App\Models\CoinTransaction;
use App\Models\ContactsRequest;
use App\Services\Chat\ContactsRequestService;
use App\Services\Coins\Coins;
use App\Services\Coins\CoinTransactionDto;
use App\Services\PhotoAccessGate\PhotoAccessGate;

class HandleChatFinished
{
    private Coins $coins;

    private ContactsRequestService $contactsRequestService;

    private PhotoAccessGate $photoAccessGate;

    public function __construct(
        Coins                  $coins,
        ContactsRequestService $contactsRequestService,
        PhotoAccessGate        $photoAccessGate,
    ) {
        $this->coins = $coins;
        $this->contactsRequestService = $contactsRequestService;
        $this->photoAccessGate = $photoAccessGate;
    }

    public function handle(ChatFinished $event): void
    {
        $chatId = $event->getChatId();

        $this->checkChatFinishedTooFast($chatId);
        $this->updatePhotoAccessCache($chatId);
        $this->cancelActiveContactsRequest($chatId);

        $chat = (new Chat())->findOrFail($chatId, [
            'requestor_id',
            'target_user_id',
        ]);

        cache_info()->usersChatId($chat->requestor_id, $chat->target_user_id)->forget();
        cache_info()->chatIds($chat->requestor_id)->forget();
        cache_info()->chatIds($chat->target_user_id)->forget();
        cache_info()->newMessageCount($chat->requestor_id)->forget();
        cache_info()->newMessageCount($chat->target_user_id)->forget();
    }

    private function checkChatFinishedTooFast(int $chatId): void
    {
        $chat = (new Chat())
            ->with('chatRequest:id,chat_id,transaction_id')
            ->with('chatRequest.transaction:id,amount')
            ->findOrFail($chatId, [
                'id',
                'chat_request_id',
                'requestor_id',
                'target_user_id',
            ])
        ;

        $messageCount = $chat->messages()->count();

        if ($chat->target_user_id === current_user()->id && $messageCount < 10) {
            $chatRequest = $chat->chatRequest;

            $returnCoinsAmount = abs($chatRequest->transaction->amount) - config('chats.chat_finish_without_messages_commission');

            $coinTransactionDto = (new CoinTransactionDto())
                ->setType(CoinTransaction::TYPE_CHAT_FINISHED_TOO_FAST)
                ->setAmount($returnCoinsAmount)
                ->setUserId($chatRequest->requestor_id)
                ->setDescription(sprintf('Chat #%d finished without messages', $chat->id))
            ;

            $transaction = $this->coins->changeBalance($coinTransactionDto);

            $chatRequest->update([
                'return_transaction_id' => $transaction->id,
            ]);
        }
    }

    public function updatePhotoAccessCache(int $chatId): void
    {
        $chat = (new Chat())->findOrFail($chatId, [
            'id',
            'requestor_id',
            'target_user_id',
        ]);

        $this->photoAccessGate->checkAccess($chat->requestor_id, $chat->target_user_id);
        $this->photoAccessGate->checkAccess($chat->target_user_id, $chat->requestor_id);
    }
}
