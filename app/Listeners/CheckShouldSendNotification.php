<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Models\User;
use App\Services\NotificationDelivery\NotificationDelivery;
use Illuminate\Notifications\Events\NotificationSending;

class CheckShouldSendNotification
{
    public function handle(NotificationSending $event): bool
    {
        /**
         * @var User $user
         */
        $user = $event->notifiable;

        $settings = $user->notificationSettings();

        if (
            $event->channel === 'mail' &&
            ! (
                $settings->isChannelEnabled(NotificationDelivery::CHANNEL_EMAIL) &&
                $user->isEmailVerified()
            )
        ) {
            return false;
        }

        return true;
    }
}
