<?php

declare(strict_types=1);

namespace App\Services\UserPhotoUploader;

use Ramsey\Uuid\Uuid;

class UploadPathGenerator
{
    public static function generateUploadPath(): string
    {
        $fileName = self::generateUploadName();

        $directory1 = substr($fileName, 0, 2);
        $directory2 = substr($fileName, 2, 2);

        return sprintf('%s/%s/%s.jpg', $directory1, $directory2, $fileName);
    }

    public static function generateUploadName(): string
    {
        return hash('sha256', Uuid::uuid4()->toString());
    }
}
