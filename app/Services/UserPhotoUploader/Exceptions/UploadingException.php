<?php

declare(strict_types=1);

namespace App\Services\UserPhotoUploader\Exceptions;

use Exception;

class UploadingException extends Exception
{
    //
}
