<?php

declare(strict_types=1);

namespace App\Services\UserPhotoUploader;

use App\Models\UserPhoto;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface UploaderInterface
{
    public function putUploadedPhoto(
        string $storageType,
        UploadedFile $uploadedPhoto,
        string $caption,
        int $userId,
//        ?array $cropArea,
//        array $previewArea,
//        int $rotationAngle
    ): UserPhoto;

    public function getPhotoUrl(string $storageType, string $path): string;
}
