<?php

declare(strict_types=1);

namespace App\Services\UserPhotoUploader;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use ExampleProject\FileStorage\StorageFactory;
use ExampleProject\FileStorage\StorageFactoryInterface;

class ServiceProvider extends LaravelServiceProvider
{
    public function boot(): void
    {
        //
    }

    public function register(): void
    {
        $this->app->singleton(StorageFactoryInterface::class, StorageFactory::class);
        $this->app->singleton(UploaderInterface::class, UserPhotoUploader::class);
    }
}
