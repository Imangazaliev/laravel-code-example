<?php

declare(strict_types=1);

namespace App\Services\UserPhotoUploader;

use App\Models\UserPhoto;
use App\Services\UserPhotoUploader\Exceptions\UploadingException;
use Carbon\Carbon;
use Imagick;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserPhotoUploader implements UploaderInterface
{
    private StorageFactoryInterface $storageAdapterFactory;

    private array $storageAdapters = [];

    public function __construct(StorageFactoryInterface $storageAdapterFactory)
    {
        $this->storageAdapterFactory = $storageAdapterFactory;
    }

    public function putUploadedPhoto(
        string $storageType,
        UploadedFile $uploadedPhoto,
        ?string $caption,
        int $userId,
//        ?array $cropArea,
//        array $previewArea,
//        int $rotationAngle
    ): UserPhoto {
        if ( ! $uploadedPhoto->isValid()) {
            throw new UploadingException('Uploaded file is invalid');
        }

        $storage = $this->getStorage($storageType);

        $uploadPath = UploadPathGenerator::generateUploadPath();

        $storage->putFile($uploadedPhoto->getRealPath(), $uploadPath);

        return $this->saveInformationToDatabase(
            $storageType,
            $uploadedPhoto,
            $uploadPath,
            $caption,
            $userId,
//            $cropArea,
//            $previewArea,
//            $rotationAngle,
        );
    }

    private function saveInformationToDatabase(
        string $storageType,
        UploadedFile $uploadedPhoto,
        string $uploadPath,
        ?string $caption,
        int $userId,
//        ?array $cropArea,
//        array $previewArea,
//        int $rotationAngle
    ) {
        $filePath = $uploadedPhoto->getRealPath();

        $image = new Imagick($filePath);

        $exifData = $this->getExifData($image);

        $currentTime = Carbon::now();

        return (new UserPhoto())->create([
            'user_id' => $userId,
            'original_file_name' => $uploadedPhoto->getClientOriginalName(),
            'original_file_name_transliterated' => transliterate($uploadedPhoto->getClientOriginalName()),
            'width' => $image->getImageWidth(),
            'height' => $image->getImageHeight(),
            'size' => $uploadedPhoto->getSize(),
            'exif_data' => count($exifData) === 0 ? null : $exifData,
            'sha256_hash' => hash_file('sha256', $uploadedPhoto->getPathname()),
            'caption' => $caption,
//            'crop_area' => $cropArea,
//            'preview_area' => $previewArea,
//            'rotation_angle' => $rotationAngle,
            'storage_type' => $storageType,
            'original_image_file_name' => basename($uploadPath),
            'original_image_path' => $uploadPath,
            'uploaded_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'is_being_processed' => true,
            'status' => UserPhoto::STATUS_PENDING_MODERATION,
            'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
        ]);
    }

    private function getExifData(Imagick $image): array
    {
        $imageProperties = $image->getImageProperties('exif:*');

        $metaData = [];

        // drop "exif:" prefix (namespace)
        foreach ($imageProperties as $name => $value) {
            [, $name] = explode(':', $name);

            $metaData[$name] = $value;
        }

        return $metaData;
    }

    private function getStorage(string $storageType): StorageInterface
    {
        if ( ! array_key_exists($storageType, $this->storageAdapters)) {
            $this->storageAdapters[$storageType] = $this->storageAdapterFactory->make($storageType);
        }

        return $this->storageAdapters[$storageType];
    }

    public function getPhotoUrl(string $storageType, string $path): string
    {
        $storage = $this->getStorage($storageType);

        return $storage->getFileUrl($path);
    }
}
