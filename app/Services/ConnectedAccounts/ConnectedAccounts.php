<?php

declare(strict_types=1);

namespace App\Services\ConnectedAccounts;

use App\Models\ConnectedAccount;
use Carbon\Carbon;

class ConnectedAccounts
{
    public function getAccount(int $userId, string $provider): ?ConnectedAccount
    {
        return (new ConnectedAccount())
            ->where('user_id', $userId)
            ->where('provider_code', $provider)
            ->whereNull('disconnected_at')
            ->first()
        ;
    }

    public function connect(int $userId, string $provider, string $providerUserId, array $data): void
    {
        (new ConnectedAccount())->create([
            'user_id' => $userId,
            'provider_code' => $provider,
            'provider_user_id' => $providerUserId,
            'data' => $data,
            'added_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);
    }

    public function disconnect(int $userId, string $provider): void
    {
        $account = $this->getAccount($userId, $provider);

        if ($account === null) {
            return;
        }

        $account->update([
            'disconnected_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);
    }

    public function isConnected(int $userId, string $provider): bool
    {
        // TODO: cache
        return (new ConnectedAccount())
            ->where('user_id', $userId)
            ->where('provider_code', $provider)
            ->whereNull('disconnected_at')
            ->exists()
        ;
    }
}
