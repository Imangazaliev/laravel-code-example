<?php

declare(strict_types=1);

namespace App\Services\NotificationDelivery;

class NotificationDelivery
{
    public const CHANNEL_EMAIL = 'email';
    public const CHANNEL_TELEGRAM = 'telegram';
    public const TYPE_PROFILE_MODERATION = 'profile_moderation';
    public const TYPE_PHOTO_MODERATION = 'photo_moderation';
    public const TYPE_INCOMING_CHAT_REQUEST = 'incoming_chat_request';
    public const TYPE_CHAT_REQUEST_ANSWER = 'chat_request_answer';
    public const TYPE_NEW_MESSAGES = 'new_messages';
    public const TYPE_PROFILE_DUPLICATES = 'profile_duplicates';
    public const TYPE_SUSPICIOUS_MESSAGES = 'suspicious_messages';

    public static function getNotificationChannels(): array
    {
        return [
            self::CHANNEL_EMAIL,
            self::CHANNEL_TELEGRAM,
        ];
    }

    public static function getNotificationTypes(): array
    {
        return [
            self::TYPE_PROFILE_MODERATION,
            self::TYPE_PHOTO_MODERATION,
            self::TYPE_INCOMING_CHAT_REQUEST,
            self::TYPE_CHAT_REQUEST_ANSWER,
            self::TYPE_NEW_MESSAGES,
            self::TYPE_PROFILE_DUPLICATES,
            self::TYPE_SUSPICIOUS_MESSAGES,
        ];
    }
}
