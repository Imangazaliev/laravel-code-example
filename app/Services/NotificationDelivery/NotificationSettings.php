<?php

declare(strict_types=1);

namespace App\Services\NotificationDelivery;

class NotificationSettings
{
    private array $settings;

    public function __construct(array $settings)
    {
        $this->settings = array_merge(self::getDefaultSettings(), $settings);
    }

    public function getChannels(): array
    {
        return $this->settings['channels'];
    }

    public function setChannels(array $channels): self
    {
        $this->settings['channels'] = $channels;

        return $this;
    }

    public function setNotificationTypes(array $notificationTypes): self
    {
        $this->settings['notification_types'] = $notificationTypes;

        return $this;
    }

    public function isChannelEnabled(string $channel): bool
    {
        return in_array($channel, $this->settings['channels'], true);
    }

    public function isNotificationTypeEnabled(string $notificationTypes): bool
    {
        return in_array($notificationTypes, $this->settings['notification_types'], true);
    }

    public function toArray(): array
    {
        return$this->settings;
    }

    public static function getDefaultSettings(): array
    {
        return [
            'channels' => [],
            'notification_types' => NotificationDelivery::getNotificationTypes(),
        ];
    }
}
