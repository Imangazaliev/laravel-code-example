<?php

declare(strict_types=1);

namespace App\Services\UserAuthentications;

use App\Models\UserAuthentication;
use Carbon\Carbon;
use UAParser\Parser as UserAgentParser;

class UserAuthentications
{
    public static function logAuthentication(
        ?string $userAgent,
        ?string $fingerPrint,
        ?string $ipAddress,
        ?string $authKey,
        int $adminId = null,
    ): void {
        $platform = UserAuthentication::PLATFORM_UNKNOWN;
        $browser = null;
        $operatingSystem = null;
        $browserFingerprint = null;
        $mobileAppDeviceId = null;

        // TODO: detect Android and iOS applications
        if ($userAgent !== null) {
            $platform = UserAuthentication::PLATFORM_WEB;

            $userAgent = substr($userAgent, 0, 255);
            $uaParsed = UserAgentParser::create()->parse($userAgent);

            $browser = $uaParsed->ua->toString();
            $operatingSystem = $uaParsed->os->toString();

            $browserFingerprint = $fingerPrint;
        }

        $currentTime = Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT);

        (new UserAuthentication())
            ->create([
                'user_id' => current_user()->id,
                'admin_id' => $adminId,
                'platform' => $platform,
                'auth_key' => $authKey,
                'user_agent' => $userAgent,
                'browser' => $browser,
                'operating_system' => $operatingSystem,
                'browser_fingerprint' => $browserFingerprint,
                'mobile_app_device_id' => $mobileAppDeviceId,
                'logged_in_at' => $currentTime,
                'login_ip' => $ipAddress,
                'was_online_at' => $currentTime,
            ])
        ;
    }

    public static function logLogout(?string $authKey, ?string $ipAddress): void
    {
        $userAuthentication = (new UserAuthentication())
            ->where('auth_key', $authKey)
            ->first()
        ;

        if ($userAuthentication !== null) {
            $userAuthentication->update([
                'logged_out_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'logout_ip' => $ipAddress,
            ]);
        }
    }
}
