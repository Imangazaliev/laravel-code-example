<?php

declare(strict_types=1);

namespace App\Services\Api;

use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Throwable;

class ResponseFactory
{
    /**
     * @var string[]
     */
    private array $messages;

    public function __construct(array $messages)
    {
        $this->messages = $messages;
    }

    public function notFound(Throwable $exception): JsonResponse
    {
        $additionalData = [];

        if (config('app.debug')) {
            $additionalData['exception'] = $this->convertException($exception);
        }

        $errorMessage = $exception->getMessage() === '' ? null : $exception->getMessage();

        return $this->response(ApiErrors::NOT_FOUND, $errorMessage, $additionalData);
    }

    public function methodNotAllowed(): JsonResponse
    {
        return $this->response(ApiErrors::METHOD_NOT_ALLOWED);
    }

    public function invalidCsrfToken(): JsonResponse
    {
        return $this->response(ApiErrors::INVALID_CSRF_TOKEN);
    }

    public function unauthenticated(): JsonResponse
    {
        return $this->response(ApiErrors::UNAUTHENTICATED);
    }

    public function unauthorized(string $message = null): JsonResponse
    {
        return $this->response(ApiErrors::UNAUTHORIZED, $message);
    }

    public function profileBanned(): JsonResponse
    {
        return $this->response(ApiErrors::PROFILE_BANNED);
    }

    public function invalidRequest(Throwable $exception): JsonResponse
    {
        $additionalData = [];

        if (config('app.debug')) {
            $additionalData['exception'] = $this->convertException($exception);
        }

        $errorMessage = $exception->getMessage() === '' ? null : $exception->getMessage();

        return $this->response(ApiErrors::INVALID_REQUEST, $errorMessage, $additionalData);
    }

    public function forbidden(Throwable $exception): JsonResponse
    {
        $message = $exception === null || $exception->getMessage() === '' ? null : $exception->getMessage();

        $additionalData = [];

        if (config('app.debug')) {
            $additionalData['exception'] = $this->convertException($exception);
        }

        return $this->response(ApiErrors::FORBIDDEN, $message, $additionalData);
    }

    public function requestTooLarge(): JsonResponse
    {
        return $this->response(ApiErrors::REQUEST_ENTITY_TOO_LARGE);
    }

    public function tooManyRequests(?string $message = null): JsonResponse
    {
        return $this->response(ApiErrors::TOO_MANY_REQUESTS, $message);
    }

    public function invalidParameters(ValidationException $exception): JsonResponse
    {
        $additionalData = [
            'validation_errors' => $exception->errors(),
        ];

        if (config('app.debug')) {
            $additionalData['exception'] = $this->convertException($exception);
        }

        return $this->response(ApiErrors::INVALID_PARAMETERS, null, $additionalData);
    }

    public function internalServerError(Throwable $exception): JsonResponse
    {
        $additionalData = [];

        if (config('app.debug')) {
            $additionalData['exception'] = $this->convertException($exception);
        }

        $errorMessage = config('app.debug') ? $exception->getMessage() : null;

        return $this->response(ApiErrors::INTERNAL_SERVER_ERROR, $errorMessage, $additionalData);
    }

    public function insufficientCoinsBalanceException(): JsonResponse
    {
        return $this->response(ApiErrors::INSUFFICIENT_COINS_BALANCE);
    }

    private function response(string $code, ?string $message = null, array $additionalData = []): JsonResponse
    {
        $data = array_merge([
            'code' => $code,
            'message' => $message ?? $this->getMessage($code),
        ], $additionalData);

        return response()->json($data, ApiErrors::HTTP_CODES[$code]);
    }

    private function getMessage(string $code): string
    {
        $defaultMessage = ApiErrors::MESSAGES[$code];

        return $this->messages[$code] ?? $defaultMessage;
    }

    private function convertException(Throwable $exception): array
    {
        return[
            'type' => get_class($exception),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            // TODO: add backtrace formatter to make it more readable
            'backtrace' => $exception->getTrace(),
        ];
    }
}
