<?php

declare(strict_types=1);

namespace App\Services\Api;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->bind(ResponseFactory::class, function () {
            $messages = [
                ApiErrors::UNAUTHENTICATED => __('errors.unauthenticated'),
                ApiErrors::UNAUTHORIZED => __('errors.unauthorized'),
                ApiErrors::PROFILE_BANNED => __('errors.profile-banned'),
                ApiErrors::FORBIDDEN => __('errors.forbidden'),
                ApiErrors::TOO_MANY_REQUESTS => __('errors.too-many-requests'),
                ApiErrors::INTERNAL_SERVER_ERROR => __('errors.internal-server-error'),
                ApiErrors::INSUFFICIENT_COINS_BALANCE => __('errors.insufficient-coins-balance'),
            ];

            return new ResponseFactory($messages);
        });
    }

    public function provides(): array
    {
        return [ResponseFactory::class];
    }
}
