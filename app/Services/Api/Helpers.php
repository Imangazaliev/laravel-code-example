<?php

declare(strict_types=1);

namespace App\Services\Api;

trait Helpers
{
    protected function response(): ResponseFactory
    {
        return app(ResponseFactory::class);
    }
}
