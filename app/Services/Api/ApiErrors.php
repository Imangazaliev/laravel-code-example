<?php

declare(strict_types=1);

namespace App\Services\Api;

class ApiErrors
{
    public const NOT_FOUND = 'NOT_FOUND';
    public const METHOD_NOT_ALLOWED = 'METHOD_NOT_ALLOWED';
    public const INVALID_CSRF_TOKEN = 'INVALID_CSRF_TOKEN';
    public const UNAUTHENTICATED = 'UNAUTHENTICATED';
    public const UNAUTHORIZED = 'UNAUTHORIZED';
    public const PROFILE_BANNED = 'PROFILE_BANNED';
    public const INVALID_REQUEST = 'INVALID_REQUEST';
    public const FORBIDDEN = 'FORBIDDEN';
    public const TOO_MANY_REQUESTS = 'TOO_MANY_REQUESTS';
    public const REQUEST_ENTITY_TOO_LARGE = 'REQUEST_ENTITY_TOO_LARGE';
    public const INVALID_PARAMETERS = 'INVALID_PARAMETERS';
    public const INTERNAL_SERVER_ERROR = 'INTERNAL_SERVER_ERROR';
    public const INSUFFICIENT_COINS_BALANCE = 'INSUFFICIENT_BALANCE';
    public const HTTP_CODES = [
        self::NOT_FOUND => 404,
        self::METHOD_NOT_ALLOWED => 405,
        self::INVALID_CSRF_TOKEN => 400,
        self::UNAUTHENTICATED => 401,
        self::UNAUTHORIZED => 401,
        self::PROFILE_BANNED => 403,
        self::INVALID_REQUEST => 400,
        self::FORBIDDEN => 403,
        self::REQUEST_ENTITY_TOO_LARGE => 413,
        self::TOO_MANY_REQUESTS => 429,
        self::INVALID_PARAMETERS => 422,
        self::INTERNAL_SERVER_ERROR => 500,
        self::INSUFFICIENT_COINS_BALANCE => 400,
    ];
    public const MESSAGES = [
        self::NOT_FOUND => 'Not found.',
        self::METHOD_NOT_ALLOWED => 'Method not allowed.',
        self::INVALID_CSRF_TOKEN => 'Invalid CSRF token.',
        self::UNAUTHENTICATED => 'You are not logged in.',
        self::UNAUTHORIZED => 'You don\'t have the right permissions.',
        self::PROFILE_BANNED => 'Profile banned.',
        self::INVALID_REQUEST => 'Invalid request.',
        self::FORBIDDEN => 'Forbidden.',
        self::REQUEST_ENTITY_TOO_LARGE => 'Request entity too large.',
        self::TOO_MANY_REQUESTS => 'Too many requests.',
        self::INVALID_PARAMETERS => 'Some parameters are invalid.',
        self::INTERNAL_SERVER_ERROR => 'Internal server error.',
        self::INSUFFICIENT_COINS_BALANCE => 'Insufficient coins balance.',
    ];
}
