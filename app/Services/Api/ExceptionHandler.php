<?php

declare(strict_types=1);

namespace App\Services\Api;

use App\Exceptions\ProfileBannedException;
use App\Services\Coins\Exception\InsufficientCoinsBalanceException;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Debug\ExceptionHandler as ExceptionHandlerContract;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use ExampleProject\Permissions\Exceptions\UnauthorizedException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\HttpFoundation\Exception\SuspiciousOperationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class ExceptionHandler implements ExceptionHandlerContract
{
    use Helpers;

    /**
     * The container implementation.
     */
    protected Container $container;

    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected array $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        HttpResponseException::class,
        InsufficientCoinsBalanceException::class,
        ModelNotFoundException::class,
        ProfileBannedException::class,
        SuspiciousOperationException::class,
        TokenMismatchException::class,
        UnauthorizedException::class,
        ValidationException::class,
    ];

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function report(Throwable $exception): void
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        if ( ! $this->shouldReport($exception)) {
            return;
        }

        if (is_callable($reportCallable = [$exception, 'report'])) {
            $this->container->call($reportCallable);

            return;
        }

        try {
            $logger = $this->container->make(LoggerInterface::class);
        } catch (Exception $exception2) {
            throw $exception2;
        }

        $logger->error(
            $exception->getMessage(),
            array_merge(
                $this->context(),
                ['exception' => $exception],
            ),
        );
    }

    /**
     * Get the default context variables for logging.
     *
     * @return array
     */
    protected function context(): array
    {
        $currentUser = current_user();

        try {
            return array_filter([
                'userId' => $currentUser === null ? null : $currentUser->id,
            ]);
        } catch (Throwable $exception) {
            return [];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function shouldReport(Throwable $exception): bool
    {
        foreach ($this->dontReport as $excludedExceptionClass) {
            if ($exception instanceof $excludedExceptionClass) {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function render($request, Throwable $exception): Response
    {
        if ($exception instanceof NotFoundHttpException) {
            return $this->response()->notFound($exception);
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->response()->methodNotAllowed();
        }

        if ($exception instanceof TokenMismatchException) {
            return $this->response()->invalidCsrfToken();
        }

        if ($exception instanceof AuthenticationException) {
            return $this->response()->unauthenticated();
        }

        if ($exception instanceof AuthorizationException) {
            return $this->response()->unauthorized();
        }

        if ($exception instanceof UnauthorizedException) {
            return $this->response()->unauthorized($exception->getMessage());
        }

        if ($exception instanceof ProfileBannedException) {
            return $this->response()->profileBanned();
        }

        if ($exception instanceof HttpException && $exception->getStatusCode() === 400) {
            return $this->response()->invalidRequest($exception);
        }

        if ($exception instanceof HttpException && $exception->getStatusCode() === 403) {
            return $this->response()->forbidden($exception);
        }

        if ($exception instanceof ModelNotFoundException) {
            return $this->response()->notFound($exception);
        }

        if ($exception instanceof HttpException && $exception->getStatusCode() === 413) {
            return $this->response()->requestTooLarge();
        }

        if ($exception instanceof HttpException && $exception->getStatusCode() === 429) {
            $message = $exception->getMessage();

            if ($message === '') {
                $message = null;
            }

            return $this->response()->tooManyRequests($message);
        }

        if ($exception instanceof ValidationException) {
            return $this->response()->invalidParameters($exception);
        }

        if ($exception instanceof InsufficientCoinsBalanceException) {
            return $this->response()->insufficientCoinsBalanceException();
        }

        if ($exception instanceof SuspiciousOperationException) {
            return $this->response()->notFound($exception);
        }

        return $this->response()->internalServerError($exception);
    }

    /**
     * {@inheritdoc}
     */
    public function renderForConsole($output, Throwable $exception): void
    {
        (new ConsoleApplication())->renderThrowable($exception, $output);
    }
}
