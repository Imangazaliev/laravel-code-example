<?php

declare(strict_types=1);

namespace App\Services\OAuth;

class User
{
    /**
     * The unique identifier for the user.
     */
    private string $id;

    /**
     * The user's full name.
     */
    private string $name;

    /**
     * The user's e-mail address.
     */
    private ?string $email;

    public function __construct(string $id, string $name, ?string $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Get the full name of the user.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the e-mail address of the user.
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }
}
