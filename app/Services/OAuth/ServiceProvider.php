<?php

declare(strict_types=1);

namespace App\Services\OAuth;

use App\Services\OAuth\Providers\FacebookProvider;
use App\Services\OAuth\Providers\GoogleProvider;
use App\Services\OAuth\Providers\OdnoklassnikiProvider;
use App\Services\OAuth\Providers\VkProvider;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->singleton(OAuthManager::class, function ($app) {
            return $this->createOauthManager($app);
        });
    }

    private function createOauthManager($app): OAuthManager
    {
        $oauthManager = new OAuthManager($app);

        $oauthManager->registerProvider('facebook', function () use ($app, $oauthManager) {
            $config = $app['config']['services.facebook'];

            return $oauthManager->buildProvider(FacebookProvider::class, $config);
        });

        $oauthManager->registerProvider('google', function () use ($app, $oauthManager) {
            $config = $app['config']['services.google'];

            return $oauthManager->buildProvider(GoogleProvider::class, $config);
        });

        $oauthManager->registerProvider('odnoklassniki', function () use ($app, $oauthManager) {
            $config = $app['config']['services.odnoklassniki'];

            return $oauthManager->buildProvider(OdnoklassnikiProvider::class, $config);
        });

        $oauthManager->registerProvider('vkontakte', function () use ($app, $oauthManager) {
            $config = $app['config']['services.vkontakte'];

            return $oauthManager->buildProvider(VkProvider::class, $config);
        });

        return $oauthManager;
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [OAuthManager::class];
    }
}
