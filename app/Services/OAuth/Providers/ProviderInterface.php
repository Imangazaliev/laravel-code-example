<?php

declare(strict_types=1);

namespace App\Services\OAuth\Providers;

use App\Services\OAuth\Exceptions\OAuthException;
use App\Services\OAuth\User;

interface ProviderInterface
{
    /**
     * Get the URL of authentication page for the provider.
     *
     * @param string $redirectUrl
     *
     * @return string
     */
    public function getAuthUrl(string $redirectUrl): string;

    /**
     * Get the User instance for the authenticated user.
     *
     * @throws OAuthException
     *
     * @return User
     */
    public function getUser(): User;
}
