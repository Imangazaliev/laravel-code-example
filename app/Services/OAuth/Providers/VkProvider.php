<?php

declare(strict_types=1);

namespace App\Services\OAuth\Providers;

use App\Services\OAuth\Exceptions\OAuthException;
use App\Services\OAuth\User;
use Illuminate\Support\Arr;

class VkProvider extends AbstractProvider
{
    /**
     * The API version.
     *
     * @var string
     */
    private $version = '5.92';

    /**
     * The user fields being requested.
     *
     * @var array
     */
    private $fields = ['email'];

    /**
     * {@inheritdoc}
     */
    protected $scopes = ['email'];

    /**
     * {@inheritdoc}
     */
    public function getAuthUrlBase(): string
    {
        return 'https://oauth.vk.com/authorize';
    }

    /**
     * {@inheritdoc}
     */
    public function getUser(): User
    {
        $this->validateState();

        if ($this->getInput('error') !== null) {
            throw new OAuthException(sprintf('Authentication failed with error "%s"', $this->getInput('error')));
        }

        $response = $this->getAccessTokenResponse($this->getCode());

        $token = Arr::get($response, 'access_token');

        $user = $this->getUserByToken($token);

        $user['email'] = Arr::get($response, 'email');

        return $this->mapUserToObject($user);
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl(): string
    {
        return 'https://oauth.vk.com/access_token';
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenFields(string $code): array
    {
        return array_merge(parent::getTokenFields($code), [
            'grant_type' => 'authorization_code',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function getUserByToken(string $token): array
    {
        $params = http_build_query([
            'access_token' => $token,
            'fields' => implode(',', $this->fields),
            'v' => $this->version,
        ]);

        $response = $this->httpClient->get('https://api.vk.com/method/users.get?' . $params);

        $decodedResponse = json_decode($response->getBody()->getContents(), true);

        return $decodedResponse['response'][0];
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user): User
    {
        $name = Arr::get($user, 'first_name');

        if (array_key_exists('last_name', $user)) {
            $name .= ' ' . $user['last_name'];
        }

        return new User((string) $user['id'], $name, $user['email']);
    }
}
