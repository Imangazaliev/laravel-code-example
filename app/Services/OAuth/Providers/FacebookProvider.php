<?php

declare(strict_types=1);

namespace App\Services\OAuth\Providers;

use App\Services\OAuth\User;

class FacebookProvider extends AbstractProvider
{
    /**
     * The base Facebook Graph URL.
     *
     * @var string
     */
    private $graphUrl = 'https://graph.facebook.com';

    /**
     * The Graph API version for the request.
     *
     * @var string
     */
    private $version = 'v3.0';

    /**
     * The user fields being requested.
     *
     * @var array
     */
    private $fields = ['name', 'email'];

    /**
     * {@inheritdoc}
     */
    protected $scopes = ['email'];

    /**
     * {@inheritdoc}
     */
    public function getAuthUrlBase(): string
    {
        return 'https://www.facebook.com/' . $this->version . '/dialog/oauth';
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl(): string
    {
        return $this->graphUrl . '/' . $this->version . '/oauth/access_token';
    }

    /**
     * {@inheritdoc}
     */
    protected function getUserByToken(string $token): array
    {
        $meUrl = $this->graphUrl . '/'.$this->version . '/me?access_token=' . $token . '&fields=' . implode(',', $this->fields);

        $appSecretProof = hash_hmac('sha256', $token, $this->getClientSecret());
        $meUrl .= '&appsecret_proof=' . $appSecretProof;

        $response = $this->httpClient->get($meUrl, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user): User
    {
        $email = $user['email'] ?? null;

        return new User($user['id'], $user['name'], $email);
    }
}
