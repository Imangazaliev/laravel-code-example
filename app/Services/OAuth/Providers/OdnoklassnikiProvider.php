<?php

declare(strict_types=1);

namespace App\Services\OAuth\Providers;

use App\Services\OAuth\User;

class OdnoklassnikiProvider extends AbstractProvider
{
    /**
     * The user fields being requested.
     *
     * @var array
     */
    private $fields = ['name', 'has_email', 'email'];

    /**
     * {@inheritdoc}
     */
    protected $scopes = ['GET_EMAIL'];

    /**
     * {@inheritdoc}
     */
    public function getAuthUrlBase(): string
    {
        return 'https://connect.ok.ru/oauth/authorize';
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenUrl(): string
    {
        return 'https://api.odnoklassniki.ru/oauth/token.do';
    }

    /**
     * {@inheritdoc}
     */
    protected function getTokenFields(string $code): array
    {
        return array_merge(parent::getTokenFields($code), [
            'grant_type' => 'authorization_code',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function getUserByToken(string $token): array
    {
        $parameters = [
            'format' => 'json',
            'method' => 'users.getCurrentUser',
            'application_key' => $this->getConfig('application_key'),
            'fields' => implode(',', $this->fields),
        ];

        $parameters['sig'] = $this->calculateSignature($parameters, $token);

        $parameters['access_token'] = $token;

        $response = $this->httpClient->get(
            'https://api.ok.ru/fb.do?' . http_build_query($parameters),
        );

        return json_decode($response->getBody()->getContents(), true);
    }

    private function calculateSignature(array $parameters, string $accessToken): string
    {
        unset($parameters['access_token']);

        ksort($parameters, SORT_STRING);

        $parametersStr = '';

        foreach ($parameters as $name => $value) {
            $parametersStr .= "$name=$value";
        }

        $secretKey = md5($accessToken . $this->getClientSecret());

        $sig = md5($parametersStr . $secretKey);

        return strtolower($sig);
    }

    /**
     * {@inheritdoc}
     */
    protected function mapUserToObject(array $user): User
    {
        $email = $user['has_email'] ? $user['email'] : null;

        return new User($user['uid'], $user['name'], $email);
    }
}
