<?php

declare(strict_types=1);

namespace App\Services\OAuth\Providers;

use App\Services\OAuth\Exceptions\InvalidStateException;
use App\Services\OAuth\Exceptions\OAuthException;
use App\Services\OAuth\User;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\ClientInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

abstract class AbstractProvider implements ProviderInterface
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @var array
     */
    protected $config;

    /**
     * The scopes being requested.
     *
     * @var array
     */
    protected $scopes = [];

    /**
     * The separating character for the requested scopes.
     *
     * @var string
     */
    protected $scopeSeparator = ',';

    /**
     * The custom parameters to be sent with the request.
     *
     * @var array
     */
    protected $parameters = [];

    /**
     * Constructor.
     *
     * @param Request $request
     * @param array $config
     */
    public function __construct(Request $request, array $config)
    {
        $this->request = $request;
        $this->httpClient = new HttpClient();
        $this->config = $config;
    }

    /**
     * Get the base URL of authentication page.
     *
     * @return string
     */
    abstract protected function getAuthUrlBase(): string;

    /**
     * Get the token URL for the provider.
     *
     * @return string
     */
    abstract protected function getTokenUrl(): string;

    /**
     * Get the raw user for the given access token.
     *
     * @param string $token
     *
     * @return array
     */
    abstract protected function getUserByToken(string $token): array;

    /**
     * Map the raw user array to a Socialite User instance.
     *
     * @param array $user
     *
     * @return User
     */
    abstract protected function mapUserToObject(array $user): User;

    /**
     * {@inheritdoc}
     */
    public function getAuthUrl(string $redirectUrl): string
    {
        $state = $this->generateState();

        $this->request->session()->put('oauth_state', $state);
        $this->request->session()->put('oauth_redirect_url', $redirectUrl);

        return $this->getAuthUrlBase() . '?' . http_build_query($this->getCodeFields($redirectUrl, $state));
    }

    /**
     * Get the string used for session state.
     *
     * @return string
     */
    protected function generateState(): string
    {
        return Str::random(40);
    }

    /**
     * Get the GET parameters for the code request.
     *
     * @param string $redirectUrl
     * @param string|null $state
     *
     * @return array
     */
    protected function getCodeFields(string $redirectUrl, string $state): array
    {
        $fields = [
            'client_id' => $this->getClientId(),
            'redirect_uri' => $redirectUrl,
            'scope' => $this->formatScopes($this->getScopes(), $this->scopeSeparator),
            'response_type' => 'code',
            'state' => $state,
        ];

        return array_merge($fields, $this->parameters);
    }

    /**
     * Format the given scopes.
     *
     * @param array $scopes
     * @param string $scopeSeparator
     *
     * @return string
     */
    protected function formatScopes(array $scopes, $scopeSeparator): string
    {
        return implode($scopeSeparator, $scopes);
    }

    /**
     * {@inheritdoc}
     */
    public function getUser(): User
    {
        $this->validateState();

        if ($this->getInput('error') !== null) {
            throw new OAuthException(sprintf('Authentication failed with error "%s"', $this->getInput('error')));
        }

        $response = $this->getAccessTokenResponse($this->getCode());

        if (array_key_exists('error', $response)) {
            throw new OAuthException(sprintf('Authentication failed with error "%s"', $response['error']));
        }

        $token = Arr::get($response, 'access_token');

        return $this->mapUserToObject($this->getUserByToken($token));
    }

    protected function validateState(): void
    {
        // retrieve the state from the session and forget it
        $state = $this->request->session()->pull('oauth_state');

        if ($this->getInput('state') !== $state) {
            throw new InvalidStateException('The given state is invalid');
        }
    }

    /**
     * Get the code from the request.
     *
     * @return string
     */
    protected function getCode(): string
    {
        return $this->request->input('code');
    }

    /**
     * Get the access token response for the given code.
     *
     * @param string $code
     *
     * @return array
     */
    public function getAccessTokenResponse(string $code): array
    {
        $postKey = version_compare(ClientInterface::VERSION, '6') === 1 ? 'form_params' : 'body';

        $response = $this->httpClient->post($this->getTokenUrl(), [
            'headers' => ['Accept' => 'application/json'],
            $postKey => $this->getTokenFields($code),
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * Get the POST fields for the token request.
     *
     * @param string $code
     *
     * @return array
     */
    protected function getTokenFields(string $code): array
    {
        return [
            'client_id' => $this->getClientId(),
            'client_secret' => $this->getClientSecret(),
            'code' => $code,
            'redirect_uri' => $this->request->session()->get('oauth_redirect_url'),
        ];
    }

    protected function getClientId(): string
    {
        return $this->getConfig('client_id');
    }

    protected function getClientSecret(): string
    {
        return $this->getConfig('client_secret');
    }

    /**
     * @param string $key
     *
     * @return string|null
     */
    protected function getInput(string $key): ?string
    {
        return $this->request->input($key);
    }

    /**
     * @param string $key
     * @param mixed $default
     *
     * @return mixed
     */
    protected function getConfig(string $key, $default = null)
    {
        return Arr::get($this->config, $key, $default);
    }

    /**
     * Get the current scopes.
     *
     * @return array
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }
}
