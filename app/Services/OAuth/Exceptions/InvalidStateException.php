<?php

declare(strict_types=1);

namespace App\Services\OAuth\Exceptions;

class InvalidStateException extends OAuthException
{
    //
}
