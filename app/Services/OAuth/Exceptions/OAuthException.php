<?php

declare(strict_types=1);

namespace App\Services\OAuth\Exceptions;

use Exception;

class OAuthException extends Exception
{
    //
}
