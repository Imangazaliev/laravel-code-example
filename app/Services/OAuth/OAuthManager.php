<?php

declare(strict_types=1);

namespace App\Services\OAuth;

use App\Services\OAuth\Exceptions\UnknownProviderException;
use App\Services\OAuth\Providers\ProviderInterface;
use Closure;
use Illuminate\Foundation\Application;

class OAuthManager
{
    /**
     * The application instance.
     */
    protected Application $app;

    /**
     * The registered custom provider creators.
     */
    protected array $providerCreators = [];

    /**
     * The array of created provider.
     */
    protected array $providers = [];

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Check if a provider exists.
     *
     * @param string $providerName
     *
     * @return bool
     */
    public function hasProvider(string $providerName): bool
    {
        return array_key_exists($providerName, $this->providerCreators);
    }

    /**
     * Get a provider instance.
     *
     * @param string $providerName
     *
     * @return ProviderInterface
     */
    public function provider(string $providerName): ProviderInterface
    {
        if ( ! array_key_exists($providerName, $this->providers)) {
            $this->providers[$providerName] = $this->createProvider($providerName);
        }

        return $this->providers[$providerName];
    }

    /**
     * Create a new provider instance.
     *
     * @param string $providerName
     *
     * @throws UnknownProviderException
     *
     * @return ProviderInterface
     */
    protected function createProvider(string $providerName): ProviderInterface
    {
        // We'll check for a custom provider creator, which allows developers to create
        // providers using their own customized provider creator Closure to create it.
        // If not we will check to see if a creator method exists for the given provider.
        if (array_key_exists($providerName, $this->providerCreators)) {
            return $this->providerCreators[$providerName]($this->app);
        }

        throw new UnknownProviderException("Provider \"$providerName\" not supported.");
    }

    /**
     * Register a custom driver creator Closure.
     *
     * @param string $name
     * @param Closure $callback
     */
    public function registerProvider($name, Closure $callback): void
    {
        $this->providerCreators[$name] = $callback;
    }

    /**
     * Get all of the created providers.
     *
     * @return array
     */
    public function getProviders(): array
    {
        return $this->providers;
    }

    /**
     * Build an OAuth 2 provider instance.
     *
     * @param string $provider
     * @param array $config
     *
     * @return ProviderInterface
     */
    public function buildProvider($provider, $config): ProviderInterface
    {
        return new $provider($this->app['request'], $config);
    }
}
