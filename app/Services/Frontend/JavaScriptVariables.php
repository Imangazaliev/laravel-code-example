<?php

declare(strict_types=1);

namespace App\Services\Frontend;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Models\UserPhoto;
use App\Queries\UserLastStatusQuery;

class JavaScriptVariables
{
    public static function getVariables(): array
    {
        return [
            'siteName' => config('app.name'),
            'version' => app_version(),
            'debug' => config('app.debug'),
            'environment' => app()->environment(),
            'currentLanguage' => [
                'code' => language()->code,
                'englishName' => language()->english_name,
                'nativeName' => language()->native_name,
                'scriptDirection' => language()->script_direction,
            ],
            'siteUrl' => config('app.url'),
            'supportEmail' => config('app.support_email'),
            'user' => self::getUser(),
            'chatRules' => self::getChatRules(),
            'profileRules' => self::getProfileRules(),
            'supportChatRules' => self::getSupportChatRules(),
            'prices' => [
                'chat_request' => config('chats.chat_request_price'),
                'chat_request_acceptance' => config('chats.chat_request_acceptance_price'),
                'gift' => config('profile.gift_coins_amount'),
            ],
            'services' => [
                'googleAnalytics' => [
                    'enabled' => config('services.google_analytics.enabled'),
                ],
                'recaptcha' => [
                    'enabled' => config('services.google_recaptcha.enabled'),
                    'siteKey' => config('services.google_recaptcha.site_key'),
                ],
                'sentry' => [
                    'dsn' => config('sentry.dsn'),
                ],
                'tawk' => [
                    'enabled' => config('services.tawk.enabled'),
                    'chatId' => config('services.tawk.chat_id'),
                    'guestEmail' => config('services.tawk.guest_email'),
                ],
                'telegram' => [
                    'botLogin' => config('services.telegram.bot_login'),
                ],
                'yandexMetrika' => [
                    'enabled' => config('services.yandex_metrika.enabled'),
                ],
            ],
            'moderators_recruitment_alert_enabled' => config('app.moderators_recruitment_alert_enabled'),
        ];
    }

    private static function getProfileRules(): array
    {
        return [
            'minPasswordLength' => config('profile.minimum_password_length'),
            'minAge' => config('profile.min_age'),
            'maxAge' => config('profile.max_age'),
            'maxHeight' => config('profile.max_height'),
            'minWeight' => config('profile.min_weight'),
            'maxWeight' => config('profile.max_weight'),
            'photo' => [
                'maxCount' => config('profile.photo.max_count'),
                'maxFileSize' => config('profile.photo.max_file_size'),
                'maxCaptionLength' => config('profile.photo.max_caption_length'),
            ],
            'weightStep' => config('profile.weight_step'),
        ];
    }

    private static function getChatRules(): array
    {
        return [
            'maxMessageLength' => config('chats.max_message_length'),
        ];
    }

    private static function getSupportChatRules(): array
    {
        return [
            'maxMessageLength' => config('support-chats.max_message_length'),
        ];
    }

    private static function getUser(): ?array
    {
        $user = current_user();

        if ($user === null) {
            return null;
        }

        return self::convertUserData($user);
    }

    public static function convertUserData(User $user): array
    {
        $supportChatId = cache_info()->supportChatId(current_user()->id)->remember(function () {
            return (new SupportChat())
                ->where('user_id', current_user()->id)
                ->value('id')
            ;
        });

        return [
            'id' => $user->id,
            'name' => $user->name,
            'phone_number' => $user->phone_number,
            'phone_number_country_code' => $user->phone_number_country_code,
            'is_phone_number_verified' => $user->isPhoneNumberVerified(),
            'email' => $user->email,
            'is_email_verified' => $user->isEmailVerified(),
            'completed_registration' => $user->completed_registration_at !== null,
            'sex' => $user->sex,
            'birthday' => $user->birthday->format(DATE_FORMAT),
            'photo_url' => self::getPhotoUrl($user),
            'status' => $user->status,
            'last_status' => cache_info()->userLastModerationStatus(current_user()->id)->remember(function () {
                return UserLastStatusQuery::get(current_user()->id);
            }),
            'reputation' => $user->reputation,
            'is_verified' => $user->is_verified,
            'coins' => $user->coins,
            'profile_completion_percent' => $user->profile_completion_percent,
            'required_fields_filled' => $user->required_fields_filled,
            'permissions' => self::getPermissions($user),
            'support_chat_exists' => $supportChatId !== null,
        ];
    }

    private static function getPhotoUrl(User $user): ?string
    {
        return cache_info()->mainPhotoUrl($user->id)->remember(function () use ($user) {
            $mainPhoto = $user->mainPhoto()->first([
                'id',
                'user_id',
                'storage_type',
                'path',
                'thumbnails',
            ]);

            // put the URL to an array to save null values too
            return [
                'url' => $mainPhoto === null ? null : $mainPhoto->getImageUrl(false, UserPhoto::SIZE_300),
            ];
        })['url'];
    }

    private static function getPermissions(User $user): array
    {
        $userPermissionsCache = cache_info()->userPermissions($user->id, language()->id);

        if ($userPermissionsCache->exists()) {
            return $userPermissionsCache->get();
        }

        $rolePermissions = (new Role())
            ->with('permissions:permissions.id,permissions.code')
            ->whereIn('id', $user->getRoleIds(language()->id))
            ->get(['roles.id'])
            ->pluck('permissions')
        ;

        $rolePermissions = $rolePermissions->count() === 0 ? [] : $rolePermissions
            ->flatten()
            ->pluck('code')
            ->all()
        ;

        $directPermissions = (new Permission())
            ->whereIn('id', $user->getPermissionIds(language()->id))
            ->pluck('code')
            ->all()
        ;

        $permissions = array_unique([...$rolePermissions, ...$directPermissions]);

        sort($permissions);

        $userPermissionsCache->put($permissions);

        return $permissions;
    }
}
