<?php

declare(strict_types=1);

namespace App\Services\SessionKeys;

class SessionKeys
{
    public const CURRENT_LANGUAGE_CODE = 'language';
    public const AUTH_KEY = 'auth_key';
}
