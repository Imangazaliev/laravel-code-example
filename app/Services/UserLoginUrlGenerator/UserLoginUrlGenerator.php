<?php

declare(strict_types=1);

namespace App\Services\UserLoginUrlGenerator;

use App\Models\User;
use App\Models\UserLoginToken;
use Carbon\Carbon;

class UserLoginUrlGenerator
{
    public function generateUrl(int $userId, ?int $createdBy): string
    {
        $phoneNumber = (new User())->where('id', $userId)->value('phone_number');

        // remove plus sign
        $phoneNumber = substr($phoneNumber, 1);

        $token = $this->createToken($userId, $createdBy);

        return url('/login') . '?' . http_build_query([
            'phone_number' => $phoneNumber,
            'token' => $token->token,
        ]);
    }

    public function createToken(int $userId, ?int $createdBy): UserLoginToken
    {
        $tokenLifetime = config('admin.authentication_token_lifetime');

        $token = (new UserLoginToken())
            ->where('user_id', $userId)
            ->where('created_by', $createdBy)
            ->where('created_at', '>=', Carbon::now()->subHours($tokenLifetime)->format(DATE_TIME_WITH_TIME_ZONE_FORMAT))
            ->first()
        ;

        if ($token !== null) {
            return $token;
        }

        return (new UserLoginToken())->create([
            'user_id' => $userId,
            'token' => str_random(64),
            'created_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'created_by' => $createdBy,
        ]);
    }
}
