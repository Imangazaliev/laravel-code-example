<?php

declare(strict_types=1);

namespace App\Services\PhoneNumberVerification;

use App\Models\PhoneNumberVerificationCode;
use App\Models\User;
use App\Services\PhoneNumberVerification\Exception\VerificationCodeExpiredException;
use App\Services\PhoneNumberVerification\Exception\VerificationCodeNotFoundException;
use Carbon\Carbon;

class PhoneNumberVerification
{
    public function generateCode(): string
    {
        $code = '';
        $codeLength = config('auth.phone_number_verification.code_length');

        for ($i = 0; $i < $codeLength; $i++) {
            $code .= random_int(0, 9);
        }

        return $code;
    }

    public function createCode(string $phoneNumber): PhoneNumberVerificationCode
    {
        return PhoneNumberVerificationCode::create([
            'phone_number' => $phoneNumber,
            'code' => $this->generateCode(),
            'created_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);
    }

    public function isCodeExpired(PhoneNumberVerificationCode $verificationCode): bool
    {
        return Carbon::now()->diffInMinutes($verificationCode->created_at) > config('auth.phone_number_verification.code_lifetime');
    }

    /**
     * @param string $phoneNumber
     * @param string $code
     * @param bool $delete
     *
     * @throws VerificationCodeExpiredException
     * @throws VerificationCodeNotFoundException
     */
    public function checkVerificationCode(string $phoneNumber, string $code, bool $delete): void
    {
        $verificationCode = (new PhoneNumberVerificationCode())
            ->where('phone_number', $phoneNumber)
            ->where('code', $code)
            ->orderBy('created_at', 'desc')
            ->first()
        ;

        if ($verificationCode === null) {
            throw new VerificationCodeNotFoundException('Verification code not found');
        }

        if ($this->isCodeExpired($verificationCode)) {
            throw new VerificationCodeExpiredException('Verification code expired');
        }

        if ($delete) {
            $verificationCode->delete();
        }
    }

    /**
     * @param User $user
     * @param string $code
     *
     * @throws VerificationCodeExpiredException
     * @throws VerificationCodeNotFoundException
     */
    public function confirmPhoneNumber(User $user, string $code): void
    {
        $this->checkVerificationCode($user->phone_number, $code, true);

        $user->update([
            'phone_number_verified_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);
    }
}
