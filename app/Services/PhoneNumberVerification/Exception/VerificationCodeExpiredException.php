<?php

declare(strict_types=1);

namespace App\Services\PhoneNumberVerification\Exception;

use Exception;

class VerificationCodeExpiredException extends Exception
{
    //
}
