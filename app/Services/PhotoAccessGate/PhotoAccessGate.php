<?php

declare(strict_types=1);

namespace App\Services\PhotoAccessGate;

use App\Models\Chat;
use App\Models\ChatRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Redis;

class PhotoAccessGate
{
    private const LIFETIME = 60 * 60 * 24 * 3;

    public function canViewPhotos(int $userId, int $photoOwnerId): bool
    {
        if ($photoOwnerId === $userId) {
            return true;
        }

        $key = $this->getKey($userId);

        $value = Redis::hget($key, $photoOwnerId);

        if ($value !== false) {
            return (int) $value === 1;
        }

        return $this->checkAccess($userId, $photoOwnerId);
    }

    public function checkAccess(int $userId, int $photoOwnerId): bool
    {
        $canViewPhotos = (
            $this->chatExists($userId, $photoOwnerId) ||
            $this->incomingChatRequestExists($userId, $photoOwnerId)
        );

        $key = $this->getKey($userId);
        $value = $canViewPhotos ? 1 : 0;

        Redis::hset($key, $photoOwnerId, (string) $value);
        Redis::expire($key, self::LIFETIME);

        return $canViewPhotos;
    }

    private function getKey(int $userId): string
    {
        return "users:photos-access:{$userId}";
    }

    private function chatExists(int $userId, int $photoOwnerId): bool
    {
        return (new Chat())
            ->whereUser($userId)
            ->whereUser($photoOwnerId)
            ->whereActive()
            ->exists()
        ;
    }

    private function incomingChatRequestExists(int $userId, int $photoOwnerId): bool
    {
        return (new ChatRequest())
            ->where('requestor_id', $photoOwnerId)
            ->where('target_user_id', $userId)
            ->where('status', ChatRequest::STATUS_NEW)
            ->exists()
        ;
    }
}
