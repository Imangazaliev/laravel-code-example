<?php

declare(strict_types=1);

namespace App\Services\GoogleRecaptcha;

use GuzzleHttp\ClientInterface;

class GoogleRecaptcha
{
    private const API_URL = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var string
     */
    private $secretKey;

    public function __construct(ClientInterface $httpClient, string $secretKey)
    {
        $this->httpClient = $httpClient;
        $this->secretKey = $secretKey;
    }

    public function check(string $recaptchaResponse): bool
    {
        $parameters = [
            'secret' => $this->secretKey,
            'response' => $recaptchaResponse,
        ];

        $response = $this->httpClient->request('POST', self::API_URL, [
            'form_params' => $parameters,
        ]);

        $decodedResponse = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

        return $decodedResponse['success'];
    }
}
