<?php

declare(strict_types=1);

namespace App\Services\GoogleRecaptcha;

use GuzzleHttp\Client;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider implements DeferrableProvider
{
    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->app->bind(GoogleRecaptcha::class, function () {
            $secretKey = config('services.google_recaptcha.secret_key');

            return new GoogleRecaptcha(new Client(), $secretKey);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [GoogleRecaptcha::class];
    }
}
