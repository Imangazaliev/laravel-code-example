<?php

declare(strict_types=1);

namespace App\Services\Profile;

use App\Models\UserAuthentication;

class ProfileDuplicatesDetector
{
    public function getDuplicates(int $userId): array
    {
        $authentications = (new UserAuthentication())
            ->where('user_id', $userId)
            ->get(['id', 'browser_fingerprint', 'login_ip'])
        ;

        $fingerprints = $authentications->pluck('browser_fingerprint');
        $ips = $authentications->pluck('login_ip');

        $byFingerprintAndIpUserIds = (new UserAuthentication())
            ->whereIn('browser_fingerprint', $fingerprints)
            ->whereIn('login_ip', $ips)
            ->where('user_id', '!=', $userId)
            ->whereNull('admin_id')
            ->pluck('user_id')
            ->all()
        ;

        $byFingerprintUserIds = (new UserAuthentication())
            ->whereIn('browser_fingerprint', $fingerprints)
            ->where('user_id', '!=', $userId)
            ->whereNull('admin_id')
            ->whereNotIn('user_id', $byFingerprintAndIpUserIds)
            ->pluck('user_id')
            ->all()
        ;

        $byIpUserIds = (new UserAuthentication())
            ->whereIn('login_ip', $ips)
            ->where('user_id', '!=', $userId)
            ->whereNull('admin_id')
            ->whereNotIn('user_id', $byFingerprintAndIpUserIds)
            ->pluck('user_id')
            ->all()
        ;

        return [
            'fingerprint_and_ip' => $byFingerprintAndIpUserIds,
            'fingerprint' => $byFingerprintUserIds,
            'ip' => $byIpUserIds,
        ];
    }
}
