<?php

declare(strict_types=1);

namespace App\Services\Profile;

use Carbon\Carbon;
use Throwable;

class UserPhotoMeta
{
    public static function parseMadeAt($madeAt): ?string
    {
        try {
            if (is_numeric($madeAt)) {
                return Carbon::createFromTimestamp(floor((int) $madeAt / 1000))->format(DATE_TIME_FORMAT);
            }

            return Carbon::parse($madeAt)->format(DATE_TIME_FORMAT);
        } catch (Throwable $exception) {
            return null;
        }
    }
}
