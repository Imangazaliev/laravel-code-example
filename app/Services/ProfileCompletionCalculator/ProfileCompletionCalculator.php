<?php

declare(strict_types=1);

namespace App\Services\ProfileCompletionCalculator;

use App\Models\User;
use RuntimeException;

class ProfileCompletionCalculator
{
    public function calculateProfileCompletion(int $userId): int
    {
        $properties = [
            'locality_id',
            'height',
            'weight_from',
            'weight_to',
            'body_type',
            'hair_color',
            'eye_color',
            'facial_hair',
            'smoking',
            'alcohol_consumption',
            'self_description',
            'hobbies_and_interests',
            'sport',
            'sport_exercises_frequency',
            'marital_status',
            'child_count',
            'desired_number_of_children',
            'children_adoption',
            'housing_type',
            'relocation',
            'education',
            'profession_or_occupation',
            'employment',
            'job_title',
            'financial_status',
            'religion',
            'future_spouse_age_from',
            'future_spouse_age_to',
            'future_spouse_height_from',
            'future_spouse_height_to',
            'future_spouse_weight_from',
            'future_spouse_weight_to',
            'future_spouse_body_type',
            'future_spouse_character_traits',
            'future_spouse_description',
            'future_spouse_children',
        ];

        $maleSpecificProperties = [
            'body_type',
            'hair_color',
            'facial_hair',
            'employment',
            'job_title',
            'financial_status',
            'housing_type',
        ];

        $femaleSpecificProperties = [
            'hijab_wearing',
        ];

        $user = (new User())->find($userId);

        if ($user === null) {
            throw new RuntimeException(sprintf('User with ID %d not found', $userId));
        }

        if ($user->sex === User::SEX_MALE) {
            $properties = array_diff($properties, $femaleSpecificProperties);
        } else {
            $properties = array_diff($properties, $maleSpecificProperties);
        }

        $filledPropertyCount = 0;

        foreach ($properties as $propertyName) {
            $value = $user->{$propertyName};

            if ($value !== null && $value !== '') {
                $filledPropertyCount++;
            }
        }

        $percent = (int) ceil($filledPropertyCount / count($properties) * 100);

        return clamp($percent, 0, 100);
    }
}
