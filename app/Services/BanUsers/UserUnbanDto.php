<?php

declare(strict_types=1);

namespace App\Services\BanUsers;

class UserUnbanDto
{
    /**
     * The ID of a user.
     *
     * @var int
     */
    private int $userId;

    /**
     * Comment for administrators.
     *
     * @var string | null
     */
    private ?string $comment = null;

    /**
     * The ID of a moderator.
     *
     * @var int|null
     */
    private ?int $adminId = null;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getAdminId(): ?int
    {
        return $this->adminId;
    }

    public function setAdminId(int $adminId): self
    {
        $this->adminId = $adminId;

        return $this;
    }
}
