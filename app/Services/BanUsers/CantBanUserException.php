<?php

declare(strict_types=1);

namespace App\Services\BanUsers;

use Exception;

class CantBanUserException extends Exception
{
    //
}
