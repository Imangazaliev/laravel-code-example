<?php

declare(strict_types=1);

namespace App\Services\BanUsers;

use App\Models\Notification;
use App\Models\User;
use App\Models\UserBan;
use Carbon\Carbon;
use DB;
use Throwable;

class BanUsers
{
    public function ban(UserBanDto $userBanDto): void
    {
        $user = (new User())->findOrFail($userBanDto->getUserId(), [
            'id',
            'status',
            'status_updated_at',
        ]);

        if (in_array($user->status, [
            User::STATUS_BANNED,
            User::STATUS_DELETED,
        ], true)) {
            throw new CantBanUserException(sprintf('Can\'t ban the user #%d', $user->id));
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $banPeriod = $userBanDto->getBanPeriod();

            $ban = (new UserBan())->create([
                'user_id' => $userBanDto->getUserId(),
                'reasons' => $userBanDto->getReasons(),
                'comment' => $userBanDto->getComment(),
                'comment_for_user' => $userBanDto->getCommentForUser(),
                'banned_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'banned_by' => $userBanDto->getAdminId(),
                'unban_at' => $banPeriod === 0 ? null : $currentTime->addMinute($banPeriod)->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);

            (new User())
                ->where('id', $userBanDto->getUserId())
                ->where('status_updated_at', $user->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'status' => User::STATUS_BANNED,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'ban_id' => $ban->id,
                    'moderation_moderator_id' => null,
                    'moderation_claimed_at' => null,
                    'moderation_forwarded_to' => null,
                    'moderation_forwarded_by' => null,
                    'moderation_forwarded_at' => null,
                ])
            ;

            (new Notification())->create([
                'user_id' => $userBanDto->getUserId(),
                'type' => Notification::TYPE_PROFILE_BANNED,
                'title' => 'notifications:profile-banned.title',
                'body' => 'notifications:profile-banned.body',
                'is_hidden' => true,
                'created_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();

            cache_info()->userProfile($user->id)->forget();
            cache_info()->userLastModerationStatus($user->id)->forget();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function unban(UserUnbanDto $userUnbanDto): void
    {
        $user = (new User())->findOrFail($userUnbanDto->getUserId(), [
            'id',
            'status',
            'status_updated_at',
            'ban_id',
        ]);

        if ($user->status !== User::STATUS_BANNED) {
            throw new UserNotBannedException(sprintf('User #%d is not banned', $user->id));
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            (new User())
                ->where('id', $user->id)
                ->where('status_updated_at', $user->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'status' => User::STATUS_NOT_MODERATED,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                ])
            ;

            $user->ban->update([
                'unbanned_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'unbanned_by' => $userUnbanDto->getAdminId(),
                'unban_comment' => $userUnbanDto->getComment(),
            ]);

            (new Notification())->create([
                'user_id' => $user->id,
                'type' => Notification::TYPE_PROFILE_UNBANNED,
                'title' => 'notifications:profile-unbanned.title',
                'body' => 'notifications:profile-unbanned.body',
                'is_hidden' => true,
                'created_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();

            cache_info()->userProfile($user->id)->forget();
            cache_info()->userLastModerationStatus($user->id)->forget();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }
}
