<?php

declare(strict_types=1);

namespace App\Services\BanUsers;

class UserBanDto
{
    /**
     * The ID of a user.
     *
     * @var int
     */
    private int $userId;

    /**
     * Ban reasons.
     *
     * @var string[]
     */
    private array $reasons;

    /**
     * Comment for administrators.
     *
     * @var string | null
     */
    private ?string $comment = null;

    /**
     * Comment for a user.
     *
     * @var string | null
     */
    private ?string $commentForUser = null;

    /**
     * The ID of a moderator.
     *
     * @var int|null
     */
    private ?int $adminId = null;

    /**
     * Ban period in minutes.
     *
     * @var int
     */
    private int $banPeriod = 0;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getReasons(): array
    {
        return $this->reasons;
    }

    public function setReasons(array $reasons): self
    {
        $this->reasons = $reasons;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCommentForUser(): ?string
    {
        return $this->commentForUser;
    }

    public function setCommentForUser(?string $comment): self
    {
        $this->commentForUser = $comment;

        return $this;
    }

    public function getAdminId(): ?int
    {
        return $this->adminId;
    }

    public function setAdminId(int $adminId): self
    {
        $this->adminId = $adminId;

        return $this;
    }

    public function getBanPeriod(): int
    {
        return $this->banPeriod;
    }

    public function setBanPeriod(int $banPeriod): self
    {
        $this->banPeriod = $banPeriod;

        return $this;
    }
}
