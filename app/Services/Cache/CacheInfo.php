<?php

declare(strict_types=1);

namespace App\Services\Cache;

final class CacheInfo
{
    /**
     * @var CacheInfo
     */
    private static $instance;

    /**
     * Constructor is private to avoid creating a class instance outside of it.
     */
    private function __construct()
    {
        //
    }

    public static function instance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function entry(string $key, ?int $ttl): CacheEntryBuilder
    {
        return new CacheEntryBuilder(new LaravelCacheRepository(), $key, $ttl);
    }

    private function key(string $key, ...$parameters): string
    {
        return $key . '.' . implode('.', array_filter($parameters));
    }

    public function availableLocales(): CacheEntry
    {
        return $this->entry('available-locales', null)
            ->notBindToLanguage()
            ->useFallback(60 * 5)
            ->get()
        ;
    }

    public function badWords(): CacheEntry
    {
        return $this->entry($this->key('bad-words'), 60 * 60 * 24 * 7)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function browserStrings(string $languageCode, int $lastCacheUpdateTime): CacheEntry
    {
        return $this->entry($this->key('browser-strings', $languageCode, $lastCacheUpdateTime), 60 * 60 * 24 * 7)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function chatIds(int $userId): CacheEntry
    {
        return $this->entry($this->key('chat-ids', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function countries(): CacheEntry
    {
        return $this->entry('countries', 60 * 60 * 24 * 10)
            ->useFallback(60 * 10)
            ->get()
            ;
    }

    public function currentLanguage(string $languageCode): CacheEntry
    {
        return $this->entry($this->key('current-language', $languageCode), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function ethnicities(string $sex): CacheEntry
    {
        return $this->entry($this->key('ethnicities', $sex), 60 * 60 * 24 * 10)
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function isFavoriteUser(int $userId, int $targetUserId): CacheEntry
    {
        return $this
            ->entry($this->key('is-favorite-user', $userId, $targetUserId), 60 * 60 * 24)
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function isLikedUser(int $userId, int $targetUserId): CacheEntry
    {
        return $this
            ->entry($this->key('is-liked-user', $userId, $targetUserId), 60 * 60 * 24)
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function languages(bool $supportedOnly): CacheEntry
    {
        return $this->entry($this->key('languages', $supportedOnly ? 'supported_only' : null), 60 * 60 * 24 * 10)
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function localities(int $countryId, ?int $subdivisionId): CacheEntry
    {
        return $this->entry($this->key('localities', $countryId, $subdivisionId), 60 * 60 * 24 * 10)
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function mainPhotoUrl(int $userId): CacheEntry
    {
        return $this->entry($this->key('main-photo-url', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function newChatRequestCount(int $userId): CacheEntry
    {
        return $this->entry($this->key('new-chat-request-count', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function newMessageCount(int $userId): CacheEntry
    {
        return $this->entry($this->key('new-message-count', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function newMessageModerationCount(int $userId): CacheEntry
    {
        return $this->entry($this->key('new-message-moderation-count', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function newProfileModerationCount(int $userId): CacheEntry
    {
        return $this->entry($this->key('new-profile-moderation-count', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function newPhotoModerationCount(int $userId): CacheEntry
    {
        return $this->entry($this->key('new-photo-moderation-count', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function newProfileViewCount(int $userId): CacheEntry
    {
        return $this->entry($this->key('new-profile-view-count', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function newLikeCount(int $userId): CacheEntry
    {
        return $this->entry($this->key('new-profile-like-count', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function newSupportMessageCount(int $userId): CacheEntry
    {
        return $this->entry($this->key('new-support-message-count', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function newNotificationCount(int $userId): CacheEntry
    {
        return $this->entry($this->key('new-notification-count', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function sentChatRequestPreviously(int $userId, int $targetUserId): CacheEntry
    {
        return $this
            ->entry($this->key('sent-chat-request-previously', $userId, $targetUserId), 60 * 60 * 24)
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function savedFilters(int $userId): CacheEntry
    {
        return $this->entry($this->key('saved-filters', $userId), 60 * 60 * 24 * 10)
            ->useFallback(60 * 10)
            ->notBindToLanguage()
            ->get()
        ;
    }

    public function subdivisions(int $countryId): CacheEntry
    {
        return $this->entry($this->key('subdivisions', $countryId), 60 * 60 * 24 * 10)
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function supportChatId(int $userId): CacheEntry
    {
        return $this->entry($this->key('support-chat-id', $userId), 60 * 60 * 24)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function userLastModerationStatus(int $userId): CacheEntry
    {
        return $this
            ->entry($this->key('user-last-status', $userId), 60 * 60 * 24 * 10)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function userLastOnlineTime(int $userId): CacheEntry
    {
        return $this
            ->entry($this->key('user-last-online-time', $userId), 60 * 60)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function userPermissions(int $userId, int $languageId): CacheEntry
    {
        return $this
            ->entry($this->key('user-permissions', $userId, $languageId), 60 * 60 * 24 * 10)
            ->notBindToLanguage()
            ->get()
        ;
    }

    public function userPhotos(int $userId): CacheEntry
    {
        return $this
            ->entry($this->key('user-photos', $userId), 60 * 60 * 24 * 10)
            ->notBindToLanguage()
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function userProfile(int $userId): CacheEntry
    {
        return $this
            ->entry($this->key('user-profile', $userId), 60 * 60 * 24 * 10)
            ->useFallback(60 * 10)
            ->notBindToLanguage()
            ->get()
        ;
    }

    public function userProfileTranslatableData(int $userId): CacheEntry
    {
        return $this
            ->entry($this->key('user-profile-translatable-data', $userId), 60 * 60 * 24 * 10)
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function usersChatId(int $user1Id, int $user2Id): CacheEntry
    {
        // sort user IDs
        [$user1Id, $user2Id] = [min($user1Id, $user2Id), max($user1Id, $user2Id)];

        return $this
            ->entry($this->key('users-chat-id', $user1Id, $user2Id), 60 * 60 * 24)
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function usersIncomingChatRequestId(int $targetUserId, int $requestorId): CacheEntry
    {
        return $this
            ->entry($this->key('users-incoming-chat-request-id', $targetUserId, $requestorId), 60 * 60 * 24)
            ->useFallback(60 * 10)
            ->get()
        ;
    }

    public function usersOutgoingChatRequestId(int $requestorId, int $targetUserId): CacheEntry
    {
        return $this
            ->entry($this->key('users-outgoing-chat-request-id', $requestorId, $targetUserId), 60 * 60 * 24)
            ->useFallback(60 * 10)
            ->get()
        ;
    }
}
