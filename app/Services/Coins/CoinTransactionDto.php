<?php

declare(strict_types=1);

namespace App\Services\Coins;

class CoinTransactionDto
{
    /**
     * The type of a transaction.
     *
     * @var string
     */
    private string $type;

    /**
     * The ID of a user.
     *
     * @var int
     */
    private int $userId;

    /**
     * The amount of coins.
     *
     * @var int
     */
    private int $amount;

    /**
     * The description of a transaction.
     *
     * @var string
     */
    private string $description;

    /**
     * The ID of an invoice (when a user topping up the balance).
     *
     * @var int|null
     */
    private ?int $invoiceId = null;

    /**
     * The ID of a source transaction (while transferring money between users).
     *
     * @var int|null
     */
    private ?int $transferSourceTransactionId = null;

    /**
     * The ID of a moderator.
     *
     * @var int|null
     */
    private ?int $adminId = null;

    /**
     * A comment from a moderator.
     *
     * @var string|null
     */
    private ?string $adminComment = null;

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getInvoiceId(): ?int
    {
        return $this->invoiceId;
    }

    public function setInvoiceId(int $invoiceId): self
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    public function getTransferSourceTransactionId(): ?int
    {
        return $this->transferSourceTransactionId;
    }

    public function setTransferSourceTransactionId(int $transferSourceTransactionId): self
    {
        $this->transferSourceTransactionId = $transferSourceTransactionId;

        return $this;
    }

    public function getAdminId(): ?int
    {
        return $this->adminId;
    }

    public function setAdminId(int $id): self
    {
        $this->adminId = $id;

        return $this;
    }

    public function getAdminComment(): ?string
    {
        return $this->adminComment;
    }

    public function setAdminComment(string $comment): self
    {
        $this->adminComment = $comment;

        return $this;
    }
}
