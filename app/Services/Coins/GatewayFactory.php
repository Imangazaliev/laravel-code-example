<?php

declare(strict_types=1);

namespace App\Services\Coins;

use App\Services\Coins\Exception\UnknownGatewayException;
use Omnipay\Common\GatewayInterface;
use Omnipay\Omnipay;

class GatewayFactory implements GatewayFactoryInterface
{
    public const PAYMENT_GATEWAY_TINKOFF = 'tinkoff';
    public const PAYMENT_GATEWAY_YOOKASSA = 'yookassa';

    public static function getSupportedGateways(): array
    {
        return [
            self::PAYMENT_GATEWAY_TINKOFF,
            self::PAYMENT_GATEWAY_YOOKASSA,
        ];
    }

    public function createGateway(string $gatewayCode): GatewayInterface
    {
        switch ($gatewayCode) {
            case self::PAYMENT_GATEWAY_TINKOFF:
                return Omnipay::create('Tinkoff')->initialize([
                    'terminal_key' => config('services.tinkoff.terminal_key'),
                ]);
            case self::PAYMENT_GATEWAY_YOOKASSA:
                return Omnipay::create('YooKassa')->initialize([
                    'shopId' => config('services.yookassa.shop_id'),
                    'secret' => config('services.yookassa.secret_key'),
                ]);
            default:
                throw new UnknownGatewayException(sprintf('Unknown gateway "%s"', $gatewayCode));
        }
    }
}
