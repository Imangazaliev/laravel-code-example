<?php

declare(strict_types=1);

namespace App\Services\Coins\Exception;

use Exception;

class InsufficientCoinsBalanceException extends Exception
{
    //
}
