<?php

declare(strict_types=1);

namespace App\Services\Coins;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->bind(GatewayFactoryInterface::class, GatewayFactory::class);
    }

    public function provides(): array
    {
        return [GatewayFactoryInterface::class];
    }
}
