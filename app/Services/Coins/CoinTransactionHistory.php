<?php

declare(strict_types=1);

namespace App\Services\Coins;

use App\Models\CoinTransaction;

class CoinTransactionHistory
{
    public static function transaction(int $userId, ?int $page = null): array
    {
        $transactionsPaginator = (new CoinTransaction())
            ->with('invoice:id,price,currency_code')
            ->where('user_id', $userId)
            ->orderBy('transaction_time', 'desc')
            ->paginate(30, ['*'], 'page', $page)
        ;

        $transactionTitles = self::getTransactionTitles();

        $transactions = array_map(function (CoinTransaction $transaction) use ($transactionTitles) {
            return [
                'id' => $transaction->id,
                'title' => $transactionTitles[$transaction->type] ?? $transaction->type,
                'amount' => $transaction->amount,
                'time' => $transaction->transaction_time->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'invoice' => $transaction->type !== CoinTransaction::TYPE_BALANCE_RECHARGE ? null : [
                    'price' => $transaction->invoice->price,
                    'currency' => $transaction->invoice->currency_code,
                ],
            ];
        }, $transactionsPaginator->items());

        return [
            'transactions' => $transactions,
            'page_count' => $transactionsPaginator->lastPage(),
        ];
    }

    private static function getTransactionTitles(): array
    {
        return [
            CoinTransaction::TYPE_REGISTRATION_GIFT => __('coins:transaction-types.registration-gift'),
            CoinTransaction::TYPE_BALANCE_RECHARGE => __('coins:transaction-types.balance-recharge'),
            CoinTransaction::TYPE_GIFT_TO_ANOTHER_USER => __('coins:transaction-types.gift-to-another-user'),
            CoinTransaction::TYPE_GIFT_FROM_ANOTHER_USER => __('coins:transaction-types.gift-from-another-user'),
            CoinTransaction::TYPE_CHANGE_BY_ADMINISTRATION => __('coins:transaction-types.change-by-administration'),
            CoinTransaction::TYPE_CHAT_REQUEST => __('coins:transaction-types.chat-request'),
            CoinTransaction::TYPE_CHAT_REQUEST_ACCEPTANCE => __('coins:transaction-types.chat-request-acceptance'),
            CoinTransaction::TYPE_CHAT_REQUEST_CANCELLATION => __('coins:transaction-types.chat-request-cancellation'),
            CoinTransaction::TYPE_CHAT_REQUEST_REJECTION => __('coins:transaction-types.chat-request-rejection'),
            CoinTransaction::TYPE_CHAT_REQUEST_AUTO_CANCELLATION => __('coins:transaction-types.chat-request-auto-cancellation'),
            CoinTransaction::TYPE_CHAT_MESSAGES => __('coins:transaction-types.chat-messages'),
            CoinTransaction::TYPE_CHAT_FINISHED_TOO_FAST => __('coins:transaction-types.chat-finished-too-fast'),
        ];
    }
}
