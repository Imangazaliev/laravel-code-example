<?php

declare(strict_types=1);

namespace App\Services\Coins;

use Omnipay\Common\GatewayInterface;

interface GatewayFactoryInterface
{
    public static function getSupportedGateways(): array;

    public function createGateway(string $gatewayCode): GatewayInterface;
}
