<?php

declare(strict_types=1);

namespace App\Services\Coins;

use App\Models\CoinTransaction;
use App\Models\Invoice;
use App\Models\PaymentGatewayNotification;
use App\Services\Coins\Exception\InvalidInvoiceReference;
use App\Services\Coins\Exception\PaymentVerificationException;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Money\Currency;
use Money\Money;
use Omnipay\Common\GatewayInterface;
use Ramsey\Uuid\Uuid;
use RuntimeException;
use Swap\Swap;
use Throwable;

class BalanceRecharge
{
    private const INVOICE_REFERENCE_TYPE_INVOICE_ID = 'invoice-id';
    private const INVOICE_REFERENCE_TYPE_TOKEN = 'token';
    private const INVOICE_REFERENCE_TYPE_GATEWAY_TRANSACTION_ID = 'gateway-transaction-id';

    private GatewayFactoryInterface $gatewayFactory;

    private Coins $coins;

    private Swap $swap;

    public function __construct(GatewayFactoryInterface $gatewayFactory, Coins $coins, Swap $swap)
    {
        $this->gatewayFactory = $gatewayFactory;
        $this->coins = $coins;
        $this->swap = $swap;
    }

    public static function getAmountOptions(bool $withTestOptions): array
    {
        $options = [30, 40, 50, 100, 200, 300, 500, 1000];

        if ($withTestOptions) {
            $options = array_merge([1, 5, 10], $options);
        }

        array_sort($options);

        return $options;
    }

    public static function getDefaultAmount(): int
    {
        return 30;
    }

    public function calculatePrice(int $amount, string $currency): float
    {
        $coinPriceInUsd = config('payments.coin_price');
        $currencyRate = $this->swap->latest("USD/$currency")->getValue();

        // TODO: test with EUR and UAH
        $price = (float) (new Money($coinPriceInUsd, new Currency('USD')))
            ->multiply($amount)
            ->multiply($currencyRate)
            ->getAmount()
        ;

        return $price / 100;
    }

    public function createInvoice(
        int $amount,
        string $currency,
        string $gatewayCode,
    ): array {
        $currencyRate = $this->swap->latest("USD/$currency")->getValue();
        $coinsPrice = config('payments.test_mode') ? 1 : $this->calculatePrice($amount, $currency);

        DB::beginTransaction();

        try {
            $invoice = (new Invoice())->create([
                'user_id' => current_user()->id,
                'coins_amount' => $amount,
                'payment_gateway' => $gatewayCode,
                'price' => $coinsPrice,
                'currency_code' => $currency,
                'currency_rate' => $currencyRate,
                'status' => Invoice::STATUS_NEW,
                'created_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                'token' => $this->generateToken(),
            ]);

            $returnUrl = url('/add-coins/success');
            $failUrl = url('/add-coins/failed');

            $gateway = $this->gatewayFactory->createGateway($gatewayCode);

            $description = sprintf('Top up coins of user %s (#%d) by %d coins on %s.', current_user()->name, current_user()->id, $amount, config('app.name'));

            $parameters = [
                'transaction_id' => $invoice->id,
                'amount' => $this->formatPrice($gatewayCode, $coinsPrice),
                'currency' => $currency,
                'description' => $description,
                'return_url' => $returnUrl,
                'cancel_url' => $failUrl,
                'notify_url' => route('api.coins.complete-purchase', $gatewayCode),
            ];

            $response = $gateway
                ->purchase(array_merge(
                    $parameters,
                    $this->getPurchaseAdditionalParameters($gatewayCode),
                ))
                ->send()
            ;

            $invoice->update([
                'payment_gateway_transaction_id' => $response->getTransactionReference(),
            ]);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }

        return [
            'redirect_url'=> $response->getRedirectUrl(),
            'redirect_method'=> $response->getRedirectMethod(),
            'redirect_data'=> $response->getRedirectData(),
        ];
    }

    /**
     * Generated super secure tokens.
     *
     * @return string
     *
     * @throws Exception
     */
    private function generateToken(): string
    {
        $token = Uuid::uuid4()->toString();

        // use random number of hashing rounds because UUID are theoretically predictable
        $hashRounds = random_int(1, 10);

        for ($i = 0; $i < $hashRounds; $i++) {
            $token = hash('sha256', $token);
        }

        return $token;
    }

    private function formatPrice(string $gateway, float $price)
    {
        if ($gateway === GatewayFactory::PAYMENT_GATEWAY_YOOKASSA) {
            return number_format($price, 2, '.', '');
        }

        return $price;
    }

    private function getPurchaseAdditionalParameters(string $gateway): array
    {
        if ($gateway === GatewayFactory::PAYMENT_GATEWAY_YOOKASSA) {
            return [
                'capture' => true,
                'receipt' => [],
            ];
        }

        return [];
    }

    /**
     * @param string $gatewayCode
     * @param array $requestData
     *
     * @throws InvalidInvoiceReference
     * @throws PaymentVerificationException
     * @throws Throwable
     */
    public function completePurchase(string $gatewayCode, array $requestData): ?string
    {
        $gateway = $this->gatewayFactory->createGateway($gatewayCode);

        $invoiceReference = $this->getInvoiceReference($gatewayCode, $requestData);

        $invoice = (new Invoice())
            ->when($invoiceReference['type'] === self::INVOICE_REFERENCE_TYPE_INVOICE_ID, function (Builder $query) use ($invoiceReference) {
                $query->where('id', $invoiceReference['value']);
            })
            ->when($invoiceReference['type'] === self::INVOICE_REFERENCE_TYPE_TOKEN, function (Builder $query) use ($invoiceReference) {
                $query->where('token', $invoiceReference['value']);
            })
            ->when($invoiceReference['type'] === self::INVOICE_REFERENCE_TYPE_GATEWAY_TRANSACTION_ID, function (Builder $query) use ($invoiceReference) {
                $query->where('payment_gateway_transaction_id', $invoiceReference['value']);
            })
            ->first([
                'id',
                'user_id',
                'coins_amount',
                'payment_gateway',
                'price',
                'status',
                'payment_gateway_transaction_id',
            ])
        ;

        if ($invoice === null) {
            throw new InvalidInvoiceReference('Invoice not found.');
        }

        DB::beginTransaction();

        try {
            (new PaymentGatewayNotification())->create([
                'invoice_id' => $invoice->id,
                'data' => $requestData,
                'created_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);

            $result = null;

            if ($invoice->payment_gateway === GatewayFactory::PAYMENT_GATEWAY_TINKOFF) {
                $result = $this->handleTinkoff($invoice, $requestData);
            }

            if ($invoice->payment_gateway === GatewayFactory::PAYMENT_GATEWAY_YOOKASSA) {
                $result = $this->handleYooKassa($gateway, $invoice, $requestData);
            }

            if ($result === null) {
                return null;
            }

            $this->handleResult($result['status'], $invoice);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }

        return $result['response'];
    }

    private function getInvoiceReference(string $gateway, array $requestData): array
    {
        switch ($gateway) {
            case GatewayFactory::PAYMENT_GATEWAY_TINKOFF:
                return [
                    'type' => self::INVOICE_REFERENCE_TYPE_INVOICE_ID,
                    'value' => $requestData['OrderId'],
                ];
            case GatewayFactory::PAYMENT_GATEWAY_YOOKASSA:
                return [
                    'type' => self::INVOICE_REFERENCE_TYPE_GATEWAY_TRANSACTION_ID,
                    'value' => $requestData['object']['id'],
                ];
            default:
                throw new RuntimeException(sprintf('Invalid gateway "%s".', $gateway));
        }
    }

    private function handleTinkoff(Invoice $invoice, array $requestData): array
    {
        switch ($requestData['Status']) {
            case 'AUTHORIZED':
            case 'REFUNDED':
                return [
                    'status' => null,
                    'response' => 'OK',
                ];
            case 'REJECTED':
                return [
                    'status' => 'failed',
                    'response' => 'OK',
                ];
            case 'CONFIRMED':
                $isRequestDataValid = (
                    $requestData['Success'] &&
                    (float) $requestData['Amount'] === $invoice->price * 100
                );

                if ( ! $isRequestDataValid) {
                    throw new PaymentVerificationException('Payment verification failed: notification request data is invalid');
                }

                return [
                    'status' => 'success',
                    'response' => 'OK',
                ];
            default:
                throw new RuntimeException(sprintf('Invalid payment status "%s".', $requestData['Status']));
        }
    }

    private function handleYooKassa(GatewayInterface $gateway, Invoice $invoice, array $requestData): array
    {
        switch ($requestData['event']) {
            case 'payment.canceled':
                return [
                    'status' => 'failed',
                    'response' => null,
                ];
            case 'payment.succeeded':
                $gateway->acceptNotification();

                return [
                    'status' => 'success',
                    'response' => null,
                ];
            default:
                throw new RuntimeException(sprintf('Invalid payment status "%s".', $requestData['Status']));
        }
    }

    private function handleResult(?string $status, Invoice $invoice): void
    {
        switch ($status) {
            case 'success':
                $this->changeBalance($invoice);

                break;
            case 'failed':
                $invoice->update([
                    'status' => Invoice::STATUS_FAILED,
                    'finished_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                ]);

                break;
            case null:
                return;
            default:
                throw new RuntimeException(sprintf('Unknown status "%s".', $status));
        }
    }

    private function changeBalance(Invoice $invoice): void
    {
        $coinTransactionDto = (new CoinTransactionDto())
            ->setType(CoinTransaction::TYPE_BALANCE_RECHARGE)
            ->setAmount($invoice->coins_amount)
            ->setUserId($invoice->user_id)
            ->setDescription('Top up coin balance')
            ->setInvoiceId($invoice->id)
        ;

        $this->coins->changeBalance($coinTransactionDto);

        $invoice->update([
            'status' => Invoice::STATUS_SUCCESS,
            'finished_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
        ]);
    }
}
