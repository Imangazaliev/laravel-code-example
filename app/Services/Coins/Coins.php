<?php

declare(strict_types=1);

namespace App\Services\Coins;

use App\Models\CoinTransaction;
use App\Models\User;
use App\Services\Coins\Exception\InsufficientCoinsBalanceException;
use Carbon\Carbon;
use DB;
use RuntimeException;
use Throwable;

class Coins
{
    /**
     * @param CoinTransactionDto $coinTransactionDto
     *
     * @return CoinTransaction
     *
     * @throws InsufficientCoinsBalanceException
     */
    public function changeBalance(CoinTransactionDto $coinTransactionDto): CoinTransaction
    {
        // TODO: check that transaction amount is not equals 0

        DB::beginTransaction();

        try {
            $attemptCount = 0;

            // update the balance using optimistic lock
            while (true) {
                $user = (new User())->find($coinTransactionDto->getUserId(), [
                    'id',
                    'coins',
                    'coins_updated_at',
                ]);

                // TODO: handle a case when a user not found

                $balance = $user->coins;
                $newBalance = $balance + $coinTransactionDto->getAmount();

                if (
                    $newBalance < 0 &&
                    // if is a subtraction
                    $coinTransactionDto->getAmount() < 0 &&
                    $coinTransactionDto->getType() !== CoinTransaction::TYPE_CHANGE_BY_ADMINISTRATION
                ) {
                    throw new InsufficientCoinsBalanceException('Insufficient coins balance');
                }

                $updateTime = Carbon::now();

                $affectedRowCount = (new User())
                    ->where('id', $user->id)
                    ->where('coins_updated_at', $user->coins_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                    ->update([
                        'coins' => $newBalance,
                        'coins_updated_at' => $updateTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    ])
                ;

                if ($affectedRowCount > 0) {
                    break;
                }

                $attemptCount++;

                if ($attemptCount === 3) {
                    throw new RuntimeException('Can\'t update coin balance');
                }
            }

            $transaction = (new CoinTransaction())->create([
                'type' => $coinTransactionDto->getType(),
                'user_id' => $coinTransactionDto->getUserId(),
                'amount' => $coinTransactionDto->getAmount(),
                'description' => $coinTransactionDto->getDescription(),
                'balance_before' => $balance,
                'balance_after' => $newBalance,
                'transaction_time' => $updateTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                'invoice_id' => $coinTransactionDto->getInvoiceId(),
                'transfer_source_transaction_id' => $coinTransactionDto->getTransferSourceTransactionId(),
                'admin_id' => $coinTransactionDto->getAdminId(),
                'admin_comment' => $coinTransactionDto->getAdminComment(),
            ]);

            DB::commit();

            return $transaction;
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }
}
