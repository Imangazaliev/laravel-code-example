<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class CurrencyText extends Model
{
    protected $table = 'currency_texts';

    protected $fillable = [
        'currency_id',
        'language_id',
        'name',
    ];

    protected $casts = [
        'currency_id' => 'integer',
        'language_id' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;
}
