<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class UserProfileRevision extends Model
{
    protected $table = 'user_profile_revisions';

    protected $fillable = [
        'user_id',
        'data',
        'created_at',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'data' => 'json',
        'created_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
