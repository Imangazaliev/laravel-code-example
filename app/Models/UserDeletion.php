<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use RuntimeException;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class UserDeletion extends Model
{
    public const DELETION_REASON_FOUND_SPOUSE_ON_SITE = 'found-spouse-on-site';
    public const DELETION_REASON_FOUND_SPOUSE_IN_ANOTHER_PLACE = 'found-spouse-in-another-place';
    public const DELETION_REASON_DISAPPOINTED_TO_FIND_SPOUSE_ON_SITE = 'disappointed-to-find-spouse-on-site';
    public const DELETION_REASON_AT_REQUEST_OF_USER = 'at-request-of-user';
    public const DELETION_REASON_OTHER = 'other';

    protected $table = 'user_deletions';

    protected $fillable = [
        'user_id',
        'name',
        'phone_number',
        'phone_number_verified_at',
        'email',
        'email_verified_at',
        'reason',
        'comment',
        'deleted_at',
        'deleted_by',
        'canceled_at',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'phone_number_verified_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'email_verified_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'deleted_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'deleted_by' => 'integer',
        'canceled_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
    ];

    public $timestamps = false;

    public static function getReasons(string $sex, string $type): array
    {
        if ($type === 'user') {
            return [
                self::DELETION_REASON_FOUND_SPOUSE_ON_SITE,
                self::DELETION_REASON_FOUND_SPOUSE_IN_ANOTHER_PLACE,
                self::DELETION_REASON_DISAPPOINTED_TO_FIND_SPOUSE_ON_SITE,
                self::DELETION_REASON_OTHER,
            ];
        }

        if ($type === 'admin') {
            return [
                self::DELETION_REASON_AT_REQUEST_OF_USER,
                self::DELETION_REASON_OTHER,
            ];
        }

        throw new RuntimeException(sprintf('Unknown type "%s"', $type));
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function deletedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }
}
