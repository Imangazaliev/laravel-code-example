<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class UserLoginToken extends Model
{
    protected $table = 'user_login_tokens';

    protected $fillable = [
        'user_id',
        'token',
        'created_at',
        'created_by',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'created_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'created_by' => 'integer',
    ];

    public $timestamps = false;
}
