<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int|string $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class LocalityText extends Model
{
    protected $table = 'locality_texts';

    protected $fillable = [
        'locality_id',
        'language_id',
        'name',
    ];

    protected $casts = [
        'locality_id' => 'integer',
        'language_id' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;
}
