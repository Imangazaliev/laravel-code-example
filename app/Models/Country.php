<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ExampleProject\Models\Traits\Translatable;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Country extends Model
{
    use Translatable;

    protected $table = 'countries';

    protected $fillable = [
        'code_alpha2',
        'code_alpha3',
        'code_numeric',
        'native_name',
        'native_official_name',
        'english_name',
        'english_official_name',
        'native_name_transliteration',
        'native_official_name_transliteration',
        'continent_code',
        'is_independent',
        'sovereign_state_country_code',
        'show_in_list',
        'is_in_european_union',
        'currencies',
    ];

    protected $casts = [
        'is_independent' => 'boolean',
        'show_in_list' => 'boolean',
        'is_in_european_union' => 'boolean',
        'currencies' => 'json',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function getTranslationModelInfo(): array
    {
        return [CountryText::class, 'country_id'];
    }
}
