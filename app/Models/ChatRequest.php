<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class ChatRequest extends Model
{
    public const STATUS_NEW = 'new';
    public const STATUS_CANCELED = 'canceled';
    public const STATUS_ACCEPTED = 'accepted';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_CANCELED_AUTOMATICALLY = 'canceled-automatically';
    public const GREETING_MESSAGE_STATUS_PENDING_MODERATION = 'pending-moderation';
    public const GREETING_MESSAGE_STATUS_ACCEPTED = 'accepted';
    public const GREETING_MESSAGE_STATUS_REJECTED = 'rejected';

    protected $table = 'chat_requests';

    protected $fillable = [
        'requestor_id',
        'target_user_id',
        'greeting_message',
        'greeting_message_moderation_status',
        'greeting_message_moderation_status_updated_at',
        'status',
        'status_updated_at',
        'sent_at',
        'canceled_at',
        'seen_at',
        'answered_at',
        'rejection_reasons',
        'rejection_comment',
        'rejection_comment_moderation_status',
        'rejection_comment_moderation_status_updated_at',
        'transaction_id',
        'acceptance_transaction_id',
        'return_transaction_id',
        'moderation_moderator_id',
        'moderation_claimed_at',
    ];

    protected $casts = [
        'requestor_id' => 'integer',
        'target_user_id' => 'integer',
        'greeting_message_moderation_status_updated_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'status_updated_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'created_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'canceled_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'seen_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'answered_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'rejection_reasons' => 'json',
        'rejection_comment_moderation_status_updated_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'transaction_id' => 'integer',
        'acceptance_transaction_id' => 'integer',
        'return_transaction_id' => 'integer',
        'moderation_moderator_id' => 'integer',
        'moderation_claimed_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function requestor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'requestor_id');
    }

    public function targetUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'target_user_id');
    }

    public function transaction(): BelongsTo
    {
        return $this->belongsTo(CoinTransaction::class, 'transaction_id');
    }

    public function acceptTransaction(): BelongsTo
    {
        return $this->belongsTo(CoinTransaction::class, 'acceptance_transaction_id');
    }
}
