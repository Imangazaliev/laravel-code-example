<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\NotificationDelivery\NotificationSettings;
use Carbon\Carbon;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use ExampleProject\Permissions\Models\HasRolesAndPermissions;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class User extends Authenticatable implements HasLocalePreference
{
    use HasFactory, Notifiable, HasRolesAndPermissions;

    public const STATUS_NOT_MODERATED = 'not-moderated';
    public const STATUS_PENDING_MODERATION = 'pending-moderation';
    public const STATUS_ACCEPTED = 'accepted';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_BANNED = 'banned';
    public const STATUS_DEACTIVATED = 'deactivated';
    public const STATUS_PENDING_DELETION = 'pending-deletion';
    public const STATUS_DELETED = 'deleted';
    public const PRESENCE_STATUS_ONLINE = 'online';
    public const PRESENCE_STATUS_IDLE = 'idle';
    public const PRESENCE_STATUS_OFFLINE = 'offline';
    public const SEX_MALE = 'm';
    public const SEX_FEMALE = 'f';
    public const BODY_TYPE_SKINNY = 'skinny';
    public const BODY_TYPE_SLIM = 'slim';
    public const BODY_TYPE_NORMAL = 'normal';
    public const BODY_TYPE_ATHLETIC = 'athletic';
    public const BODY_TYPE_PLUMP = 'plump';
    public const HAIR_COLOR_BLACK = 'black';
    public const HAIR_COLOR_LIGHT_BROWN = 'light-brown';
    public const HAIR_COLOR_DARK_BROWN = 'dark-brown';
    public const HAIR_COLOR_BLOND = 'blond';
    public const HAIR_COLOR_GINGER = 'ginger';
    public const HAIR_COLOR_GRAY = 'gray';
    public const HAIR_COLOR_BALD = 'bald';

    protected $fillable = [
        'name',
        'last_accepted_name',
        'sex',
        'birthday',
        'phone_number',
        'phone_number_country_code',
        'phone_number_in_national_format',
        'phone_number_verified_at',
        'email',
        'email_verified_at',
        'password',
        'password_updated_at',
        'remember_token',
        'registered_at',
        'completed_registration_at',
        'updated_at',
        'language_id',
        'language_code',
        'status',
        'status_updated_at',
        'reputation', // denormalization
        'reputation_updated_at', // denormalization
        'internal_reputation', // denormalization
        'internal_reputation_updated_at', // denormalization
        'coins',
        'coins_updated_at',
        'profile_completion_percent', // denormalization
        'profile_completion_percent_updated_at',
        'required_fields_filled', // denormalization
        'required_fields_checked_at',
        'main_photo_id',
        'notification_settings',
        'is_test_user',
        'is_protected',
        'is_hidden',
        'is_verified',
        'is_incognito_mode_enabled',
        'ban_id',
        'deactivation_id',
        'deletion_id',
        'was_online_at',
        'last_activity_at',
        'attached_moderator_id',
        'moderation_moderator_id',
        'moderation_claimed_at',
        'moderation_forwarded_to',
        'moderation_forwarded_by',
        'moderation_forwarded_at',
        'ethnicity_id',
        'country_id', // denormalization
        'subdivision_level_1_id', // denormalization
        'subdivision_level_2_id', // denormalization
        'subdivision_level_3_id', // denormalization
        'locality_id',
        'native_country_id', // denormalization
        'native_subdivision_level_1_id', // denormalization
        'native_subdivision_level_2_id', // denormalization
        'native_subdivision_level_3_id', // denormalization
        'native_locality_id',
        'time_zone_id',
        'time_zone_name',
        'date_time_format',
        'length_unit',
        'weight_unit',
        'profile_headline',
        'height',
        'weight_from',
        'weight_to',
        'body_type',
        'hair_color',
        'eye_color',
        'facial_hair',
        'smoking',
        'alcohol_consumption',
        'self_description',
        'hobbies_and_interests',
        'sport',
        'sport_exercises_frequency',
        'marital_status',
        'child_count',
        'children_in_household',
        'living_with_parents',
        'desired_number_of_children',
        'children_adoption',
        'housing_type',
        'relocation',
        'education',
        'profession_or_occupation',
        'employment',
        'job_title',
        'financial_status',
        'religion',
        'future_spouse_age_from',
        'future_spouse_age_to',
        'future_spouse_height_from',
        'future_spouse_height_to',
        'future_spouse_weight_from',
        'future_spouse_weight_to',
        'future_spouse_body_type',
        'future_spouse_character_traits',
        'future_spouse_description',
        'future_spouse_children',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'phone_number_verified_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'email_verified_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'password_updated_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'registered_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'completed_registration_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'updated_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'language_id' => 'integer',
        'is_test_user' => 'boolean',
        'is_protected' => 'boolean',
        'is_hidden' => 'boolean',
        'is_verified' => 'boolean',
        'is_incognito_mode_enabled' => 'boolean',
        'reputation' => 'integer',
        'reputation_updated_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'internal_reputation' => 'integer',
        'internal_reputation_updated_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'coins' => 'integer',
        'coins_updated_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'status_updated_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'profile_completion_percent' => 'integer',
        'profile_completion_percent_updated_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'required_fields_filled' => 'boolean',
        'required_fields_checked_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'main_photo_id' => 'integer',
        'notification_settings' => 'json',
        'ban_id' => 'integer',
        'deactivation_id' => 'integer',
        'deletion_id' => 'integer',
        'was_online_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'last_activity_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'attached_moderator_id' => 'integer',
        'moderation_moderator_id' => 'integer',
        'moderation_claimed_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'moderation_forwarded_to' => 'integer',
        'moderation_forwarded_by' => 'integer',
        'moderation_forwarded_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'birthday' => 'datetime:Y-m-d',
        'ethnicity_id' => 'integer',
        'country_id' => 'integer',
        'subdivision_level_1_id' => 'integer',
        'subdivision_level_2_id' => 'integer',
        'subdivision_level_3_id' => 'integer',
        'locality_id' => 'integer',
        'native_country_id' => 'integer',
        'native_subdivision_level_1_id' => 'integer',
        'native_subdivision_level_2_id' => 'integer',
        'native_subdivision_level_3_id' => 'integer',
        'native_locality_id' => 'integer',
        'time_zone_id' => 'integer',
        'height' => 'integer',
        'weight_from' => 'integer',
        'weight_to' => 'integer',
        'child_count' => 'integer',
        'children_in_household' => 'integer',
        'future_spouse_age_from' => 'integer',
        'future_spouse_age_to' => 'integer',
        'future_spouse_height_from' => 'integer',
        'future_spouse_height_to' => 'integer',
        'future_spouse_weight_from' => 'integer',
        'future_spouse_weight_to' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function getName(): string
    {
        if ($this->status === self::STATUS_ACCEPTED) {
            return $this->name;
        }

        return  $this->last_accepted_name ?? __('user-x', ['userId' => $this->id]);
    }

    public function getAge(): int
    {
        return Carbon::today()->diffInYears($this->birthday);
    }

    public function getPresenceStatus(): string
    {
        $lastOnlineTime = Carbon::parse($this->was_online_at);
        $diff = Carbon::now()->diffInSeconds($lastOnlineTime);

        $offlineTimeout = config('profile.offline_timeout');

        if ($diff > $offlineTimeout) {
            return self::PRESENCE_STATUS_OFFLINE;
        }

        return self::PRESENCE_STATUS_ONLINE;
    }

    public function getCountryName(): ?string
    {
        return optional($this->country, fn (Country $country) => $country->getTranslation()->name);
    }

    public function getLocalityName(): ?string
    {
        return optional($this->locality, fn (Locality $locality) => $locality->getTranslation()->name);
    }

    public function getEthnicityName(): ?string
    {
        return optional($this->ethnicity, function (Ethnicity $ethnicity) {
            $nameColumn = $this->sex === self::SEX_MALE ? 'male_name' : 'female_name';

            return $ethnicity->getTranslation()->$nameColumn ?? $ethnicity->getTranslation()->plural_name;
        });
    }

    public static function getStatusOptions(): array
    {
        return [
            self::STATUS_NOT_MODERATED => __('profile:status.not-moderated'),
            self::STATUS_PENDING_MODERATION => __('profile:status.pending-moderation'),
            self::STATUS_ACCEPTED => __('profile:status.accepted'),
            self::STATUS_REJECTED => __('profile:status.rejected'),
            self::STATUS_BANNED => __('profile:status.banned'),
            self::STATUS_DEACTIVATED => __('profile:status.deactivated'),
            self::STATUS_PENDING_DELETION => __('profile:status.pending-deletion'),
            self::STATUS_DELETED => __('profile:status.deleted'),
        ];
    }

    public static function getBodyTypeOptions(): array
    {
        return [
            self::BODY_TYPE_SKINNY,
            self::BODY_TYPE_SLIM,
            self::BODY_TYPE_NORMAL,
            self::BODY_TYPE_ATHLETIC,
            self::BODY_TYPE_PLUMP,
        ];
    }

    public static function getHairColorOptions(): array
    {
        return [
            self::HAIR_COLOR_BLACK,
            self::HAIR_COLOR_LIGHT_BROWN,
            self::HAIR_COLOR_DARK_BROWN,
            self::HAIR_COLOR_BLOND,
            self::HAIR_COLOR_GINGER,
            self::HAIR_COLOR_GRAY,
            self::HAIR_COLOR_BALD,
        ];
    }

    public static function getEyeColorOptions(): array
    {
        return [
            self::EYE_COLOR_BROWN,
            self::EYE_COLOR_BLUE,
            self::EYE_COLOR_GREEN,
            self::EYE_COLOR_GRAY,
            self::EYE_COLOR_BLACK,
            self::EYE_COLOR_AMBER,
            self::EYE_COLOR_HAZEL,
            self::EYE_COLOR_HETEROCHROMIA,
            self::EYE_COLOR_OTHER,
        ];
    }

    public static function getFacialHairOptions(): array
    {
        return [
            self::FACIAL_HAIR_BEARD,
            self::FACIAL_HAIR_MUSTACHE,
            self::FACIAL_HAIR_BEARD_AND_MUSTACHE,
            self::FACIAL_HAIR_STUBBLE,
            self::FACIAL_HAIR_NO,
            self::FACIAL_HAIR_CAN_NOT_DUE_WORK,
            self::FACIAL_HAIR_NOT_GROWING,
        ];
    }

    public static function getSportExercisesFrequencyOptions(): array
    {
        return [
            self::SPORT_EXERCISES_FREQUENCY_DO_NOT_DO,
            self::SPORT_EXERCISES_FREQUENCY_OCCASIONALLY,
            self::SPORT_EXERCISES_FREQUENCY_REGULAR,
        ];
    }

    public static function getSmokingOptions(): array
    {
        return [
            self::SMOKING_NO,
            self::SMOKING_YES,
            self::SMOKING_SOMETIMES,
        ];
    }

    public static function getAlcoholConsumptionOptions(): array
    {
        return [
            self::ALCOHOL_CONSUMPTION_NO,
            self::ALCOHOL_CONSUMPTION_YES,
            self::ALCOHOL_CONSUMPTION_SOMETIMES,
        ];
    }

    public static function getLivingWithParentsOptions(): array
    {
        return [
            self::LIVING_WITH_PARENT_YES,
            self::LIVING_WITH_PARENT_NO,
            self::LIVING_WITH_PARENT_OTHER,
        ];
    }

    public static function getDesiredNumberOfChildrenOptions(): array
    {
        return [
            self::DESIRED_NUMBER_OF_CHILDREN_NOT_PLANNED,
            self::DESIRED_NUMBER_OF_CHILDREN_WANT_ONE_CHILD,
            self::DESIRED_NUMBER_OF_CHILDREN_WANT_TWO_CHILDREN,
            self::DESIRED_NUMBER_OF_CHILDREN_WANT_THREE_CHILDREN,
            self::DESIRED_NUMBER_OF_CHILDREN_WANT_MANY_CHILDREN,
            self::DESIRED_NUMBER_OF_CHILDREN_NOT_SURE,
        ];
    }

    public static function getChildrenAdoptionOptions(): array
    {
        return [
            self::CHILDREN_ADOPTION_POSSIBLE,
            self::CHILDREN_ADOPTION_NOT_POSSIBLE,
            self::CHILDREN_ADOPTION_NOT_SURE,
        ];
    }

    public static function getHousingTypeOptions(): array
    {
        return [
            self::HOUSING_TYPE_OWN_APARTMENT,
            self::HOUSING_TYPE_OWN_HOUSE,
            self::HOUSING_TYPE_RENTAL_APARTMENT,
            self::HOUSING_TYPE_RENTAL_HOUSE,
            self::HOUSING_TYPE_DORMITORY,
            self::HOUSING_TYPE_OTHER,
        ];
    }

    public static function getRelocationOptions(): array
    {
        return [
            self::RELOCATION_POSSIBLE,
            self::RELOCATION_WITHIN_CITY_OR_LOCALITY,
            self::RELOCATION_WITHIN_REGION,
            self::RELOCATION_WITHIN_COUNTRY,
            self::RELOCATION_NOT_POSSIBLE,
            self::RELOCATION_NOT_SURE,
        ];
    }

    public static function getEducationOptions(): array
    {
        return [
            self::EDUCATION_PRIMARY,
            self::EDUCATION_SECONDARY,
            self::EDUCATION_SECONDARY_NON_COMPLETED,
            self::EDUCATION_VOCATIONAL_COLLEGE,
            self::EDUCATION_HIGHER,
            self::EDUCATION_HIGHER_NON_COMPLETED,
            self::EDUCATION_SEVERAL_HIGHER,
            self::EDUCATION_DONT_HAVE_EDUCATION,
        ];
    }

    public static function getEmploymentOptions(): array
    {
        return [
            self::EMPLOYMENT_FULL,
            self::EMPLOYMENT_PARTIAL,
            self::EMPLOYMENT_NOT_WORKING,
            self::EMPLOYMENT_SELF_EMPLOYMENT,
            self::EMPLOYMENT_RETIRED,
            self::EMPLOYMENT_HOUSEWIFE,
            self::EMPLOYMENT_OTHER,
        ];
    }

    public static function getFinancialStatusOptions(): array
    {
        return [
            self::FINANCIAL_STATUS_DO_NOT_HAVE_REGULAR_INCOME,
            self::FINANCIAL_STATUS_LIVING_WAGE,
            self::FINANCIAL_STATUS_AVERAGE,
            self::FINANCIAL_STATUS_RICH,
        ];
    }

    public static function getReligionOptions(): array
    {
        return [
            self::RELIGION_ISLAM,
            self::RELIGION_CHRISTIANITY,
            self::RELIGION_JUDAISM,
            self::RELIGION_OTHER,
        ];
    }

    public static function getFutureSpouseChildrenOptions(): array
    {
        return [
            self::FUTURE_SPOUSE_CHILDREN_POSSIBLE_WITH_CHILDREN,
            self::FUTURE_SPOUSE_CHILDREN_WITHOUT_CHILDREN,
            self::FUTURE_SPOUSE_CHILDREN_NOT_SURE,
        ];
    }

    public function isPhoneNumberVerified(): bool
    {
        return $this->phone_number_verified_at !== null;
    }

    public function isEmailVerified(): bool
    {
        return $this->email_verified_at !== null;
    }

    public function language(): BelongsTo
    {
        return $this->belongsTo(Language::class, 'language_id');
    }

    public function ethnicity(): BelongsTo
    {
        return $this->belongsTo(Ethnicity::class, 'ethnicity_id');
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function locality(): BelongsTo
    {
        return $this->belongsTo(Locality::class, 'locality_id');
    }

    public function mainPhoto(): BelongsTo
    {
        return $this->belongsTo(UserPhoto::class, 'main_photo_id');
    }

    public function photos(): HasMany
    {
        return $this->hasMany(UserPhoto::class, 'user_id');
    }

    public function ban(): BelongsTo
    {
        return $this->belongsTo(UserBan::class, 'ban_id');
    }

    public function deactivation(): BelongsTo
    {
        return $this->belongsTo(UserDeactivation::class, 'deactivation_id');
    }

    public function deletion(): BelongsTo
    {
        return $this->belongsTo(UserDeletion::class, 'deletion_id');
    }

    public function knownLanguages(): BelongsToMany
    {
        return $this->belongsToMany(Language::class, 'user_known_languages', 'user_id', 'language_id');
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'roles_users', 'user_id', 'role_id');
    }

    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class, 'permissions_users', 'user_id', 'permission_id');
    }

    public function favoriteProfiles(): BelongsToMany
    {
        return $this->belongsToMany(self::class, 'favorite_profiles', 'user_id', 'target_user_id');
    }

    public function likedMeUsers(): BelongsToMany
    {
        return $this->belongsToMany(self::class, 'profile_likes', 'target_user_id', 'user_id');
    }

    public function likedUsers(): BelongsToMany
    {
        return $this->belongsToMany(self::class, 'profile_likes', 'user_id', 'target_user_id');
    }

    public function notificationSettings(): NotificationSettings
    {
        return new NotificationSettings($this->notification_settings ?? []);
    }

    public function connectedAccounts(): HasMany
    {
        return $this->hasMany(ConnectedAccount::class, 'user_id');
    }

    public function preferredLocale(): ?string
    {
        return $this->language_code;
    }

    public function routeNotificationForMail(): ?string
    {
        return $this->email;
    }
}
