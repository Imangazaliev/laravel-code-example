<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class ProfileView extends Model
{
    protected $table = 'profile_views';

    protected $fillable = [
        'visitor_id',
        'viewed_profile_id',
        'viewed_at',
        'is_incognito',
        'seen_by_owner_at',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'viewed_profile_id' => 'integer',
        'viewed_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'is_incognito' => 'boolean',
        'seen_by_owner_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function visitor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'visitor_id');
    }

    public function viewedProfile(): BelongsTo
    {
        return $this->belongsTo(User::class, 'viewed_profile_id');
    }
}
