<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ExampleProject\Models\Traits\Translatable;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Currency extends Model
{
    use Translatable;

    protected $table = 'currencies';

    protected $fillable = [
        'code',
        'code_numeric',
        'english_display_name',
        'name_single',
        'name_plural',
        'symbol',
        'decimal_digits',
        'thousand_delimiter',
        'decimal_separator',
    ];

    protected $casts = [
        'decimal_digits' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function getTranslationModelInfo(): array
    {
        return [CurrencyText::class, 'currency_id'];
    }
}
