<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class ChatMessageStatusChange extends Model
{
    protected $table = 'chat_message_status_changes';

    protected $fillable = [
        'message_id',
        'message_author_id',
        'new_status',
        'rejection_reasons',
        'comment',
        'comment_for_user',
        'changed_at',
        'changed_by',
        'seen_by_user_at',
    ];

    protected $casts = [
        'message_id' => 'integer',
        'message_author_id' => 'integer',
        'rejection_reasons' => 'json',
        'changed_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'changed_by' => 'integer',
        'seen_by_user_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
    ];

    public $timestamps = false;

    public function moderationSession(): BelongsTo
    {
        return $this->belongsTo(ModerationSession::class, 'moderation_session_id');
    }

    public function message(): BelongsTo
    {
        return $this->belongsTo(ChatMessage::class, 'message_id');
    }

    public function messageAuthor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'message_author_id');
    }

    public function admin(): BelongsTo
    {
        return $this->belongsTo(User::class, 'changed_by');
    }

    public function getRejectionReasonsStr(): string
    {
        $rejectionReasons = array_only(ChatMessageModeration::getRejectionReasons(), $this->rejection_reasons);

        $rejectionReasons = array_map('mb_strtolower', $rejectionReasons);

        return implode(', ', $rejectionReasons);
    }
}
