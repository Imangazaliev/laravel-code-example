<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use ExampleProject\Models\Traits\Translatable;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Language extends Model
{
    use Translatable;

    public const SCRIPT_DIRECTION_LTR = 'ltr';
    public const SCRIPT_DIRECTION_RTL = 'rtl';

    protected $table = 'languages';

    protected $fillable = [
        'code',
        'code_iso_639_1',
        'code_iso_639_2b',
        'code_iso_639_2t',
        'code_iso_639_3',
        'locale',
        'script_direction',
        'native_name',
        'english_name',
        'fallback_language_id',
        'level',
        'is_active',
    ];

    protected $casts = [
        'fallback_language_id' => 'integer',
        'level' => 'integer',
        'is_active' => 'boolean',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function getTranslationModelInfo(): array
    {
        return [LanguageText::class, 'language_entity_id'];
    }

    public function isActive(): bool
    {
        return $this->is_active;
    }

    public function isRtl(): bool
    {
        return $this->script_direction === self::SCRIPT_DIRECTION_RTL;
    }

    public function fallbackLanguage(): BelongsTo
    {
        return $this->belongsTo(self::class, 'fallback_language_id');
    }
}
