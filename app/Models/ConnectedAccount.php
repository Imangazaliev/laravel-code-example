<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 * @method $this withTrashed()
 * @method $this onlyTrashed()
 * @method $this withoutTrashed()
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class ConnectedAccount extends Model
{
    protected $table = 'connected_accounts';

    protected $fillable = [
        'user_id',
        'provider_code',
        'provider_user_id',
        'data',
        'added_at',
        'disconnected_at',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'data' => 'json',
        'added_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'disconnected_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
