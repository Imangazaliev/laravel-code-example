<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class UserDeactivation extends Model
{
    protected $table = 'user_deactivations';

    protected $fillable = [
        'user_id',
        'comment',
        'deactivated_at',
        'deactivated_by',
        'reactivated_at',
        'reactivated_by',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'deactivated_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'deactivated_by' => 'integer',
        'reactivated_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'reactivated_by' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function deactivatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'deactivated_by');
    }

    public function reactivatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'reactivated_by');
    }
}
