<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class UserAuthentication extends Model
{
    public const PLATFORM_WEB = 'web';
    public const PLATFORM_ANDROID = 'android';
    public const PLATFORM_IOS = 'ios';
    public const PLATFORM_UNKNOWN = 'unknown';

    protected $table = 'user_authentications';

    protected $fillable = [
        'user_id',
        'admin_id', // if an admin authenticated under a user
        'platform',
        'auth_key',
        'user_agent',
        'browser',
        'operating_system',
        'browser_fingerprint',
        'mobile_app_device_id',
        'authentication_provider_code',
        'authentication_provider_user_id',
        'logged_in_at',
        'login_ip',
        'login_ip_country_code',
        'was_online_at',
        'logged_out_at',
        'logout_ip',
        'logout_ip_country_code',
        'forced_logout_at',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'admin_id' => 'integer',
        'logged_in_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'was_online_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'logged_out_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'forced_logout_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;
}
