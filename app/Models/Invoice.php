<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Invoice extends Model
{
    public const STATUS_NEW = 'new';
    public const STATUS_SUCCESS = 'success';
    public const STATUS_FAILED = 'failed';
    public const STATUS_EXPIRED = 'expired';

    protected $table = 'invoices';

    protected $fillable = [
        'user_id',
        'coins_amount',
        'payment_gateway',
        'price',
        'currency_code',
        'currency_rate',
        'status',
        'created_at',
        'finished_at',
        'token',
        'payment_gateway_transaction_id',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'coins_amount' => 'integer',
        'price' => 'float',
        'currency_rate' => 'float',
        'created_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'finished_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
    ];

    protected $dateFormat = DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}
