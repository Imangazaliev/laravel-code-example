<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class BadWord extends Model
{
    protected $table = 'bad_words';

    protected $fillable = [
        'language_id',
        'text',
        'description',
        'is_enabled',
    ];

    protected $casts = [
        'language_id' => 'integer',
        'is_enabled' => 'boolean',
    ];

    public $timestamps = false;
}
