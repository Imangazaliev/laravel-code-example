<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ExampleProject\Models\Traits\Translatable;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Ethnicity extends Model
{
    use Translatable;

    protected $table = 'ethnicities';

    protected $fillable = [
        'native_name',
        'english_name',
        'show_in_list',
    ];

    protected $casts = [
        'show_in_list' => 'boolean',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function getTranslationModelInfo(): array
    {
        return [EthnicityText::class, 'ethnicity_id'];
    }
}
