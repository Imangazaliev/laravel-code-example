<?php

declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 * @method $this whereUser(int $userId)
 * @method $this whereActive()
 * @method $this whereFinished()
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Chat extends Model
{
    protected $table = 'chats';

    protected $fillable = [
        'chat_request_id',
        'requestor_id', // denormalization
        'target_user_id', // denormalization
        'created_at',
        'last_activity_at',
        'language_id',
        'attached_moderator_id',
        'finished_at',
        'finished_by',
        'is_finished_automatically',
        'moderation_moderator_id',
        'moderation_claimed_at',
        'moderation_forwarded_to',
        'moderation_forwarded_by',
        'moderation_forwarded_at',
    ];

    protected $casts = [
        'chat_request_id' => 'integer',
        'requestor_id' => 'integer',
        'target_user_id' => 'integer',
        'created_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'last_activity_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'language_id' => 'integer',
        'attached_moderator_id' => 'integer',
        'finished_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'finished_by' => 'integer',
        'is_finished_automatically' => 'boolean',
        'moderation_moderator_id' => 'integer',
        'moderation_claimed_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'moderation_forwarded_to' => 'integer',
        'moderation_forwarded_by' => 'integer',
        'moderation_forwarded_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function scopeWhereUser(Builder $query, int $userId): void
    {
        $query
            ->where(function (Builder $query) use ($userId) {
                $query
                    ->where('requestor_id', $userId)
                    ->orWhere('target_user_id', $userId)
                ;
            })
        ;
    }

    public function scopeWhereActive(Builder $query): void
    {
        $query->whereNull('finished_at');
    }

    public function scopeWhereFinished(Builder $query): void
    {
        $query->whereNotNull('finished_at');
    }

    public function requestor(): BelongsTo
    {
        return $this->belongsTo(User::class, 'requestor_id');
    }

    public function targetUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'target_user_id');
    }

    public function chatRequest(): BelongsTo
    {
        return $this->belongsTo(ChatRequest::class, 'chat_request_id');
    }

    public function messages(): HasMany
    {
        return $this->hasMany(ChatMessage::class, 'chat_id');
    }

    public function finishedBy(): BelongsTo
    {
        return $this->belongsTo(ChatRequest::class, 'finished_by');
    }

    public function finish(): void
    {
        $this->update([
            'finished_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'finished_by' => current_user()->id,
            'is_finished_automatically' => false,
        ]);
    }

    public function isFinished(): bool
    {
        return $this->finished_at !== null;
    }
}
