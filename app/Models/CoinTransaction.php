<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class CoinTransaction extends Model
{
    public const TYPE_REGISTRATION_GIFT = 'registration-gift';
    public const TYPE_BALANCE_RECHARGE = 'balance-recharge';
    public const TYPE_GIFT_TO_ANOTHER_USER = 'gift-to-another-user';
    public const TYPE_GIFT_FROM_ANOTHER_USER = 'gift-from-another-user';
    public const TYPE_CHANGE_BY_ADMINISTRATION = 'change-by-administration';
    public const TYPE_CHAT_REQUEST = 'chat-request';
    public const TYPE_CHAT_REQUEST_ACCEPTANCE = 'chat-request-acceptance';
    public const TYPE_CHAT_REQUEST_CANCELLATION = 'chat-request-cancellation';
    public const TYPE_CHAT_REQUEST_REJECTION = 'chat-request-rejection';
    public const TYPE_CHAT_REQUEST_AUTO_CANCELLATION = 'chat-request-auto-cancellation';
    public const TYPE_CHAT_MESSAGES = 'chat-messages';
    public const TYPE_CHAT_FINISHED_TOO_FAST = 'chat-finished-too-fast';

    protected $table = 'coin_transactions';

    protected $fillable = [
        'type',
        'user_id',
        'amount',
        'description',
        'balance_before',
        'balance_after',
        'transaction_time',
        'invoice_id',
        'transfer_source_transaction_id',
        'admin_id',
        'admin_comment',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'amount' => 'integer',
        'balance_before' => 'integer',
        'balance_after' => 'integer',
        'transaction_time' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'invoice_id' => 'integer',
        'transfer_source_transaction_id' => 'integer',
        'admin_id' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function invoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class, 'invoice_id');
    }

    public function transferSourceTransaction(): BelongsTo
    {
        return $this->belongsTo(self::class, 'transfer_source_transaction_id');
    }

    public function admin(): BelongsTo
    {
        return $this->belongsTo(User::class, 'admin_id');
    }
}
