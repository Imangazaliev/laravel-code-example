<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class PhoneNumberChangeRequest extends Model
{
    public const STATUS_NEW = 'new';
    public const STATUS_VERIFIED = 'verified';
    public const STATUS_CANCELED = 'canceled';

    protected $table = 'phone_number_change_requests';

    protected $fillable = [
        'user_id',
        'current_phone_number',
        'current_phone_number_country_code',
        'new_phone_number',
        'new_phone_number_country_code',
        'code',
        'status',
        'status_updated_at',
        'created_at',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'status_updated_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'created_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $incrementing = false;

    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
