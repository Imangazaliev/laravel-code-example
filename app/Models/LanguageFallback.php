<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class LanguageFallback extends Model
{
    protected $table = 'language_fallbacks';

    protected $fillable = [
        'language_id',
        'fallback_language_id',
        'level',
    ];

    protected $casts = [
        'language_id' => 'integer',
        'fallback_language_id' => 'integer',
        'level' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function language(): BelongsTo
    {
        return $this->belongsTo(Language::class, 'language_id');
    }

    public function fallbackLanguage(): BelongsTo
    {
        return $this->belongsTo(Language::class, 'fallback_language_id');
    }
}
