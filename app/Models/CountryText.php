<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int|string $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class CountryText extends Model
{
    protected $table = 'country_texts';

    protected $fillable = [
        'country_id',
        'language_id',
        'name',
        'name_official',
    ];

    protected $casts = [
        'country_id' => 'integer',
        'language_id' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;
}
