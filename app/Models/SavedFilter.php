<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class SavedFilter extends Model
{
    protected $table = 'saved_filters';

    protected $fillable = [
        'user_id',
        'name',
        'search_parameters',
        'notifications_enabled',
        'created_at',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'search_parameters' => 'json',
        'notifications_enabled' => 'boolean',
        'created_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
    ];

    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
