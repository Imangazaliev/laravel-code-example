<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class ChatMessage extends Model
{
    public const STATUS_ACCEPTED = 'accepted';
    public const STATUS_CANCELED = 'canceled';
    public const STATUS_PENDING_MODERATION = 'pending-moderation';
    public const STATUS_REJECTED_BY_MODERATOR = 'rejected-by-moderator';
    public const DIRECTION_INCOMING = 'incoming';
    public const DIRECTION_OUTGOING = 'outgoing';

    protected $table = 'chat_messages';

    protected $fillable = [
        'chat_id',
        'user_id',
        'peer_id',
        'replied_message_id',
        'replied_message_text',
        'text',
        'sent_at',
        'seen_at',
        'status',
        'status_updated_at',
        'auto_rejection_reasons',
        'transaction_id',
        'auto_detected_language_id',
    ];

    protected $casts = [
        'chat_id' => 'integer',
        'user_id' => 'integer',
        'replied_message_id' => 'integer',
        'sent_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'seen_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'status_updated_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'auto_rejection_reasons' => 'json',
        'transaction_id' => 'integer',
        'auto_detected_language_id' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function chat(): BelongsTo
    {
        return $this->belongsTo(Chat::class, 'chat_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function peer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'peer_id');
    }

    public function repliedMessage(): BelongsTo
    {
        return $this->belongsTo(self::class, 'replied_message_id');
    }

    public function replies(): HasMany
    {
        return $this->hasMany(self::class, 'replied_message_id');
    }

    public function moderations(): HasMany
    {
        return $this->hasMany(ChatMessageModeration::class, 'message_id');
    }

    public function statusChanges(): HasMany
    {
        return $this->hasMany(ChatMessageStatusChange::class, 'message_id');
    }
}
