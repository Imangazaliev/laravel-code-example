<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ExampleProject\Models\Traits\Translatable;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Locality extends Model
{
    use Translatable;

    protected $table = 'localities';

    protected $fillable = [
        'native_name',
        'english_name',
        'type',
        'country_id',
        'country_code',
        'subdivision_level_1_id',
        'subdivision_level_1_code',
        'subdivision_level_2_id',
        'subdivision_level_2_code',
        'subdivision_level_3_id',
        'subdivision_level_3_code',
        'geoname_id',
        'is_capital',
        'population',
        'latitude',
        'longitude',
    ];

    protected $casts = [
        'country_id' => 'integer',
        'subdivision_id' => 'integer',
        'geoname_id' => 'integer',
        'is_capital' => 'boolean',
        'population' => 'integer',
        'latitude' => 'float',
        'longitude' => 'float',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function getTranslationModelInfo(): array
    {
        return [LocalityText::class, 'locality_id'];
    }
}
