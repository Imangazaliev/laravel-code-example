<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\PhotoAccessGate\PhotoAccessGate;
use App\Services\UserPhotoUploader\UploaderInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletingScope;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 * @method $this withTrashed()
 * @method $this onlyTrashed()
 * @method $this withoutTrashed()
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class UserPhoto extends Model
{
    public const STATUS_PENDING_MODERATION = 'pending-moderation';
    public const STATUS_ACCEPTED = 'accepted';
    public const STATUS_REJECTED = 'rejected';
    public const STATUS_DELETED = 'deleted';
    public const SIZE_100 = 100;
    public const SIZE_200 = 200;
    public const SIZE_300 = 300;
    public const SIZE_400 = 400;

    protected $table = 'user_photos';

    protected $fillable = [
        'user_id',
        'original_file_name',
        'original_file_name_transliterated',
        'width',
        'height',
        'size',
        'exif_data',
        'sha256_hash',
        'caption',
        'crop_area',
        'preview_area',
        'rotation_angle',
        'storage_type',
        'original_image_file_name',
        'original_image_path',
        'file_name',
        'path',
        'blurred_image_file_name',
        'blurred_image_path',
        'uploaded_at',
        'is_being_processed',
        'thumbnails',
        'deleted_at',
        'status',
        'status_updated_at',
        'moderation_moderator_id',
        'moderation_claimed_at',
        'moderation_forwarded_to',
        'moderation_forwarded_by',
        'moderation_forwarded_at',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'width' => 'integer',
        'height' => 'integer',
        'size' => 'integer',
        'exif_data' => 'json',
        'crop_area' => 'json',
        'preview_area' => 'json',
        'rotation_angle' => 'integer',
        'is_being_processed' => 'boolean',
        'uploaded_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'thumbnails' => 'json',
        'deleted_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'status_updated_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'moderation_moderator_id' => 'integer',
        'moderation_claimed_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'moderation_forwarded_to' => 'integer',
        'moderation_forwarded_by' => 'integer',
        'moderation_forwarded_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    protected static function boot(): void
    {
        parent::boot();

        static::addGlobalScope(new SoftDeletingScope());
    }

    /**
     * Get the fully qualified "deleted_at" column (used in SoftDeletingScope).
     *
     * @return string
     */
    public function getQualifiedDeletedAtColumn(): string
    {
        return $this->qualifyColumn('deleted_at');
    }

    public function getImagePath(bool $blurred = false, ?int $size = null)
    {
        if ($this->thumbnails !== null && $size !== null) {
            $thumbnails = $blurred ? $this->thumbnails['blurred'] : $this->thumbnails['normal'];

            // if we have a thumbnail then return it
            // elsewhere return the full size image
            if (array_key_exists($size, $thumbnails)) {
                return $thumbnails[$size];
            }
        }

        return $blurred ? $this->blurred_image_path : $this->path;
    }

    public function getImageUrl(bool $blurred = false, ?int $size = null): string
    {
        /**
         * @var UploaderInterface $uploader
         */
        $uploader = app(UploaderInterface::class);

        return $uploader->getPhotoUrl($this->storage_type, $this->getImagePath($blurred, $size));
    }

    public function getImageUrlWithCheck(?int $size = null): string
    {
        /**
         * @var PhotoAccessGate $photoAccessGate
         */
        $photoAccessGate = app(PhotoAccessGate::class);

        $currentUser = current_user();

        $canViewPhoto = (
            $currentUser !== null &&
            (
                $currentUser->can('view-original-photos') ||
                $photoAccessGate->canViewPhotos(current_user()->id, $this->user_id)
            )
        );

        return $this->getImageUrl( ! $canViewPhoto, $size);
    }

    public function getOriginalImageUrl(): string
    {
        /**
         * @var UploaderInterface $uploader
         */
        $uploader = app(UploaderInterface::class);

        return $uploader->getPhotoUrl($this->storage_type, $this->original_image_path);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function moderatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'moderator_id');
    }
}
