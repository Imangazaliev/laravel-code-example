<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EthnicityText extends Model
{
    protected $table = 'ethnicity_texts';

    protected $fillable = [
        'ethnicity_id',
        'language_id',
        'plural_name',
        'female_name',
        'male_name',
    ];

    protected $casts = [
        'ethnicity_id' => 'integer',
        'language_id' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;
}
