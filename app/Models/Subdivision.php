<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use ExampleProject\Models\Traits\Translatable;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Subdivision extends Model
{
    use Translatable;

    protected $table = 'subdivisions';

    protected $fillable = [
        'code',
        'native_name',
        'english_name',
        'type',
        'country_id',
        'country_code',
        'parent_subdivision_id',
        'parent_subdivision_code',
        'has_child_subdivisions',
        'latitude',
        'longitude',
    ];

    protected $casts = [
        'country_id' => 'number',
        'parent_subdivision_id' => 'number',
        'has_child_subdivisions' => 'boolean',
        'latitude' => 'float',
        'longitude' => 'float',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;

    public function getTranslationModelInfo(): array
    {
        return [SubdivisionText::class, 'subdivision_id'];
    }
}
