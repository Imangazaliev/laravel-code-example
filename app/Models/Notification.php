<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Notification extends Model
{
    public const TYPE_NEW_MESSAGES = 'new-messages';
    public const TYPE_PHONE_NUMBER_CHANGED = 'phone-number-changed';
    public const TYPE_EMAIL_CHANGED = 'email-changed';
    public const TYPE_PASSWORD_CHANGED = 'password-changed';
    public const TYPE_PROFILE_BANNED = 'profile-banned';
    public const TYPE_PROFILE_UNBANNED = 'profile-unbanned';
    public const TYPE_PROFILE_DEACTIVATED = 'profile-deactivated';
    public const TYPE_PROFILE_REACTIVATED = 'profile-reactivated';
    public const TYPE_REQUESTED_PROFILE_DELETION = 'requested-profile-deletion';
    public const TYPE_PROFILE_DELETION_CANCELED = 'profile-deletion-canceled';
    public const TYPE_PROFILE_DELETED = 'profile-deleted';

    protected $table = 'notifications';

    protected $fillable = [
        'user_id',
        'type',
        'title',
        'body',
        'parameters',
        'context',
        'additional_data',
        'is_hidden',
        'created_at',
        'sent_at',
        'seen_at',
    ];

    protected $casts = [
        'user_id' => 'integer',
        'parameters' => 'json',
        'additional_data' => 'json',
        'is_hidden' => 'boolean',
        'created_at' => DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_CASTING,
        'notified_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
        'seen_at' => DATE_TIME_WITH_TIME_ZONE_CASTING,
    ];

    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
