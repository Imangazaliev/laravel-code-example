<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method $this with(array|string $relations)
 * @method $this findOrFail(int $id, array $columns = [])
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class LanguageText extends Model
{
    protected $table = 'language_texts';

    protected $fillable = [
        'language_entity_id',
        'language_id',
        'name',
    ];

    protected $casts = [
        'language_entity_id' => 'integer',
        'language_id' => 'integer',
    ];

    protected $dateFormat = DATE_TIME_WITH_TIME_ZONE_FORMAT;

    public $timestamps = false;
}
