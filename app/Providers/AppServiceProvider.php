<?php

declare(strict_types=1);

namespace App\Providers;

use App\Exceptions\Handler as ExceptionHandler;
use App\Models\Language;
use App\Models\Notification;
use App\Services\Api\ExceptionHandler as ApiExceptionHandler;
use Illuminate\Contracts\Debug\ExceptionHandler as ExceptionHandlerContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->registerDefaultLanguage();
        $this->registerApiExceptionHandler();
    }

    private function registerDefaultLanguage(): void
    {
        $this->app->singleton('default_language', function () {
            if (language()->code === config('app.fallback_locale')) {
                return language();
            }

            return (new Language())
                ->where('code', config('app.fallback_locale'))
                ->firstOrFail()
            ;
        });
    }

    private function registerApiExceptionHandler(): void
    {
        $this->app->bind(ExceptionHandlerContract::class, function () {
            $isApiRequest = starts_with(request()->path(), 'api/');

            return $this->app->make($isApiRequest ? ApiExceptionHandler::class : ExceptionHandler::class);
        });
    }

    public function boot(): void
    {
        Notification::created(function (Notification $notification) {
            cache_info()->newNotificationCount($notification->user_id)->forget();
        });
    }
}
