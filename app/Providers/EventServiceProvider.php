<?php

declare(strict_types=1);

namespace App\Providers;

use App\Events\ChatFinished;
use App\Events\MessageStatusChanged;
use App\Events\PhotoStatusChanged;
use App\Listeners\CheckAdditionalServicesAccessWhenPermissionsUpdated;
use App\Listeners\CheckShouldSendNotification;
use App\Listeners\ClearPermissionsCache;
use App\Listeners\HandleChatFinished;
use App\Listeners\HandleMessageStatusChanged;
use App\Listeners\SetMainPhoto;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Notifications\Events\NotificationSending;
use ExampleProject\Permissions\Events\PermissionsUpdated;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NotificationSending::class => [
            CheckShouldSendNotification::class,
        ],
        ChatFinished::class => [
            HandleChatFinished::class,
        ],
        MessageStatusChanged::class => [
            HandleMessageStatusChanged::class,
        ],
        PermissionsUpdated::class => [
            // must be the first
            ClearPermissionsCache::class,
        ],
        PhotoStatusChanged::class => [
            SetMainPhoto::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        parent::boot();

        //
    }
}
