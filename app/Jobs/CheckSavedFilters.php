<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\ProfileModeration;
use App\Models\SavedFilter;
use App\Models\User;
use App\Notifications\Notification;
use App\Services\NotificationDelivery\NotificationDelivery;
use App\Services\Search\SearchParameters;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckSavedFilters implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    public function handle(): void
    {
        $simpleParameters = SearchParameters::getSimpleFields();

        $simpleParameters = array_map(function (int|string $key, $value) {
            return SearchParameters::expandParameter($key, $value);
        }, array_keys($simpleParameters), $simpleParameters);

        $arrayParameters = SearchParameters::getArrayFields();

        $arrayParameters = array_map(function (int|string $key, $value) {
            return SearchParameters::expandParameter($key, $value);
        }, array_keys($arrayParameters), $arrayParameters);

        $user = (new User())
            ->with([
                'ethnicity' => function (BelongsTo $query) {
                    $query->select('id');
                    $query->withTranslation(null, ['plural_name', 'male_name', 'female_name']);
                },
                'country' => function (BelongsTo $query) {
                    $query->select('id');
                    $query->withTranslation(null, ['name']);
                },
                'locality' => function (BelongsTo $query) {
                    $query->select('id');
                    $query->withTranslation(null, ['name']);
                },
            ])
            ->findOrFail($this->userId, [
                'id',
                'name',
                'last_accepted_name',
                'sex',
                'birthday',
                'status',
                'ethnicity_id',
                'country_id',
                'locality_id',
                ...array_column($simpleParameters, 'fieldName'),
                ...array_column($arrayParameters, 'fieldName'),
            ])
        ;

        if ($user->status !== User::STATUS_ACCEPTED) {
            return;
        }

        $acceptModerationCount = (new ProfileModeration())
            ->where('user_id', $user->id)
            ->where('status', ProfileModeration::STATUS_ACCEPTED)
            ->count()
        ;

        $acceptStatusChangeCount = (new ProfileModeration())
            ->where('user_id', $user->id)
            ->where('status', ProfileModeration::STATUS_ACCEPTED)
            ->count()
        ;

        // if a profile was accepted before don't do anything
        if ($acceptModerationCount + $acceptStatusChangeCount > 1) {
            return;
        }

        $query = (new SavedFilter())
            ->with('user:id')
            ->where('search_parameters->sex', $user->sex)
            ->where('notifications_enabled', true)
        ;

        $this->addSimpleParameter($query, 'age_from', $user->getAge(), '<=');
        $this->addSimpleParameter($query, 'age_to', $user->getAge(), '>=');

        foreach ($simpleParameters as $parameter) {
            [
                'paramName' => $paramName,
                'fieldName' => $fieldName,
            ] = $parameter;

            $this->addSimpleParameter($query, $paramName, $user->$fieldName);
        }

        foreach ($arrayParameters as $parameter) {
            [
                'paramName' => $paramName,
                'fieldName' => $fieldName,
                'not' => $not,
            ] = $parameter;

            $this->addArrayParameter($query, $paramName, $user->$fieldName, $not);
        }

        $savedFilters = $query->get(['user_id', 'name']);

        $userUrl = url('/user/' .  $user->id);

        foreach ($savedFilters as $filter) {
            $parameters = [
                'age' => $user->getAge(),
                'country' => $user->getCountryName(),
                'ethnicity' => $user->getEthnicityName(),
                'filterName' => $filter->name,
                'locality' => $user->getLocalityName(),
                'name' => $user->last_accepted_name ?? $user->name,
                'userId' => $user->id,
                'userUrl' => $userUrl,
            ];

            $filter->user->notify(new Notification(
                '',
                'notifications:saved-filter-match',
                $parameters,
                null,
                [NotificationDelivery::CHANNEL_TELEGRAM],
            ));
        }
    }

    private function addSimpleParameter(Builder $query, string $name, $value, string $operator = '='): void
    {
        $query->where(function (Builder $query) use ($name, $value, $operator) {
            if (is_int($value)) {
                $fieldName = DB::raw(sprintf('("search_parameters"->>\'%s\')::int', $name));
            } else {
                $fieldName = "search_parameters->{$name}";
            }

            $query->where($fieldName, $operator, $value)->orWhereNull($fieldName);
        });
    }

    private function addArrayParameter(Builder $query, string $name, $value, bool $not = false): void
    {
        $query->where(function (Builder $query) use ($name, $value, $not) {
            $fieldName = "search_parameters->{$name}";

            if ($not) {
                $query->whereJsonDoesntContain($fieldName, $value);
            } else {
                $query->whereJsonContains($fieldName, $value);
            }

            $query->orWhereJsonLength($fieldName, 0);
        });
    }
}
