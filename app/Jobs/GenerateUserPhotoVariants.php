<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\UserPhoto;
use App\Services\UserPhotoUploader\UploadPathGenerator;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Image;
use Imagick;
use ExampleProject\FileStorage\StorageFactoryInterface;
use RuntimeException;

class GenerateUserPhotoVariants implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private const MAX_IMAGE_SIZE = 1024;

    public int $timeout = 120;

    private int $photoId;

    private ?string $tempDirectory = null;

    public function __construct(int $photoId)
    {
        $this->photoId = $photoId;
    }

    public function handle(StorageFactoryInterface $storageFactory): void
    {
        $this->tempDirectory = sys_get_temp_dir() . '/example-project-user-photo';

        $photo = (new UserPhoto())
            ->withTrashed()
            ->findOrFail($this->photoId, [
                'id',
                'crop_area',
                'preview_area',
                'storage_type',
                'original_image_file_name',
                'original_image_path',
                'file_name',
                'path',
                'blurred_image_file_name',
                'blurred_image_path',
                'rotation_angle',
            ])
        ;

        // create a temporary directory
        if (
            ! file_exists($this->tempDirectory)
            && ! mkdir($this->tempDirectory, 0755, true)
            && ! is_dir($this->tempDirectory)
        ) {
            throw new RuntimeException(sprintf('Can not create a directory "%s"', $this->tempDirectory));
        }

        $originalImagePath = $this->generateTempFilePath();
        $processedImagePath = $this->generateTempFilePath();
        $blurredImagePath = $this->generateTempFilePath();

        $storage = $storageFactory->make($photo->storage_type);

        $storage->getFile($photo->original_image_path, $originalImagePath);

        $this->resizeImage($originalImagePath);
        $this->processImage($originalImagePath, $processedImagePath, $photo->rotation_angle, $photo->crop_area);
        $this->blurImage($processedImagePath, $blurredImagePath);

        $thumbnails = $this->generateThumbnails($processedImagePath, $photo->preview_area);

        $processedImageUploadPath = UploadPathGenerator::generateUploadPath();
        $blurredImageUploadPath = UploadPathGenerator::generateUploadPath();

        $storage->putFile($processedImagePath, $processedImageUploadPath);
        $storage->putFile($blurredImagePath, $blurredImageUploadPath);

        $uploadedThumbnails = [
            'normal' => [],
            'blurred' => [],
        ];

        foreach ($thumbnails['normal'] as $size => $thumbnailPath) {
            $normalImageBasename = basename($processedImageUploadPath, '.jpg');
            $thumbnailUploadPath = sprintf('%s/%s@%d.jpg', dirname($processedImageUploadPath), $normalImageBasename, $size);

            $blurredImageBasename = basename($blurredImageUploadPath, '.jpg');
            $blurredThumbnailUploadPath = sprintf('%s/%s@%d.jpg', dirname($blurredImageUploadPath), $blurredImageBasename, $size);

            $storage->putFile($thumbnailPath, $thumbnailUploadPath);
            $storage->putFile($thumbnails['blurred'][$size], $blurredThumbnailUploadPath);

            $uploadedThumbnails['normal'][$size] = $thumbnailUploadPath;
            $uploadedThumbnails['blurred'][$size] = $blurredThumbnailUploadPath;
        }

        $photo->update([
            'file_name' => basename($processedImageUploadPath),
            'path' => $processedImageUploadPath,
            'blurred_image_file_name' => basename($blurredImageUploadPath),
            'blurred_image_path' => $blurredImageUploadPath,
            'is_being_processed' => false,
            'thumbnails' => $uploadedThumbnails,
        ]);

        // delete locally generated images
        unlink($originalImagePath);
        unlink($processedImagePath);
        unlink($blurredImagePath);
    }

    private function generateTempFilePath(): string
    {
        return sprintf('%s/%s.jpg', $this->tempDirectory, UploadPathGenerator::generateUploadName());
    }

    private function resizeImage(string $originalImagePath): void
    {
        $image = Image::make($originalImagePath);

        // rotate image according "Orientation" EXIF setting
        $image->orientate();

        $width = $image->width();
        $height = $image->height();

        if ($width <= self::MAX_IMAGE_SIZE && $height <= self::MAX_IMAGE_SIZE) {
            return;
        }

        if ($width === $height || $width > $height) {
            $image->widen(self::MAX_IMAGE_SIZE);
        } else {
            $image->heighten(self::MAX_IMAGE_SIZE);
        }

        $image->save($originalImagePath);
    }

    private function processImage(string $originalImagePath, string $processedImagePath, int $rotationAngle, ?array $cropArea): void
    {
        $image = new Imagick($originalImagePath);

        $this->deleteExifData($image);

        if ($rotationAngle !== 0) {
            $image->rotateImage('#fff', $rotationAngle);
        }

        if ($cropArea !== null) {
            [$x, $y, $width, $height] = $cropArea;

            $image->cropImage($width, $height, $x, $y);
        }

        $image->setCompression(Imagick::COMPRESSION_JPEG);
        $image->setCompressionQuality(80);
        // save as progressive JPEG
        $image->setImageInterlaceScheme(Imagick::INTERLACE_PLANE);
        $image->writeImage($processedImagePath);
    }

    private function deleteExifData(Imagick $image): void
    {
        $exifPropertyNames = array_keys($image->getImageProperties('exif:*'));

        foreach ($exifPropertyNames as $propertyName) {
            $image->deleteImageProperty($propertyName);
        }
    }

    private function blurImage(string $filePath, string $blurredImageFilePath): void
    {
        $image = new Imagick($filePath);

        $image->blurImage(120, 60);
        $image->writeImage($blurredImageFilePath);
    }

    private function generateThumbnails(string $normalImagePath, ?array $previewArea = null): array
    {
        $result = [
            'normal' => [],
            'blurred' => [],
        ];

        $croppedImagePath = $this->generateTempFilePath();
        $blurredImagePath = $this->generateTempFilePath();

        $this->cropImage($normalImagePath, $croppedImagePath, $previewArea);

        $croppedImageBasename = basename($croppedImagePath, '.jpg');
        $blurredImageBasename = basename($blurredImagePath, '.jpg');

        $thumbnailSizes = config('profile.photo.thumbnail_sizes');

        foreach ($thumbnailSizes as $size) {
            $thumbnailPath = sprintf('%s/%s@%d.jpg', dirname($croppedImagePath), $croppedImageBasename, $size);
            $blurredThumbnailPath = sprintf('%s/%s@%d.jpg', dirname($blurredImagePath), $blurredImageBasename, $size);

            Image::make($croppedImagePath)->widen($size)->save($thumbnailPath);

            $this->blurImage($thumbnailPath, $blurredThumbnailPath);

            $result['normal'][$size] = $thumbnailPath;
            $result['blurred'][$size] = $blurredThumbnailPath;
        }

        return $result;
    }

    private function cropImage(string $imagePath, string $croppedImagePath, ?array $previewArea = null): void
    {
        $image = new Imagick($imagePath);

        if ($previewArea !== null) {
            [$x, $y, $cropSize] = $previewArea;
        } else {
            $width = $image->getImageWidth();
            $height = $image->getImageHeight();

            $cropSize = min($width, $height);

            // crop from center
            $x = $cropSize === $width ? 0 : (int) round(($width - $cropSize) / 2);
            $y = $cropSize === $height ? 0 : (int) round(($width - $cropSize) / 2);
        }

        $image->cropImage($cropSize, $cropSize, $x, $y);

        $image->writeImage($croppedImagePath);
    }
}
