<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\User;
use App\Notifications\Notification;
use App\Services\NotificationDelivery\NotificationDelivery;
use App\Services\Profile\ProfileDuplicatesDetector;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyAboutProfileDuplicates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    public function handle(ProfileDuplicatesDetector $duplicatesDetector): void
    {
        $duplicatesIds = $duplicatesDetector->getDuplicates($this->userId)['fingerprint_and_ip'];

        if (count($duplicatesIds) === 0) {
            return;
        }

        $user = (new User())->find($this->userId, ['id', 'name', 'birthday']);
        $duplicates = (new User())->findMany($duplicatesIds, ['id', 'name', 'birthday', 'status'])->sortBy('id');

        $admins = (new User())
            ->whereJsonContains(
                'notification_settings->notification_types',
                NotificationDelivery::TYPE_PROFILE_DUPLICATES,
            )
            ->get(['id', 'notification_settings'])
        ;

        $body = 'notifications:profile-duplicates-detected.body';
        $duplicateLines = [];

        $statusOptions = User::getStatusOptions();

        foreach ($duplicates as $duplicate) {
            $duplicateUrl = url('/admin/users/' .  $duplicate['id']);

            $duplicateLines[] = sprintf(
                '%s, %d ([#%s](%s)) - %s',
                $duplicate['name'],
                $duplicate->getAge(),
                $duplicate['id'],
                $duplicateUrl,
                $statusOptions[$duplicate->status],
            );
        }

        $userUrl = url('/admin/users/' .  $user->id);

        $parameters = [
            'userInfo' => sprintf('[#%d](%s)', $user->id, $userUrl),
            'userId' => $user->id,
            'userName' => $user->name,
            'userAge' => $user->getAge(),
            'duplicates' => implode("\n", $duplicateLines),
        ];

        $channel[] = NotificationDelivery::CHANNEL_TELEGRAM;

        foreach ($admins as $admin) {
            if ( ! $admin->can('view-profile-duplicates')) {
                continue;
            }

            $admin->notify(new Notification(
                '',
                $body,
                $parameters,
                null,
                $channel,
            ));
        }
    }
}
