<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\SmsLogRecord;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use ExampleProject\SmsApi\Exceptions\SmsSendFailedException;
use ExampleProject\SmsApi\SmsClientInterface;

class SendMessageToPhoneNumber implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     */
    public int $tries = 1;

    private string $phoneNumber;

    private string $text;

    public function __construct(string $phoneNumber, string $text)
    {
        $this->phoneNumber = $phoneNumber;
        $this->text = $text;
    }

    public function handle(SmsClientInterface $smsClient): void
    {
        try {
            $smsClient->sendMessage($this->phoneNumber, $this->text);

            $this->logSms(SmsLogRecord::RESULT_SUCCESS);
        } catch (SmsSendFailedException $exception) {
            $this->logSms(SmsLogRecord::RESULT_FAILED);
        }
    }

    private function logSms(string $result): void
    {
        (new SmsLogRecord())->create([
            'phone_number' => $this->phoneNumber,
            'text' => $this->text,
            'sent_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'result' => $result,
        ]);
    }
}
