<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\User;
use App\Services\RatingCalculator\ReputationCalculator;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CalculateUserReputation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    public function handle(ReputationCalculator $reputationCalculator): void
    {
        $user = (new User())->findOrFail($this->userId, ['id']);

        $reputation = $reputationCalculator->calculateReputation($user->id);

        $user->update([
            'reputation' => $reputation,
            'reputation_updated_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);
    }
}
