<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Models\BadWord;
use App\Models\ChatMessage;
use App\Models\User;
use App\Notifications\Notification;
use App\Services\NotificationDelivery\NotificationDelivery;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckChatMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $messageId;

    public function __construct(int $messageId)
    {
        $this->messageId = $messageId;
    }

    public function handle(): void
    {
        $message = (new ChatMessage())
            ->with('user:id,name')
            ->find($this->messageId, ['id', 'chat_id', 'user_id', 'text'])
        ;

        $admins = (new User())
            ->whereJsonContains(
                'notification_settings->notification_types',
                NotificationDelivery::TYPE_SUSPICIOUS_MESSAGES,
            )
            ->get(['id', 'language_code', 'notification_settings'])
        ;

        $normalizeWords = function (array $words) {
            $words = array_map(function (string $word) {
                return mb_strtolower(preg_replace('/[^\p{L}\p{N}]/u', '', $word));
            }, $words);

            // filter empty strings
            return array_filter($words);
        };

        $badWords = cache_info()->badWords()->remember(function () use ($normalizeWords) {
            $words = (new BadWord())
                ->where('is_enabled', true)
                ->pluck('text')
                ->all()
            ;

            return $normalizeWords($words);
        });

        $words = preg_split('/[\s]/', $message->text, -1, PREG_SPLIT_NO_EMPTY);

        $words = $normalizeWords($words);

        $foundBadWords = array_intersect($words, $badWords);

        if (count($foundBadWords) === 0) {
            return;
        }

        $userUrl = url('/admin/users/' .  $message->user->id);
        $chatUrl = url('/admin/chat/' .  $message->chat_id);

        $parameters = [
            'userInfo' => sprintf('%s ([#%d](%s))', $message->user->name, $message->user->id, $userUrl),
            'chatInfo' => sprintf('[#%d](%s)', $message->chat_id, $chatUrl),
            'text' => $message->text,
        ];

        $channel[] = NotificationDelivery::CHANNEL_TELEGRAM;

        foreach ($admins as $admin) {
            if ( ! $admin->can('view-suspicious-messages')) {
                continue;
            }

            $admin->notify(new Notification(
                '',
                'notifications:bad-words-in-message.body',
                $parameters,
                null,
                $channel,
            ));
        }
    }
}
