<?php

declare(strict_types=1);

namespace App\Jobs\Notifications;

use App\Models\ProfileModeration;
use App\Models\ProfileStatusChange;
use App\Models\User;
use App\Notifications\Notification;
use App\Services\NotificationDelivery\NotificationDelivery;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyAboutProfileStatusChange implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $statusChangeId;

    public function __construct(int $statusChangeId)
    {
        $this->statusChangeId = $statusChangeId;
    }

    public function handle(): void
    {
        $moderation = (new ProfileStatusChange())
            ->with('user:id,notification_settings')
            ->findOrFail($this->statusChangeId)
        ;

        /**
         * @var User $user
         */
        $user = $moderation->user;

        if ( ! $user->notificationSettings()->isNotificationTypeEnabled(NotificationDelivery::TYPE_PROFILE_MODERATION)) {
            return;
        }

        $isAccepted = $moderation->new_status === ProfileModeration::STATUS_ACCEPTED;

        $title = $isAccepted ? 'notifications:profile-accepted.title' : 'notifications:profile-rejected.title';
        $body = $isAccepted ? 'notifications:profile-accepted.body' : 'notifications:profile-rejected.body';
        $parameters = $isAccepted ? [] : ['rejectionReasons' => $moderation->getRejectionReasonsStr()];

        $user->notify(new Notification(
            $title,
            $body,
            $parameters,
        ));
    }
}
