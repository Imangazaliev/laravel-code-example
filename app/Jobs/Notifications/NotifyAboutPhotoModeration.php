<?php

declare(strict_types=1);

namespace App\Jobs\Notifications;

use App\Models\PhotoModeration;
use App\Models\User;
use App\Notifications\Notification;
use App\Services\NotificationDelivery\NotificationDelivery;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyAboutPhotoModeration implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $moderationId;

    public function __construct(int $moderationId)
    {
        $this->moderationId = $moderationId;
    }

    public function handle(): void
    {
        $moderation = (new PhotoModeration())
            ->with('photoOwner:id,notification_settings')
            ->findOrFail($this->moderationId)
        ;

        /**
         * @var User $user
         */
        $user = $moderation->photoOwner;

        if ( ! $user->notificationSettings()->isNotificationTypeEnabled(NotificationDelivery::TYPE_PHOTO_MODERATION)) {
            return;
        }

        $isAccepted = $moderation->status === PhotoModeration::STATUS_ACCEPTED;

        $title = $isAccepted ? 'notifications:photo-accepted.title' : 'notifications:photo-rejected.title';
        $body = $isAccepted ? 'notifications:photo-accepted.body' : 'notifications:photo-rejected.body';
        $parameters = $isAccepted ? ['photoId' => $moderation->photo_id] : ['photoId' => $moderation->photo_id, 'rejectionReasons' => $moderation->getRejectionReasonsStr()];

        $user->notify(new Notification(
            $title,
            $body,
            $parameters,
        ));
    }
}
