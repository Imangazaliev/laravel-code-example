<?php

declare(strict_types=1);

namespace App\Jobs\Notifications;

use App\Models\ProfileModeration;
use App\Models\User;
use App\Notifications\Notification;
use App\Services\NotificationDelivery\NotificationDelivery;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyAboutProfileModeration implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $moderationId;

    public function __construct(int $moderationId)
    {
        $this->moderationId = $moderationId;
    }

    public function handle(): void
    {
        $moderation = (new ProfileModeration())
            ->with('user:id,notification_settings')
            ->findOrFail($this->moderationId)
        ;

        /**
         * @var User $user
         */
        $user = $moderation->user;

        if ( ! $user->notificationSettings()->isNotificationTypeEnabled(NotificationDelivery::TYPE_PROFILE_MODERATION)) {
            return;
        }

        $isAccepted = $moderation->status === ProfileModeration::STATUS_ACCEPTED;

        $title = $isAccepted ? 'notifications:profile-accepted.title' : 'notifications:profile-rejected.title';
        $body = $isAccepted ? 'notifications:profile-accepted.body' : 'notifications:profile-rejected.body';
        $parameters = $isAccepted ? [] : ['rejectionReasons' => $moderation->getRejectionReasonsStr()];

        $user->notify(new Notification(
            $title,
            $body,
            $parameters,
        ));
    }
}
