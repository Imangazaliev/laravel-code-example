<?php

declare(strict_types=1);

namespace App\Jobs\Notifications;

use App\Models\PhotoModeration;
use App\Models\PhotoStatusChange;
use App\Models\User;
use App\Notifications\Notification;
use App\Services\NotificationDelivery\NotificationDelivery;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyAboutPhotoStatusChange implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $statusChangeId;

    public function __construct(int $statusChangeId)
    {
        $this->statusChangeId = $statusChangeId;
    }

    public function handle(): void
    {
        $statusChange = (new PhotoStatusChange())
            ->with('photoOwner:id,notification_settings')
            ->findOrFail($this->statusChangeId)
        ;

        /**
         * @var User $user
         */
        $user = $statusChange->photoOwner;

        if ( ! $user->notificationSettings()->isNotificationTypeEnabled(NotificationDelivery::TYPE_PHOTO_MODERATION)) {
            return;
        }

        $isAccepted = $statusChange->new_status === PhotoModeration::STATUS_ACCEPTED;

        $title = $isAccepted ? 'notifications:photo-accepted.title' : 'notifications:photo-rejected.title';
        $body = $isAccepted ? 'notifications:photo-accepted.body' : 'notifications:photo-rejected.body';
        $parameters = $isAccepted ? ['photoId' => $statusChange->photo_id] : ['photoId' => $statusChange->photo_id, 'rejectionReasons' => $statusChange->getRejectionReasonsStr()];

        $user->notify(new Notification(
            $title,
            $body,
            $parameters,
        ));
    }
}
