<?php

declare(strict_types=1);

namespace App\Jobs\Notifications;

use App\Models\Chat;
use App\Models\ChatMessage;
use App\Notifications\Notification;
use App\Services\NotificationDelivery\NotificationDelivery;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyAboutNewMessages implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $messageId;

    public function __construct(int $messageId)
    {
        $this->messageId = $messageId;
    }

    public function handle(): void
    {
        $message = (new ChatMessage())
            ->with('chat:id,finished_at')
            ->findOrFail($this->messageId, ['id', 'chat_id', 'user_id', 'peer_id', 'sent_at'])
        ;

        /**
         * @var Chat $chat
         */
        $chat = $message->chat;

        if ($chat->isFinished()) {
            return;
        }

        $newMessages = $chat->messages()
            ->where('user_id', $message->user_id)
            ->where('status', ChatMessage::STATUS_ACCEPTED)
            ->whereNull('seen_at')
            ->selectRaw(implode(', ', [
                'COUNT(*) count',
                'COUNT(*) FILTER (WHERE sent_at > ?) sent_after_count',
            ]), [
                $message->sent_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ])
            ->firstOrFail()
            ->toArray()
        ;

        if ($newMessages['count'] === 0 || $newMessages['sent_after_count'] > 0) {
            return;
        }

        $peer = $message->peer()->firstOrFail(['id', 'notification_settings']);

        if ( ! $peer->notificationSettings()->isNotificationTypeEnabled(NotificationDelivery::TYPE_NEW_MESSAGES)) {
            return;
        }

        $peer->notify(new Notification(
            'notifications:new-messages.title',
            'notifications:new-messages.body',
            [
                'messageCount' => $newMessages['count'],
            ],
        ));
    }
}
