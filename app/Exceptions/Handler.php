<?php

declare(strict_types=1);

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Sentry\State\Scope;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Throwable $exception
     *
     * @throws Throwable
     */
    public function report(Throwable $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            $sentry = app('sentry');

            $sentry->configureScope(function (Scope $scope): void {
                $currentUser = current_user();

                $scope->setUser([
                    'user' => $currentUser !== null ? [
                        'id' => $currentUser->id,
                        'name' => $currentUser->name,
                    ] : null,
                ]);
            });

            $sentry->captureException($exception);
        }

        parent::report($exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        $redirectTo = $exception->redirectTo();

        if ($redirectTo !== null) {
            return $redirectTo;
        }

        $guard = $exception->guards()[0] ?? null;

        return redirect()->to('/login');
    }
}
