<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Throwable;

class ProfileBannedException extends Exception
{
    public function __construct($message = 'Profile banned.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
