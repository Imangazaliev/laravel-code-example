<?php

declare(strict_types=1);

namespace App\Queries;

use App\Models\ProfileModeration;
use App\Models\ProfileStatusChange;
use App\Models\User;

class UserLastStatusQuery
{
    public static function get(int $userId): ?string
    {
        $lastModeration = (new ProfileModeration())
            ->where('user_id', $userId)
            ->whereIn('status', [ProfileModeration::STATUS_ACCEPTED, ProfileModeration::STATUS_REJECTED])
            ->orderBy('moderated_at', 'desc')
            ->first(['status', 'moderated_at'])
        ;

        if ($lastModeration === null) {
            return null;
        }

        $lastStatusChange = (new ProfileStatusChange())
            ->where('user_id', $userId)
            ->whereIn('new_status', [ProfileModeration::STATUS_ACCEPTED, ProfileModeration::STATUS_REJECTED])
            ->where('changed_at', '>', $lastModeration->moderated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
            ->orderBy('changed_at', 'desc')
            ->first(['new_status'])
        ;

        $lastStatus = $lastStatusChange !== null ? $lastStatusChange->new_status : $lastModeration->status;

        $statusMap = [
            ProfileModeration::STATUS_ACCEPTED => User::STATUS_ACCEPTED,
            ProfileModeration::STATUS_REJECTED => User::STATUS_REJECTED,
        ];

        return $statusMap[$lastStatus];
    }
}
