<?php

declare(strict_types=1);

namespace App\Transformers;

use App\Models\SupportChatMessage;
use RuntimeException;

class SupportChatElementsTransformer
{
    public const ELEMENT_TYPE_MESSAGE = 'message';

    public static function transformMessage(?SupportChatMessage $message, int $chatUserId, int $userId): ?array
    {
        if ($message === null) {
            return null;
        }

        $getMessageDirection = function (int $messageAuthorId) use ($chatUserId, $userId): string {
            if ($chatUserId === $userId) {
                return $messageAuthorId !== $userId ? SupportChatMessage::DIRECTION_INCOMING : SupportChatMessage::DIRECTION_OUTGOING;
            }

            return $messageAuthorId === $chatUserId ? SupportChatMessage::DIRECTION_INCOMING : SupportChatMessage::DIRECTION_OUTGOING;
        };

        $repliedMessage = $message->repliedMessage;

        return [
            'id' => $message->id,
            'type' => self::ELEMENT_TYPE_MESSAGE,
            'text' => $message->text,
            'direction' => $getMessageDirection($message->user_id),
            'sent_at' => $message->sent_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            'seen_at' => $message->seen_at === null ? null : $message->seen_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'replied_message' => $repliedMessage === null ? null : [
                'id' => $repliedMessage->id,
                'text' => $repliedMessage->text,
                'direction' => $getMessageDirection($repliedMessage->user_id),
            ],
        ];
    }

    public static function getDate(array $transformedChatElement): string
    {
        if ($transformedChatElement['type'] === self::ELEMENT_TYPE_MESSAGE) {
            return $transformedChatElement['sent_at'];
        }

        throw new RuntimeException(sprintf('Unknown element type "%s"', $transformedChatElement['type']));
    }
}
