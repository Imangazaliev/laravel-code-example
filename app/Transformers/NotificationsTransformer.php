<?php

declare(strict_types=1);

namespace App\Transformers;

use App\Models\ChatMessage;
use App\Models\ChatMessageModeration;
use App\Models\ChatMessageStatusChange;
use App\Models\Notification;
use App\Models\PhotoModeration;
use App\Models\PhotoStatusChange;
use App\Models\ProfileModeration;
use App\Models\ProfileStatusChange;
use App\Models\User;

class NotificationsTransformer
{
    public const TYPE_PROFILE_MODERATION = 'profile-moderation';
    public const TYPE_PHOTO_MODERATION = 'photo-moderation';
    public const TYPE_CHAT_MESSAGE_MODERATION = 'chat-message-moderation';
    public const TYPE_PROFILE_STATUS_CHANGE = 'profile-status-change';
    public const TYPE_PHOTO_STATUS_CHANGE = 'photo-status-change';
    public const TYPE_CHAT_MESSAGE_STATUS_CHANGE = 'chat-message-status-change';
    public const TYPE_NOTIFICATION = 'notification';

    public static function transformProfileModeration(ProfileModeration $moderation): array
    {
        $isAccepted = $moderation->status === ProfileModeration::STATUS_ACCEPTED;

        $color = $isAccepted ? 'success' : 'error';
        $title = $isAccepted ? __('notifications:profile-accepted.title') : __('notifications:profile-rejected.title');
        $body = $isAccepted ? __('notifications:profile-accepted.body') : __('notifications:profile-rejected.body', ['rejectionReasons' => $moderation->getRejectionReasonsStr()]);

        return [
            'id' => $moderation->id,
            'type' => self::TYPE_PROFILE_MODERATION,
            'color' => $color,
            'title' => $title,
            'body' => $body,
            'comment' => $moderation->comment_for_user,
            'time' => $moderation->moderated_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'is_appealable' => $moderation->status === ProfileModeration::STATUS_REJECTED,
            'is_appealed' => $moderation->appealed_at !== null,
        ];
    }

    public static function transformPhotoModeration(PhotoModeration $moderation): array
    {
        $isAccepted = $moderation->status === PhotoModeration::STATUS_ACCEPTED;

        $color = $isAccepted ? 'success' : 'error';
        $title = $isAccepted ? __('notifications:photo-accepted.title') : __('notifications:photo-rejected.title');
        $body =
            $isAccepted ?
                __('notifications:photo-accepted.body', ['photoId' => $moderation->photo_id]) :
                __('notifications:photo-rejected.body', ['photoId' => $moderation->photo_id, 'rejectionReasons' => $moderation->getRejectionReasonsStr()])
        ;

        return [
            'id' => $moderation->id,
            'type' => self::TYPE_PHOTO_MODERATION,
            'color' => $color,
            'title' => $title,
            'body' => $body,
            'comment' => $moderation->comment_for_user,
            'time' => $moderation->moderated_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'is_appealable' => $moderation->status === PhotoModeration::STATUS_REJECTED,
            'is_appealed' => $moderation->appealed_at !== null,
        ];
    }

    public static function transformChatMessageModeration(ChatMessageModeration $moderation): array
    {
        $title = __('notifications:message-rejected.title');
        $body = __('notifications:message-rejected.body', [
            'messageText' => $moderation->message->text,
            'rejectionReasons' => $moderation->getRejectionReasonsStr(),
        ]);

        return [
            'id' => $moderation->id,
            'type' => self::TYPE_CHAT_MESSAGE_MODERATION,
            'color' => 'error',
            'title' => $title,
            'body' => $body,
            'comment' => $moderation->comment_for_user,
            'time' => $moderation->moderated_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'is_appealable' => true,
            'is_appealed' => $moderation->appealed_at !== null,
        ];
    }

    public static function transformProfileStatusChange(ProfileStatusChange $statusChange): array
    {
        $isAccepted = $statusChange->new_status === User::STATUS_ACCEPTED;

        $color = $isAccepted ? 'success' : 'error';
        $title = $isAccepted ? __('notifications:profile-accepted.title') : __('notifications:profile-rejected.title');
        $body = $isAccepted ? __('notifications:profile-accepted.body') : __('notifications:profile-rejected.body', [
            'rejectionReasons' => $statusChange->getRejectionReasonsStr(),
        ]);

        return [
            'id' => $statusChange->id,
            'type' => self::TYPE_PROFILE_STATUS_CHANGE,
            'color' => $color,
            'title' => $title,
            'body' => $body,
            'comment' => $statusChange->comment_for_user,
            'time' => $statusChange->changed_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ];
    }

    public static function transformPhotoStatusChange(PhotoStatusChange $statusChange): array
    {
        $isAccepted = $statusChange->new_status === PhotoModeration::STATUS_ACCEPTED;

        $color = $isAccepted ? 'success' : 'error';
        $title = $isAccepted ? __('notifications:photo-accepted.title') : __('notifications:photo-rejected.title');
        $body =
            $isAccepted ?
                __('notifications:photo-accepted.body', ['photoId' => $statusChange->photo_id]) :
                __('notifications:photo-rejected.body', ['photoId' => $statusChange->photo_id, 'rejectionReasons' => $statusChange->getRejectionReasonsStr()])
        ;

        return [
            'id' => $statusChange->id,
            'type' => self::TYPE_PHOTO_STATUS_CHANGE,
            'color' => $color,
            'title' => $title,
            'body' => $body,
            'comment' => $statusChange->comment_for_user,
            'time' => $statusChange->changed_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ];
    }

    public static function transformChatMessageStatusChange(ChatMessageStatusChange $statusChange): array
    {
        $isAccepted = $statusChange->new_status === ChatMessage::STATUS_ACCEPTED;

        $color = $isAccepted ? 'success' : 'error';
        $title = $isAccepted ? __('notifications:message-accepted.title') : __('notifications:message-rejected.title');
        $body = $isAccepted ?
            __('notifications:message-accepted.body', ['messageText' => $statusChange->message->text])
            : __('notifications:message-rejected.body', [
                'messageText' => $statusChange->message->text,
                'rejectionReasons' => $statusChange->getRejectionReasonsStr(),
            ])
        ;

        return [
            'id' => $statusChange->id,
            'type' => self::TYPE_CHAT_MESSAGE_STATUS_CHANGE,
            'color' => $color,
            'title' => $title,
            'body' => $body,
            'comment' => $statusChange->comment_for_user,
            'time' => $statusChange->changed_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ];
    }

    public static function transformNotification(Notification $notification): array
    {
        $notificationColor = [
            Notification::TYPE_CHAT_REQUEST_ACCEPTED => 'success',
            Notification::TYPE_CHAT_REQUEST_REJECTED => 'error',
        ];

        return [
            'id' => $notification->id,
            'type' => self::TYPE_NOTIFICATION,
            'color' => $notificationColor[$notification->type] ?? 'info',
            'title' => __($notification->title, $notification->parameters, [
                'context' => $notification->context,
            ]),
            'body' => __($notification->body, $notification->parameters, [
                'context' => $notification->context,
            ]),
            'time' => $notification->created_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ];
    }
}
