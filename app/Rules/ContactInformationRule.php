<?php

declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ContactInformationRule implements Rule
{
    public function passes($attribute, $value): bool
    {
        // do not allow digits
        if (preg_match('/\d+/', $value) === 1) {
            return false;
        }

        $forbiddenCharacters = [
            '@', '_',
        ];

        foreach ($forbiddenCharacters as $character) {
            if (str_contains($value, $character)) {
                return false;
            }
        }

        return true;
    }

    public function message()
    {
        return __('profile:dont-include-contact-information');
    }
}
