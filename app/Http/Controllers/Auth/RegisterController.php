<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ConfirmPhoneNumberRequest;
use App\Jobs\CalculateUserReputation;
use App\Jobs\SendMessageToPhoneNumber;
use App\Models\CoinTransaction;
use App\Models\PhoneNumberVerificationCode;
use App\Models\User;
use App\Rules\ContactInformationRule;
use App\Services\BanUsers\BanUsers;
use App\Services\BanUsers\UserBanDto;
use App\Services\Coins\Coins;
use App\Services\Coins\CoinTransactionDto;
use App\Services\PhoneNumberVerification\Exception\VerificationCodeExpiredException;
use App\Services\PhoneNumberVerification\Exception\VerificationCodeNotFoundException;
use App\Services\PhoneNumberVerification\PhoneNumberVerification;
use App\Services\RequiredFieldsChecker\RequiredFieldsChecker;
use App\Services\SessionKeys\SessionKeys;
use App\Services\UserAuthentications\UserAuthentications;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use ExampleProject\RateLimiter\RateLimiter;
use Propaganistas\LaravelPhone\PhoneNumber;
use Throwable;

class RegisterController extends Controller
{
    private const SEND_SMS_THROTTLE_KEY = 'send-phone-number-verification-code';

    public function __construct()
    {
        $this->middleware(['guest', 'recaptcha'])->only(['register']);

        $this->middleware('auth')->except(['register']);
    }

    public function register(Request $request, Coins $coins): void
    {
        if ( ! config('app.registration_open')) {
            abort(403, __('errors.registration-temporary-unavailable'));
        }

        $contactInformationRule = new ContactInformationRule();

        $minPasswordLength = config('auth.minimum_password_length');

        $this->validate($request, [
            'name' => ['required', 'string', 'max:32', $contactInformationRule],
            'sex' => ['required', 'string', Rule::in([User::SEX_MALE, User::SEX_FEMALE])],
            'birthday' => ['required', 'date_format:' . DATE_FORMAT],
            'phone_number' => ['required', 'string', 'regex:/' . E164_PHONE_NUMBER_REGEX . '/', 'unique:users,phone_number'],
            'phone_number_country_code' => ['required', 'exists:countries,code_alpha2'],
            'password' => ['required', 'string', 'min:' . $minPasswordLength],
            'fingerprint' => ['required', 'string'],
        ], [
            'phone_number.unique' => __('validation:custom.phone-number-taken'),
        ], [
            'name' => __('name_personal'),
            'phone_number' => __('phone-number'),
            'password' => __('password'),
            'sex' => __('profile:sex'),
            'birthday' => __('profile:birthday'),
        ]);

        $this->validate($request, [
            'phone_number' => ['phone:' . $request->get('phone_number_country_code')],
        ]);

        $currentTime = Carbon::now();

        $phoneNumberCountryCode = $request->get('phone_number_country_code');

        $phoneNumber = PhoneNumber::make($request->get('phone_number'), $phoneNumberCountryCode);
        $phoneNumberInNationalFormat = $phoneNumber->formatForMobileDialingInCountry($phoneNumberCountryCode);

        $this->getValidationFactory()->make([
            'phone_number' => $phoneNumberInNationalFormat,
        ], [
            'phone_number' => ['unique:users,phone_number_in_national_format'],
        ], [
            'phone_number.unique' => __('validation:custom.phone-number-taken'),
        ])->validate();

        DB::beginTransaction();

        try {
            $user = User::create([
                'name' => $request->get('name'),
                'phone_number' => $request->get('phone_number'),
                'phone_number_country_code' => $phoneNumberCountryCode,
                'phone_number_in_national_format' => $phoneNumberInNationalFormat,
                'password' => bcrypt($request->get('password')),
                'password_updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'language_id' => language()->id,
                'language_code' => language()->code,
                'sex' => $request->get('sex'),
                'registered_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'was_online_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'last_activity_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'birthday' => $request->get('birthday'),
                'status' => User::STATUS_NOT_MODERATED,
                'status_updated_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            $giftCoinTransactionDto = (new CoinTransactionDto())
                ->setType(CoinTransaction::TYPE_REGISTRATION_GIFT)
                ->setAmount(config('profile.registration_gift_coins_amount'))
                ->setUserId($user->id)
                ->setDescription('Registration gift coins')
            ;

            $coins->changeBalance($giftCoinTransactionDto);

            // load all fields including default values
            $user->refresh();

            auth()->guard('user')->login($user, true);

            $request->session()->put(SessionKeys::AUTH_KEY, str_random(64));

            UserAuthentications::logAuthentication(
                $request->header('User-Agent'),
                $request->get('fingerprint'),
                $request->ip(),
                $request->session()->get(SessionKeys::AUTH_KEY),
            );

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }

        $this->sendVerificationCode($user->id, $user->phone_number, $request->ip());
    }

    private function sendVerificationCode(int $userId, string $phoneNumber, string $ipAddress): void
    {
        $rateLimiter = $this->getRateLimiter();

        if (
            $rateLimiter->tooManyAttempts(self::SEND_SMS_THROTTLE_KEY, $userId) ||
            $rateLimiter->tooManyAttempts(self::SEND_SMS_THROTTLE_KEY, $phoneNumber) ||
            $rateLimiter->tooManyAttempts(self::SEND_SMS_THROTTLE_KEY, $ipAddress)
        ) {
            abort(429, __('auth:resend-verification-code.too-many-attempts'));
        }

        $verificationCode = (new PhoneNumberVerificationCode())
            ->where('phone_number', $phoneNumber)
            ->orderBy('created_at', 'desc')
            ->first()
        ;

        $phoneNumberVerification = app(PhoneNumberVerification::class);

        if ($verificationCode === null || $phoneNumberVerification->isCodeExpired($verificationCode)) {
            $verificationCode = $phoneNumberVerification->createCode($phoneNumber);
        }

        $this->dispatch(
            new SendMessageToPhoneNumber(
                $phoneNumber,
                __('auth:registration-message', [
                    'code' => $verificationCode->code,
                    'siteName' => config('app.name'),
                ]),
            ),
        );

        $rateLimiter->hit(self::SEND_SMS_THROTTLE_KEY, $userId);
        $rateLimiter->hit(self::SEND_SMS_THROTTLE_KEY, $phoneNumber);
        $rateLimiter->hit(self::SEND_SMS_THROTTLE_KEY, $ipAddress);
    }

    // TODO: throttle a phone number fix count
    public function fixPhoneNumber(Request $request): void
    {
        $this->validate($request, [
            'phone_number' => ['required', 'string', 'regex:/' . E164_PHONE_NUMBER_REGEX . '/', 'unique:users,phone_number'],
            'phone_number_country_code' => ['required', 'exists:countries,code_alpha2'],
        ], [
            'phone_number.unique' => __('validation:custom.phone-number-taken'),
        ]);

        $this->validate($request, [
            'phone_number' => ['phone:' . $request->get('phone_number_country_code')],
        ]);

        if (current_user()->completed_registration_at !== null) {
            abort(403);
        }

        if (current_user()->status !== User::STATUS_NOT_MODERATED) {
            abort(403);
        }

        $phoneNumberCountryCode = $request->get('phone_number_country_code');

        $phoneNumber = PhoneNumber::make($request->get('phone_number'), $phoneNumberCountryCode);
        $phoneNumberInNationalFormat = $phoneNumber->formatForMobileDialingInCountry($phoneNumberCountryCode);

        $this->getValidationFactory()->make([
            'phone_number' => $phoneNumberInNationalFormat,
        ], [
            'phone_number' => ['unique:users,phone_number_in_national_format'],
        ], [
            'phone_number.unique' => __('validation:custom.phone-number-taken'),
        ])->validate();

        current_user()->update([
            'phone_number' => $request->get('phone_number'),
            'phone_number_country_code' => $phoneNumberCountryCode,
            'phone_number_in_national_format' => $phoneNumberInNationalFormat,
            'updated_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);

        $this->sendVerificationCode(current_user()->id, current_user()->phone_number, $request->ip());
    }

    public function resendVerificationCode(Request $request): void
    {
        $this->sendVerificationCode(current_user()->id, current_user()->phone_number, $request->ip());
    }

    public function verificationCodeResendInfo(Request $request): array
    {
        $rateLimiter = $this->getRateLimiter();

        $availableIn = max(
            $rateLimiter->availableIn(self::SEND_SMS_THROTTLE_KEY, current_user()->id),
            $rateLimiter->availableIn(self::SEND_SMS_THROTTLE_KEY, current_user()->phone_number),
            $rateLimiter->availableIn(self::SEND_SMS_THROTTLE_KEY, $request->ip()),
        );

        return [
            'available_at' => $availableIn === 0 ? null : Carbon::now()->addSeconds($availableIn)->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ];
    }

    // TODO: throttle confirmation attempts
    public function confirmPhoneNumber(ConfirmPhoneNumberRequest $request, PhoneNumberVerification $phoneNumberVerification): void
    {
        $this->validate($request, [
            'code' => ['required'],
        ]);

        try {
            $phoneNumberVerification->confirmPhoneNumber(current_user(), $request->get('code'));

            $rateLimiter = $this->getRateLimiter();

            $rateLimiter->clear(self::SEND_SMS_THROTTLE_KEY, current_user()->id);
            $rateLimiter->clear(self::SEND_SMS_THROTTLE_KEY, current_user()->phone_number);
            $rateLimiter->clear(self::SEND_SMS_THROTTLE_KEY, $request->ip());
        } catch (VerificationCodeNotFoundException $exception) {
            throw ValidationException::withMessages([
                'code' => __('auth:verification-code.code-invalid-or-expired-error'),
            ]);
        } catch (VerificationCodeExpiredException $exception) {
            throw ValidationException::withMessages([
                'code' => __('auth:verification-code.code-invalid-or-expired-error'),
            ]);
        }
    }

    private function getRateLimiter(): RateLimiter
    {
        /**
         * @var RateLimiter $rateLimiter | null
         */
        $rateLimiter = null;

        if ($rateLimiter !== null) {
            return $rateLimiter;
        }

        $rateLimiter = app(RateLimiter::class);

        $rateLimiter->registerRateLimits(self::SEND_SMS_THROTTLE_KEY, [
            [1, config('auth.phone_number_verification.resend_interval')],
            [config('auth.phone_number_verification.max_attempts'), config('auth.phone_number_verification.decay_time') * 60],
            [config('auth.phone_number_verification.max_attempts') * 2, 60 * 60 * 24],
        ]);

        return $rateLimiter;
    }

    public function registrationCompleted(): void
    {
        // do not allow to mark that the registration is completed
        // if a user is not verified the phone number
        // TODO: add a security warning because this case is not possible in normal way
        if ( ! current_user()->isPhoneNumberVerified()) {
            abort(403);
        }

        $banUsers = new BanUsers();

        $banReasons = $banUsers->getBanReasons(current_user());

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            current_user()->update([
                'completed_registration_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);

            if (count($banReasons) > 0) {
                $ban = (new UserBanDto())
                    ->setUserId(current_user()->id)
                    ->setReasons($banReasons)
                ;

                $banUsers->ban($ban);

                DB::commit();

                return;
            }

            $requiredFieldsFilled = RequiredFieldsChecker::checkRequiredFieldsFilled(current_user());

            current_user()->update([
                'status' => $requiredFieldsFilled ? User::STATUS_PENDING_MODERATION : User::STATUS_NOT_MODERATED,
                'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                'required_fields_filled' => $requiredFieldsFilled,
                'required_fields_checked_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }

        $this->dispatch(new CalculateUserReputation(current_user()->id));
    }
}
