<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\NotifyAboutProfileDuplicates;
use App\Models\UserLoginToken;
use App\Services\SessionKeys\SessionKeys;
use App\Services\UserAuthentications\UserAuthentications;
use Carbon\Carbon;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /**
     * The maximum number of attempts to allow.
     *
     * @var int
     */
    private int $maxAttempts = 10;

    /**
     * The number of minutes to throttle for.
     *
     * @var int
     */
    private int $decayMinutes = 1;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @param Request $request
     *
     * @return array
     *
     * @throws ValidationException
     */
    public function login(Request $request): void
    {
        $this->validate($request, [
            'phone_number_or_email' => ['required', 'string'],
            'password' => ['required', 'string'],
            'by_token' => ['boolean'],
            'remember' => ['boolean'],
            'fingerprint' => ['required', 'string'],
        ]);

        // if user wants to login by Email
        if (str_contains($request->get('phone_number_or_email'), '@')) {
            $this->validate($request, [
                'phone_number_or_email' => 'email',
            ]);
        }

        // throttle login attempts
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            $this->sendLockoutResponse($request);
        }

        $loginByToken = $request->filled('by_token');

        if ($loginByToken) {
            [$loginResult, $adminId] = $this->loginByToken($request);
        } else {
            $loginResult = $this->loginByCredentials($request);
            $adminId = null;
        }

        if ($loginResult === false) {
            // Increment the number of attempts.
            // When this user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);

            throw ValidationException::withMessages([
                'phone_number_or_email' => [trans('auth.failed')],
            ]);
        }

        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        session()->put(SessionKeys::AUTH_KEY, str_random(64));

        UserAuthentications::logAuthentication(
            $request->header('User-Agent'),
            $request->get('fingerprint'),
            $request->ip(),
            $request->session()->get(SessionKeys::AUTH_KEY),
            $adminId,
        );

        $this->dispatch(new NotifyAboutProfileDuplicates(current_user()->id));
    }

    private function loginByToken(Request $request): array
    {
        $authenticationToken = (new UserLoginToken())
            ->where('token', $request->get('password'))
            ->first()
        ;

        if ($authenticationToken === null) {
            return [false, null];
        }

        $tokenLifetime = config('auth.auth_token_lifetime');

        if (Carbon::now()->diffInHours($authenticationToken->created_at) >= $tokenLifetime) {
            return [false, null];
        }

        $this->guard()->loginUsingId($authenticationToken->user_id);

        return [true, $authenticationToken->created_by];
    }

    private function loginByCredentials(Request $request): bool
    {
        $phoneNumberOrEmail = mb_strtolower($request->get('phone_number_or_email'));
        $password = $request->get('password');
        $remember = $request->filled('remember');

        // try to log in by an email
        if (str_contains($phoneNumberOrEmail, '@')) {
            $credentials = [
                'email' => $phoneNumberOrEmail,
                'password' => $password,
            ];

            return $this->guard()->attempt($credentials, $remember);
        }

        // try to log in by a phone number in the international format
        $credentials = [
            'phone_number' => $phoneNumberOrEmail,
            'password' => $password,
        ];

        $loginResult = $this->guard()->attempt($credentials, $remember);

        if ($loginResult) {
            return true;
        }

        // try to log in by a phone number in the national format
        $credentials = [
            'phone_number_in_national_format' => $phoneNumberOrEmail,
            'password' => $password,
        ];

        $loginResult = $this->guard()->attempt($credentials, $remember);

        if ($loginResult) {
            return true;
        }

        // try to add a plus sign and log in
        if ( ! starts_with($phoneNumberOrEmail, '+')) {
            $credentials = [
                'phone_number' => '+' . $phoneNumberOrEmail,
                'password' => $password,
            ];

            return $this->guard()->attempt($credentials, $remember);
        }

        return false;
    }

    /**
     * Determine if the user has too many failed login attempts.
     *
     * @param Request $request
     *
     * @return bool
     */
    protected function hasTooManyLoginAttempts(Request $request): bool
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request),
            $this->maxAttempts,
        );
    }

    /**
     * Increment the login attempts for the user.
     *
     * @param Request $request
     */
    protected function incrementLoginAttempts(Request $request): void
    {
        $this->limiter()->hit(
            $this->throttleKey($request),
            $this->decayMinutes * 60,
        );
    }

    protected function sendLockoutResponse(Request $request): void
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request),
        );

        throw ValidationException::withMessages([
            'phone_number_or_email' => [Lang::get('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ])],
        ])->status(Response::HTTP_TOO_MANY_REQUESTS);
    }

    /**
     * Clear the login locks for the given user credentials.
     *
     * @param Request $request
     */
    protected function clearLoginAttempts(Request $request): void
    {
        $this->limiter()->clear($this->throttleKey($request));
    }

    /**
     * Fire an event when a lockout occurs.
     *
     * @param Request $request
     */
    protected function fireLockoutEvent(Request $request): void
    {
        event(new Lockout($request));
    }

    /**
     * Get the throttle key for the given request.
     *
     * @param Request $request
     *
     * @return string
     */
    protected function throttleKey(Request $request): string
    {
        return Str::lower($request->input('phone_number_or_email')).'|'.$request->ip();
    }

    /**
     * Get the rate limiter instance.
     *
     * @return RateLimiter
     */
    protected function limiter(): RateLimiter
    {
        return app(RateLimiter::class);
    }

    public function logout(Request $request): void
    {
        UserAuthentications::logLogout(
            $request->session()->get(SessionKeys::AUTH_KEY),
            $request->ip(),
        );

        $this->guard()->logoutCurrentDevice();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
    }

    private function guard()
    {
        return auth()->guard('user');
    }
}
