<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\ConnectedAccount;
use App\Services\OAuth\Exceptions\OAuthException;
use App\Services\OAuth\OAuthManager;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use UAParser\Parser as UserAgentParser;

class OAuthController extends Controller
{
    public function loginRedirectUrl(Request $request, OAuthManager $oauthManager): array
    {
        $this->validate($request, [
            'provider' => ['required', 'string'],
        ]);

        $oauthProvider = $request->get('provider');
        $redirectUrl = route('oauth.login.callback');

        $authPageUrl = $oauthManager->provider($oauthProvider)->getAuthUrl($redirectUrl);

        return [
            'redirect_url' => $authPageUrl,
        ];
    }

    public function loginCallback(Request $request, OAuthManager $oauthManager): RedirectResponse
    {
        if ( ! session()->has('oauth_provider')) {
            return redirect()->route('auth.login')->with('oauth_error', __('auth.social-network-login-error'));
        }

        $socialNetwork = session()->get('oauth_provider');

        if ( ! $oauthManager->hasProvider($socialNetwork)) {
            return redirect()->route('auth.login')->with('oauth_error', __('auth.social-network-login-error'));
        }

        try {
            $user = $oauthManager->provider($socialNetwork)->getUser();
        } catch (OAuthException $exception) {
            return redirect()->route('auth.login')->with('oauth_error', __('auth.social-network-login-error'));
        }

        $socialNetworkAccount = (new ConnectedAccount())
            ->with('user')
            ->where('social_network_user_id', $user->getId())
            ->first()
        ;

        if ($socialNetworkAccount === null) {
            session()->forget([
                'oauth_social_network', 'oauth_user_id', 'oauth_user_name', 'oauth_user_email',
            ]);

            return redirect()->route('auth.login')->with('oauth_error', __('auth.user-associated-with-this-social-account-not-found'));
        }

        auth()->login($socialNetworkAccount->user);

        $userAgent = substr($request->header('User-Agent'), 0, 255);
        $uaParsed = UserAgentParser::create()->parse($userAgent);

        // TODO: Log authentication!!!

        return redirect('/');
    }
}
