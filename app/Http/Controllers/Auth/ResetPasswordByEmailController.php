<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Exceptions\ProfileBannedException;
use App\Http\Controllers\Controller;
use App\Mail\ResetPassword;
use App\Models\EmailVerificationToken;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use Throwable;

class ResetPasswordByEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLink(Request $request, Mailer $mailer): void
    {
        $this->validate($request, [
            'email' => ['required', 'email'],
        ]);

        $email = mb_strtolower($request->get('email'));

        $user = (new User())
            ->where('email', $email)
            ->first(['id', 'name', 'email', 'status'])
        ;

        if ($user === null) {
            // TODO: translate
            abort(400, 'User with the specified Email not found.');
        }

        if ($user->status === User::STATUS_BANNED) {
            throw new ProfileBannedException();
        }

        $verificationToken = (new EmailVerificationToken())->where('email', $user->email)->first();

        if ($verificationToken === null) {
            $verificationToken = $this->createVerificationToken($user->email);
        } else {
            $throttleTimeout = config('auth.password_reset.by_phone_number.throttle_timeout');

            $recentlySentEmail = $verificationToken->created_at
                ->addSeconds($throttleTimeout)
                ->isPast()
            ;

            if ( ! $recentlySentEmail) {
                // TODO: translate
                abort(403, 'Too many password reset attempts, try again later.');
            }

            $lifetime = config('auth.password_reset.by_email.lifetime');

            $linkExpired = $verificationToken->created_at
                ->addMinutes($lifetime)
                ->isPast()
            ;

            if ($linkExpired) {
                $verificationToken = $this->createVerificationToken($user->email);
            }
        }

        $mailer->send(new ResetPassword($user->name, $email, $verificationToken->token));
    }

    private function createVerificationToken(string $email): EmailVerificationToken
    {
        return EmailVerificationToken::create([
            'email' => $email,
            'token' => str_random(64),
            'created_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);
    }

    public function reset(Request $request): void
    {
        $minPasswordLength = config('auth.minimum_password_length');

        $this->validate($request, [
            'token' => ['required'],
            'new_password' => ['required', 'string', 'min:' . $minPasswordLength],
            'new_password_confirmation' => ['required', 'string', 'same:new_password'],
        ]);

        $verificationToken = (new EmailVerificationToken())->where('token', $request->get('token'))->first();

        if ($verificationToken === null) {
            // TODO: translate
            abort(400, 'Reset token is invalid or expired.');
        }

        $lifetime = config('auth.password_reset.by_email.lifetime');

        $linkExpired = $verificationToken->created_at
            ->addMinutes($lifetime)
            ->isPast()
        ;

        if ($linkExpired) {
            abort(400, 'Reset token is invalid or expired.');
        }

        $user = $verificationToken->user()->first();

        if ($user === null) {
            // TODO: translate
            abort(400, 'User not found.');
        }

        if ($user->status === User::STATUS_BANNED) {
            throw new ProfileBannedException();
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $user->update([
                'password' => bcrypt($request->get('new_password')),
                'password_updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'remember_token' => str_random(60),
            ]);

            $verificationToken->delete();

            (new Notification())->create([
                'user_id' => $user->id,
                'type' => Notification::TYPE_PASSWORD_CHANGED,
                'title' => 'notifications:password-changed.title',
                'body' => 'notifications:password-changed.body',
                'is_hidden' => true,
                'created_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }
}
