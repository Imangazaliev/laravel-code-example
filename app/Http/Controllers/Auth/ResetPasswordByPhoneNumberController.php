<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Exceptions\ProfileBannedException;
use App\Http\Controllers\Controller;
use App\Jobs\SendMessageToPhoneNumber;
use App\Models\Notification;
use App\Models\PhoneNumberVerificationCode;
use App\Models\User;
use App\Services\PhoneNumberVerification\PhoneNumberVerification;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Throwable;

class ResetPasswordByPhoneNumberController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetCode(Request $request, PhoneNumberVerification $phoneNumberVerification): void
    {
        $this->validate($request, [
            'phone_number' => ['required', 'string', 'regex:/^\+[0-9]+$/'],
            'phone_number_country_code' => ['required', 'exists:countries,code_alpha2'],
        ]);

        $this->validate($request, [
            'phone_number' => ['phone:' . $request->get('phone_number_country_code')],
        ]);

        $phoneNumber = mb_strtolower($request->get('phone_number'));

        $user = (new User())
            ->where('phone_number', $phoneNumber)
            ->first(['id', 'phone_number', 'status'])
        ;

        if ($user === null) {
            // TODO: translate
            abort(400, 'User with the specified phone number not found.');
        }

        if ($user->status === User::STATUS_BANNED) {
            throw new ProfileBannedException();
        }

        $verificationCode = (new PhoneNumberVerificationCode())->where('phone_number', $user->phone_number)->first();

        if ($verificationCode === null) {
            $verificationCode = $phoneNumberVerification->createCode($user->phone_number);
        } else {
            $throttleTimeout = config('auth.password_reset.by_phone_number.throttle_timeout');

            $recentlySentEmail = $verificationCode->created_at
                ->addSeconds($throttleTimeout)
                ->isPast()
            ;

            if ( ! $recentlySentEmail) {
                abort(403, 'Too many password reset attempts, try again later.');
            }

            $lifetime = config('auth.password_reset.by_phone_number.lifetime');

            $linkExpired = $verificationCode->created_at
                ->addMinutes($lifetime)
                ->isPast()
            ;

            if ($linkExpired) {
                $verificationCode = $phoneNumberVerification->createCode($user->phone_number);
            }
        }

        // TODO: translate
        $this->dispatch(new SendMessageToPhoneNumber($verificationCode->phone_number, "Password reset code: {$verificationCode->code}"));
    }

    public function checkCode(Request $request): void
    {
        $this->validate($request, [
            'phone_number' => ['required', 'string', 'regex:/^\+[0-9]+$/'],
            'phone_number_country_code' => ['required', 'exists:countries,code_alpha2'],
            'code' => ['required'],
        ]);

        $this->validate($request, [
            'phone_number' => ['phone:' . $request->get('phone_number_country_code')],
        ]);

        $verificationCode = (new PhoneNumberVerificationCode())
            ->where('phone_number', $request->get('phone_number'))
            ->where('code', $request->get('code'))
            ->first()
        ;

        if ($verificationCode === null) {
            // TODO: translate
            abort(400, 'Reset code is invalid or expired.');
        }

        $lifetime = config('auth.password_reset.by_phone_number.lifetime');

        $linkExpired = $verificationCode->created_at
            ->addMinutes($lifetime)
            ->isPast()
        ;

        if ($linkExpired) {
            abort(400, 'Reset token is invalid or expired.');
        }

        $user = $verificationCode->user()->first();

        if ($user === null) {
            // TODO: translate
            abort(400, 'User not found.');
        }

        if ($user->status === User::STATUS_BANNED) {
            throw new ProfileBannedException();
        }
    }

    public function reset(Request $request): void
    {
        $minPasswordLength = config('auth.minimum_password_length');

        $this->validate($request, [
            'phone_number' => ['required', 'string', 'regex:/^\+[0-9]+$/'],
            'phone_number_country_code' => ['required', 'exists:countries,code_alpha2'],
            'code' => ['required'],
            'new_password' => ['required', 'string', 'min:' . $minPasswordLength],
            'new_password_confirmation' => ['required', 'string', 'same:new_password'],
        ]);

        $this->validate($request, [
            'phone_number' => ['phone:' . $request->get('phone_number_country_code')],
        ]);

        $verificationCode = (new PhoneNumberVerificationCode())
            ->where('phone_number', $request->get('phone_number'))
            ->where('code', $request->get('code'))
            ->first()
        ;

        if ($verificationCode === null) {
            // TODO: translate
            abort(400, 'Reset code is invalid or expired.');
        }

        $lifetime = config('auth.password_reset.by_phone_number.lifetime');

        $linkExpired = $verificationCode->created_at
            ->addMinutes($lifetime)
            ->isPast()
        ;

        if ($linkExpired) {
            abort(400, 'Reset token is invalid or expired.');
        }

        $user = $verificationCode->user()->first();

        if ($user === null) {
            // TODO: translate
            abort(400, 'User not found.');
        }

        if ($user->status === User::STATUS_BANNED) {
            throw new ProfileBannedException();
        }

        $user->update([
            'password' => bcrypt($request->get('new_password')),
            'password_updated_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'remember_token' => str_random(60),
        ]);

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $user->update([
                'password' => bcrypt($request->get('new_password')),
                'password_updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'remember_token' => str_random(60),
            ]);

            $verificationCode->delete();

            (new Notification())->create([
                'user_id' => $user->id,
                'type' => Notification::TYPE_PASSWORD_CHANGED,
                'title' => 'notifications:password-changed.title',
                'body' => 'notifications:password-changed.body',
                'is_hidden' => true,
                'created_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }
}
