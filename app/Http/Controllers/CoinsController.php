<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\Coins\BalanceRecharge;
use App\Services\Coins\CoinTransactionHistory;
use App\Services\Coins\Exception\InvalidInvoiceReference;
use App\Services\Coins\Exception\PaymentVerificationException;
use App\Services\Coins\GatewayFactory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CoinsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['completePurchase', 'getPrices']);
    }

    public function coinsAmountOptions(): array
    {
        return [
            'options' => BalanceRecharge::getAmountOptions(current_user()->can('test-payments')),
            'default_value' => BalanceRecharge::getDefaultAmount(),
        ];
    }

    public function calculatePrice(BalanceRecharge $balanceRecharge, Request $request): array
    {
        $this->validate($request, [
            'amount' => ['required', 'integer', Rule::in(BalanceRecharge::getAmountOptions(current_user()->can('test-payments')))],
            'currency' => ['required', Rule::in(config('payments.supported_currencies'))],
        ]);

        $amount = (int) $request->get('amount');
        $currency = $request->get('currency');

        return [
            'price' => $balanceRecharge->calculatePrice($amount, $currency),
        ];
    }

    public function purchase(BalanceRecharge $balanceRecharge, Request $request): array
    {
        $this->validate($request, [
            'gateway' => ['required', Rule::in(GatewayFactory::getSupportedGateways())],
            'amount' => ['required', Rule::in(BalanceRecharge::getAmountOptions(current_user()->can('test-payments')))],
            'currency' => [Rule::in(config('payments.supported_currencies'))],
        ]);

        $gateway = $request->get('gateway');
        $amount = $request->get('amount');
        $currency = $request->get('currency');

        [
            'redirect_url'=> $redirectUrl,
            'redirect_method'=> $redirectMethod,
            'redirect_data'=> $redirectData,
        ] = $balanceRecharge->createInvoice($amount, $currency, $gateway);

        return [
            'success' => true,
            'redirect_url' => $redirectUrl,
            'redirect_method' => $redirectMethod,
            'redirect_data' => $redirectData,
        ];
    }

    public function completePurchase(string $gateway, BalanceRecharge $balanceRecharge, Request $request): ?string
    {
        try {
            return $balanceRecharge->completePurchase($gateway, $request->all());
        } catch (InvalidInvoiceReference $exception) {
            abort(400);
        } catch (PaymentVerificationException $exception) {
            abort(400);
        }
    }

    public function getHistory(Request $request): array
    {
        return CoinTransactionHistory::transaction(current_user()->id, (int) $request->get('page'));
    }

    public function getPrices(BalanceRecharge $balanceRecharge): array
    {
        $currency = 'RUB';

        $chatRequestPrice = config('chats.chat_request_price');
        $chatRequestAcceptancePrice = config('chats.chat_request_acceptance_price');
        $giftCoinsAmount = config('profile.gift_coins_amount');
        $contactsRequest = config('chats.contacts_request_price');

        return [
            'chat_request' => [
                'coins' => $chatRequestPrice,
                'currency' => $balanceRecharge->calculatePrice($chatRequestPrice, $currency),
            ],
            'chat_request_acceptance' => [
                'coins' => $chatRequestAcceptancePrice,
                'currency' => $balanceRecharge->calculatePrice($chatRequestAcceptancePrice, $currency),
            ],
            'coin_price' => [
                'usd' => $balanceRecharge->calculatePrice(1, 'USD'),
                'converted' => $balanceRecharge->calculatePrice(1, $currency),
            ],
            'gift' => [
                'coins' => $giftCoinsAmount,
                'currency' => $balanceRecharge->calculatePrice($giftCoinsAmount, $currency),
            ],

            'contacts_request' => [
                'coins' => $contactsRequest,
                'currency' => $balanceRecharge->calculatePrice($contactsRequest, $currency),
            ],
            'messages' => [
                'per_coin' => config('chats.message_count_per_coin'),
            ],
        ];
    }
}
