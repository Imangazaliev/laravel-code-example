<?php

declare(strict_types=1);

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Chat;
use App\Models\ChatRequest;
use App\Models\CoinTransaction;
use App\Models\Notification;
use App\Models\ProfileView;
use App\Models\User;
use App\Models\UserPhoto;
use App\Services\Coins\Coins;
use App\Services\Coins\CoinTransactionDto;
use App\Services\PhotoAccessGate\PhotoAccessGate;
use App\Transformers\UserInfoTransformer;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Throwable;

class ViewProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile(Request $request, PhotoAccessGate $photoAccessGate): array
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer'],
        ]);

        $userId = (int) $request->get('user_id');

        $user = cache_info()->userProfile($userId)->remember(function () use ($userId) {
            return (new User())->find($userId);
        });

        [
            $ethnicity,
            $country,
            $locality,
            $knownLanguages,
        ] = cache_info()->userProfileTranslatableData($userId)->remember(function () use ($userId) {
            $user = (new User())->find($userId, [
                'id',
                'ethnicity_id',
                'country_id',
                'locality_id',
            ]);

            if ($user === null) {
                return null;
            }

            UserInfoTransformer::loadRelations($user);

            return [
                $user->ethnicity,
                $user->country,
                $user->locality,
                $user->knownLanguages,
            ];
        });

        if ($user === null) {
            abort(404, __('profile:errors.user-not-found'));
        }

        $user->ethnicity = $ethnicity;
        $user->country = $country;
        $user->locality = $locality;
        $user->knownLanguages = $knownLanguages;

        // WARNING: it's necessary to set "was_online_at" for method "getPresenceStatus" to work correctly
        $user->was_online_at = cache_info()->userLastOnlineTime($userId)->remember(function () use ($user) {
            $lastOnlineTime = (new User())
                ->where('id', $user->id)
                ->first(['was_online_at'])
                ->was_online_at
            ;

            return $lastOnlineTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT);
        });

        if ($user->status === User::STATUS_BANNED) {
            $isTemporaryBan = $user->ban()->value('unban_at') !== null;

            abort(403, $isTemporaryBan ? __('profile:errors.user-temporary-banned') : __('profile:errors.user-banned'));
        }

        if ($user->status === User::STATUS_DEACTIVATED) {
            abort(403, __('profile:errors.user-deactivated'));
        }

        if ($user->status === User::STATUS_PENDING_DELETION || $user->status === User::STATUS_DELETED) {
            abort(403, __('profile:errors.user-deleted'));
        }

        if ($user->status !== User::STATUS_ACCEPTED && $user->id !== current_user()->id) {
            abort(403, __('profile:errors.user-profile-is-not-accepted'));
        }

        $incomingChatRequestId = cache_info()->usersIncomingChatRequestId(current_user()->id, $user->id)->remember(function () use ($user) {
            $requestId = (new ChatRequest())
                ->where('status', ChatRequest::STATUS_NEW)
                ->where('requestor_id', $user->id)
                ->where('target_user_id', current_user()->id)
                ->value('id')
            ;

            // put the ID to an array to save null values too
            return [
                'id' => $requestId,
            ];
        })['id'];

        $outgoingChatRequestId = cache_info()->usersOutgoingChatRequestId(current_user()->id, $user->id)->remember(function () use ($user) {
            $requestId = (new ChatRequest())
                ->where('status', ChatRequest::STATUS_NEW)
                ->where('requestor_id', current_user()->id)
                ->where('target_user_id', $user->id)
                ->value('id')
            ;

            // put the ID to an array to save null values too
            return [
                'id' => $requestId,
            ];
        })['id'];

        if ($user->id === current_user()->id) {
            $chatId = null;
        } else {
            $chatId = cache_info()->usersChatId(current_user()->id, $user->id)->remember(function () use ($user) {
                $chatId = (new Chat())
                    ->whereUser(current_user()->id)
                    ->whereUser($user->id)
                    ->whereActive()
                    ->value('id')
                ;

                // put the ID to an array to save null values too
                return [
                    'id' => $chatId,
                ];
            })['id'];
        }

        $isFavorite = cache_info()->isFavoriteUser(current_user()->id, $user->id)->remember(function () use ($user) {
            return current_user()
                ->favoriteProfiles()
                ->where('users.id', $user->id)
                ->exists()
            ;
        });

        $isLiked = cache_info()->isLikedUser(current_user()->id, $user->id)->remember(function () use ($user) {
            return current_user()
                ->likedUsers()
                ->where('users.id', $user->id)
                ->whereNull('removed_at')
                ->exists()
            ;
        });

        $photos = cache_info()->userPhotos($user->id)->remember(function () use ($user) {
            return $user
                ->photos()
                ->where('status', UserPhoto::STATUS_ACCEPTED)
                ->where('is_being_processed', false)
                ->orderBy('is_main', 'desc')
                ->orderBy('uploaded_at')
                ->addBinding($user->main_photo_id, 'select')
                ->get([
                    'id',
                    'caption',
                    'storage_type',
                    'user_id',
                    'path',
                    'blurred_image_path',
                    db_raw('case when id = ? then true else false end as is_main'),
                ])
                ->all()
            ;
        });

        $sentChatRequestPreviously = cache_info()->sentChatRequestPreviously(current_user()->id, $user->id)->remember(function () use ($user) {
            return (new ChatRequest())
                ->where('requestor_id', current_user()->id)
                ->where('target_user_id', $user->id)
                ->exists()
            ;
        });

        return array_merge([
            'id' => $user->id,
            'name' => $user->name,
            'presence_status' => $user->getPresenceStatus(),
            'was_online_at' => $user->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'is_verified' => $user->is_verified,
            'sex' => $user->sex,
            'birthday' => $user->birthday->format(DATE_FORMAT),
            'profile_headline' => $user->profile_headline,
            'can_view_photo' => (
                current_user()->can('view-original-photos') ||
                $photoAccessGate->canViewPhotos(current_user()->id, $user->id)
            ),
            'photos' => array_map(function (UserPhoto $photo) {
                return [
                    'id' => $photo->id,
                    'url' => $photo->getImageUrlWithCheck(UserPhoto::SIZE_400),
                    'caption' => $photo->caption,
                    'is_main' => $photo->is_main,
                ];
            }, $photos),
            'incoming_chat_request_id' => $incomingChatRequestId,
            'outgoing_chat_request_id' => $outgoingChatRequestId,
            'chat_id' => $chatId,
            'is_favorite' => $isFavorite,
            'is_liked' => $isLiked,
            'sent_chat_request_previously' => $sentChatRequestPreviously,
        ], UserInfoTransformer::transform($user));
    }

    public function profileViewed(Request $request): void
    {
        $this->validate($request, [
            'user_id' => 'required|integer',
        ]);

        $userId = (int) $request->get('user_id');

        // do not log views of own profile
        if (current_user()->id === $userId) {
            abort(403);
        }

        $userExists = (new User())
            ->where('id', $userId)
            ->exists()
        ;

        if ( ! $userExists) {
            abort(404);
        }

        $hasUnseenProfileView = (new ProfileView())
            ->where('visitor_id', current_user()->id)
            ->where('viewed_profile_id', $userId)
            ->whereNull('seen_by_owner_at')
            ->exists()
        ;

        // do not log a visit if the previous visit
        // is not seen by a profile owner yet
        if ($hasUnseenProfileView) {
            return;
        }

        $profileViewSaveInterval = config('profile.profile_view_save_interval');

        $hasRecentProfileView = (new ProfileView())
            ->where('visitor_id', current_user()->id)
            ->where('viewed_profile_id', $userId)
            ->where('viewed_at', '>=', Carbon::now()->subMinutes($profileViewSaveInterval)->format(DATE_TIME_WITH_TIME_ZONE_FORMAT))
            ->exists()
        ;

        if ($hasRecentProfileView) {
            return;
        }

        (new ProfileView())->create([
            'visitor_id' => current_user()->id,
            'viewed_profile_id' => $userId,
            'viewed_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'is_incognito' => current_user()->is_incognito_mode_enabled,
        ]);

        cache_info()->newProfileViewCount($userId)->forget();
    }

    public function makeGift(Request $request, Coins $coins): void
    {
        $this->validate($request, [
            'user_id' => 'required|integer',
        ]);

        // don't allow to make a gift another profiles if the current user profile is not accepted
        if (current_user()->status !== User::STATUS_ACCEPTED) {
            abort(403, __('errors.your-profile-is-not-accepted'));
        }

        $targetUserId = (int) $request->get('user_id');

        // do not allow to make a gift to self
        if ($targetUserId === current_user()->id) {
            abort(403);
        }

        // don't allow to send a chat request if the profile of the current user is not accepted
        if (current_user()->status !== User::STATUS_ACCEPTED) {
            abort(403, __('errors.your-profile-is-not-accepted'));
        }

        $targetUser = (new User())->find($targetUserId, [
            'status',
            'sex',
        ]);

        if ($targetUser === null) {
            abort(400);
        }

        // don't allow to send a chat request it the profile of the target user is not accepted
        if ($targetUser->status !== User::STATUS_ACCEPTED) {
            abort(403);
        }

        $amount = config('profile.gift_coins_amount');

        DB::beginTransaction();

        try {
            $coinTransactionDto = (new CoinTransactionDto())
                ->setType(CoinTransaction::TYPE_GIFT_TO_ANOTHER_USER)
                ->setAmount($amount * -1)
                ->setUserId(current_user()->id)
                ->setDescription(sprintf('A gift from user #%d to user #%d', current_user()->id, $targetUserId))
            ;

            $transferSourceTransaction = $coins->changeBalance($coinTransactionDto);

            $coinTransactionDto = (new CoinTransactionDto())
                ->setType(CoinTransaction::TYPE_GIFT_FROM_ANOTHER_USER)
                ->setAmount($amount)
                ->setUserId($targetUserId)
                ->setDescription(sprintf('A gift from user #%d to user #%d', current_user()->id, $targetUserId))
                ->setTransferSourceTransactionId($transferSourceTransaction->id)
            ;

            $transferTargetTransaction = $coins->changeBalance($coinTransactionDto);

            (new Notification())->create([
                'user_id' => $targetUserId,
                'type' => Notification::TYPE_GIFT,
                'title' => 'notifications:gift.title',
                'body' => 'notifications:gift.body',
                'parameters' => [
                    'userId' => current_user()->id,
                    'userName' => current_user()->name,
                    'coinsAmount' => $amount,
                ],
                'context' => current_user()->sex === User::SEX_MALE ? 'male' : 'female',
                'additional_data' => [
                    'transfer_source_transaction_id' => $transferSourceTransaction->id,
                    'transfer_target_transaction_id' => $transferTargetTransaction->id,
                ],
                'is_hidden' => false,
                'created_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }
}
