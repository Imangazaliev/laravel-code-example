<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\CountryText;
use App\Models\Locality;
use App\Models\LocalityText;
use App\Models\Subdivision;
use App\Models\SubdivisionText;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class GeoController extends Controller
{
    public function countries(): array
    {
        return cache_info()->countries()->remember(function () {
            $countries = (new Country())
                ->select('countries.*')
                ->joinTranslation()
                ->where('show_in_list', true)
                ->orderBy('country_texts.name')
                ->toBase()
                ->get(['id', 'code_alpha2'])
                ->all()
            ;

            $countryNames = (new CountryText())
                ->whereIn('country_id', array_column($countries, 'id'))
                ->where('language_id', language()->id)
                ->pluck('name', 'country_id')
            ;

            return array_map(function ($country) use ($countryNames) {
                return [
                    'id' => $country->id,
                    'code' => $country->code_alpha2,
                    'name' => $countryNames[$country->id] ?? $country->english_name,
                ];
            }, $countries);
        });
    }

    public function subdivisions(Request $request): array
    {
        $this->validate($request, [
            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')],
        ]);

        $countryId = (int) $request->get('country_id');

        return cache_info()->subdivisions($countryId)->remember(function () use ($countryId) {
            $subdivisions = (new Subdivision())
                ->select('subdivisions.*')
                ->withTranslation()
                ->joinTranslation()
                ->where('country_id', $countryId)
                ->orderBy('subdivision_texts.name')
                ->toBase()
                ->get(['id'])
                ->toArray()
            ;

            $subdivisionNames = (new SubdivisionText())
                ->whereIn('subdivision_id', array_column($subdivisions, 'id'))
                ->where('language_id', language()->id)
                ->pluck('name', 'subdivision_id')
            ;

            return array_map(function ($subdivision) use ($subdivisionNames) {
                return [
                    'id' => $subdivision->id,
                    'name' => $subdivisionNames[$subdivision->id] ?? $subdivision->english_name,
                ];
            }, $subdivisions);
        });
    }

    public function localities(Request $request): array
    {
        $this->validate($request, [
            'country_id' => ['required', 'integer', Rule::exists('countries', 'id')],
            'subdivision' => ['integer', Rule::exists('subdivisions', 'id')],
        ]);

        $countryId = (int) $request->get('country_id');
        $subdivisionId = $request->has('subdivision_id') ? (int) $request->get('subdivision_id') : null;

        return cache_info()->localities($countryId, $subdivisionId)->remember(function () use ($countryId, $subdivisionId) {
            $localities = (new Locality())
                ->select('localities.*')
                ->joinTranslation()
                ->where('country_id', $countryId)
                // WARNING: nested "where" is required to put the condition in brackets
                ->when($subdivisionId !== null, function (Builder $query) use ($subdivisionId) {
                    $query
                        ->where('subdivision_level_1_id', $subdivisionId)
                        ->orWhere('subdivision_level_2_id', $subdivisionId)
                    ;
                })
                ->orderBy('locality_texts.name')
                ->toBase()
                ->get(['id', 'subdivision_level_1_id', 'subdivision_level_2_id'])
                ->all()
            ;

            $localityNames = (new LocalityText())
                ->whereIn('locality_id', array_column($localities, 'id'))
                ->where('language_id', language()->id)
                ->pluck('name', 'locality_id')
            ;

            $subdivisionIds = array_column($localities, 'subdivision_level_1_id');

            $subdivisionNames = (new SubdivisionText())
                ->whereIn('subdivision_id', $subdivisionIds)
                ->where('language_id', language()->id)
                ->pluck('name', 'subdivision_id')
            ;

            $subdivisions = (new Subdivision())
                ->whereIn('id', $subdivisionIds)
                ->get(['id'])
                ->map(function (Subdivision $subdivision) use ($subdivisionNames) {
                    return [
                        'id' => $subdivision->id,
                        'name' => $subdivisionNames[$subdivision->id] ?? $subdivision->english_name,
                    ];
                })
                ->toArray()
            ;

            $subdivisionsById = array_combine(array_column($subdivisions, 'id'), $subdivisions);

            return array_map(function ($locality) use ($localityNames, $subdivisionsById) {
                $subdivisionLevel1Id = $locality->subdivision_level_1_id;
                $subdivisionLevel2Id = $locality->subdivision_level_2_id;

                $subdivisionLevel1 = $subdivisionLevel1Id !== null && array_key_exists($subdivisionLevel1Id, $subdivisionsById) ? $subdivisionsById[$subdivisionLevel1Id]['name'] : null;
                $subdivisionLevel2 = $subdivisionLevel2Id !== null && array_key_exists($subdivisionLevel2Id, $subdivisionsById) ? $subdivisionsById[$subdivisionLevel2Id]['name'] : null;

                return [
                    'id' => $locality->id,
                    'name' => $localityNames[$locality->id] ?? $locality->english_name,
                    'subdivision_level_1' => $subdivisionLevel1Id !== null ? [
                        'id' => $subdivisionLevel1Id,
                        'name' => $subdivisionLevel1,
                    ] : null,
                    'subdivision_level_2' => $subdivisionLevel2Id !== null ? [
                        'id' => $subdivisionLevel2Id,
                        'name' => $subdivisionLevel2,
                    ] : null,
                ];
            }, $localities);
        });
    }
}
