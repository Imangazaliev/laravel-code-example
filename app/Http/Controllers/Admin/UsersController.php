<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\CheckSavedFilters;
use App\Jobs\Notifications\NotifyAboutProfileStatusChange;
use App\Models\Chat;
use App\Models\CoinTransaction;
use App\Models\Notification;
use App\Models\ProfileModeration;
use App\Models\ProfileStatusChange;
use App\Models\SupportChat;
use App\Models\User;
use App\Models\UserBan;
use App\Models\UserDeletion;
use App\Models\UserPhoto;
use App\Models\UserProfileRevision;
use App\Services\BanUsers\BanUsers;
use App\Services\BanUsers\UserBanDto;
use App\Services\BanUsers\UserUnbanDto;
use App\Services\Coins\Coins;
use App\Services\Coins\CoinTransactionDto;
use App\Services\Coins\CoinTransactionHistory;
use App\Services\UserLoginUrlGenerator\UserLoginUrlGenerator;
use App\Transformers\UserInfoTransformer;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Throwable;

class UsersController extends Controller
{
    private const USERS_PER_PAGE = 20;

    public function users(Request $request): array
    {
        $this->validate($request, [
            'query' => ['nullable', 'string'],
        ]);

        $searchQuery = $request->get('query');
        $searchById = $searchQuery !== null && starts_with($searchQuery, '#');

        $usersPaginator = (new User())
            ->when($searchById && $searchQuery !== '#', function (Builder $query) use ($searchQuery) {
                $query->where('id', substr($searchQuery, 1));
            })
            ->when($searchQuery !== null && ! $searchById, function (Builder $query) use ($searchQuery) {
                $query->where(function (Builder $query) use ($searchQuery) {
                    $query
                        ->where('id', 'ilike', "%{$searchQuery}%")
                        ->orWhere('name', 'ilike', "%{$searchQuery}%")
                        ->orWhere('phone_number', 'ilike', "%{$searchQuery}%")
                        ->orWhere('email', 'ilike', "%{$searchQuery}%")
                    ;
                });
            })
            ->orderBy('id')
            ->paginate(self::USERS_PER_PAGE, [
                'id',
                'name',
                'sex',
                'phone_number',
                'status',
            ])
        ;

        $canViewContacts = current_user()->can('view-user-contacts');

        return [
            'users' => array_map(function (User $user) use ($canViewContacts) {
                return [
                    'id' => $user->id,
                    'phone_number' => $canViewContacts ? $user->phone_number : null,
                    'name' => $user->name,
                    'sex' => $user->sex,
                    'status' => $user->status,
                ];
            }, $usersPaginator->items()),
            'total_count' => $usersPaginator->total(),
            'page_count' => $usersPaginator->lastPage(),
        ];
    }

    public function userDetails(Request $request): array
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
        ]);

        $user = (new User())
            ->with('ban')
            ->with('ban.bannedBy:id,name')
            ->with('deactivation')
            ->with('deactivation.deactivatedBy:id,name')
            ->with('deletion')
            ->with('deletion.deletedBy:id,name')
            ->findOrFail($request->get('user_id'))
        ;

        UserInfoTransformer::loadRelations($user);

        $moderator = $user->moderation_moderator_id === null ? null : (new User())->findOrFail($user->moderation_moderator_id, ['id', 'name']);

        $supportChatId = (new SupportChat())
            ->where('user_id', $user->id)
            ->value('id')
        ;

        $canViewContacts = current_user()->can('view-user-contacts');

        $photoCount = (new UserPhoto())
            ->withTrashed()
            ->select([
                db_raw('COUNT(*) count'),
                db_raw("COUNT(*) FILTER (WHERE status != 'deleted') active_count"),
            ])
            ->where('user_id', $user->id)
            ->firstOrFail()
        ;

        $chatCount = (new Chat())
            ->select([
                db_raw('COUNT(*) count'),
                db_raw('COUNT(*) FILTER (WHERE finished_at IS NULL) active_count'),
            ])
            ->whereUser($user->id)
            ->firstOrFail()
        ;

        $banCount = (new UserBan())
            ->where('user_id', $user->id)
            ->count()
        ;

        $moderationCount = (new ProfileModeration())
            ->where('user_id', $user->id)
            ->count()
        ;

        return array_merge([
            'id' => $user->id,
            'name' => $user->name,
            'sex' => $user->sex,
            'birthday' => $user->birthday->format(DATE_FORMAT),
            'phone_number' => $canViewContacts ? $user->phone_number : null,
            'email' => $canViewContacts ? $user->email : null,
            'registered_at' => $user->registered_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'status' => $user->status,
            'coins' => $user->coins,
            'required_fields_filled' => $user->required_fields_filled,
            'presence_status' => $user->getPresenceStatus(),
            'was_online_at' => $user->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'photo_url' => $user->photo_url,
            'active_photo_count' => $photoCount->active_count,
            'total_photo_count' => $photoCount->count,
            'active_chat_count' => $chatCount->active_count,
            'total_chat_count' => $chatCount->count,
            'ban_count' => $banCount,
            'moderation_count' => $moderationCount,
            'moderation' => $user->status === User::STATUS_PENDING_MODERATION ? [
                'moderator' => $moderator === null ? null : [
                    'id' => $moderator->id,
                    'name' => $moderator->name,
                ],
            ] : null,
            'support_chat_id' => $supportChatId,
            'ban' => $user->status === User::STATUS_BANNED ? [
                'reasons' => $user->ban->reasons,
                'comment' => $user->ban->comment,
                'comment_for_user' => $user->ban->comment_for_user,
                'banned_at' => $user->ban->banned_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'banned_by' => $user->ban->bannedBy !== null ? [
                    'id' => $user->ban->bannedBy->id,
                    'name' => $user->ban->bannedBy->name,
                ] : null,
                'unban_at' => $user->ban->unban_at !== null ? $user->ban->unban_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT) : null,
            ] : null,
            'deactivation' => $user->status === User::STATUS_DEACTIVATED ? [
                'comment' => $user->deactivation->comment,
                'deactivated_at' => $user->deactivation->deactivated_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'deactivated_by' => $user->deactivation->deactivatedBy !== null ? [
                    'id' => $user->deactivation->deactivatedBy->id,
                    'name' => $user->deactivation->deactivatedBy->name,
                ] : null,
            ] : null,
            'deletion' => $user->status === User::STATUS_PENDING_DELETION || $user->status === User::STATUS_DELETED ? [
                'name' => $user->deletion->name,
                'phone_number' => $canViewContacts ? $user->deletion->phone_number : null,
                'email' => $canViewContacts ? $user->deletion->email : null,
                'reason' => $user->deletion->reason,
                'comment' => $user->deletion->comment,
                'deleted_at' => $user->deletion->deleted_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'deleted_by' => $user->deletion->deletedBy !== null ? [
                    'id' => $user->deletion->deletedBy->id,
                    'name' => $user->deletion->deletedBy->name,
                ] : null,
            ] : null,
        ], UserInfoTransformer::transform($user));
    }

    public function chats(Request $request): array
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer', 'exists:users,id'],
        ]);

        $userId = (int) $request->get('user_id');

        $chats = (new Chat())
            ->select([
                'id',
                'requestor_id',
                'target_user_id',
                'last_activity_at',
                'finished_at',
            ])
            ->withCount('messages')
            ->whereUser($userId)
            ->orderBy('last_activity_at', 'desc')
            ->orderBy('created_at', 'desc')
            ->get()
        ;

        $chatIdToPeerId = [];

        foreach ($chats as $chat) {
            $chatIdToPeerId[$chat->id] = $chat->requestor_id === $userId ? $chat->target_user_id : $chat->requestor_id;
        }

        $peers = (new User())
            ->with('mainPhoto')
            ->whereIn('id', array_values($chatIdToPeerId))
            ->get([
                'id',
                'name',
            ])
        ;

        $peersById = $peers->pluck('id')->combine($peers)->all();

        $transformChat = function (Chat $chat) use (
            $chatIdToPeerId,
            $peersById
        ) {
            $peerId = $chatIdToPeerId[$chat->id];
            $peer = $peersById[$peerId];
            $lastActivityAt = (
                $chat->last_activity_at !== null ? $chat->last_activity_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT) : null
            );

            return [
                'id' => $chat->id,
                'peer' => [
                    'id' => $peer->id,
                    'name' => $peer->name,
                ],
                'message_count' => $chat->messages_count,
                'last_activity_at' => $lastActivityAt,
            ];
        };

        $activeChats = $chats
            ->filter(fn (Chat $chat) => $chat->finished_at === null)
            ->values()
            ->map($transformChat)
            ->all()
        ;

        $finishedChats = $chats
            ->filter(fn (Chat $chat) => $chat->finished_at !== null)
            ->values()
            ->map($transformChat)
            ->all()
        ;

        return [
            'chats' => [
                'active' => $activeChats,
                'finished' => $finishedChats,
            ],
        ];
    }

    public function photos(Request $request): array
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer', 'exists:users,id'],
        ]);

        $userId = (int) $request->get('user_id');

        $userSex = (new User())
            ->where('id', $userId)
            ->value('sex')
        ;

        if ($userSex !== current_user()->sex && ! current_user()->can('view-all-photos')) {
            abort(403);
        }

        $photos = (new UserPhoto())
            ->withTrashed()
            ->where('user_id', $userId)
            ->orderBy('uploaded_at', 'desc')
            ->get([
                'id',
                'storage_type',
                'path',
                'status',
                'deleted_at',
            ])
        ;

        $transformPhoto = function (UserPhoto $photo) {
            return [
                'id' => $photo->id,
                'url' => $photo->getImageUrl(),
                'status' => $photo->status,
            ];
        };

        $activePhotos = $photos
            ->filter(fn (UserPhoto $photo) => $photo->deleted_at === null)
            ->values()
            ->map($transformPhoto)
            ->all()
        ;

        $deletedPhotos = $photos
            ->filter(fn (UserPhoto $photo) => $photo->deleted_at !== null)
            ->values()
            ->map($transformPhoto)
            ->all()
        ;

        return [
            'photos' => [
                'active' => $activePhotos,
                'deleted' => $deletedPhotos,
            ],
        ];
    }

    public function bans(Request $request): array
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer', 'exists:users,id'],
        ]);

        $userId = (int) $request->get('user_id');

        $bans = (new UserBan())
            ->with('bannedBy:id,name')
            ->with('unbannedBy:id,name')
            ->where('user_id', $userId)
            ->orderBy('banned_at', 'desc')
            ->get([
                'id',
                'reasons',
                'comment',
                'comment_for_user',
                'banned_by',
                'banned_at',
                'unban_at',
                'unbanned_at',
                'unbanned_by',
                'unban_comment',
            ])
        ;

        return [
            'bans' => $bans
                ->map(function (UserBan $ban) {
                    return [
                        'id' => $ban->id,
                        'reasons' => $ban->reasons,
                        'comment' => $ban->comment,
                        'comment_for_user' => $ban->comment_for_user,
                        'banned_at' => $ban->banned_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'banned_by' => $ban->bannedBy !== null ? [
                            'id' => $ban->bannedBy->id,
                            'name' => $ban->bannedBy->name,
                        ] : null,
                        'unban_at' => $ban->unban_at !== null ? $ban->unban_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT) : null,
                        'unbanned_at' => $ban->unbanned_at !== null ? $ban->unbanned_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT) : null,
                        'unbanned_by' => $ban->unbannedBy !== null ? [
                            'id' => $ban->unbannedBy->id,
                            'name' => $ban->unbannedBy->name,
                        ] : null,
                    ];
                })
                ->all(),
        ];
    }

    public function moderations(Request $request): array
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer', 'exists:users,id'],
        ]);

        $userId = (int) $request->get('user_id');

        $moderations = (new ProfileModeration())
            ->with([
                'moderationSession' => function (BelongsTo $query) {
                    $query
                        ->with('moderator:id,name')
                        ->select('id', 'moderator_id')
                    ;
                },
            ])
            ->where('user_id', $userId)
            ->orderBy('moderated_at', 'desc')
            ->paginate(30, [
                'id',
                'moderation_session_id',
                'user_id',
                'status',
                'rejection_reasons',
                'comment',
                'comment_for_user',
                'moderated_at',
            ])
        ;

        return [
            'moderations' => array_map(function (ProfileModeration $moderation) {
                return [
                    'id' => $moderation->id,
                    'status' => $moderation->status,
                    'rejection_reasons' => (function () use ($moderation) {
                        if ($moderation->rejection_reasons === null) {
                            return null;
                        }

                        return array_values(array_only(ProfileModeration::getRejectionReasonTitles(), $moderation->rejection_reasons));
                    })(),
                    'comment' => $moderation->comment,
                    'comment_for_user' => $moderation->comment_for_user,
                    'moderated_at' => $moderation->moderated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'moderator' => [
                        'id' => $moderation->moderationSession->moderator->id,
                        'name' => $moderation->moderationSession->moderator->name,
                    ],
                ];
            }, $moderations->items()),
            'page_count' => $moderations->lastPage(),
        ];
    }

    public function coinTransactions(Request $request): array
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer', 'exists:users,id'],
        ]);

        $userId = (int) $request->get('user_id');

        return CoinTransactionHistory::transaction($userId, (int) $request->get('page'));
    }

    public function acceptProfile(Request $request): void
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
        ]);

        $user = (new User())->find($request->get('user_id'), [
            'id',
            'name',
            'status',
            'status_updated_at',
        ]);

        if ( ! in_array($user->status, [User::STATUS_NOT_MODERATED, User::STATUS_REJECTED])) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $statusChange = (new ProfileStatusChange())->create([
                'user_id' => $user->id,
                'new_status' => ProfileModeration::STATUS_ACCEPTED,
                'changed_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_FORMAT),
                'changed_by' => current_user()->id,
            ]);

            $updatedProfileCount = (new User())
                ->where('id', $user->id)
                ->where('status_updated_at', $user->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'last_accepted_name' => $user->name,
                    'status' => User::STATUS_ACCEPTED,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'moderation_moderator_id' => null,
                    'moderation_claimed_at' => null,
                    'moderation_forwarded_to' => null,
                    'moderation_forwarded_by' => null,
                    'moderation_forwarded_at' => null,
                ])
            ;

            cache_info()->userProfile($user->id)->forget();
            cache_info()->userLastModerationStatus($user->id)->forget();
            cache_info()->newProfileModerationCount($user->id)->forget();

            // if the status of the profile was already changed by someone else
            if ($updatedProfileCount === 0) {
                abort(403);
            }

            DB::commit();

            $this->dispatch(new NotifyAboutProfileStatusChange($statusChange->id));
            // add a delay for the case when the admin accepted the questionnaire by mistake
            CheckSavedFilters::dispatch($user->id)->delay(Carbon::now()->addMinutes(5));
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function rejectProfile(Request $request): void
    {
        $rejectionReasons = array_keys(ProfileModeration::getRejectionReasonTitles());

        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
            'rejection_reasons' => ['required', 'array', 'min:1'],
            'rejection_reasons.*' => ['required', Rule::in($rejectionReasons)],
            'comment' => ['nullable', 'string'],
            'comment_for_user' => ['nullable', 'string'],
        ]);

        $user = (new User())->find($request->get('user_id'), [
            'id',
            'status',
            'status_updated_at',
            'is_protected',
        ]);

        if ($user->is_protected && $user->id !== current_user()->id) {
            abort(403);
        }

        if ( ! in_array($user->status, [User::STATUS_NOT_MODERATED, User::STATUS_ACCEPTED])) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $rejectionReasons = $request->get('rejection_reasons');

            $currentTime = Carbon::now();

            $statusChange = (new ProfileStatusChange())->create([
                'user_id' => $user->id,
                'new_status' => ProfileModeration::STATUS_REJECTED,
                'rejection_reasons' => $rejectionReasons,
                'comment' => $request->get('comment'),
                'comment_for_user' => $request->get('comment_for_user'),
                'changed_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_FORMAT),
                'changed_by' => current_user()->id,
            ]);

            $lastUserProfileRevision = (new UserProfileRevision())
                ->where('user_id', $user->id)
                ->orderBy('created_at', 'desc')
                ->value('data')
            ;

            $userData = [
                'last_accepted_name' => $lastUserProfileRevision['last_accepted_name'] ?? null,
                'status' => User::STATUS_REJECTED,
                'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                'moderation_moderator_id' => null,
                'moderation_claimed_at' => null,
                'moderation_forwarded_to' => null,
                'moderation_forwarded_by' => null,
                'moderation_forwarded_at' => null,
            ];

            if (in_array(ProfileModeration::REJECTION_REASON_INVALID_NAME, $rejectionReasons, true)) {
                $lastUserProfileRevision = (new UserProfileRevision())
                    ->where('user_id', $user->id)
                    ->orderBy('created_at', 'desc')
                    ->value('data')
                ;

                $userData['last_accepted_name'] = $lastUserProfileRevision['last_accepted_name'] ?? null;
            }

            $updatedProfileCount = (new User())
                ->where('id', $user->id)
                ->where('status_updated_at', $user->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update($userData)
            ;

            cache_info()->userProfile($user->id)->forget();
            cache_info()->userLastModerationStatus($user->id)->forget();
            cache_info()->newProfileModerationCount($user->id)->forget();

            // if the status of the profile was already changed by someone else
            if ($updatedProfileCount === 0) {
                abort(403);
            }

            DB::commit();

            $this->dispatch(new NotifyAboutProfileStatusChange($statusChange->id));
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function changePassword(Request $request): void
    {
        $minPasswordLength = config('auth.minimum_password_length');

        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
            'new_password' => ['required', 'string', 'min:' . $minPasswordLength],
        ]);

        $user = (new User())->findOrFail($request->get('user_id'), ['id', 'status', 'is_protected']);

        if ($user->is_protected && $user->id !== current_user()->id) {
            abort(403);
        }

        if (in_array($user->status, [
            User::STATUS_BANNED,
            User::STATUS_DEACTIVATED,
            User::STATUS_PENDING_DELETION,
            User::STATUS_DELETED,
        ], true)) {
            abort(403);
        }

        $user->update([
            'password' => bcrypt($request->get('new_password')),
            'password_updated_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'remember_token' => str_random(60),
        ]);
    }

    public function changeBirthday(Request $request): void
    {
        $minPasswordLength = config('auth.minimum_password_length');

        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
            'birthday' => ['required', 'date_format:' . DATE_FORMAT],
        ]);

        $user = (new User())->findOrFail($request->get('user_id'), ['id', 'status', 'is_protected']);

        if ($user->is_protected && $user->id !== current_user()->id) {
            abort(403);
        }

        if (in_array($user->status, [
            User::STATUS_BANNED,
            User::STATUS_DEACTIVATED,
            User::STATUS_PENDING_DELETION,
            User::STATUS_DELETED,
        ], true)) {
            abort(403);
        }

        $user->update([
            'birthday' => $request->get('birthday'),
        ]);

        cache_info()->userProfile($user->id)->forget();
    }

    public function changeCoinsBalance(Request $request, Coins $coins): void
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
            'amount' => ['required', 'integer'],
            'comment' => ['nullable', 'string'],
        ]);

        $user = (new User())->findOrFail($request->get('user_id'), ['id', 'status', 'is_protected']);

        if ($user->is_protected && $user->id !== current_user()->id) {
            abort(403);
        }

        if (in_array($user->status, [
            User::STATUS_BANNED,
            User::STATUS_DEACTIVATED,
            User::STATUS_PENDING_DELETION,
            User::STATUS_DELETED,
        ], true)) {
            abort(403);
        }

        $coinTransactionDto = (new CoinTransactionDto())
            ->setType(CoinTransaction::TYPE_CHANGE_BY_ADMINISTRATION)
            ->setAmount($request->get('amount'))
            ->setUserId($user->id)
            ->setAdminId(current_user()->id)
            ->setAdminComment($request->get('comment') ?? '')
            ->setDescription(sprintf('Balance change by admin #%d', current_user()->id))
        ;

        $coins->changeBalance($coinTransactionDto);
    }

    public function generateLoginUrl(Request $request): array
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
        ]);

        $user = (new User())->findOrFail($request->get('user_id'), ['id', 'status', 'is_protected']);

        if ($user->is_protected) {
            abort(403);
        }

        if (in_array($user->status, [
            User::STATUS_BANNED,
            User::STATUS_DELETED,
        ], true)) {
            abort(403);
        }

        $userLoginUrlGenerator = new UserLoginUrlGenerator();

        return [
            'login_url' => $userLoginUrlGenerator->generateUrl($request->get('user_id'), current_user()->id),
        ];
    }

    public function ban(Request $request, BanUsers $banUsers): void
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
        ]);

        $userSex = (new User())->where('id', $request->get('user_id'))->value('sex');

        $banReasons = [
            //
        ];

        $this->validate($request, [
            'reasons' => ['required', 'array', 'min:1'],
            'reasons.*' => ['required', Rule::in($banReasons)],
            'comment' => ['nullable', 'string'],
            'comment_for_user' => ['nullable', 'string'],
            'ban_period' => ['required', Rule::in([0, 30, 60, 180, 60 * 12, 60 * 24, 60 * 24 * 3, 60 * 24 * 7])],
        ]);

        $user = (new User())->findOrFail($request->get('user_id'), [
            'id',
            'status',
            'status_updated_at',
            'is_protected',
        ]);

        if ($user->is_protected) {
            abort(403);
        }

        if (in_array($user->status, [
            User::STATUS_BANNED,
            User::STATUS_PENDING_DELETION,
            User::STATUS_DELETED,
        ], true)) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $ban = (new UserBanDto())
                ->setUserId($user->id)
                ->setReasons($request->get('reasons'))
                ->setComment($request->get('comment'))
                ->setCommentForUser($request->get('comment_for_user'))
                ->setAdminId(current_user()->id)
                ->setBanPeriod($request->get('ban_period'))
            ;

            $banUsers->ban($ban);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function unban(Request $request, BanUsers $banUsers): void
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
            'comment' => ['required', 'string'],
        ]);

        $user = (new User())->findOrFail($request->get('user_id'), ['id', 'status']);

        if ($user->status !== User::STATUS_BANNED) {
            abort(403);
        }

        $userUnbanDto = (new UserUnbanDto())
            ->setUserId($user->id)
            ->setAdminId(current_user()->id)
            ->setComment($request->get('comment'))
        ;

        $banUsers->unban($userUnbanDto);
    }

    public function deleteUser(Request $request): void
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
        ]);

        $user = (new User())->findOrFail($request->get('user_id'), [
            'id',
            'name',
            'phone_number',
            'phone_number_verified_at',
            'email',
            'email_verified_at',
            'sex',
            'status',
            'status_updated_at',
            'is_protected',
        ]);

        if ($user->is_protected) {
            abort(403);
        }

        $commentRequiredRule = Rule::requiredIf(function () use ($request) {
            return $request->get('reason') === UserDeletion::DELETION_REASON_OTHER;
        });

        $this->validate($request, [
            'reason' => ['required', Rule::in(UserDeletion::getReasons($user->sex, 'admin'))],
            'comment' => ['nullable', $commentRequiredRule, 'string'],
        ]);

        if (in_array($user->status, [
            User::STATUS_BANNED,
            User::STATUS_PENDING_DELETION,
            User::STATUS_DELETED,
        ], true)) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $deletion = (new UserDeletion())->create([
                'user_id' => $user->id,
                'name' => $user->name,
                'phone_number' => $user->phone_number,
                'phone_number_verified_at' => $user->phone_number_verified_at,
                'email' => $user->email,
                'email_verified_at' => $user->email_verified_at,
                'reason' => $request->get('reason'),
                'comment' => $request->get('comment'),
                'deleted_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'deleted_by' => current_user()->id,
            ]);

            (new User())
                ->where('id', $user->id)
                ->where('status_updated_at', $user->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'name' => 'DELETED USER',
                    'phone_number' => sprintf('DELETED #%d', $user->id),
                    'phone_number_country_code' => null,
                    'phone_number_verified_at' => null,
                    'phone_number_in_national_format' => sprintf('DELETED #%d', $user->id),
                    'email' => sprintf('DELETED #%d', $user->id),
                    'email_verified_at' => null,
                    'password' => bcrypt(str_random(config('auth.minimum_password_length'))),
                    'password_updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                    'remember_token' => null,
                    'status' => User::STATUS_DELETED,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'deletion_id' => $deletion->id,
                    'moderation_moderator_id' => null,
                    'moderation_claimed_at' => null,
                    'moderation_forwarded_to' => null,
                    'moderation_forwarded_by' => null,
                    'moderation_forwarded_at' => null,
                ])
            ;

            (new Notification())->create([
                'user_id' => $user->id,
                'type' => Notification::TYPE_PROFILE_DELETED,
                'title' => 'notifications:profile-deleted.title',
                'body' => 'notifications:profile-deleted.body',
                'is_hidden' => true,
                'created_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();

            cache_info()->userProfile($user->id)->forget();
            cache_info()->userLastModerationStatus($user->id)->forget();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }
}
