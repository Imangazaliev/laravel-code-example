<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Events\PhotoStatusChanged;
use App\Http\Controllers\Controller;
use App\Jobs\GenerateUserPhotoVariants;
use App\Jobs\Notifications\NotifyAboutPhotoStatusChange;
use App\Models\PhotoModeration;
use App\Models\PhotoStatusChange;
use App\Models\UserPhoto;
use App\Services\Profile\UserPhotoMeta;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Throwable;

class PhotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:moderate-photos');
    }

    public function getPhoto(Request $request): array
    {
        $this->validate($request, [
            'photo_id' => ['required', 'integer', 'exists:user_photos,id'],
        ]);

        $photo = (new UserPhoto())
            ->with('user:id,name,sex,birthday')
            ->withTrashed()
            ->findOrFail($request->get('photo_id'), [
                'id',
                'user_id',
                'status',
                'original_file_name',
                'exif_data',
                'sha256_hash',
                'rotation_angle',
                'storage_type',
                'path',
                'uploaded_at',
            ])
        ;

        if ($photo->user->sex !== current_user()->sex && ! current_user()->can('view-all-photos')) {
            abort(403);
        }

        $duplicatePhotoIds = (new UserPhoto())
            ->with('user:id,name')
            ->withTrashed()
            ->where('sha256_hash', $photo->sha256_hash)
            ->where('id', '!=', $photo->id)
            ->get(['id', 'user_id'])
            ->all()
        ;

        $madeAt = null;

        if ($photo->exif_data !== null && array_key_exists('DateTime', $photo->exif_data)) {
            $madeAt = UserPhotoMeta::parseMadeAt($photo->exif_data['DateTime']);
        }

        return [
            'photo' => [
                'id' => $photo->id,
                'status' => $photo->status,
                'url' => $photo->getImageUrl(),
                'original_file_name' => $photo->original_file_name,
                'additional_data' => $photo->exif_data === null ? null : [
                    'device_vendor' => $photo->exif_data['Make'] ?? null,
                    'device_model' => $photo->exif_data['Model'] ?? null,
                    'made_at' => $madeAt,
                ],
                'rotation_angle' => $photo->rotation_angle,
                'uploaded_at' => $photo->uploaded_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'user' => [
                    'id' => $photo->user->id,
                    'name' => $photo->user->name,
                    'birthday' => $photo->user->birthday->format(DATE_FORMAT),
                ],
                'duplicates' => array_map(function (UserPhoto $photo) {
                    return [
                        'id' => $photo->id,
                        'user' => [
                            'id' => $photo->user->id,
                            'name' => $photo->user->name,
                        ],
                    ];
                }, $duplicatePhotoIds),
            ],
        ];
    }

    public function acceptPhoto(Request $request): void
    {
        $this->validate($request, [
            'photo_id' => ['required', 'exists:user_photos,id'],
        ]);

        $photo = (new UserPhoto())->find($request->get('photo_id'), [
            'id',
            'user_id',
            'status',
            'status_updated_at',
        ]);

        if ($photo->status !== UserPhoto::STATUS_REJECTED) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $statusChange = (new PhotoStatusChange())->create([
                'photo_id' => $photo->id,
                'photo_owner_id' => $photo->user_id,
                'new_status' => UserPhoto::STATUS_ACCEPTED,
                'changed_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_FORMAT),
                'changed_by' => current_user()->id,
            ]);

            $updatedPhotoCount = (new UserPhoto())
                ->where('id', $photo->id)
                ->where('status_updated_at', $photo->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'status' => UserPhoto::STATUS_ACCEPTED,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'moderation_moderator_id' => null,
                    'moderation_claimed_at' => null,
                    'moderation_forwarded_to' => null,
                    'moderation_forwarded_by' => null,
                    'moderation_forwarded_at' => null,
                ])
            ;

            // if the status of the profile was already changed by someone else
            if ($updatedPhotoCount === 0) {
                abort(403);
            }

            event(new PhotoStatusChanged($photo->id));

            DB::commit();

            $this->dispatch(new NotifyAboutPhotoStatusChange($statusChange->id));
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function rejectPhoto(Request $request): void
    {
        $rejectionReasons = array_keys(PhotoModeration::getRejectionReasons());

        $this->validate($request, [
            'photo_id' => ['required', 'exists:user_photos,id'],
            'rejection_reasons' => ['required', 'array', 'min:1'],
            'rejection_reasons.*' => ['required', Rule::in($rejectionReasons)],
            'comment' => ['nullable', 'string'],
            'comment_for_user' => ['nullable', 'string'],
        ]);

        $photo = (new UserPhoto())->find($request->get('photo_id'), [
            'id',
            'user_id',
            'status',
            'status_updated_at',
        ]);

        if ($photo->status !== UserPhoto::STATUS_ACCEPTED) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $statusChange = (new PhotoStatusChange())->create([
                'photo_id' => $photo->id,
                'photo_owner_id' => $photo->user_id,
                'new_status' => UserPhoto::STATUS_REJECTED,
                'rejection_reasons' => $request->get('rejection_reasons'),
                'comment' => $request->get('comment'),
                'comment_for_user' => $request->get('comment_for_user'),
                'changed_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_FORMAT),
                'changed_by' => current_user()->id,
            ]);

            $updatedPhotoCount = (new UserPhoto())
                ->where('id', $photo->id)
                ->where('status_updated_at', $photo->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'status' => UserPhoto::STATUS_REJECTED,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'moderation_moderator_id' => null,
                    'moderation_claimed_at' => null,
                    'moderation_forwarded_to' => null,
                    'moderation_forwarded_by' => null,
                    'moderation_forwarded_at' => null,
                ])
            ;

            // if the status of the profile was already changed by someone else
            if ($updatedPhotoCount === 0) {
                abort(403);
            }

            event(new PhotoStatusChanged($photo->id));

            DB::commit();

            $this->dispatch(new NotifyAboutPhotoStatusChange($statusChange->id));
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function editPhoto(Request $request)
    {
        $this->validate($request, [
            'photo_id' => ['required', 'exists:user_photos,id'],
            'rotation_angle' => ['required', Rule::in(0, 90, 180, 270)],
        ]);

        $photo = (new UserPhoto())
            ->withTrashed()
            ->find($request->get('photo_id'), [
                'id',
                'user_id',
                'rotation_angle',
                'status',
            ])
        ;

        if ( ! in_array($photo->status, [UserPhoto::STATUS_ACCEPTED, UserPhoto::STATUS_REJECTED], true)) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            (new UserPhoto())
                ->where('id', $photo->id)
                ->update([
                    'rotation_angle' => $request->get('rotation_angle'),
                    'is_being_processed' => false,
                ])
            ;

            DB::commit();

            $this->dispatch(new GenerateUserPhotoVariants($photo->id));
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }
}
