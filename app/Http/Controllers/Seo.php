<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use ExampleProject\RobotsTxtGenerator\ArrayRobotsTxtGeneratorBuilder;
use ExampleProject\RobotsTxtGenerator\UnknownRuleException;
use Symfony\Component\HttpFoundation\Response;

class Seo extends Controller
{
    public function getRobotsTxtFile(): Response
    {
        $robotsContent = config('seo.robots_access')
            ? $this->robotsForProduction()
            : $this->robotsForDebug()
        ;

        return response($robotsContent)->header('Content-type', 'text/plain');
    }

    /**
     * @throws UnknownRuleException
     *
     * @return string
     */
    private function robotsForProduction(): string
    {
        $host = config('app.url');

        $sitemapUrl = $host . '/sitemap.xml';

        $rules = [
            'common' => [
                'Disallow' => [],
            ],
            'specific' => [
                'Yandex' => [
                    'Crawl-delay' => 5,
                ],
            ],
        ];

        $robotsTxtGeneratorBuilder = new ArrayRobotsTxtGeneratorBuilder();

        $robotsTxtGeneratorBuilder
            ->setHost($host)
            ->setSitemapUrl($sitemapUrl)
            ->setCommonRules($rules['common'])
            ->setSpecificRules($rules['specific'])
        ;

        $robotsTxtGenerator = $robotsTxtGeneratorBuilder->make();

        return $robotsTxtGenerator->generate();
    }

    /**
     * @return string
     */
    private function robotsForDebug(): string
    {
        return implode("\n", [
            'User-agent: *',
            'Disallow: /',
        ]);
    }

    public function getSitemap(): Response
    {
        $lines = [];

        $lines[] = '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" '
            . 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            . 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 '
            . 'http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">';

        $languageCode = language()->code;

        $files = File::glob(public_path("sitemaps/{$languageCode}") . '/*');
        $files = array_map('basename', $files);

        $files = array_filter($files, function ($file) {
            return mb_strpos($file, 'sitemap_') === 0;
        });

        $siteUrl = config('app.url');

        foreach ($files as $file) {
            $lines[] = '<sitemap>';
            $lines[] = "<loc>{$siteUrl}/sitemaps/{$languageCode}/{$file}</loc>";
            $lines[] = '</sitemap>';
        }

        $lines[] = '</sitemapindex>';

        $xml = implode("\n", $lines);

        return response($xml, 200)->header('Content-Type', 'application/xml; charset=utf-8');
    }
}
