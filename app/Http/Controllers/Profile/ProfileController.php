<?php

declare(strict_types=1);

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileEditRequest;
use App\Jobs\CalculateUserReputation;
use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\ChatMessageModeration;
use App\Models\ChatMessageStatusChange;
use App\Models\ChatRequest;
use App\Models\Country;
use App\Models\ContactsRequest;
use App\Models\Language;
use App\Models\Locality;
use App\Models\Notification;
use App\Models\PhotoModeration;
use App\Models\PhotoStatusChange;
use App\Models\ProfileLike;
use App\Models\ProfileModeration;
use App\Models\ProfileStatusChange;
use App\Models\ProfileView;
use App\Models\SupportChat;
use App\Models\SupportChatMessage;
use App\Models\User;
use App\Models\UserBan;
use App\Models\UserDeactivation;
use App\Models\UserDeletion;
use App\Models\UserPhoto;
use App\Services\BanUsers\BanUsers;
use App\Services\BanUsers\UserBanDto;
use App\Services\Frontend\JavaScriptVariables;
use App\Services\ProfileCompletionCalculator\ProfileCompletionCalculator;
use App\Services\RequiredFieldsChecker\RequiredFieldsChecker;
use App\Transformers\NotificationsTransformer;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Throwable;

class ProfileController extends Controller
{
    private const VIEWS_PER_PAGE = 24;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile(): array
    {
        return JavaScriptVariables::convertUserData(current_user());
    }

    public function extendedInformation(): array
    {
        /**
         * @var User $currentUser
         */
        $currentUser = current_user();

        $knownLanguages = $currentUser
            ->knownLanguages()
            ->withTranslation(null, ['name'])
            ->get(['languages.id'])
            ->sortBy('name')
        ;

        return [
            'country_id' => $currentUser->country_id,
            'locality_id' => $currentUser->locality_id,
            'ethnicity_id' => $currentUser->ethnicity_id,
            'profile_headline' => $currentUser->profile_headline,
            'height' => $currentUser->height,
            'weight_from' => $currentUser->weight_from,
            'weight_to' => $currentUser->weight_to,
            'body_type' => $currentUser->body_type,
            'hair_color' => $currentUser->hair_color,
            'eye_color' => $currentUser->eye_color,
            'facial_hair' => $currentUser->facial_hair,
            'smoking' => $currentUser->smoking,
            'alcohol_consumption' => $currentUser->alcohol_consumption,
            'self_description' => $currentUser->self_description,
            'hobbies_and_interests' => $currentUser->hobbies_and_interests,
            'sport' => $currentUser->sport,
            'sport_exercises_frequency' => $currentUser->sport_exercises_frequency,
            'marital_status' => $currentUser->marital_status,
            'child_count' => $currentUser->child_count,
            'children_in_household' => $currentUser->children_in_household,
            'living_with_parents' => $currentUser->living_with_parents,
            'desired_number_of_children' => $currentUser->desired_number_of_children,
            'children_adoption' => $currentUser->children_adoption,
            'housing_type' => $currentUser->housing_type,
            'relocation' => $currentUser->relocation,
            'education' => $currentUser->education,
            'profession_or_occupation' => $currentUser->profession_or_occupation,
            'employment' => $currentUser->employment,
            'job_title' => $currentUser->job_title,
            'financial_status' => $currentUser->financial_status,
            'known_languages' => $knownLanguages->map(function (Language $language) {
                return [
                    'id' => $language->id,
                    'name' => $language->getTranslation()->name,
                ];
            }),
            'religion' => $currentUser->religion,
            'future_spouse_age_from' => $currentUser->future_spouse_age_from,
            'future_spouse_age_to' => $currentUser->future_spouse_age_to,
            'future_spouse_height_from' => $currentUser->future_spouse_height_from,
            'future_spouse_height_to' => $currentUser->future_spouse_height_to,
            'future_spouse_weight_from' => $currentUser->future_spouse_weight_from,
            'future_spouse_weight_to' => $currentUser->future_spouse_weight_to,
            'future_spouse_body_type' => $currentUser->future_spouse_body_type,
            'future_spouse_character_traits' => $currentUser->future_spouse_character_traits,
            'future_spouse_description' => $currentUser->future_spouse_description,
            'future_spouse_children' => $currentUser->future_spouse_children,
        ];
    }

    public function edit(ProfileEditRequest $request, ProfileCompletionCalculator $profileCompletionCalculator): void
    {
        $data = $request->validated();

        if (array_key_exists('locality_id', $data)) {
            $locality = (new Locality())->find($data['locality_id'], [
                'country_id',
                'subdivision_level_1_id',
                'subdivision_level_2_id',
                'subdivision_level_3_id',
            ]);

            $data['country_id'] = $locality->country_id;
            $data['subdivision_level_1_id'] = $locality->subdivision_level_1_id;
            $data['subdivision_level_2_id'] = $locality->subdivision_level_2_id;
            $data['subdivision_level_3_id'] = $locality->subdivision_level_3_id;
        }

        if (array_key_exists('native_locality_id', $data)) {
            $nativeLocality = (new Locality())->find($data['native_locality_id'], [
                'country_id',
                'subdivision_level_1_id',
                'subdivision_level_2_id',
                'subdivision_level_3_id',
            ]);

            $data['native_country_id'] = $nativeLocality->country_id;
            $data['native_subdivision_level_1_id'] = $nativeLocality->subdivision_level_1_id;
            $data['native_subdivision_level_2_id'] = $nativeLocality->subdivision_level_2_id;
            $data['native_subdivision_level_3_id'] = $nativeLocality->subdivision_level_3_id;
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            current_user()->update(array_merge($data, [
                'status' => User::STATUS_NOT_MODERATED,
                'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                'updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]));

            $completionPercent = $profileCompletionCalculator->calculateProfileCompletion(current_user()->id);

            current_user()->update([
                'profile_completion_percent' => $completionPercent,
                'profile_completion_percent_updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'required_fields_filled' => RequiredFieldsChecker::checkRequiredFieldsFilled(current_user()),
                'required_fields_checked_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);

            if (array_key_exists('known_language_ids', $data)) {
                current_user()->knownLanguages()->sync($data['known_language_ids']);
            }

            DB::commit();

            cache_info()->userProfile(current_user()->id)->forget();
            cache_info()->userLastModerationStatus(current_user()->id)->forget();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }

        if (current_user()->completed_registration_at !== null) {
            $this->dispatch(new CalculateUserReputation(current_user()->id));

            $banUsers = new BanUsers();

            $banReasons = $banUsers->getBanReasons(current_user());

            if (count($banReasons) > 0) {
                $ban = (new UserBanDto())
                    ->setUserId(current_user()->id)
                    ->setReasons($banReasons)
                ;

                $banUsers->ban($ban);

                return;
            }
        }
    }

    public function rejectionInfo(): ?array
    {
        $lastModeration = (new ProfileModeration())
            ->where('user_id', current_user()->id)
            ->orderBy('moderated_at', 'desc')
            ->first()
        ;

        if ($lastModeration !== null) {
            $lastStatusChange = (new ProfileStatusChange())
                ->where('user_id', current_user()->id)
                ->where('changed_at', '>', $lastModeration->moderated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->orderBy('changed_at', 'desc')
                ->first()
            ;

            if ($lastStatusChange !== null) {
                return [
                    'notification' => NotificationsTransformer::transformProfileStatusChange($lastStatusChange),
                ];
            }
        }

        return [
            'notification' => $lastModeration !== null ? NotificationsTransformer::transformProfileModeration($lastModeration) : null,
        ];
    }

    public function missedFields(): array
    {
        $futureSpouseContext = current_user()->sex === User::SEX_MALE ? 'wife' : 'husband';

        $fieldTitles = [
            'ethnicity_id' => __('profile:ethnicity'),
            'country_id' => __('country'),
            'locality_id' => __('city-or-locality'),
            'height' => __('profile:height'),
            'weight_from' => __('profile:weight'),
            'weight_to' => __('profile:weight'),
            'smoking' => __('profile:smoking'),
            'alcohol_consumption' => __('profile:alcohol-consumption'),
            'marital_status' => __('profile:marital-status'),
            'child_count' => __('profile:child-count'),
            'body_type' => __('profile:body-type'),
            'hair_color' => __('profile:hair-color'),
            'facial_hair' => __('profile:facial-hair'),
            'employment' => __('profile:employment'),
            'housing_type' => __('profile:housing-type'),
            'financial_status' => __('profile:financial-status'),
            'religion' => __('profile:religion'),
            'future_spouse_age_from' => __('profile:future-spouse-age.from', [], ['context' => $futureSpouseContext]),
            'future_spouse_age_to' => __('profile:future-spouse-age.to', [], ['context' => $futureSpouseContext]),
        ];

        $missedFields = RequiredFieldsChecker::getMissedFields(current_user());

        $response = [];

        foreach ($missedFields as $fieldName) {
            $response[$fieldName] = $fieldTitles[$fieldName] ?? $fieldName;
        }

        return $response;
    }

    public function banInfo(): array
    {
        if (current_user()->status !== User::STATUS_BANNED) {
            abort(403);
        }

        $sexSuffix = current_user()->sex === User::SEX_MALE ? 'male' : 'female';

        $banReasonTitles = [
            //
        ];

        $ban = current_user()->ban()->firstOrFail();

        return [
            'reasons' => array_values(array_only($banReasonTitles, $ban->reasons)),
            'banned_at' => $ban->banned_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'unban_at' => $ban->unban_at !== null ? $ban->unban_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT) : null,
            'comment' => $ban->comment_for_user,
        ];
    }

    public function updates(): array
    {
        /**
         * @var User $currentUser
         */
        $currentUser = current_user();

        $viewCount = (int) cache_info()->newProfileViewCount($currentUser->id)->remember(function () use ($currentUser) {
            return (new ProfileView())
                ->where('viewed_profile_id', $currentUser->id)
                ->where('is_incognito', false)
                ->whereNull('seen_by_owner_at')
                ->count()
            ;
        });

        $likeCount = (int) cache_info()->newLikeCount($currentUser->id)->remember(function () use ($currentUser) {
            return (new ProfileLike())
                ->where('target_user_id', $currentUser->id)
                ->whereNull('seen_by_owner_at')
                ->whereNull('removed_at')
                ->count()
            ;
        });

        $chatRequestsCount = (int) cache_info()->newChatRequestCount($currentUser->id)->remember(function () use ($currentUser) {
            return (new ChatRequest())
                ->where('target_user_id', $currentUser->id)
                ->where('status', ChatRequest::STATUS_NEW)
                ->count()
            ;
        });

        $chatIds = cache_info()->chatIds($currentUser->id)->remember(function () use ($currentUser) {
            return (new Chat())
                ->whereActive()
                ->where(function (Builder $query) use ($currentUser) {
                    $query
                        ->where('requestor_id', $currentUser->id)
                        ->orWhere('target_user_id', $currentUser->id)
                    ;
                })
                ->pluck('id')
                ->all()
            ;
        });

        $messageCount = (int) cache_info()->newMessageCount($currentUser->id)->remember(function () use ($chatIds, $currentUser) {
            return (new ChatMessage())
                ->whereIn('chat_id', $chatIds)
                ->where('user_id', '!=', $currentUser->id)
                ->whereIn('status', [
                    ChatMessage::STATUS_ACCEPTED,
                    ChatMessage::STATUS_REJECTED_BY_MODERATOR,
                ])
                ->whereNull('seen_at')
                ->count()
            ;
        });

        if ($currentUser->sex === User::SEX_FEMALE) {
            $messageCount += (int) cache_info()->newContactsRequestCount($currentUser->id)->remember(function () use ($chatIds, $currentUser) {
                return (new ContactsRequest())
                    ->whereIn('chat_id', $chatIds)
                    ->where('target_user_id', $currentUser->id)
                    ->where('status', ContactsRequest::STATUS_NEW)
                    ->count()
                ;
            });
        }

        if ($currentUser->sex === User::SEX_MALE) {
            $messageCount += (int) cache_info()->newContactsRequestAnswerCount($currentUser->id)->remember(function () use ($chatIds, $currentUser) {
                return (new ContactsRequest())
                    ->whereIn('chat_id', $chatIds)
                    ->where('requestor_id', $currentUser->id)
                    ->whereIn('status', [ContactsRequest::STATUS_ACCEPTED, ContactsRequest::STATUS_REJECTED])
                    ->whereNull('answer_seen_at')
                    ->count()
                ;
            });
        }

        $supportChatId = cache_info()->supportChatId($currentUser->id)->remember(function () use ($currentUser) {
            return (new SupportChat())
                ->where('user_id', $currentUser->id)
                ->value('id')
            ;
        });

        $supportChatId = $supportChatId !== null ? (int) $supportChatId : null;

        $supportChatMessageCount = (int) cache_info()->newSupportMessageCount($currentUser->id)->remember(function () use ($currentUser, $supportChatId) {
            return (new SupportChatMessage())
                ->where('chat_id', $supportChatId)
                ->where('user_id', '!=', $currentUser->id)
                ->where('status', SupportChatMessage::STATUS_SENT)
                ->whereNull('seen_at')
                ->count()
            ;
        });

        $notificationCount = (int) cache_info()->newNotificationCount($currentUser->id)->remember(function () use ($currentUser) {
            return (new Notification())
                ->where('user_id', $currentUser->id)
                ->where('is_hidden', false)
                ->whereNull('seen_at')
                ->count()
            ;
        });

        $notificationCount += (int) cache_info()->newProfileModerationCount($currentUser->id)->remember(function () use ($currentUser) {
            $moderationCount = (new ProfileModeration())
                ->where('user_id', $currentUser->id)
                ->whereIn('status', [ProfileModeration::STATUS_ACCEPTED, ProfileModeration::STATUS_REJECTED])
                ->whereNull('seen_by_user_at')
                ->count()
            ;

            return $moderationCount + (new ProfileStatusChange())
                ->where('user_id', $currentUser->id)
                ->whereNull('seen_by_user_at')
                ->count()
            ;
        });

        $notificationCount += (int) cache_info()->newPhotoModerationCount($currentUser->id)->remember(function () use ($currentUser) {
            $moderationCount = (new PhotoModeration())
                ->where('photo_owner_id', $currentUser->id)
                ->whereIn('status', [PhotoModeration::STATUS_ACCEPTED, PhotoModeration::STATUS_REJECTED])
                ->whereNull('seen_by_user_at')
                ->count()
                ;

            return $moderationCount + (new PhotoStatusChange())
                ->where('photo_owner_id', $currentUser->id)
                ->whereNull('seen_by_user_at')
                ->count()
            ;
        });

        $notificationCount += (int) cache_info()->newMessageModerationCount($currentUser->id)->remember(function () use ($currentUser) {
            $moderationCount = (new ChatMessageModeration())
                ->where('message_author_id', $currentUser->id)
                ->where('status', ChatMessageModeration::STATUS_REJECTED)
                ->whereNull('seen_by_user_at')
                ->count()
            ;

            return $moderationCount + (new ChatMessageStatusChange())
                ->where('message_author_id', $currentUser->id)
                ->whereNull('seen_by_user_at')
                ->count()
            ;
        });

        return [
            'new_profile_view_count' => $viewCount,
            'new_like_count' => $likeCount,
            'new_chat_request_count' => $chatRequestsCount,
            'new_message_count' => $messageCount + $supportChatMessageCount,
            'new_notification_count' => $notificationCount,
        ];
    }

    public function guests(): array
    {
        $views = (new ProfileView())
            ->with([
                'visitor' => function (BelongsTo $query) {
                    $query->select([
                        'id',
                        'name',
                        'last_accepted_name',
                        'status',
                        'was_online_at',
                        'main_photo_id',
                        'is_verified',
                        'sex',
                        'birthday',
                        'country_id',
                        'locality_id',
                    ]);

                    $query->with([
                        'country' => function (BelongsTo $query) {
                            $query->withTranslation(null, ['name']);
                        },
                        'locality' => function (BelongsTo $query) {
                            $query->withTranslation(null, ['name']);
                        },
                    ]);
                },
            ])
            ->where('viewed_profile_id', current_user()->id)
            ->where('is_incognito', false)
            ->orderBy('viewed_at', 'desc')
            ->paginate(self::VIEWS_PER_PAGE, [
                'id',
                'visitor_id',
                'viewed_at',
                'seen_by_owner_at',
            ])
        ;

        return [
            'current_page' => $views->currentPage(),
            'page_count' => $views->lastPage(),
            'views' => array_map(function (ProfileView $view) {
                $visitor = $view->visitor;

                return [
                    'id' => $view->id,
                    'visitor' => [
                        'id' => $visitor->id,
                        'name' => $visitor->getName(),
                        'sex' => $visitor->sex,
                        'birthday' => $visitor->birthday->format(DATE_FORMAT),
                        'photo_url' => optional($visitor->mainPhoto, fn (UserPhoto $photo) => $photo->getImageUrlWithCheck(UserPhoto::SIZE_200)),
                        'country_name' => optional($visitor->country, fn (Country $country) => $country->getTranslation()->name),
                        'locality_name' => optional($visitor->locality, fn (Locality $locality) => $locality->getTranslation()->name),
                        'presence_status' => $visitor->getPresenceStatus(),
                        'was_online_at' => $visitor->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'is_verified' => $visitor->is_verified,
                    ],
                    'viewed_at' => $view->viewed_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                    'is_new' => $view->seen_by_owner_at === null,
                ];
            }, $views->items()),
        ];
    }

    public function markGuestsAsSeen(): void
    {
        (new ProfileView())
            ->where('viewed_profile_id', current_user()->id)
            ->where('is_incognito', false)
            ->whereNull('seen_by_owner_at')
            ->update([
                'seen_by_owner_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ])
        ;

        cache_info()->newProfileViewCount(current_user()->id)->forget();
    }

    public function viewedProfiles(): array
    {
        $views = (new ProfileView())
            ->with([
                'viewedProfile' => function (BelongsTo $query) {
                    $query->select([
                        'id',
                        'name',
                        'last_accepted_name',
                        'status',
                        'was_online_at',
                        'main_photo_id',
                        'is_verified',
                        'sex',
                        'birthday',
                        'country_id',
                        'locality_id',
                    ]);

                    $query->with([
                        'country' => function (BelongsTo $query) {
                            $query->withTranslation(null, ['name']);
                        },
                        'locality' => function (BelongsTo $query) {
                            $query->withTranslation(null, ['name']);
                        },
                    ]);
                },
            ])
            ->where('visitor_id', current_user()->id)
            ->orderBy('viewed_at', 'desc')
            ->paginate(self::VIEWS_PER_PAGE, [
                'id',
                'viewed_profile_id',
                'viewed_at',
            ])
        ;

        return [
            'current_page' => $views->currentPage(),
            'page_count' => $views->lastPage(),
            'views' => array_map(function (ProfileView $view) {
                $viewedProfile = $view->viewedProfile;

                return [
                    'id' => $view->id,
                    'profile' => [
                        'id' => $viewedProfile->id,
                        'name' => $viewedProfile->getName(),
                        'sex' => $viewedProfile->sex,
                        'birthday' => $viewedProfile->birthday->format(DATE_FORMAT),
                        'photo_url' => optional($viewedProfile->mainPhoto, fn (UserPhoto $photo) => $photo->getImageUrlWithCheck(UserPhoto::SIZE_200)),
                        'country_name' => optional($viewedProfile->country, fn (Country $country) => $country->getTranslation()->name),
                        'locality_name' => optional($viewedProfile->locality, fn (Locality $locality) => $locality->getTranslation()->name),
                        'presence_status' => $viewedProfile->getPresenceStatus(),
                        'was_online_at' => $viewedProfile->was_online_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                        'is_verified' => $viewedProfile->is_verified,
                    ],
                    'viewed_at' => $view->viewed_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ];
            }, $views->items()),
        ];
    }

    public function changePassword(Request $request): void
    {
        $minPasswordLength = config('auth.minimum_password_length');

        $this->validate($request, [
            'current_password' => ['required', 'string'],
            'new_password' => ['required', 'string', 'min:' . $minPasswordLength],
            'new_password_confirmation' => ['required', 'string', 'same:new_password'],
        ]);

        if ( ! Hash::check($request->get('current_password'), current_user()->password)) {
            throw ValidationException::withMessages([
                'current_password' => [trans('validation.password')],
            ]);
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            current_user()->update([
                'password' => bcrypt($request->get('new_password')),
                'password_updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'remember_token' => str_random(60),
            ]);

            (new Notification())->create([
                'user_id' => current_user()->id,
                'type' => Notification::TYPE_PASSWORD_CHANGED,
                'title' => 'notifications:password-changed.title',
                'body' => 'notifications:password-changed.body',
                'is_hidden' => true,
                'created_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function deactivateProfile(Request $request): void
    {
        $this->validate($request, [
            'comment' => ['nullable', 'string'],
        ]);

        /**
         * @var User $user
         */
        $user = current_user();

        // if a user already banned, deactivated or deleted
        if (in_array($user->status, [
            User::STATUS_BANNED,
            User::STATUS_DEACTIVATED,
            User::STATUS_PENDING_DELETION,
            User::STATUS_DELETED,
        ], true)) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $deactivation = (new UserDeactivation())->create([
                'user_id' => $user->id,
                'comment' => $request->get('comment') ?? '',
                'deactivated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);

            (new User())
                ->where('id', $user->id)
                ->where('status_updated_at', $user->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'status' => User::STATUS_DEACTIVATED,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'deactivation_id' => $deactivation->id,
                    'remember_token' => null,
                    'moderation_moderator_id' => null,
                    'moderation_claimed_at' => null,
                    'moderation_forwarded_to' => null,
                    'moderation_forwarded_by' => null,
                    'moderation_forwarded_at' => null,
                ])
            ;

            (new Notification())->create([
                'user_id' => $user->id,
                'type' => Notification::TYPE_PROFILE_DEACTIVATED,
                'title' => 'notifications:profile-deactivated.title',
                'body' => 'notifications:profile-deactivated.body',
                'is_hidden' => true,
                'created_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();

            cache_info()->userProfile(current_user()->id)->forget();
            cache_info()->userLastModerationStatus(current_user()->id)->forget();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function reactivateProfile(): void
    {
        /**
         * @var User $user
         */
        $user = current_user();

        // if a user already banned, deactivated or deleted
        if ($user->status !== User::STATUS_DEACTIVATED) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $user->deactivation()->update([
                'reactivated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);

            (new User())
                ->where('id', $user->id)
                ->where('status_updated_at', $user->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'status' => User::STATUS_NOT_MODERATED,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'deactivation_id' => null,
                ])
            ;

            (new Notification())->create([
                'user_id' => $user->id,
                'type' => Notification::TYPE_PROFILE_REACTIVATED,
                'title' => 'notifications:profile-reactivated.title',
                'body' => 'notifications:profile-reactivated.body',
                'is_hidden' => true,
                'created_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();

            cache_info()->userProfile(current_user()->id)->forget();
            cache_info()->userLastModerationStatus(current_user()->id)->forget();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function deleteProfile(Request $request): void
    {
        /**
         * @var User $user
         */
        $user = current_user();

        $this->validate($request, [
            'reason' => ['required', Rule::in(UserDeletion::getReasons($user->sex, 'user'))],
            'comment' => ['nullable', 'string'],
        ]);

        // if a user already banned, deactivated or deleted
        if (in_array($user->status, [
            User::STATUS_BANNED,
            User::STATUS_DEACTIVATED,
            User::STATUS_PENDING_DELETION,
            User::STATUS_DELETED,
        ], true)) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $deletion = (new UserDeletion())->create([
                'user_id' => $user->id,
                'name' => $user->name,
                'phone_number' => $user->phone_number,
                'phone_number_verified_at' => $user->phone_number_verified_at,
                'email' => $user->email,
                'email_verified_at' => $user->email_verified_at,
                'reason' => $request->get('reason'),
                'comment' => $request->get('comment'),
                'deleted_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);

            (new User())
                ->where('id', $user->id)
                ->where('status_updated_at', $user->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'status' => User::STATUS_PENDING_DELETION,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'deletion_id' => $deletion->id,
                    'moderation_moderator_id' => null,
                    'moderation_claimed_at' => null,
                    'moderation_forwarded_to' => null,
                    'moderation_forwarded_by' => null,
                    'moderation_forwarded_at' => null,
                ])
            ;

            (new Notification())->create([
                'user_id' => $user->id,
                'type' => Notification::TYPE_REQUESTED_PROFILE_DELETION,
                'title' => 'notifications:requested-profile-deletion.title',
                'body' => 'notifications:requested-profile-deletion.body',
                'is_hidden' => true,
                'created_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();

            cache_info()->userProfile(current_user()->id)->forget();
            cache_info()->userLastModerationStatus(current_user()->id)->forget();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function profileDeletionInfo(): array
    {
        /**
         * @var User $user
         */
        $user = current_user();

        if ($user->status !== User::STATUS_PENDING_DELETION) {
            abort(403);
        }

        $userDeletion = $user->deletion()->first();

        return [
            'deleted_at' => $userDeletion->deleted_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            'deletion_timeout' => config('profile.deletion_timeout'),
        ];
    }

    public function cancelProfileDeletion(): void
    {
        /**
         * @var User $user
         */
        $user = current_user();

        // if a user is not pending deletion
        if ($user->status !== User::STATUS_PENDING_DELETION) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            $userDeletion = $user->deletion()->first();

            (new User())
                ->where('id', $user->id)
                ->where('status_updated_at', $user->status_updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'status' => User::STATUS_NOT_MODERATED,
                    'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
                    'deletion_id' => null,
                ])
            ;

            $userDeletion->update([
                'canceled_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);

            (new Notification())->create([
                'user_id' => $user->id,
                'type' => Notification::TYPE_PROFILE_DELETION_CANCELED,
                'title' => 'notifications:profile-deletion-canceled.title',
                'body' => 'notifications:profile-deletion-canceled.body',
                'is_hidden' => true,
                'created_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();

            cache_info()->userProfile(current_user()->id)->forget();
            cache_info()->userLastModerationStatus(current_user()->id)->forget();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }
}
