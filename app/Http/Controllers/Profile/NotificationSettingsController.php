<?php

declare(strict_types=1);

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Services\ConnectedAccounts\ConnectedAccounts;
use App\Services\NotificationDelivery\NotificationDelivery;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class NotificationSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function notificationSettings(ConnectedAccounts $connectedAccounts): array
    {
        $notificationSettings = current_user()->notificationSettings();

        $telegramConnected = $connectedAccounts->isConnected(current_user()->id, 'telegram');

        $channels = [
            [
                'code' => NotificationDelivery::CHANNEL_EMAIL,
                'available' => current_user()->isEmailVerified(),
                'enabled' => current_user()->isEmailVerified() && $notificationSettings->isChannelEnabled(NotificationDelivery::CHANNEL_EMAIL),
            ],
            [
                'code' => NotificationDelivery::CHANNEL_TELEGRAM,
                'available' => $telegramConnected,
                'enabled' => $telegramConnected && $notificationSettings->isChannelEnabled(NotificationDelivery::CHANNEL_TELEGRAM),
            ],
        ];

        $notificationTypes = [
            [
                'code' => NotificationDelivery::TYPE_PROFILE_MODERATION,
                'label' => 'Принятие/отклонение анкеты',
                'enabled' => $notificationSettings->isNotificationTypeEnabled(NotificationDelivery::TYPE_PROFILE_MODERATION),
            ],
            [
                'code' => NotificationDelivery::TYPE_PHOTO_MODERATION,
                'label' => 'Принятие/отклонение фото',
                'enabled' => $notificationSettings->isNotificationTypeEnabled(NotificationDelivery::TYPE_PHOTO_MODERATION),
            ],
            [
                'code' => NotificationDelivery::TYPE_INCOMING_CHAT_REQUEST,
                'label' => 'Входящий запрос на знакомство',
                'enabled' => $notificationSettings->isNotificationTypeEnabled(NotificationDelivery::TYPE_INCOMING_CHAT_REQUEST),
            ],
            [
                'code' => NotificationDelivery::TYPE_CHAT_REQUEST_ANSWER,
                'label' => 'Ответ на исходящий запрос на знакомство (отклонено/принято)',
                'enabled' => $notificationSettings->isNotificationTypeEnabled(NotificationDelivery::TYPE_CHAT_REQUEST_ANSWER),
            ],
            [
                'code' => NotificationDelivery::TYPE_NEW_MESSAGES,
                'label' => 'Новые сообщения',
                'enabled' => $notificationSettings->isNotificationTypeEnabled(NotificationDelivery::TYPE_NEW_MESSAGES),
            ],
            [
                'code' => NotificationDelivery::TYPE_PROFILE_DUPLICATES,
                'label' => 'Дубликаты профилей',
                'enabled' => $notificationSettings->isNotificationTypeEnabled(NotificationDelivery::TYPE_PROFILE_DUPLICATES),
                'permission' => 'view-profile-duplicates',
            ],
            [
                'code' => NotificationDelivery::TYPE_SUSPICIOUS_MESSAGES,
                'label' => 'Подозрительные сообщения',
                'enabled' => $notificationSettings->isNotificationTypeEnabled(NotificationDelivery::TYPE_SUSPICIOUS_MESSAGES),
                'permission' => 'view-suspicious-messages',
            ],
        ];

        // array values is required to reset keys
        $notificationTypes = array_values(array_filter($notificationTypes, function (array $type) {
            return array_key_exists('permission', $type) ? current_user()->can($type['permission']) : true;
        }));

        return [
            'channels' => $channels,
            'notification_types' => $notificationTypes,
        ];
    }

    public function update(Request $request): void
    {
        $this->validate($request, [
            'channels' => ['present', 'array'],
            'channels.*' => ['string', Rule::in(NotificationDelivery::getNotificationChannels())],
            'notification_types' => ['present', 'array'],
            'notification_types.*' => ['string', Rule::in(NotificationDelivery::getNotificationTypes())],
        ]);

        $notificationSettings = current_user()->notificationSettings();

        $notificationSettings->setChannels($request->get('channels'));
        $notificationSettings->setNotificationTypes($request->get('notification_types'));

        current_user()->update([
            'notification_settings' => $notificationSettings->toArray(),
        ]);
    }
}
