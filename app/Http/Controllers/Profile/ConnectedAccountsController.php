<?php

declare(strict_types=1);

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Services\ConnectedAccounts\ConnectedAccounts;
use App\Services\OAuth\OAuthManager;
use Illuminate\Http\Request;

class ConnectedAccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function connectedAccounts(): array
    {
        $connectedServiceCodes = current_user()
            ->connectedAccounts()
            ->whereNull('disconnected_at')
            ->pluck('provider_code')
            ->all()
        ;

        return [
            'connected_accounts' => $connectedServiceCodes,
        ];
    }

    public function redirectUrl(Request $request, OAuthManager $oauthManager): array
    {
        $this->validate($request, [
            'provider' => ['required', 'string'],
        ]);

        $oauthProvider = $request->get('provider');
        $redirectUrl = route('connected-accounts.callback');

        return [
            'redirect_url' => $oauthManager->provider($oauthProvider)->getAuthUrl($redirectUrl),
        ];
    }

    public function callback(Request $request)
    {
        // TODO: check there's no connected account (respecting disconnected_at)
    }

    public function connectTelegram(Request $request, ConnectedAccounts $connectedAccounts): void
    {
        $data = $this->validate($request, [
            'id' => ['required', 'integer'],
            'username' => ['string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['string'],
            'photo_url' => ['string'],
            'auth_date' => ['required', 'integer'],
            'hash' => ['required', 'string'],
        ]);

        $dataCheck = array_except($data, ['hash']);

        ksort($dataCheck);

        $dataCheckString = implode("\n", array_map(function ($key, $value) {
            return "{$key}={$value}";
        }, array_keys($dataCheck), $dataCheck));

        $secretKey = hash('sha256', config('services.telegram.bot_token'), true);

        if (hash_hmac('sha256', $dataCheckString, $secretKey) !== $data['hash']) {
            abort(403);
        }

        if ($connectedAccounts->isConnected(current_user()->id, 'telegram')) {
            abort(403);
        }

        $connectedAccounts->connect(
            current_user()->id,
            'telegram',
            (string) $data['id'],
            $data,
        );
    }

    public function disconnectAccount(Request $request, ConnectedAccounts $connectedAccounts): void
    {
        $this->validate($request, [
            'provider' => ['required', 'string'],
        ]);

        $oauthProvider = $request->get('provider');

        $connectedAccounts->disconnect(current_user()->id, $oauthProvider);
    }
}
