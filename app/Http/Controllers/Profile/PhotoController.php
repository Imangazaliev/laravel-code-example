<?php

declare(strict_types=1);

namespace App\Http\Controllers\Profile;

use App\Events\PhotoStatusChanged;
use App\Http\Controllers\Controller;
use App\Jobs\CalculateUserReputation;
use App\Jobs\GenerateUserPhotoVariants;
use App\Models\UserPhoto;
use App\Services\UserPhotoUploader\UserPhotoUploader;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Throwable;

class PhotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function photos(): array
    {
        $photos = (new UserPhoto())
            ->where('user_id', current_user()->id)
            ->orderBy('is_main', 'desc')
            ->orderBy('uploaded_at')
            ->addBinding(current_user()->main_photo_id, 'select')
            ->get([
                'id',
                'caption',
                'storage_type',
                'path',
                'blurred_image_path',
                'uploaded_at',
                'is_being_processed',
                db_raw('case when id = ? then true else false end as is_main'),
                'status',
            ])
        ;

        return $photos->map(function (UserPhoto $photo) {
            return [
                'id' => $photo->id,
                'url' => $photo->is_being_processed ? null : $photo->getImageUrl(false, UserPhoto::SIZE_200),
                'caption' => $photo->caption,
                'uploaded_at' => $photo->uploaded_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'status' => $photo->status,
                'is_main' => $photo->is_main,
                'is_being_processed' => $photo->is_being_processed,
            ];
        })->all();
    }

    public function uploadPhoto(Request $request): void
    {
        $maxPhotoCount = config('profile.photo.max_count');

        $photoCount = (new UserPhoto())
            ->where('user_id', current_user()->id)
            ->count()
        ;

        if ($photoCount >= $maxPhotoCount) {
            abort(403);
        }

        $maxFileSize = config('profile.photo.max_file_size');
        $allowedMimeTypes = config('profile.photo.allowed_mime_types');
        $maxCaptionLength = config('profile.photo.max_caption_length');

        $this->validate($request, [
            'photo' => [
                'required',
                'file',
                'max:' . $maxFileSize,
                'mimetypes:' . implode(',', $allowedMimeTypes),
                Rule::dimensions()
                    ->minWidth(400)
                    ->minHeight(400)
                    ->maxWidth(5000)
                    ->maxHeight(5000),
            ],
            'caption' => ['max:' . $maxCaptionLength],
//            'crop_area' => ['present', 'nullable'],
//            'crop_area.x' => ['required', 'number'],
//            'crop_area.y' => ['required', 'number'],
//            'crop_area.width' => ['required', 'number'],
//            'crop_area.height' => ['required', 'number'],
//            'preview_area' => ['required', 'array'],
//            'preview_area.x' => ['required', 'number'],
//            'preview_area.y' => ['required', 'number'],
//            'preview_area.width' => ['required', 'number'],
//            'preview_area.height' => ['required', 'number'],
//            'preview_area',
//            'rotation_angle' => ['required', Rule::in(0, 90, 180, 270)],
        ], [
            'photo.max' => __('photo:errors.invalid-size-or-dimensions'),
            'photo.dimensions' => __('photo:errors.invalid-size-or-dimensions'),
        ]);

        $uploadedPhoto = $request->file('photo');

        /**
         * @var UserPhotoUploader $uploader
         */
        $uploader = app(UserPhotoUploader::class);

        $storageType = config('app.user_photo_storage_driver');

        $cropArea = $request->get('preview') === null ? null : [
            $request->get('crop_area.x'),
            $request->get('crop_area.y'),
            $request->get('crop_area.width'),
            $request->get('crop_area.height'),
        ];

        $previewArea = [
            $request->get('preview_area.x'),
            $request->get('preview_area.y'),
            $request->get('preview_area.width'),
            $request->get('preview_area.height'),
        ];

        $photo = $uploader->putUploadedPhoto(
            $storageType,
            $uploadedPhoto,
            $request->get('caption'),
            current_user()->id,
//            $cropArea,
//            $previewArea,
//            $request->get('rotation_angle'),
        );

        $this->dispatch(new GenerateUserPhotoVariants($photo->id));
    }

    public function setMainPhoto(Request $request): void
    {
        $this->validate($request, [
            'photo_id' => ['required', 'integer'],
        ]);

        $photoId = $request->get('photo_id');

        $photo = (new UserPhoto())
            ->where('id', $photoId)
            ->first([
                'user_id',
                'is_being_processed',
                'status',
            ])
        ;

        if ($photo->user_id !== current_user()->id) {
            abort(403);
        }

        if ($photo === null) {
            abort(403);
        }

        if ($photo->is_being_processed) {
            abort(403);
        }

        if ($photo->status !== UserPhoto::STATUS_ACCEPTED) {
            abort(403);
        }

        current_user()->update([
            'main_photo_id' => $photoId,
        ]);

        cache_info()->userProfile(current_user()->id)->forget();
        cache_info()->userPhotos(current_user()->id)->forget();
        cache_info()->mainPhotoUrl(current_user()->id)->forget();
    }

    public function deletePhoto(Request $request): void
    {
        $this->validate($request, [
            'photo_id' => ['required', 'integer'],
        ]);

        $photoId = $request->get('photo_id');

        $photo = (new UserPhoto())->findOrFail($photoId, [
            'id',
            'user_id',
            'is_being_processed',
            'deleted_at',
        ]);

        if ($photo->is_being_processed) {
            abort(403, __('profile:errors.photo-is-being-processed'));
        }

        if ($photo->deleted_at !== null) {
            abort(403);
        }

        if ($photo->user_id !== current_user()->id) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $photo->update([
                'status' => UserPhoto::STATUS_DELETED,
                'deleted_at' => Carbon::now()->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                'moderation_moderator_id' => null,
                'moderation_claimed_at' => null,
                'moderation_forwarded_to' => null,
                'moderation_forwarded_by' => null,
                'moderation_forwarded_at' => null,
            ]);

            event(new PhotoStatusChanged($photo->id));

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }

        $this->dispatch(new CalculateUserReputation(current_user()->id));
    }
}
