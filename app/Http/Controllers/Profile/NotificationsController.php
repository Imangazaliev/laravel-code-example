<?php

declare(strict_types=1);

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\ChatMessageModeration;
use App\Models\ChatMessageStatusChange;
use App\Models\Notification;
use App\Models\PhotoModeration;
use App\Models\PhotoStatusChange;
use App\Models\ProfileModeration;
use App\Models\ProfileStatusChange;
use App\Transformers\NotificationsTransformer;
use Carbon\Carbon;
use DB;
use RuntimeException;
use Throwable;

class NotificationsController extends Controller
{
    private const NOTIFICATIONS_PER_PAGE = 24;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function notifications(): array
    {
        $profileModerationsQuery = DB::table('profile_moderations')
            ->select(['id', db_raw(sprintf("'%s' as type", NotificationsTransformer::TYPE_PROFILE_MODERATION)), db_raw('moderated_at as time')])
            ->where('user_id', current_user()->id)
            ->whereIn('status', [ProfileModeration::STATUS_ACCEPTED, ProfileModeration::STATUS_REJECTED])
        ;

        $photoModerationsQuery = DB::table('photo_moderations')
            ->select(['id', db_raw(sprintf("'%s' as type", NotificationsTransformer::TYPE_PHOTO_MODERATION)), db_raw('moderated_at as time')])
            ->where('photo_owner_id', current_user()->id)
            ->whereIn('status', [PhotoModeration::STATUS_ACCEPTED, PhotoModeration::STATUS_REJECTED])
        ;

        $chatMessageModerationsQuery = DB::table('chat_message_moderations')
            ->select(['id', db_raw(sprintf("'%s' as type", NotificationsTransformer::TYPE_CHAT_MESSAGE_MODERATION)), db_raw('moderated_at as time')])
            ->where('message_author_id', current_user()->id)
            ->where('status', ChatMessageModeration::STATUS_REJECTED)
        ;

        $profileStatusChangesQuery = DB::table('profile_status_changes')
            ->select(['id', db_raw(sprintf("'%s' as type", NotificationsTransformer::TYPE_PROFILE_STATUS_CHANGE)), db_raw('changed_at as time')])
            ->where('user_id', current_user()->id)
        ;

        $photoStatusChangesQuery = DB::table('photo_status_changes')
            ->select(['id', db_raw(sprintf("'%s' as type", NotificationsTransformer::TYPE_PHOTO_STATUS_CHANGE)), db_raw('changed_at as time')])
            ->where('photo_owner_id', current_user()->id)
        ;

        $chatMessageStatusChangesQuery = DB::table('chat_message_status_changes')
            ->select(['id', db_raw(sprintf("'%s' as type", NotificationsTransformer::TYPE_CHAT_MESSAGE_STATUS_CHANGE)), db_raw('changed_at as time')])
            ->where('message_author_id', current_user()->id)
        ;

        $notificationsPaginator = DB::table('notifications')
            ->select(['id', db_raw(sprintf("'%s' as type", NotificationsTransformer::TYPE_NOTIFICATION)), db_raw('created_at as time')])
            ->where('user_id', current_user()->id)
            ->where('is_hidden', false)
            ->union($profileModerationsQuery)
            ->union($photoModerationsQuery)
            ->union($chatMessageModerationsQuery)
            ->union($profileStatusChangesQuery)
            ->union($photoStatusChangesQuery)
            ->union($chatMessageStatusChangesQuery)
            ->orderBy('time', 'desc')
            ->paginate(self::NOTIFICATIONS_PER_PAGE)
        ;

        $unitedNotifications = collect($notificationsPaginator->items());

        $profileModerationIds = $unitedNotifications->where('type', NotificationsTransformer::TYPE_PROFILE_MODERATION)->pluck('id');

        $profileModerations = (new ProfileModeration())
            ->whereIn('id', $profileModerationIds)
            ->get([
                'id',
                'status',
                'rejection_reasons',
                'comment_for_user',
                'moderated_at',
                'appealed_at',
            ])
        ;

        $profileModerationsById = $profileModerations->pluck('id')->combine($profileModerations);

        $photoModerationIds = $unitedNotifications->where('type', NotificationsTransformer::TYPE_PHOTO_MODERATION)->pluck('id');

        $photoModerations = (new PhotoModeration())
            ->whereIn('id', $photoModerationIds)
            ->get([
                'id',
                'photo_id',
                'status',
                'rejection_reasons',
                'comment_for_user',
                'moderated_at',
                'appealed_at',
            ])
        ;

        $photoModerationsById = $photoModerations->pluck('id')->combine($photoModerations);

        $chatMessageModerationIds = $unitedNotifications->where('type', NotificationsTransformer::TYPE_CHAT_MESSAGE_MODERATION)->pluck('id');

        $chatMessageModerations = (new ChatMessageModeration())
            ->with('message:id,text')
            ->whereIn('id', $chatMessageModerationIds)
            ->get([
                'id',
                'message_id',
                'status',
                'rejection_reasons',
                'comment_for_user',
                'moderated_at',
                'appealed_at',
            ])
        ;

        $chatMessageModerationsById = $chatMessageModerations->pluck('id')->combine($chatMessageModerations);

        $profileStatusChangeIds = $unitedNotifications->where('type', NotificationsTransformer::TYPE_PROFILE_STATUS_CHANGE)->pluck('id');

        $profileStatusChanges = (new ProfileStatusChange())
            ->whereIn('id', $profileStatusChangeIds)
            ->get([
                'id',
                'new_status',
                'rejection_reasons',
                'comment_for_user',
                'changed_at',
            ])
        ;

        $profileStatusChanges = $profileStatusChanges->pluck('id')->combine($profileStatusChanges);

        $photoStatusChangeIds = $unitedNotifications->where('type', NotificationsTransformer::TYPE_PHOTO_STATUS_CHANGE)->pluck('id');

        $photoStatusChanges = (new PhotoStatusChange())
            ->whereIn('id', $photoStatusChangeIds)
            ->get([
                'id',
                'photo_id',
                'new_status',
                'rejection_reasons',
                'comment_for_user',
                'changed_at',
            ])
        ;

        $photoStatusChanges = $photoStatusChanges->pluck('id')->combine($photoStatusChanges);

        $chatMessageStatusChangeIds = $unitedNotifications->where('type', NotificationsTransformer::TYPE_CHAT_MESSAGE_STATUS_CHANGE)->pluck('id');

        $chatMessageStatusChanges = (new ChatMessageStatusChange())
            ->with('message:id,text')
            ->whereIn('id', $chatMessageStatusChangeIds)
            ->get([
                'id',
                'message_id',
                'new_status',
                'rejection_reasons',
                'comment_for_user',
                'changed_at',
            ])
        ;

        $chatMessageStatusChanges = $chatMessageStatusChanges->pluck('id')->combine($chatMessageStatusChanges);

        $notificationIds = $unitedNotifications->where('type', NotificationsTransformer::TYPE_NOTIFICATION)->pluck('id');

        $notifications = (new Notification())
            ->whereIn('id', $notificationIds)
            ->get([
                'id',
                'type',
                'title',
                'body',
                'parameters',
                'context',
                'created_at',
            ])
        ;

        $notificationsById = $notifications->pluck('id')->combine($notifications);

        return [
            'notifications' => $unitedNotifications->map(function ($notification) use (
                $profileModerationsById,
                $photoModerationsById,
                $chatMessageModerationsById,
                $profileStatusChanges,
                $photoStatusChanges,
                $chatMessageStatusChanges,
                $notificationsById
            ) {
                switch ($notification->type) {
                    case NotificationsTransformer::TYPE_PROFILE_MODERATION:
                        return NotificationsTransformer::transformProfileModeration($profileModerationsById[$notification->id]);

                    case NotificationsTransformer::TYPE_PHOTO_MODERATION:
                        return NotificationsTransformer::transformPhotoModeration($photoModerationsById[$notification->id]);

                    case NotificationsTransformer::TYPE_CHAT_MESSAGE_MODERATION:
                        return NotificationsTransformer::transformChatMessageModeration($chatMessageModerationsById[$notification->id]);

                    case NotificationsTransformer::TYPE_PROFILE_STATUS_CHANGE:
                        return NotificationsTransformer::transformProfileStatusChange($profileStatusChanges[$notification->id]);

                    case NotificationsTransformer::TYPE_PHOTO_STATUS_CHANGE:
                        return NotificationsTransformer::transformPhotoStatusChange($photoStatusChanges[$notification->id]);

                    case NotificationsTransformer::TYPE_CHAT_MESSAGE_STATUS_CHANGE:
                        return NotificationsTransformer::transformChatMessageStatusChange($chatMessageStatusChanges[$notification->id]);

                    case NotificationsTransformer::TYPE_NOTIFICATION:
                        return NotificationsTransformer::transformNotification($notificationsById[$notification->id]);

                    default:
                        throw new RuntimeException(sprintf('Unknown notification type "%s"', $notification->type));
                }
            }),
            'page_count' => $notificationsPaginator->lastPage(),
        ];
    }

    public function markAsSeen(): void
    {
        DB::beginTransaction();

        try {
            $currentUser = current_user();
            $currentTime = Carbon::now();

            (new ProfileModeration())
                ->where('user_id', $currentUser->id)
                ->whereIn('status', [ProfileModeration::STATUS_ACCEPTED, ProfileModeration::STATUS_REJECTED])
                ->whereNull('seen_by_user_at')
                ->update([
                    'seen_by_user_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ])
            ;

            (new ProfileStatusChange())
                ->where('user_id', $currentUser->id)
                ->whereNull('seen_by_user_at')
                ->update([
                    'seen_by_user_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ])
            ;

            (new PhotoModeration())
                ->where('photo_owner_id', $currentUser->id)
                ->whereIn('status', [PhotoModeration::STATUS_ACCEPTED, PhotoModeration::STATUS_REJECTED])
                ->whereNull('seen_by_user_at')
                ->update([
                    'seen_by_user_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ])
            ;

            (new PhotoStatusChange())
                ->where('photo_owner_id', $currentUser->id)
                ->whereNull('seen_by_user_at')
                ->update([
                    'seen_by_user_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ])
            ;

            (new ChatMessageModeration())
                ->where('message_author_id', $currentUser->id)
                ->where('status', ChatMessageModeration::STATUS_REJECTED)
                ->whereNull('seen_by_user_at')
                ->update([
                    'seen_by_user_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ])
            ;

            (new ChatMessageStatusChange())
                ->where('message_author_id', $currentUser->id)
                ->whereNull('seen_by_user_at')
                ->update([
                    'seen_by_user_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ])
            ;

            (new Notification())
                ->where('user_id', $currentUser->id)
                ->where('is_hidden', false)
                ->whereNull('seen_at')
                ->update([
                    'seen_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ])
            ;

            cache_info()->newProfileModerationCount($currentUser->id)->forget();
            cache_info()->newPhotoModerationCount($currentUser->id)->forget();
            cache_info()->newMessageModerationCount($currentUser->id)->forget();
            cache_info()->newNotificationCount($currentUser->id)->forget();

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }
}
