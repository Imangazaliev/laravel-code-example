<?php

declare(strict_types=1);

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Mail\ChangeEmail;
use App\Models\EmailChangeRequest;
use App\Models\Notification;
use App\Models\User;
use App\Services\PhoneNumberVerification\PhoneNumberVerification;
use Carbon\Carbon;
use DB;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use Throwable;

class ChangeEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function currentRequest(): array
    {
        $changeRequest = (new EmailChangeRequest())
            ->where('user_id', current_user()->id)
            ->where('status', EmailChangeRequest::STATUS_NEW)
            ->first()
        ;

        return [
            'request' => $changeRequest === null ? null : [
                'id' => $changeRequest->id,
                'new_email' => $changeRequest->new_email,
                'created_at' => $changeRequest->created_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ],
        ];
    }

    // TODO: replace PhoneNumberVerification to a separate code generator
    public function change(Request $request, PhoneNumberVerification $phoneNumberVerification, Mailer $mailer): void
    {
        $this->validate($request, [
            'new_email' => ['nullable', 'string', 'email', 'unique:users,email'],
        ]);

        $changeRequestExists = (new EmailChangeRequest())
            ->where('user_id', current_user()->id)
            ->where('status', EmailChangeRequest::STATUS_NEW)
            ->exists()
        ;

        if ($changeRequestExists) {
            abort(403);
        }

        $newEmail = $request->get('new_email');

        if ($newEmail === null) {
            $currentTime = Carbon::now();

            (new User())
                ->where('id', current_user()->id)
                ->where('updated_at', current_user()->updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'email' => null,
                    'email_verified_at' => null,
                    'updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ])
            ;

            return;
        }

        $currentTime = Carbon::now();

        $changeRequest = (new EmailChangeRequest())->create([
            'user_id' => current_user()->id,
            'current_email' => current_user()->email,
            'new_email' => $newEmail,
            'code' => $phoneNumberVerification->generateCode(),
            'status' => EmailChangeRequest::STATUS_NEW,
            'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            'created_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);

        $mailer->send(new ChangeEmail(current_user()->name, $changeRequest->code, $changeRequest->new_email));
    }

    public function confirm(Request $request): void
    {
        // TODO: make a separate value in the config
        $codeLength = config('auth.phone_number_verification.code_length');

        $this->validate($request, [
            'code' => ['required', "regex:/^[0-9]{{$codeLength}}$/"],
        ]);

        $changeRequest = (new EmailChangeRequest())
            ->where('user_id', current_user()->id)
            ->where('status', EmailChangeRequest::STATUS_NEW)
            ->first()
        ;

        if ($changeRequest === null) {
            abort(403);
        }

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            (new User())
                ->where('id', current_user()->id)
                ->where('updated_at', current_user()->updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'email' => $changeRequest->new_email,
                    'email_verified_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                    'updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ])
            ;

            $changeRequest->update([
                'status' => EmailChangeRequest::STATUS_VERIFIED,
                'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            (new Notification())->create([
                'user_id' => current_user()->id,
                'type' => Notification::TYPE_EMAIL_CHANGED,
                'title' => 'notifications:email-changed.title',
                'body' => 'notifications:email-changed.body',
                'parameters' => [
                    'newEmail' => $changeRequest->new_email,
                ],
                'additional_data' => [
                    'new_email' => $changeRequest->new_email,
                    'change_request_id' => $changeRequest->id,
                ],
                'is_hidden' => true,
                'created_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function cancel(): void
    {
        $changeRequest = (new EmailChangeRequest())
            ->where('user_id', current_user()->id)
            ->where('status', EmailChangeRequest::STATUS_NEW)
            ->first(['id'])
        ;

        if ($changeRequest === null) {
            abort(400);
        }

        $changeRequest->update([
            'status' => EmailChangeRequest::STATUS_CANCELED,
            'status_updated_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
        ]);
    }
}
