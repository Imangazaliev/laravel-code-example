<?php

declare(strict_types=1);

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Jobs\SendMessageToPhoneNumber;
use App\Models\Notification;
use App\Models\PhoneNumberChangeRequest;
use App\Models\User;
use App\Services\PhoneNumberVerification\PhoneNumberVerification;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Propaganistas\LaravelPhone\PhoneNumber;
use Throwable;

class ChangePhoneNumberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function currentRequest(): array
    {
        $changeRequest = (new PhoneNumberChangeRequest())
            ->where('user_id', current_user()->id)
            ->where('status', PhoneNumberChangeRequest::STATUS_NEW)
            ->first()
        ;

        return [
            'request' => $changeRequest === null ? null : [
                'id' => $changeRequest->id,
                'new_phone_number' => $changeRequest->new_phone_number,
                'new_phone_number_country_code' => $changeRequest->new_phone_number_country_code,
                'created_at' => $changeRequest->created_at->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ],
        ];
    }

    public function change(Request $request, PhoneNumberVerification $phoneNumberVerification): void
    {
        $this->validate($request, [
            'new_phone_number' => ['required', 'string', 'regex:/^\+[0-9]+$/', 'unique:users,phone_number'],
            'new_phone_number_country_code' => ['required', 'exists:countries,code_alpha2'],
        ], [
            'new_phone_number.unique' => __('validation:custom.phone-number-taken'),
        ]);

        $this->validate($request, [
            'new_phone_number' => ['phone:' . $request->get('new_phone_number_country_code')],
        ]);

        $phoneNumber = $request->get('new_phone_number');
        $phoneNumberCountryCode = $request->get('new_phone_number_country_code');

        $phoneNumberInNationalFormat = PhoneNumber::make($phoneNumber, $phoneNumberCountryCode)->formatForMobileDialingInCountry($phoneNumberCountryCode);

        $this->getValidationFactory()->make([
            'new_phone_number' => $phoneNumberInNationalFormat,
        ], [
            'new_phone_number' => ['unique:users,phone_number_in_national_format'],
        ], [
            'new_phone_number.unique' => __('validation:custom.phone-number-taken'),
        ])->validate();

        $changeRequestExists = (new PhoneNumberChangeRequest())
            ->where('user_id', current_user()->id)
            ->where('status', PhoneNumberChangeRequest::STATUS_NEW)
            ->exists()
        ;

        if ($changeRequestExists) {
            abort(403);
        }

        $currentTime = Carbon::now();

        $changeRequest = (new PhoneNumberChangeRequest())->create([
            'user_id' => current_user()->id,
            'current_phone_number' => current_user()->phone_number,
            'current_phone_number_country_code' => current_user()->phone_number_country_code,
            'new_phone_number' => $phoneNumber,
            'new_phone_number_country_code' => $phoneNumberCountryCode,
            'code' => $phoneNumberVerification->generateCode(),
            'status' => PhoneNumberChangeRequest::STATUS_NEW,
            'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            'created_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);

        $this->dispatch(
            new SendMessageToPhoneNumber(
                $changeRequest->new_phone_number,
                __('auth:phone-number-change-message', ['code' => $changeRequest->code, 'siteName' => config('app.name')]),
            ),
        );
    }

    public function confirm(Request $request): void
    {
        $codeLength = config('auth.phone_number_verification.code_length');

        $this->validate($request, [
            'code' => ['required', "regex:/^[0-9]{{$codeLength}}$/"],
        ]);

        $changeRequest = (new PhoneNumberChangeRequest())
            ->where('user_id', current_user()->id)
            ->where('status', PhoneNumberChangeRequest::STATUS_NEW)
            ->first()
        ;

        if ($changeRequest === null) {
            abort(403);
        }

        $phoneNumber = PhoneNumber::make($changeRequest->new_phone_number, $changeRequest->new_phone_number_country_code);
        $phoneNumberInNationalFormat = $phoneNumber->formatForMobileDialingInCountry($changeRequest->new_phone_number_country_code);

        DB::beginTransaction();

        try {
            $currentTime = Carbon::now();

            (new User())
                ->where('id', current_user()->id)
                ->where('updated_at', current_user()->updated_at->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT))
                ->update([
                    'phone_number' => $changeRequest->new_phone_number,
                    'phone_number_country_code' => $changeRequest->new_phone_number_country_code,
                    'phone_number_in_national_format' => $phoneNumberInNationalFormat,
                    'updated_at' => $currentTime->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
                ])
            ;

            $changeRequest->update([
                'status' => PhoneNumberChangeRequest::STATUS_VERIFIED,
                'status_updated_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            (new Notification())->create([
                'user_id' => current_user()->id,
                'type' => Notification::TYPE_PHONE_NUMBER_CHANGED,
                'title' => 'notifications:phone-number-changed.title',
                'body' => 'notifications:phone-number-changed.body',
                'parameters' => [
                    'newPhoneNumber' => $changeRequest->new_phone_number,
                ],
                'additional_data' => [
                    'new_phone_number' => $changeRequest->new_phone_number,
                    'change_request_id' => $changeRequest->id,
                ],
                'is_hidden' => true,
                'created_at' => $currentTime->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
            ]);

            DB::commit();
        } catch (Throwable $exception) {
            DB::rollBack();

            throw $exception;
        }
    }

    public function cancel(): void
    {
        $changeRequest = (new PhoneNumberChangeRequest())
            ->where('user_id', current_user()->id)
            ->where('status', PhoneNumberChangeRequest::STATUS_NEW)
            ->first(['id'])
        ;

        if ($changeRequest === null) {
            abort(400);
        }

        $changeRequest->update([
            'status' => PhoneNumberChangeRequest::STATUS_CANCELED,
            'status_updated_at' => Carbon::now()->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT),
        ]);
    }
}
