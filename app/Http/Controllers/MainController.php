<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Language;
use App\Services\Frontend\JavaScriptVariables;
use App\Services\SessionKeys\SessionKeys;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use ExampleProject\Translations\TranslatorCacheInterface;
use Symfony\Component\HttpFoundation\Cookie;

class MainController extends Controller
{
    public function getIndex(TranslatorCacheInterface $uiStringsCache): View
    {
        $jsVariables = json_encode(
            JavaScriptVariables::getVariables(),
            JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_THROW_ON_ERROR,
        );
        $translationsLastUpdateTime = $uiStringsCache->getLastUpdateTime(language()->code);

        return view('app', [
            'defaultLanguageCode' => app('default_language')->code,
            'jsVariables' => $jsVariables,
            'translationsLastUpdateTime' => $translationsLastUpdateTime,
        ]);
    }

    public function csrfToken(): Response
    {
        $config = config('session');

        $tokenCookie = new Cookie(
            'XSRF-TOKEN',
            session()->token(),
            Carbon::now()->addRealSeconds(60 * $config['lifetime'])->getTimestamp(),
            $config['path'],
            $config['domain'],
            $config['secure'],
            false,
            false,
            $config['same_site'] ?? null,
        );

        return response('')->cookie($tokenCookie);
    }

    public function changeLanguage(Request $request): Response
    {
        $this->validate($request, [
            'language_id' => ['required'],
        ]);

        $language = (new Language())
            ->where('is_active', true)
            ->where('id', $request->get('language_id'))
            ->first(['id', 'code'])
        ;

        if ($language === null) {
            abort(400);
        }

        $currentUser = current_user();

        if ($currentUser !== null) {
            $currentUser->update([
                'language_id' => $language->id,
                'language_code' => $language->code,
            ]);
        }

        $languageCookie = cookie()->forever(SessionKeys::CURRENT_LANGUAGE_CODE, $language->code);

        return response('')->cookie($languageCookie);
    }
}
