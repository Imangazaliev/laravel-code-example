<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use ExampleProject\Translations\TranslatorCacheInterface;

class TranslationsController extends Controller
{
    public function getTranslations(TranslatorCacheInterface $uiStringsCache, string $languageCode): Response
    {
        $lastCacheUpdateTime = $uiStringsCache->getLastUpdateTime($languageCode);

        // if there's no cached strings then return an empty array
        // to avoid errors
        if ($lastCacheUpdateTime === null) {
            $encodedStrings = '{}';
        } else {
            $encodedStrings = cache_info()
                ->browserStrings($languageCode, $lastCacheUpdateTime)
                ->remember(function () use ($uiStringsCache, $languageCode) {
                    foreach (TranslatorCacheInterface::NAMESPACES as $namespace) {
                        $strings[$namespace] = $uiStringsCache->getStringsInNamespace($languageCode, $namespace);
                    }

                    return json_encode($strings, JSON_THROW_ON_ERROR);
                })
            ;
        }

        return response("window.Translations['$languageCode'] = {$encodedStrings}")
            ->header('Content-Type', 'application/javascript')
            ->header('Cache-Control', sprintf('max-age=%s, public', 365 * 24 * 60 * 60))
        ;
    }
}
