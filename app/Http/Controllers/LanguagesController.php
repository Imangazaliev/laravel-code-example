<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Language;
use Illuminate\Http\Request;

class LanguagesController extends Controller
{
    public function languages(Request $request): array
    {
        $this->validate($request, [
            'supported_only' => 'boolean',
        ]);

        $languages = cache_info()->languages($request->has('supported_only'))->remember(function () use ($request) {
            return (new Language())
                ->select('languages.*')
                ->withTranslation()
                ->joinTranslation()
                ->when($request->has('supported_only'), function ($builder) {
                    $builder->where('is_active', true);
                })
                ->orderBy('language_texts.name')
                ->get(['id', 'code', 'native_name'])
                ->map(function (Language $language) {
                    return [
                        'id' => $language->id,
                        'code' => $language->code,
                        'name' => $language->getTranslation()->name,
                        'native_name' => $language->native_name,
                    ];
                })
                ->all()
            ;
        });

        return $languages;
    }
}
