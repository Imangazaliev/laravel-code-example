<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Ethnicity;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class EthnicitiesController extends Controller
{
    public function ethnicities(Request $request): array
    {
        $this->validate($request, [
            'sex' => ['required', Rule::in([User::SEX_MALE, User::SEX_FEMALE])],
        ]);

        $sex = $request->get('sex');

        return cache_info()->ethnicities($sex)->remember(function () use ($sex) {
            $nameColumn = $sex === User::SEX_MALE ? 'male_name' : 'female_name';

            return (new Ethnicity())
                ->select('ethnicities.*')
                ->withTranslation()
                ->joinTranslation()
                ->where('show_in_list', true)
                ->get([
                    'ethnicities.id',
                    'ethnicity_texts.plural_name',
                    "ethnicity_texts.{$nameColumn}",
                ])
                ->sortBy(function (Ethnicity $ethnicity) use ($nameColumn) {
                    return $ethnicity->getTranslation()->$nameColumn ?? $ethnicity->getTranslation()->plural_name;
                })
                ->map(function (Ethnicity $ethnicity) use ($nameColumn) {
                    return [
                        'id' => $ethnicity->id,
                        'name' => $ethnicity->getTranslation()->$nameColumn ?? $ethnicity->getTranslation()->plural_name,
                    ];
                })
                ->values()
                ->toArray()
            ;
        });
    }
}
