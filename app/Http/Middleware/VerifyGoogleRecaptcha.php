<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Services\GoogleRecaptcha\GoogleRecaptcha;
use Closure;
use Illuminate\Http\Request;

class VerifyGoogleRecaptcha
{
    private GoogleRecaptcha $googleRecaptcha;

    public function __construct(GoogleRecaptcha $googleRecaptcha)
    {
        $this->googleRecaptcha = $googleRecaptcha;
    }

    public function handle(Request $request, Closure $next)
    {
        if (config('services.google_recaptcha.enabled') && ! $this->isValidRecaptchaCode($request)) {
            return response([
                'recaptcha_verification_error' => true,
            ], 422);
        }

        return $next($request);
    }

    private function isValidRecaptchaCode(Request $request): bool
    {
        if (app()->environment() === 'testing') {
            return true;
        }

        return (
            $request->has('recaptcha_response') &&
            $request->get('recaptcha_response') !== null &&
            $this->googleRecaptcha->check($request->get('recaptcha_response'))
        );
    }
}
