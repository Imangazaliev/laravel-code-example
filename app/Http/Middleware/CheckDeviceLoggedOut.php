<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\UserAuthentication;
use App\Services\SessionKeys\SessionKeys;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class CheckDeviceLoggedOut
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->has(SessionKeys::AUTH_KEY)) {
            $userAuthentication = (new UserAuthentication())
                ->where('auth_key', $request->session()->get(SessionKeys::AUTH_KEY))
                ->first()
            ;

            if ($userAuthentication !== null && $userAuthentication->forced_logout_at !== null) {
                $this->logout($request, 'user');
            }
        }

        return $next($request);
    }

    private function logout(Request $request, string $guard): void
    {
        auth()->guard($guard)->logoutCurrentDevice();

        $preservedAuthKey = $request->session()->get($preservedAuthKeySessionKey);

        $request->session()->invalidate();

        $request->session()->put($preservedAuthKeySessionKey, $preservedAuthKey);

        throw new AuthenticationException('Unauthenticated.', [$guard]);
    }
}
