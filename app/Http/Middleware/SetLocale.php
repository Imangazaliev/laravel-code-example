<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\Language;
use App\Services\SessionKeys\SessionKeys;
use Closure;
use Illuminate\Http\Request;

class SetLocale
{
    public function handle(Request $request, Closure $next)
    {
        app()->setLocale($this->getLocale($request));

        return $next($request);
    }

    private function getLocale(Request $request): string
    {
        if (current_user() !== null) {
            return current_user()->language_code;
        }

        if ($request->cookies->has(SessionKeys::CURRENT_LANGUAGE_CODE)) {
            $languageCode = $request->cookies->get(SessionKeys::CURRENT_LANGUAGE_CODE);

            // in some cases it may be null (I don't know why this is happening)
            if ($languageCode !== null) {
                return $languageCode;
            }
        }

        return $this->getLocaleFromRequest($request) ?? $this->getFallbackLocale();
    }

    /**
     * Get locale from the browser ("Accept-Language" HTTP header).
     *
     * @return string|null
     */
    private function getLocaleFromRequest(Request $request): ?string
    {
        $preferredLocale = $request->getPreferredLanguage();

        if ($preferredLocale === null) {
            return null;
        }

        if (str_contains($preferredLocale, '_')) {
            $preferredLocale = explode('_', $preferredLocale)[0];
        }

        return in_array($preferredLocale, $this->getAvailableLocales(), true) ? $preferredLocale : null;
    }

    /**
     * Get available locales from config.
     *
     * @return string[]
     */
    public function getAvailableLocales(): array
    {
        return cache_info()->availableLocales()->remember(function () {
            return (new Language())
                ->where('is_active', true)
                ->pluck('code')
                ->all()
            ;
        });
    }

    /**
     * Get default locale.
     *
     * @return string
     */
    private function getFallbackLocale(): string
    {
        return config('app.fallback_locale');
    }
}
