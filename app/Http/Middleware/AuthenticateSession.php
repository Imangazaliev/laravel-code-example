<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Services\SessionKeys\SessionKeys;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class AuthenticateSession
{
    public function handle(Request $request, Closure $next)
    {
        if ( ! $request->hasSession()) {
            return $next($request);
        }

        $this->logoutIfNeeded($request, 'user');

        return $next($request);
    }

    private function logoutIfNeeded(Request $request, string $guardName): void
    {
        $guard = auth()->guard($guardName);

        if ($guard->user() === null) {
            return;
        }

        if ($guard->viaRemember()) {
            $passwordHash = explode('|', $request->cookies->get($guard->getRecallerName()))[2];

            if ($passwordHash != $guard->user()->getAuthPassword()) {
                $this->logout($request, $guardName, $guard);
            }
        }

        $passwordHashKey = "{$guardName}_password_hash";

        if ( ! $request->session()->has($passwordHashKey)) {
            $request->session()->put([
                $passwordHashKey => $request->user()->getAuthPassword(),
            ]);
        }

        if ($request->session()->get($passwordHashKey) !== $request->user()->getAuthPassword()) {
            $this->logout($request, $guardName, $guard);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     * @param string $guardName
     * @param Guard $guard
     *
     * @throws AuthenticationException
     */
    protected function logout(Request $request, string $guardName, Guard $guard): void
    {
        $guard->logoutCurrentDevice();

        $request->session()->flush();

        throw new AuthenticationException();
    }
}
