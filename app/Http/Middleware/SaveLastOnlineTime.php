<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\User;
use App\Models\UserAuthentication;
use App\Services\Cache\CacheEntry;
use App\Services\SessionKeys\SessionKeys;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class SaveLastOnlineTime
{
    public function handle(Request $request, Closure $next)
    {
        $currentUser = current_user();

        if ($currentUser !== null) {
            $cacheEntry = cache_info()->userLastOnlineTime($currentUser->id);

            $newOnlineTime = $this->getNewOnlineTime($cacheEntry);

            if ($newOnlineTime !== null && $this->putTimeToCache($cacheEntry, $newOnlineTime)) {
                $this->updateUserLastOnlineTime($currentUser, $newOnlineTime);
            }
        }

        return $next($request);
    }

    private function getNewOnlineTime(CacheEntry $cacheEntry): ?Carbon
    {
        $lastOnlineTimeStr = $cacheEntry->get();

        $currentTime = Carbon::now();

        if ($lastOnlineTimeStr === null) {
            return $currentTime;
        }

        $lastOnlineTime = Carbon::parse($lastOnlineTimeStr);
        $diff = $currentTime->diffInSeconds($lastOnlineTime);

        $lastOnlineTimeUpdateInterval = config('profile.online_status_update_interval');

        if ($diff >= $lastOnlineTimeUpdateInterval) {
            return $currentTime;
        }

        return null;
    }

    private function putTimeToCache(CacheEntry $cacheEntry, Carbon $time): bool
    {
        $lastOnlineTimeStr = $time->format(DATE_TIME_WITH_MICROSECONDS_AND_TIME_ZONE_FORMAT);

        $cacheEntry->put($lastOnlineTimeStr);

        // check the saved value to avoid a race condition
        return $cacheEntry->get() === $lastOnlineTimeStr;
    }

    private function updateUserLastOnlineTime(User $user, Carbon $time): void
    {
        $user->update([
            'was_online_at' => $time->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
        ]);

        $userAuthentication = (new UserAuthentication())
            ->where('auth_key', session()->get(SessionKeys::AUTH_KEY))
            ->first()
        ;

        if ($userAuthentication !== null) {
            $userAuthentication->update([
                'was_online_at' => $time->format(DATE_TIME_WITH_TIME_ZONE_FORMAT),
            ]);
        }
    }
}
