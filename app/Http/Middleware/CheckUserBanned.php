<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Exceptions\ProfileBannedException;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class CheckUserBanned
{
    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     *
     * @throws ProfileBannedException
     */
    public function handle(Request $request, Closure $next)
    {
        $currentUser = current_user();

        if (
            $currentUser === null ||
            $currentUser->status !== User::STATUS_BANNED
        ) {
            return $next($request);
        }

        throw new ProfileBannedException();
    }
}
