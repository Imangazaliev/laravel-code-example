<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]|null ...$guards
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guards = null)
    {
        $guards = is_array($guards) ? $guards : [$guards];
        $guards = count($guards) > 0 ? $guards : [null];

        foreach ($guards as $guard) {
            if (Auth::guard($guard)->check()) {
                return redirect('/');
            }
        }

        return $next($request);
    }
}
