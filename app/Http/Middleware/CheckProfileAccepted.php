<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\User;
use App\Queries\UserLastStatusQuery;
use Closure;
use Illuminate\Http\Request;

class CheckProfileAccepted
{
    public function handle(Request $request, Closure $next, string $context = null): mixed
    {
        if (current_user()->status !== User::STATUS_ACCEPTED) {
            $lastUserStatus = cache_info()->userLastModerationStatus(current_user()->id)->remember(function () {
                return UserLastStatusQuery::get(current_user()->id);
            });

            if ($lastUserStatus === null || $lastUserStatus === User::STATUS_REJECTED) {
                abort(403, __('errors.your-profile-is-not-accepted', [], ['context' => $context]));
            }
        }

        return $next($request);
    }
}
